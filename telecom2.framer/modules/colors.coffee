# Add the following line to your project in Framer Studio.
# myModule = require "myModule"
# Reference the contents by name, like myModule.myFunction() or myModule.myVar

# exports.myVar = "myVariable"
#
# exports.myFunction = ->
# 	print "myFunction is running"
#
# exports.myArray = [1, 2, 3]

exports.blackColor = "#1F1F1F"
exports.whiteColor = "#FFFFFF"

exports.gray800 = "#4B5161"
exports.gray600 = "#5A6175"
exports.gray400 = "#828DAB"
exports.gray200 = "#BEC6DA"

exports.stone800 = "#E1E5EA"
exports.stone600 = "#E7E9EF"
exports.stone400 = "#EFF2F6"
exports.stone200 = "#F1F4FA"

exports.blue800 = "#0095C2"
exports.blue600 = "#00B0E4"
exports.blue400 = "#68C8EE"
exports.blue200 = "#ADE8FF"

exports.green800 = "#56A479"
exports.green600 = "#5BCD8E"
exports.green400 = "#BAEED1"
exports.green200 = "#D6F6E4"

exports.red800 = "#B43737"
exports.red600 = "#E54D4D"
exports.red400 = "#FFCCCC"
exports.red200 = "#FFF0F0"

exports.yellow800 = "#EDB51D"
exports.yellow600 = "#FFC852"
exports.yellow400 = "#FED58C"
exports.yellow200 = "#FEEDCC"

exports.circleColor1 = "#FF7744"
exports.circleColor2 = "#918BE5"

exports.phoneContactBackgroundColors = [
	"#4DD0E1",
	"#9575CD",
	"#F06292",
	"#AED581",
	"#FFB74D",
	"#FF8A65",
	"#FFD54F",
	"#7986CB",
	"#4FC3F7"
]
