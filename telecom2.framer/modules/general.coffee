# Dynamic
exports.callStartDate = new Date
exports.firstTimeButton = true
exports.firstTimeKnob = true
exports.lastIdleState = 1
exports.lastInteractionDate = new Date
exports.onACall = false
exports.proximityState = false
exports.showingIdle = true
exports.swipeAnimation = false

# Static configuration
exports.language = "english"
exports.screen_height = Framer.Device.screen.height
exports.screen_width = Framer.Device.screen.width

# Development variables
exports.loadImages = true

exports.loadTeleHealth = false

exports.loadPhone = true
exports.loadPhotos = true
exports.loadSocialTalk = true
exports.loadRadio = true

# exports.loadImages = false
#
# exports.loadTeleHealth = false
#
# exports.loadPhone = false
# exports.loadPhotos = false
# exports.loadSocialTalk = true
# exports.loadRadio = false

exports.showLogs = false
