require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"FontFace":[function(require,module,exports){
exports.FontFace = (function() {
  var TEST, addFontFace, loadTestingFileError, missingArgumentError, removeTestLayer, testNewFace;

  TEST = {
    face: "monospace",
    text: "foo",
    time: .01,
    maxLoadAttempts: 50,
    hideErrorMessages: true
  };

  TEST.style = {
    width: "auto",
    fontSize: "150px",
    fontFamily: TEST.face
  };

  TEST.layer = new Layer({
    name: "FontFace Tester",
    width: 0,
    height: 1,
    maxX: -Screen.width,
    visible: false,
    html: TEST.text,
    style: TEST.style
  });

  function FontFace(options) {
    this.name = this.file = this.testLayer = this.isLoaded = this.loadFailed = this.loadAttempts = this.originalSize = this.hideErrors = null;
    if (options != null) {
      this.name = options.name || null;
      this.file = options.file || null;
    }
    if (!((this.name != null) && (this.file != null))) {
      return missingArgumentError();
    }
    this.testLayer = TEST.layer.copy();
    this.testLayer.style = TEST.style;
    this.testLayer.maxX = -Screen.width;
    this.testLayer.visible = true;
    this.isLoaded = false;
    this.loadFailed = false;
    this.loadAttempts = 0;
    this.hideErrors = options.hideErrors;
    return addFontFace(this.name, this.file, this);
  }

  addFontFace = function(name, file, object) {
    var faceCSS, styleTag;
    styleTag = document.createElement('style');
    faceCSS = document.createTextNode("@font-face { font-family: '" + name + "'; src: url('" + file + "') format('truetype'); }");
    styleTag.appendChild(faceCSS);
    document.head.appendChild(styleTag);
    return testNewFace(name, object);
  };

  removeTestLayer = function(object) {
    object.testLayer.destroy();
    return object.testLayer = null;
  };

  testNewFace = function(name, object) {
    var initialWidth, widthUpdate;
    initialWidth = object.testLayer._element.getBoundingClientRect().width;
    if (initialWidth === 0) {
      if (object.hideErrors === false || TEST.hideErrorMessages === false) {
        print("Load testing failed. Attempting again.");
      }
      return Utils.delay(TEST.time, function() {
        return testNewFace(name, object);
      });
    }
    object.loadAttempts++;
    if (object.originalSize === null) {
      object.originalSize = initialWidth;
      object.testLayer.style = {
        fontFamily: name + ", " + TEST.face
      };
    }
    widthUpdate = object.testLayer._element.getBoundingClientRect().width;
    if (object.originalSize === widthUpdate) {
      if (object.loadAttempts < TEST.maxLoadAttempts) {
        return Utils.delay(TEST.time, function() {
          return testNewFace(name, object);
        });
      }
      if (!object.hideErrors) {
        print("⚠️ Failed loading FontFace: " + name);
      }
      object.isLoaded = false;
      object.loadFailed = true;
      if (!object.hideErrors) {
        loadTestingFileError(object);
      }
      return;
    } else {
      if (!(object.hideErrors === false || TEST.hideErrorMessages)) {
        print("LOADED: " + name);
      }
      object.isLoaded = true;
      object.loadFailed = false;
    }
    removeTestLayer(object);
    return name;
  };

  missingArgumentError = function() {
    error(null);
    return console.error("Error: You must pass name & file properites when creating a new FontFace. \n\nExample: myFace = new FontFace name:\"Gotham\", file:\"gotham.ttf\" \n");
  };

  loadTestingFileError = function(object) {
    error(null);
    return console.error("Error: Couldn't detect the font: \"" + object.name + "\" and file: \"" + object.file + "\" was loaded.  \n\nEither the file couldn't be found or your browser doesn't support the file type that was provided. \n\nSuppress this message by adding \"hideErrors: true\" when creating a new FontFace. \n");
  };

  return FontFace;

})();


},{}],"YouTubePlayer":[function(require,module,exports){
var firstScriptTag, tag, youTubeReady,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

youTubeReady = new Promise(function(resolve, reject) {
  return window.onYouTubeIframeAPIReady = function() {
    return resolve();
  };
});

tag = document.createElement('script');

tag.src = 'https://www.youtube.com/iframe_api';

firstScriptTag = document.getElementsByTagName('script')[0];

firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

exports.YouTubePlayer = (function(superClass) {
  extend(YouTubePlayer, superClass);

  YouTubePlayer.Events = {
    Loaded: 'yt-loaded',
    Ready: 'yt-ready',
    StateChange: 'yt-stateChange',
    PlaybackQualityChange: 'yt-playbackQualityChange',
    PlaybackRateChange: 'yt-playbackRateChange',
    Error: 'yt-error',
    ApiChange: 'yt-apiChange'
  };

  function YouTubePlayer(options) {
    var div;
    if (options == null) {
      options = {};
    }
    div = document.createElement('div');
    this._playerReady = new Promise((function(_this) {
      return function(playerResolve, playerReject) {
        return youTubeReady.then(function() {
          _this._player = new YT.Player(div, {
            width: _this.width,
            height: _this.height,
            playerVars: options.playerVars,
            events: {
              'onReady': function(event) {
                playerResolve(event.target);
                return _this.emit(YouTubePlayer.Events.Ready, event);
              },
              'onStateChange': function(event) {
                return _this.emit(YouTubePlayer.Events.StateChange, event);
              },
              'onPlaybackQualityChange': function(event) {
                return _this.emit(YouTubePlayer.Events.PlaybackQualityChange, event);
              },
              'onPlaybackRateChange': function(event) {
                return _this.emit(YouTubePlayer.Events.PlaybackRateChange, event);
              },
              'onError': function(event) {
                playerReject(event.data);
                return _this.emit(YouTubePlayer.Events.Error, event);
              },
              'onApiChange': function(event) {
                return _this.emit(YouTubePlayer.Events.ApiChange, event);
              }
            }
          });
          _this.on("change:width", function() {
            return this._player.width = this.width;
          });
          return _this.on("change:height", function() {
            return this._player.height = this.height;
          });
        });
      };
    })(this));
    YouTubePlayer.__super__.constructor.call(this, options);
    this._element.appendChild(div);
  }

  YouTubePlayer.define("video", {
    get: function() {
      return this._video;
    },
    set: function(video) {
      this._video = video;
      return this._playerReady.then((function(_this) {
        return function() {
          var ref;
          _this._player.cueVideoById(video);
          if ((ref = _this.playerVars) != null ? ref.autoplay : void 0) {
            _this._player.playVideo();
          }
          return _this.emit(YouTubePlayer.Events.Loaded, _this._player);
        };
      })(this));
    }
  });

  YouTubePlayer.define("playerVars", {
    get: function() {
      return this._playerVars;
    },
    set: function(value) {
      return this._playerVars = value;
    }
  });

  return YouTubePlayer;

})(Layer);


},{}],"circleModule":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

exports.Circle = (function(superClass) {
  extend(Circle, superClass);

  Circle.prototype.currentValue = null;

  function Circle(options) {
    var base, base1, base2, base3, base4, base5, base6, base7, base8, counter, numberDuration, numberEnd, numberInterval, numberNow, numberStart, self, style;
    this.options = options != null ? options : {};
    if ((base = this.options).circleSize == null) {
      base.circleSize = 300;
    }
    if ((base1 = this.options).strokeWidth == null) {
      base1.strokeWidth = 24;
    }
    if ((base2 = this.options).strokeColor == null) {
      base2.strokeColor = "#fc245c";
    }
    if ((base3 = this.options).topColor == null) {
      base3.topColor = null;
    }
    if ((base4 = this.options).bottomColor == null) {
      base4.bottomColor = null;
    }
    if ((base5 = this.options).hasCounter == null) {
      base5.hasCounter = null;
    }
    if ((base6 = this.options).counterColor == null) {
      base6.counterColor = "#fff";
    }
    if ((base7 = this.options).counterFontSize == null) {
      base7.counterFontSize = 60;
    }
    if ((base8 = this.options).hasLinearEasing == null) {
      base8.hasLinearEasing = null;
    }
    this.options.value = 2;
    this.options.viewBox = this.options.circleSize + this.options.strokeWidth;
    Circle.__super__.constructor.call(this, this.options);
    this.backgroundColor = "";
    this.height = this.options.viewBox;
    this.width = this.options.viewBox;
    this.rotation = -90;
    this.pathLength = Math.PI * this.options.circleSize;
    this.circleID = "circle" + Math.floor(Math.random() * 1000);
    this.gradientID = "circle" + Math.floor(Math.random() * 1000);
    if (this.options.hasCounter !== null) {
      counter = new Layer({
        parent: this,
        html: "",
        width: this.width,
        height: this.height,
        backgroundColor: "",
        rotation: 90,
        color: this.options.counterColor
      });
      style = {
        textAlign: "center",
        fontSize: this.options.counterFontSize + "px",
        lineHeight: this.height + "px",
        fontWeight: "600",
        fontFamily: "-apple-system, Helvetica, Arial, sans-serif",
        boxSizing: "border-box",
        height: this.height
      };
      counter.style = style;
      numberStart = 0;
      numberEnd = 100;
      numberDuration = 2;
      numberNow = numberStart;
      numberInterval = numberEnd - numberStart;
    }
    this.html = "<svg viewBox='-" + (this.options.strokeWidth / 2) + " -" + (this.options.strokeWidth / 2) + " " + this.options.viewBox + " " + this.options.viewBox + "' >\n	<defs>\n	    <linearGradient id='" + this.gradientID + "' >\n	        <stop offset=\"0%\" stop-color='" + (this.options.topColor !== null ? this.options.bottomColor : this.options.strokeColor) + "'/>\n	        <stop offset=\"100%\" stop-color='" + (this.options.topColor !== null ? this.options.topColor : this.options.strokeColor) + "' stop-opacity=\"1\" />\n	    </linearGradient>\n	</defs>\n	<circle id='" + this.circleID + "'\n			fill='none'\n			stroke-linecap='round'\n			stroke-width      = '" + this.options.strokeWidth + "'\n			stroke-dasharray  = '" + this.pathLength + "'\n			stroke-dashoffset = '0'\n			stroke=\"url(#" + this.gradientID + ")\"\n			stroke-width=\"10\"\n			cx = '" + (this.options.circleSize / 2) + "'\n			cy = '" + (this.options.circleSize / 2) + "'\n			r  = '" + (this.options.circleSize / 2) + "'>\n</svg>";
    self = this;
    Utils.domComplete(function() {
      return self.path = document.querySelector("#" + self.circleID);
    });
    this.proxy = new Layer({
      opacity: 0,
      visible: false
    });
    this.proxy.on(Events.AnimationEnd, function(animation, layer) {
      return self.onFinished();
    });
    this.proxy.on('change:x', function() {
      var offset;
      offset = Utils.modulate(this.x, [0, 500], [self.pathLength, 0]);
      self.path.setAttribute('stroke-dashoffset', offset);
      if (self.options.hasCounter !== null) {
        numberNow = Utils.round(self.proxy.x / 5);
        return counter.html = numberNow;
      }
    });
    Utils.domComplete(function() {
      return self.proxy.x = 0.1;
    });
  }

  Circle.prototype.changeTo = function(value, time) {
    var customCurve;
    if (time === void 0) {
      time = 2;
    }
    if (this.options.hasCounter === true && this.options.hasLinearEasing === null) {
      customCurve = "linear";
    } else {
      customCurve = "ease-in-out";
    }
    this.proxy.animate({
      properties: {
        x: 500 * (value / 100)
      },
      time: time,
      curve: customCurve
    });
    return this.currentValue = value;
  };

  Circle.prototype.startAt = function(value) {
    this.proxy.animate({
      properties: {
        x: 500 * (value / 100)
      },
      time: 0.001
    });
    return this.currentValue = value;
  };

  Circle.prototype.hide = function() {
    return this.opacity = 0;
  };

  Circle.prototype.show = function() {
    return this.opacity = 1;
  };

  Circle.prototype.onFinished = function() {};

  return Circle;

})(Layer);


},{}],"colors":[function(require,module,exports){
exports.blackColor = "#1F1F1F";

exports.whiteColor = "#FFFFFF";

exports.gray800 = "#4B5161";

exports.gray600 = "#5A6175";

exports.gray400 = "#828DAB";

exports.gray200 = "#BEC6DA";

exports.stone800 = "#E1E5EA";

exports.stone600 = "#E7E9EF";

exports.stone400 = "#EFF2F6";

exports.stone200 = "#F1F4FA";

exports.blue800 = "#0095C2";

exports.blue600 = "#00B0E4";

exports.blue400 = "#68C8EE";

exports.blue200 = "#ADE8FF";

exports.green800 = "#56A479";

exports.green600 = "#5BCD8E";

exports.green400 = "#BAEED1";

exports.green200 = "#D6F6E4";

exports.red800 = "#B43737";

exports.red600 = "#E54D4D";

exports.red400 = "#FFCCCC";

exports.red200 = "#FFF0F0";

exports.yellow800 = "#EDB51D";

exports.yellow600 = "#FFC852";

exports.yellow400 = "#FED58C";

exports.yellow200 = "#FEEDCC";

exports.circleColor1 = "#FF7744";

exports.circleColor2 = "#918BE5";

exports.phoneContactBackgroundColors = ["#4DD0E1", "#9575CD", "#F06292", "#AED581", "#FFB74D", "#FF8A65", "#FFD54F", "#7986CB", "#4FC3F7"];


},{}],"firebase":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  slice = [].slice;

exports.Firebase = (function(superClass) {
  var parseArgs, request;

  extend(Firebase, superClass);

  Firebase.define("status", {
    get: function() {
      return this._status;
    }
  });

  function Firebase(options1) {
    var base, base1, base2;
    this.options = options1 != null ? options1 : {};
    this.projectID = (base = this.options).projectID != null ? base.projectID : base.projectID = null;
    this.secret = (base1 = this.options).secret != null ? base1.secret : base1.secret = null;
    this.debug = (base2 = this.options).debug != null ? base2.debug : base2.debug = false;
    if (this._status == null) {
      this._status = "disconnected";
    }
    this.secretEndPoint = this.secret ? "?auth=" + this.secret : "?";
    Firebase.__super__.constructor.apply(this, arguments);
    if (this.debug) {
      console.log("Firebase: Connecting to Firebase Project '" + this.projectID + "' ... \n URL: 'https://" + this.projectID + ".firebaseio.com'");
    }
    this.onChange("connection");
  }

  request = function(project, secret, path, callback, method, data, parameters, debug) {
    var options, r, url;
    url = "https://" + project + ".firebaseio.com" + path + ".json" + secret;
    if (parameters != null) {
      if (parameters.shallow) {
        url += "&shallow=true";
      }
      if (parameters.format === "export") {
        url += "&format=export";
      }
      switch (parameters.print) {
        case "pretty":
          url += "&print=pretty";
          break;
        case "silent":
          url += "&print=silent";
      }
      if (typeof parameters.download === "string") {
        url += "&download=" + parameters.download;
        window.open(url, "_self");
      }
      if (typeof parameters.orderBy === "string") {
        url += "&orderBy=" + '"' + parameters.orderBy + '"';
      }
      if (typeof parameters.limitToFirst === "number") {
        url += "&limitToFirst=" + parameters.limitToFirst;
      }
      if (typeof parameters.limitToLast === "number") {
        url += "&limitToLast=" + parameters.limitToLast;
      }
      if (typeof parameters.startAt === "number") {
        url += "&startAt=" + parameters.startAt;
      }
      if (typeof parameters.endAt === "number") {
        url += "&endAt=" + parameters.endAt;
      }
      if (typeof parameters.equalTo === "number") {
        url += "&equalTo=" + parameters.equalTo;
      }
    }
    if (debug) {
      console.log("Firebase: New '" + method + "'-request with data: '" + (JSON.stringify(data)) + "' \n URL: '" + url + "'");
    }
    options = {
      method: method,
      headers: {
        'content-type': 'application/json; charset=utf-8'
      }
    };
    if (data != null) {
      options.body = JSON.stringify(data);
    }
    r = fetch(url, options).then(function(res) {
      var json;
      if (!res.ok) {
        throw Error(res.statusText);
      }
      json = res.json();
      json.then(callback);
      return json;
    })["catch"]((function(_this) {
      return function(error) {
        return console.warn(error);
      };
    })(this));
    return r;
  };

  parseArgs = function() {
    var args, cb, i, l;
    l = arguments[0], args = 3 <= arguments.length ? slice.call(arguments, 1, i = arguments.length - 1) : (i = 1, []), cb = arguments[i++];
    if (typeof args[l - 1] === "object") {
      args[l] = args[l - 1];
      args[l - 1] = null;
    }
    return cb.apply(null, args);
  };

  Firebase.prototype.get = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [2].concat(slice.call(args), [(function(_this) {
      return function(path, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "GET", null, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.put = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "PUT", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.post = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "POST", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.patch = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "PATCH", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype["delete"] = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [2].concat(slice.call(args), [(function(_this) {
      return function(path, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "DELETE", null, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.onChange = function(path, callback) {
    var currentStatus, source, url;
    if (path === "connection") {
      url = "https://" + this.projectID + ".firebaseio.com/.json" + this.secretEndPoint;
      currentStatus = "disconnected";
      source = new EventSource(url);
      source.addEventListener("open", (function(_this) {
        return function() {
          if (currentStatus === "disconnected") {
            _this._status = "connected";
            if (callback != null) {
              callback("connected");
            }
            if (_this.debug) {
              console.log("Firebase: Connection to Firebase Project '" + _this.projectID + "' established");
            }
          }
          return currentStatus = "connected";
        };
      })(this));
      source.addEventListener("error", (function(_this) {
        return function() {
          if (currentStatus === "connected") {
            _this._status = "disconnected";
            if (callback != null) {
              callback("disconnected");
            }
            if (_this.debug) {
              console.warn("Firebase: Connection to Firebase Project '" + _this.projectID + "' closed");
            }
          }
          return currentStatus = "disconnected";
        };
      })(this));
      return;
    }
    url = "https://" + this.projectID + ".firebaseio.com" + path + ".json" + this.secretEndPoint;
    source = new EventSource(url);
    if (this.debug) {
      console.log("Firebase: Listening to changes made to '" + path + "' \n URL: '" + url + "'");
    }
    source.addEventListener("put", (function(_this) {
      return function(ev) {
        if (callback != null) {
          callback(JSON.parse(ev.data).data, "put", JSON.parse(ev.data).path, _.tail(JSON.parse(ev.data).path.split("/"), 1));
        }
        if (_this.debug) {
          return console.log("Firebase: Received changes made to '" + path + "' via 'PUT': " + (JSON.parse(ev.data).data) + " \n URL: '" + url + "'");
        }
      };
    })(this));
    return source.addEventListener("patch", (function(_this) {
      return function(ev) {
        if (callback != null) {
          callback(JSON.parse(ev.data).data, "patch", JSON.parse(ev.data).path, _.tail(JSON.parse(ev.data).path.split("/"), 1));
        }
        if (_this.debug) {
          return console.log("Firebase: Received changes made to '" + path + "' via 'PATCH': " + (JSON.parse(ev.data).data) + " \n URL: '" + url + "'");
        }
      };
    })(this));
  };

  return Firebase;

})(Framer.BaseClass);


},{}],"fonts":[function(require,module,exports){
var FontFace, ProximaNovaBlack, ProximaNovaBlackItalic, ProximaNovaBold, ProximaNovaBoldItalic, ProximaNovaExtrabold, ProximaNovaExtraboldItalic, ProximaNovaLight, ProximaNovaLightItalic, ProximaNovaMedium, ProximaNovaMediumItalic, ProximaNovaRegular, ProximaNovaRegularItalic, ProximaNovaSemiboldItalic, ProximaNovaThin, ProximaNovaThinItalic;

FontFace = require("FontFace").FontFace;

ProximaNovaBlackItalic = new FontFace({
  name: "ProximaNovaBlackItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Black Italic.otf"
});

ProximaNovaBlack = new FontFace({
  name: "ProximaNovaBlack",
  file: "Fonts/Proxima Nova/Proxima Nova Black.otf"
});

ProximaNovaBoldItalic = new FontFace({
  name: "ProximaNovaBoldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Bold Italic.otf"
});

ProximaNovaBold = new FontFace({
  name: "ProximaNovaBold",
  file: "Fonts/Proxima Nova/Proxima Nova Bold.otf"
});

ProximaNovaExtraboldItalic = new FontFace({
  name: "ProximaNovaExtraboldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Extrabold Italic.otf"
});

ProximaNovaExtrabold = new FontFace({
  name: "ProximaNovaExtrabold",
  file: "Fonts/Proxima Nova/Proxima Nova Extrabold.otf"
});

ProximaNovaLightItalic = new FontFace({
  name: "ProximaNovaLightItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Light Italic.otf"
});

ProximaNovaLight = new FontFace({
  name: "ProximaNovaLight",
  file: "Fonts/Proxima Nova/Proxima Nova Light.otf"
});

ProximaNovaMediumItalic = new FontFace({
  name: "ProximaNovaMediumItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Medium Italic.otf"
});

ProximaNovaMedium = new FontFace({
  name: "ProximaNovaMedium",
  file: "Fonts/Proxima Nova/Proxima Nova Medium.otf"
});

ProximaNovaRegularItalic = new FontFace({
  name: "ProximaNovaRegularItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Regular Italic.otf"
});

ProximaNovaRegular = new FontFace({
  name: "ProximaNovaRegular",
  file: "Fonts/Proxima Nova/Proxima Nova Regular.otf"
});

ProximaNovaSemiboldItalic = new FontFace({
  name: "ProximaNovaSemiboldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Semibold Italic.otf"
});

ProximaNovaRegular = new FontFace({
  name: "ProximaNovaSemibold",
  file: "Fonts/Proxima Nova/Proxima Nova Semibold.otf"
});

ProximaNovaThinItalic = new FontFace({
  name: "ProximaNovaThinItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Thin Italic.otf"
});

ProximaNovaThin = new FontFace({
  name: "ProximaNovaThin",
  file: "Fonts/Proxima Nova/Proxima Nova Thin.otf"
});


},{"FontFace":"FontFace"}],"general":[function(require,module,exports){
exports.callStartDate = new Date;

exports.firstTimeButton = true;

exports.firstTimeKnob = true;

exports.lastIdleState = 1;

exports.lastInteractionDate = new Date;

exports.onACall = false;

exports.proximityState = false;

exports.showingIdle = true;

exports.swipeAnimation = false;

exports.language = "english";

exports.screen_height = Framer.Device.screen.height;

exports.screen_width = Framer.Device.screen.width;

exports.loadImages = true;

exports.loadTeleHealth = false;

exports.loadPhone = true;

exports.loadPhotos = true;

exports.loadSocialTalk = true;

exports.loadRadio = true;

exports.showLogs = false;


},{}],"protoFakeInfo":[function(require,module,exports){
exports.interests = [
  {
    photoNameActive: "icon01Active.png",
    photoNameInactive: "icon01Inactive.png",
    text: "Family"
  }, {
    photoNameActive: "icon02Active.png",
    photoNameInactive: "icon02Inactive.png",
    text: "Pets"
  }, {
    photoNameActive: "icon03Active.png",
    photoNameInactive: "icon03Inactive.png",
    text: "Travel"
  }, {
    photoNameActive: "icon04Active.png",
    photoNameInactive: "icon04Inactive.png",
    text: "Reading"
  }, {
    photoNameActive: "icon05Active.png",
    photoNameInactive: "icon05Inactive.png",
    text: "Gardening"
  }, {
    photoNameActive: "icon07Active.png",
    photoNameInactive: "icon07Inactive.png",
    text: "Cooking"
  }, {
    photoNameActive: "icon06Active.png",
    photoNameInactive: "icon06Inactive.png",
    text: "Outdoors"
  }, {
    photoNameActive: "icon08Active.png",
    photoNameInactive: "icon08Inactive.png",
    text: "Current Events"
  }, {
    photoNameActive: "icon09Active.png",
    photoNameInactive: "icon09Inactive.png",
    text: "Economy"
  }, {
    photoNameActive: "icon10Active.png",
    photoNameInactive: "icon10Inactive.png",
    text: "Art & Culture"
  }, {
    photoNameActive: "icon11Active.png",
    photoNameInactive: "icon11Inactive.png",
    text: "Movies"
  }, {
    photoNameActive: "icon12Active.png",
    photoNameInactive: "icon12Inactive.png",
    text: "Television"
  }, {
    photoNameActive: "icon13Active.png",
    photoNameInactive: "icon13Inactive.png",
    text: "Sports"
  }, {
    photoNameActive: "icon14Active.png",
    photoNameInactive: "icon14Inactive.png",
    text: "Science & Tech"
  }, {
    photoNameActive: "icon15Active.png",
    photoNameInactive: "icon15Inactive.png",
    text: "Fashion"
  }
];

exports.socialTalkContactsSpanish = [
  {
    photoName: "avatar12.jpg",
    name: "Miranda",
    lastConnectionDateString: "October 15 2018 | 47 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar08.jpg",
    name: "Olivia",
    lastConnectionDateString: "October 13 2018 | 98 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar02.jpg",
    name: "Simón",
    lastConnectionDateString: "October 9 2018 | 27 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar01.jpg",
    name: "Elías",
    lastConnectionDateString: "October 8 2018 | 42 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar15.jpg",
    name: "Dante",
    lastConnectionDateString: "October 7 2018 | 37 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar05.jpg",
    name: "Irene",
    lastConnectionDateString: "October 2 2018 | 47 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar04.jpg",
    name: "Luana",
    lastConnectionDateString: "September 30 2018 | 57 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar07.jpg",
    name: "Isabel",
    lastConnectionDateString: "September 27 2018 | 72 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar09.jpg",
    name: "Ana",
    lastConnectionDateString: "September 24 2018 | 53 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Dylan",
    lastConnectionDateString: "September 23 2018 | 36 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar10.jpg",
    name: "Ángel",
    lastConnectionDateString: "September 19 2018 | 27 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar14.jpg",
    name: "Martina",
    lastConnectionDateString: "September 16 2018 | 14 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar11.jpg",
    name: "Victoria",
    lastConnectionDateString: "September 12 2018 | 22 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar01.jpg",
    name: "Camila",
    lastConnectionDateString: "September 10 2018 | 39 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Sofia",
    lastConnectionDateString: "September 7 2018 | 10 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar06.jpg",
    name: "Tomas",
    lastConnectionDateString: "September 6 2018 | 27 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar08.jpg",
    name: "Joaquín",
    lastConnectionDateString: "September 3 2018 | 8 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar07.jpg",
    name: "Mía",
    lastConnectionDateString: "September 1 2018 | 48 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar11.jpg",
    name: "Lucas",
    lastConnectionDateString: "August 29 2018 | 18 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar13.jpg",
    name: "Natalia",
    lastConnectionDateString: "August 26 2018 | 53 minutes",
    status: "Available",
    friend: true
  }
];

exports.socialTalkContactsEnglish = [
  {
    photoName: "avatar01.jpg",
    name: "Henry",
    lastConnectionDateString: "October 15 2018 | 47 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar02.jpg",
    name: "Olivia",
    lastConnectionDateString: "October 13 2018 | 98 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Miranda",
    lastConnectionDateString: "October 12 2018 | 32 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar04.jpg",
    name: "Fred",
    lastConnectionDateString: "October 11 2018 | 57 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar05.jpg",
    name: "Hermine",
    lastConnectionDateString: "October 7 2018 | 48 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar06.jpg",
    name: "Leo",
    lastConnectionDateString: "October 6 2018 | 23 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar07.jpg",
    name: "Thomas",
    lastConnectionDateString: "October 5 2018 | 53 minutes",
    status: "Available",
    friend: true
  }
];

exports.radioStationsSpanish = [
  {
    name: "Europa FM",
    number: "91.00 FM",
    favorite: true,
    artistSongName: "Estopa en Levántate y Cárdenas",
    audioID: 1,
    id: 0
  }, {
    name: "Cadena Dial",
    number: "91.70 FM",
    favorite: true,
    artistSongName: "Ana Guerra y Manel Fuentes despiertan a todos los Atrevidos",
    audioID: 2,
    id: 1
  }, {
    name: "Radiole",
    number: "92.40 FM",
    favorite: true,
    artistSongName: "El Duende Callejero - La Jungla de Alquitrán",
    audioID: 3,
    id: 2
  }, {
    name: "Cadena COPE",
    number: "94.80 FM",
    favorite: false,
    artistSongName: "#COPEhaceHistoria en la radio española",
    audioID: 4,
    id: 3
  }, {
    name: "Onda Cero",
    number: "98.00 FM",
    favorite: true,
    artistSongName: "La última pregunta a David Calle, el profesor de Youtube",
    audioID: 5,
    id: 4
  }, {
    name: "Cadena 100",
    number: "99.50 FM",
    favorite: false,
    artistSongName: "Los niños y Jimeno - ¿Playa o montaña?",
    audioID: 6,
    id: 5
  }, {
    name: "Kiss FM",
    number: "102.70 FM",
    favorite: true,
    artistSongName: "KISS FM, lo mejor de los 80 y los 90 hasta hoy",
    audioID: 7,
    id: 6
  }, {
    name: "Cadena Ser",
    number: "105.40 FM",
    favorite: false,
    artistSongName: "Por qué soy feminista y voy a la huelga",
    audioID: 8,
    id: 7
  }, {
    name: "Ghost radio, figure it out",
    number: "110.5 FM",
    favorite: false,
    artistSongName: "artistSongName",
    audioID: 0,
    id: 8
  }
];

exports.radioStationsEnglish = [
  {
    name: "BBC",
    number: "94.9 FM",
    favorite: true,
    artistSongName: "Grimmy chats to Emma Watson",
    audioID: 15,
    id: 0
  }, {
    name: "Capital FM",
    number: "95.8 FM",
    favorite: true,
    artistSongName: "Martin Garrix Capital FM Radio Interview",
    audioID: 16,
    id: 1
  }, {
    name: "LBC",
    number: "97.3 FM",
    favorite: false,
    artistSongName: "James O'Brien Rails At Brexiteers Who Want To Leave At All Costs",
    audioID: 17,
    id: 2
  }, {
    name: "Magic Radio",
    number: "105.4 FM",
    favorite: true,
    artistSongName: "Take That talk spy gadgets with Magic in the Morning",
    audioID: 18,
    id: 3
  }, {
    name: "Talk Radio",
    number: "102.7 FM",
    favorite: true,
    artistSongName: "How has Amazon halved their UK corporation tax bill?",
    audioID: 19,
    id: 4
  }, {
    name: "Radio X",
    number: "104.9 FM",
    favorite: true,
    artistSongName: "Dom's 'Off the Cuff' Knowledge is Remarkable",
    audioID: 20,
    id: 5
  }, {
    name: "Ghost radio, figure it out",
    number: "110.5 FM",
    favorite: false,
    artistSongName: "artistSongName",
    audioID: 0,
    id: 6
  }
];

exports.phoneContactsSpanish = [
  {
    initial: "A",
    photoName: "",
    name: "Sofía Alcaldo",
    nameInitials: "SA",
    lastCalledDateString: "Outgoing Call | October 15 2018, 12:34pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "B",
    photoName: "",
    name: "Valentina Barbero",
    nameInitials: "VB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }
];

exports.phoneContactsEnglish = [
  {
    initial: "A",
    photoName: "",
    name: "Sophie Anderson",
    nameInitials: "SA",
    lastCalledDateString: "Outgoing Call | October 15 2018, 12:34pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "B",
    photoName: "",
    name: "Veronica Brown",
    nameInitials: "VB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "image03.png",
    name: "Steven Baker",
    nameInitials: "SB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "C",
    photoName: "image04.png",
    name: "Milton Cox",
    nameInitials: "MC",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "",
    photoName: "image05.png",
    name: "Mary Clark",
    nameInitials: "MC",
    lastCalledDateString: "Incoming Call | October 14 2018, 5:16pm",
    lastCallType: 2,
    favorite: true
  }, {
    initial: "",
    photoName: "",
    name: "Alexander Carter",
    nameInitials: "AC",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Daryl Coleman",
    nameInitials: "DC",
    lastCalledDateString: "Outgoing Call | October 14 2018, 10:07am",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Lisa Cook",
    nameInitials: "LC",
    lastCalledDateString: "Missed Call | October 13 2018, 2:30pm",
    lastCallType: 3,
    favorite: false
  }, {
    initial: "D",
    photoName: "image09.png",
    name: "Matthew Davis",
    nameInitials: "MD",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "T",
    photoName: "",
    name: "Alasdair Taylor",
    nameInitials: "AT",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "W",
    photoName: "image10.png",
    name: "Eliza Williams",
    nameInitials: "EW",
    lastCalledDateString: "Outgoing Call | October 9 2018, 11:11am",
    lastCallType: 1,
    favorite: true
  }
];

exports.photoGallery = [
  {
    imageName: "img01.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img02.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img03.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img04.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img05.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img06.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img07.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img08.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img09.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img10.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img11.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img12.jpg",
    dateString: "June 2018"
  }
];


},{}],"styles":[function(require,module,exports){
var colors;

colors = require('colors');

exports.header0 = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "144px"
};

exports.header1 = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "92px"
};

exports.header2 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "74px"
};

exports.header3 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "56px"
};

exports.numberStyle = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "56px"
};

exports.header4 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.callTime = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.reminderMenu = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.subhead = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "38px"
};

exports.paragraph = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.tabActive = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.button = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.caption = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "25px"
};


},{"colors":"colors"}],"time":[function(require,module,exports){
exports.getTimeString = function() {
  var hour, minute, now, prepand, second, timeString;
  now = new Date;
  hour = now.getUTCHours() + 2;
  if (hour >= 24) {
    hour = hour - 24;
  }
  minute = now.getUTCMinutes();
  second = now.getUTCSeconds();
  prepand = hour >= 12 ? 'pm' : 'am';
  hour = hour >= 12 ? hour - 12 : hour;
  if (hour === 0) {
    hour = 12;
  }
  minute = minute < 10 ? "0" + minute : minute;
  timeString = hour + ":" + minute + prepand;
  return timeString;
};

exports.getTimeDifferenceString = function(difference) {
  var hour, minute, second, timeString;
  difference = difference / 1000;
  second = Math.floor(difference % 60);
  difference = difference / 60;
  minute = Math.floor(difference % 60);
  difference = difference / 60;
  hour = Math.floor(difference % 24);
  hour = hour < 10 ? "0" + hour : hour;
  minute = minute < 10 ? "0" + minute : minute;
  second = second < 10 ? "0" + second : second;
  timeString = hour + ":" + minute + ":" + second;
  return timeString;
};


},{}]},{},[])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJhbWVyLm1vZHVsZXMuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbTIuZnJhbWVyL21vZHVsZXMvdGltZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20yLmZyYW1lci9tb2R1bGVzL3N0eWxlcy5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20yLmZyYW1lci9tb2R1bGVzL3Byb3RvRmFrZUluZm8uY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tMi5mcmFtZXIvbW9kdWxlcy9nZW5lcmFsLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbTIuZnJhbWVyL21vZHVsZXMvZm9udHMuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tMi5mcmFtZXIvbW9kdWxlcy9maXJlYmFzZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20yLmZyYW1lci9tb2R1bGVzL2NvbG9ycy5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20yLmZyYW1lci9tb2R1bGVzL2NpcmNsZU1vZHVsZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20yLmZyYW1lci9tb2R1bGVzL1lvdVR1YmVQbGF5ZXIuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tMi5mcmFtZXIvbW9kdWxlcy9Gb250RmFjZS5jb2ZmZWUiLCJub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMuZ2V0VGltZVN0cmluZyA9ICgpIC0+XG4gIG5vdyA9IG5ldyBEYXRlXG4gICMgXHRkYXkgPSBub3cuZ2V0VVRDRGF5KClcbiAgIyBcdGRheWxpc3QgPSBbXG4gICMgXHRcdCdTdW5kYXknXG4gICMgXHRcdCdNb25kYXknXG4gICMgXHRcdCdUdWVzZGF5J1xuICAjIFx0XHQnV2VkbmVzZGF5J1xuICAjIFx0XHQnVGh1cnNkYXknXG4gICMgXHRcdCdGcmlkYXknXG4gICMgXHRcdCdTYXR1cmRheSdcbiAgIyBcdF1cbiAgaG91ciA9IG5vdy5nZXRVVENIb3VycygpICsgMlxuICBpZiBob3VyID49IDI0XG4gIFx0aG91ciA9IGhvdXIgLSAyNFxuICAjIFx0XHRkYXkgKz0gMVxuICBtaW51dGUgPSBub3cuZ2V0VVRDTWludXRlcygpXG4gIHNlY29uZCA9IG5vdy5nZXRVVENTZWNvbmRzKClcbiAgcHJlcGFuZCA9IGlmIGhvdXIgPj0gMTIgdGhlbiAncG0nIGVsc2UgJ2FtJ1xuICBob3VyID0gaWYgaG91ciA+PSAxMiB0aGVuIGhvdXIgLSAxMiBlbHNlIGhvdXJcbiAgaWYgaG91ciA9PSAwXG4gIFx0aG91ciA9IDEyXG4gIG1pbnV0ZSA9IGlmIG1pbnV0ZSA8IDEwIHRoZW4gXCIwXCIgKyBtaW51dGUgZWxzZSBtaW51dGVcbiAgdGltZVN0cmluZyA9IGhvdXIgKyBcIjpcIiArIG1pbnV0ZSArIHByZXBhbmRcbiAgcmV0dXJuIHRpbWVTdHJpbmdcblxuZXhwb3J0cy5nZXRUaW1lRGlmZmVyZW5jZVN0cmluZyA9IChkaWZmZXJlbmNlKSAtPlxuICBkaWZmZXJlbmNlID0gZGlmZmVyZW5jZS8xMDAwO1xuICBzZWNvbmQgPSBNYXRoLmZsb29yKGRpZmZlcmVuY2UgJSA2MCk7XG4gIGRpZmZlcmVuY2UgPSBkaWZmZXJlbmNlLzYwO1xuICBtaW51dGUgPSBNYXRoLmZsb29yKGRpZmZlcmVuY2UgJSA2MCk7XG4gIGRpZmZlcmVuY2UgPSBkaWZmZXJlbmNlLzYwO1xuICBob3VyID0gTWF0aC5mbG9vcihkaWZmZXJlbmNlICUgMjQpO1xuXG4gIGhvdXIgPSBpZiBob3VyIDwgMTAgdGhlbiBcIjBcIiArIGhvdXIgZWxzZSBob3VyXG4gIG1pbnV0ZSA9IGlmIG1pbnV0ZSA8IDEwIHRoZW4gXCIwXCIgKyBtaW51dGUgZWxzZSBtaW51dGVcbiAgc2Vjb25kID0gaWYgc2Vjb25kIDwgMTAgdGhlbiBcIjBcIiArIHNlY29uZCBlbHNlIHNlY29uZFxuXG4gIHRpbWVTdHJpbmcgPSBob3VyICsgXCI6XCIgKyBtaW51dGUgKyBcIjpcIiArIHNlY29uZFxuXG4gIHJldHVybiB0aW1lU3RyaW5nXG4iLCIjIHtGb250c30gPSByZXF1aXJlIFwiZm9udHNcIlxuY29sb3JzID0gcmVxdWlyZSAnY29sb3JzJ1xuXG5leHBvcnRzLmhlYWRlcjAgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YU1lZGl1bVwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjE0NHB4XCJcblxuZXhwb3J0cy5oZWFkZXIxID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFSZWd1bGFyXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiOTJweFwiXG5cbmV4cG9ydHMuaGVhZGVyMiA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhQm9sZFwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjc0cHhcIlxuXG5leHBvcnRzLmhlYWRlcjMgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YUJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCI1NnB4XCJcblxuZXhwb3J0cy5udW1iZXJTdHlsZSA9XG5cdGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFSZWd1bGFyXCJcblx0Y29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG5cdGZvbnRTaXplOiBcIjU2cHhcIlxuXG5leHBvcnRzLmhlYWRlcjQgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YUJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCI0NnB4XCJcblxuZXhwb3J0cy5jYWxsVGltZSA9XG5cdGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFNZWRpdW1cIlxuXHRjb2xvcjogY29sb3JzLmJsYWNrQ29sb3Jcblx0Zm9udFNpemU6IFwiNDZweFwiXG5cbmV4cG9ydHMucmVtaW5kZXJNZW51ID1cblx0Zm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuXHRjb2xvcjogY29sb3JzLmJsYWNrQ29sb3Jcblx0Zm9udFNpemU6IFwiNDZweFwiXG5cbmV4cG9ydHMuc3ViaGVhZCA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhU2VtaWJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIzOHB4XCJcblxuZXhwb3J0cy5wYXJhZ3JhcGggPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIzMHB4XCJcblxuZXhwb3J0cy50YWJBY3RpdmUgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVNlbWlib2xkXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMzBweFwiXG5cbmV4cG9ydHMuYnV0dG9uID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFCb2xkXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMzBweFwiXG5cbmV4cG9ydHMuY2FwdGlvbiA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhTWVkaXVtXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMjVweFwiXG4iLCJleHBvcnRzLmludGVyZXN0cyA9IFtcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDFBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDFJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiRmFtaWx5XCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDJBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDJJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiUGV0c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjAzQWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjAzSW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlRyYXZlbFwiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjA0QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjA0SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlJlYWRpbmdcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wNUFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wNUluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJHYXJkZW5pbmdcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wN0FjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wN0luYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJDb29raW5nXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDZBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDZJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiT3V0ZG9vcnNcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wOEFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wOEluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJDdXJyZW50IEV2ZW50c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjA5QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjA5SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIkVjb25vbXlcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24xMEFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24xMEluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJBcnQgJiBDdWx0dXJlXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTFBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTFJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiTW92aWVzXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTJBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTJJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiVGVsZXZpc2lvblwiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjEzQWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjEzSW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlNwb3J0c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjE0QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjE0SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlNjaWVuY2UgJiBUZWNoXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTVBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTVJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiRmFzaGlvblwiXG5cdH0sXG5dXG5cbmV4cG9ydHMuc29jaWFsVGFsa0NvbnRhY3RzU3BhbmlzaCA9IFtcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMi5qcGdcIlxuXHRcdG5hbWU6IFwiTWlyYW5kYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTUgMjAxOCB8IDQ3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA4LmpwZ1wiXG5cdFx0bmFtZTogXCJPbGl2aWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDEzIDIwMTggfCA5OCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMi5qcGdcIlxuXHRcdG5hbWU6IFwiU2ltw7NuXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiT2N0b2JlciA5IDIwMTggfCAyNyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDEuanBnXCJcblx0XHRuYW1lOiBcIkVsw61hc1wiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgOCAyMDE4IHwgNDIgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxNS5qcGdcIlxuXHRcdG5hbWU6IFwiRGFudGVcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDcgMjAxOCB8IDM3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJVbmF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA1LmpwZ1wiXG5cdFx0bmFtZTogXCJJcmVuZVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMiAyMDE4IHwgNDcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA0LmpwZ1wiXG5cdFx0bmFtZTogXCJMdWFuYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAzMCAyMDE4IHwgNTcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwNy5qcGdcIlxuXHRcdG5hbWU6IFwiSXNhYmVsXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDI3IDIwMTggfCA3MiBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwOS5qcGdcIlxuXHRcdG5hbWU6IFwiQW5hXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDI0IDIwMTggfCA1MyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMy5qcGdcIlxuXHRcdG5hbWU6IFwiRHlsYW5cIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMjMgMjAxOCB8IDM2IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMC5qcGdcIlxuXHRcdG5hbWU6IFwiw4FuZ2VsXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDE5IDIwMTggfCAyNyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxNC5qcGdcIlxuXHRcdG5hbWU6IFwiTWFydGluYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAxNiAyMDE4IHwgMTQgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMTEuanBnXCJcblx0XHRuYW1lOiBcIlZpY3RvcmlhXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDEyIDIwMTggfCAyMiBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMS5qcGdcIlxuXHRcdG5hbWU6IFwiQ2FtaWxhXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDEwIDIwMTggfCAzOSBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMy5qcGdcIlxuXHRcdG5hbWU6IFwiU29maWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgNyAyMDE4IHwgMTAgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDYuanBnXCJcblx0XHRuYW1lOiBcIlRvbWFzXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDYgMjAxOCB8IDI3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJVbmF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDguanBnXCJcblx0XHRuYW1lOiBcIkpvYXF1w61uXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDMgMjAxOCB8IDggbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDcuanBnXCJcblx0XHRuYW1lOiBcIk3DrWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMSAyMDE4IHwgNDggbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMTEuanBnXCJcblx0XHRuYW1lOiBcIkx1Y2FzXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiQXVndXN0IDI5IDIwMTggfCAxOCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiVW5hdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMy5qcGdcIlxuXHRcdG5hbWU6IFwiTmF0YWxpYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIkF1Z3VzdCAyNiAyMDE4IHwgNTMgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH1cbl1cblxuZXhwb3J0cy5zb2NpYWxUYWxrQ29udGFjdHNFbmdsaXNoID0gW1xuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAxLmpwZ1wiXG5cdFx0bmFtZTogXCJIZW5yeVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTUgMjAxOCB8IDQ3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAyLmpwZ1wiXG5cdFx0bmFtZTogXCJPbGl2aWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDEzIDIwMTggfCA5OCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMy5qcGdcIlxuXHRcdG5hbWU6IFwiTWlyYW5kYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTIgMjAxOCB8IDMyIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA0LmpwZ1wiXG5cdFx0bmFtZTogXCJGcmVkXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiT2N0b2JlciAxMSAyMDE4IHwgNTcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDUuanBnXCJcblx0XHRuYW1lOiBcIkhlcm1pbmVcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDcgMjAxOCB8IDQ4IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA2LmpwZ1wiXG5cdFx0bmFtZTogXCJMZW9cIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDYgMjAxOCB8IDIzIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA3LmpwZ1wiXG5cdFx0bmFtZTogXCJUaG9tYXNcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDUgMjAxOCB8IDUzIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXVxuXG5leHBvcnRzLnJhZGlvU3RhdGlvbnNTcGFuaXNoID0gW1xuXHR7XG5cdFx0bmFtZTogXCJFdXJvcGEgRk1cIlxuXHRcdG51bWJlcjogXCI5MS4wMCBGTVwiXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0XHRhcnRpc3RTb25nTmFtZTogXCJFc3RvcGEgZW4gTGV2w6FudGF0ZSB5IEPDoXJkZW5hc1wiXG5cdFx0YXVkaW9JRDogMVxuXHRcdGlkOiAwXG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIkNhZGVuYSBEaWFsXCJcblx0XHRudW1iZXI6IFwiOTEuNzAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiQW5hIEd1ZXJyYSB5IE1hbmVsIEZ1ZW50ZXMgZGVzcGllcnRhbiBhIHRvZG9zIGxvcyBBdHJldmlkb3NcIlxuXHRcdGF1ZGlvSUQ6IDJcblx0XHRpZDogMVxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJSYWRpb2xlXCJcblx0XHRudW1iZXI6IFwiOTIuNDAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiRWwgRHVlbmRlIENhbGxlamVybyAtIExhIEp1bmdsYSBkZSBBbHF1aXRyw6FuXCJcblx0XHRhdWRpb0lEOiAzXG5cdFx0aWQ6IDJcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FkZW5hIENPUEVcIlxuXHRcdG51bWJlcjogXCI5NC44MCBGTVwiXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiI0NPUEVoYWNlSGlzdG9yaWEgZW4gbGEgcmFkaW8gZXNwYcOxb2xhXCJcblx0XHRhdWRpb0lEOiA0XG5cdFx0aWQ6IDNcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiT25kYSBDZXJvXCJcblx0XHRudW1iZXI6IFwiOTguMDAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiTGEgw7psdGltYSBwcmVndW50YSBhIERhdmlkIENhbGxlLCBlbCBwcm9mZXNvciBkZSBZb3V0dWJlXCJcblx0XHRhdWRpb0lEOiA1XG5cdFx0aWQ6IDRcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FkZW5hIDEwMFwiXG5cdFx0bnVtYmVyOiBcIjk5LjUwIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJMb3MgbmnDsW9zIHkgSmltZW5vIC0gwr9QbGF5YSBvIG1vbnRhw7FhP1wiXG5cdFx0YXVkaW9JRDogNlxuXHRcdGlkOiA1XG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIktpc3MgRk1cIlxuXHRcdG51bWJlcjogXCIxMDIuNzAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiS0lTUyBGTSwgbG8gbWVqb3IgZGUgbG9zIDgwIHkgbG9zIDkwIGhhc3RhIGhveVwiXG5cdFx0YXVkaW9JRDogN1xuXHRcdGlkOiA2XG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIkNhZGVuYSBTZXJcIlxuXHRcdG51bWJlcjogXCIxMDUuNDAgRk1cIlxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHRcdGFydGlzdFNvbmdOYW1lOiBcIlBvciBxdcOpIHNveSBmZW1pbmlzdGEgeSB2b3kgYSBsYSBodWVsZ2FcIlxuXHRcdGF1ZGlvSUQ6IDhcblx0XHRpZDogN1xuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJHaG9zdCByYWRpbywgZmlndXJlIGl0IG91dFwiXG5cdFx0bnVtYmVyOiBcIjExMC41IEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJhcnRpc3RTb25nTmFtZVwiXG5cdFx0YXVkaW9JRDogMFxuXHRcdGlkOiA4XG5cdH1cbl1cblxuZXhwb3J0cy5yYWRpb1N0YXRpb25zRW5nbGlzaCA9IFtcblx0e1xuXHRcdG5hbWU6IFwiQkJDXCJcblx0XHRudW1iZXI6IFwiOTQuOSBGTVwiXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0XHRhcnRpc3RTb25nTmFtZTogXCJHcmltbXkgY2hhdHMgdG8gRW1tYSBXYXRzb25cIlxuXHRcdGF1ZGlvSUQ6IDE1XG5cdFx0aWQ6IDBcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FwaXRhbCBGTVwiXG5cdFx0bnVtYmVyOiBcIjk1LjggRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiTWFydGluIEdhcnJpeCBDYXBpdGFsIEZNIFJhZGlvIEludGVydmlld1wiXG5cdFx0YXVkaW9JRDogMTZcblx0XHRpZDogMVxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJMQkNcIlxuXHRcdG51bWJlcjogXCI5Ny4zIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJKYW1lcyBPJ0JyaWVuIFJhaWxzIEF0IEJyZXhpdGVlcnMgV2hvIFdhbnQgVG8gTGVhdmUgQXQgQWxsIENvc3RzXCJcblx0XHRhdWRpb0lEOiAxN1xuXHRcdGlkOiAyXG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIk1hZ2ljIFJhZGlvXCJcblx0XHRudW1iZXI6IFwiMTA1LjQgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiVGFrZSBUaGF0IHRhbGsgc3B5IGdhZGdldHMgd2l0aCBNYWdpYyBpbiB0aGUgTW9ybmluZ1wiXG5cdFx0YXVkaW9JRDogMThcblx0XHRpZDogM1xuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJUYWxrIFJhZGlvXCJcblx0XHRudW1iZXI6IFwiMTAyLjcgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiSG93IGhhcyBBbWF6b24gaGFsdmVkIHRoZWlyIFVLIGNvcnBvcmF0aW9uIHRheCBiaWxsP1wiXG5cdFx0YXVkaW9JRDogMTlcblx0XHRpZDogNFxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJSYWRpbyBYXCJcblx0XHRudW1iZXI6IFwiMTA0LjkgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiRG9tJ3MgJ09mZiB0aGUgQ3VmZicgS25vd2xlZGdlIGlzIFJlbWFya2FibGVcIlxuXHRcdGF1ZGlvSUQ6IDIwXG5cdFx0aWQ6IDVcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiR2hvc3QgcmFkaW8sIGZpZ3VyZSBpdCBvdXRcIlxuXHRcdG51bWJlcjogXCIxMTAuNSBGTVwiXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiYXJ0aXN0U29uZ05hbWVcIlxuXHRcdGF1ZGlvSUQ6IDBcblx0XHRpZDogNlxuXHR9XG5dXG5cbmV4cG9ydHMucGhvbmVDb250YWN0c1NwYW5pc2ggPSBbXG5cdHtcblx0XHRpbml0aWFsOiBcIkFcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiU29mw61hIEFsY2FsZG9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJTQVwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTUgMjAxOCwgMTI6MzRwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiVmFsZW50aW5hIEJhcmJlcm9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJWQlwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UwMy5wbmdcIlxuXHQjIFx0bmFtZTogXCJTZWJhc3Rpw6FuIEJyYXZvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJTQlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJDXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTA0LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIk1hdGVvIENhYnJlcm9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIk1DXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UwNS5wbmdcIlxuXHQjIFx0bmFtZTogXCJNYXLDrWEgSm9zw6kgQ2FudG9yXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNQ1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJJbmNvbWluZyBDYWxsIHwgT2N0b2JlciAxNCAyMDE4LCA1OjE2cG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAyXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkFsZWphbmRybyBDYXN0aWxsZWpvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJBQ1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJEYW5pZWwgQ29sYVwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiRENcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTQgMjAxOCwgMTA6MDdhbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDFcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkx1Y8OtYSBDb3J0w6lzXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJMQ1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IE9jdG9iZXIgMTMgMjAxOCwgMjozMHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogM1xuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIkRcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMDkucG5nXCJcblx0IyBcdG5hbWU6IFwiTWF0ZW8gRG9taW5ndWV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNRFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTAucG5nXCJcblx0IyBcdG5hbWU6IFwiTWFydGEgRGVsZ2Fkb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiRURcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgOSAyMDE4LCAxMToxMWFtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMVxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiR1wiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiRW1tYW51ZWwgR2FyemFcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkVHXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTIucG5nXCJcblx0IyBcdG5hbWU6IFwiUGF1bGEgR3JhbmRcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIlBHXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIkhcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIk1hcmlhbmEgSGVybmFuZGV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNSFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJMdWNpYSBIZXJyZXJhXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJMSFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTE1LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIlpvZSBIaWRhbGdvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJaSFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJJbmNvbWluZyBDYWxsIHwgT2N0b2JlciAyIDIwMTgsIDk6MTVhbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDJcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIkxcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTYucG5nXCJcblx0IyBcdG5hbWU6IFwiVGhpYWdvIExvcGV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJUTFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IFNlcHRlbWJlciAyOSAyMDE4LCAxMDozMHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogM1xuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTE3LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIlJlbmF0YSBMb3BlelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiUkxcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiTVwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiQ2F0YWxpbmEgTWFyaW5cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkNNXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk91dGdvaW5nIENhbGwgfCBTZXB0ZW1iZXIgMjYgMjAxOCwgMTo0N3BtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMVxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiTWFyw61hIE1hcnTDrW5lelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTU1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiRmVybmFuZG8gTWFycXVlc1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiRk1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBTZXB0ZW1iZXIgMjUgMjAxOCwgNzo1OHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogM1xuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiSnVhbiBKb3PDqSBNb2xpbmVyb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiSk1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiTmF0YWxpYSBNb3Jlbm9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIk5NXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk91dGdvaW5nIENhbGwgfCBTZXB0ZW1iZXIgMjAgMjAxOCwgMzozM3BtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMVxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkFsb25zbyBSYW1pcmV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJBUlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJPbGl2aWEgUm9kcsOtZ3VlelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiT1JcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyMCAyMDE4LCAzOjMzcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAxXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTI1LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIkxvbGEgUnViaW9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkxSXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBTZXB0ZW1iZXIgMTQgMjAxOCwgNzozMHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMlxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJKdWFuIFJ1aXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkpSXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJTXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJDb25zdGFuemEgU3VhcmV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJDU1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiVFwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UyOC5wbmdcIlxuXHQjIFx0bmFtZTogXCJBbGVzc2FuZHJhIFRvcnJlcm9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkFUXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk1pc3NlZCBDYWxsIHwgU2VwdGVtYmVyIDExIDIwMTgsIDExOjExYW1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAzXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJWXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJNaXJhbmRhIFZlbGF6cXVlelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTVZcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcbl1cblxuZXhwb3J0cy5waG9uZUNvbnRhY3RzRW5nbGlzaCA9IFtcblx0e1xuXHRcdGluaXRpYWw6IFwiQVwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJTb3BoaWUgQW5kZXJzb25cIlxuXHRcdG5hbWVJbml0aWFsczogXCJTQVwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTUgMjAxOCwgMTI6MzRwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiVmVyb25pY2EgQnJvd25cIlxuXHRcdG5hbWVJbml0aWFsczogXCJWQlwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiaW1hZ2UwMy5wbmdcIlxuXHRcdG5hbWU6IFwiU3RldmVuIEJha2VyXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiU0JcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiQ1wiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMDQucG5nXCJcblx0XHRuYW1lOiBcIk1pbHRvbiBDb3hcIlxuXHRcdG5hbWVJbml0aWFsczogXCJNQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTA1LnBuZ1wiXG5cdFx0bmFtZTogXCJNYXJ5IENsYXJrXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTUNcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBPY3RvYmVyIDE0IDIwMTgsIDU6MTZwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAyXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkFsZXhhbmRlciBDYXJ0ZXJcIlxuXHRcdG5hbWVJbml0aWFsczogXCJBQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkRhcnlsIENvbGVtYW5cIlxuXHRcdG5hbWVJbml0aWFsczogXCJEQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTQgMjAxOCwgMTA6MDdhbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJMaXNhIENvb2tcIlxuXHRcdG5hbWVJbml0aWFsczogXCJMQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBPY3RvYmVyIDEzIDIwMTgsIDI6MzBwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAzXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkRcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTA5LnBuZ1wiXG5cdFx0bmFtZTogXCJNYXR0aGV3IERhdmlzXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTURcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiVFwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJBbGFzZGFpciBUYXlsb3JcIlxuXHRcdG5hbWVJbml0aWFsczogXCJBVFwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiV1wiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMTAucG5nXCJcblx0XHRuYW1lOiBcIkVsaXphIFdpbGxpYW1zXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiRVdcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk91dGdvaW5nIENhbGwgfCBPY3RvYmVyIDkgMjAxOCwgMTE6MTFhbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UxMi5wbmdcIlxuXHQjIFx0bmFtZTogXCJQYXVsYSBHcmFuZFwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiUEdcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiSFwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiTWFyaWFuYSBIZXJuYW5kZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIk1IXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkx1Y2lhIEhlcnJlcmFcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkxIXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTUucG5nXCJcblx0IyBcdG5hbWU6IFwiWm9lIEhpZGFsZ29cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIlpIXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBPY3RvYmVyIDIgMjAxOCwgOToxNWFtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMlxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiTFwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UxNi5wbmdcIlxuXHQjIFx0bmFtZTogXCJUaGlhZ28gTG9wZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIlRMXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk1pc3NlZCBDYWxsIHwgU2VwdGVtYmVyIDI5IDIwMTgsIDEwOjMwcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAzXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTcucG5nXCJcblx0IyBcdG5hbWU6IFwiUmVuYXRhIExvcGV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJSTFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJNXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJDYXRhbGluYSBNYXJpblwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiQ01cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyNiAyMDE4LCAxOjQ3cG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAxXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJNYXLDrWEgTWFydMOtbmV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNTVwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJGZXJuYW5kbyBNYXJxdWVzXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJGTVwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IFNlcHRlbWJlciAyNSAyMDE4LCA3OjU4cG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAzXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJKdWFuIEpvc8OpIE1vbGluZXJvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJKTVwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJOYXRhbGlhIE1vcmVub1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTk1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyMCAyMDE4LCAzOjMzcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAxXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiUlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiQWxvbnNvIFJhbWlyZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkFSXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIk9saXZpYSBSb2Ryw61ndWV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJPUlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJPdXRnb2luZyBDYWxsIHwgU2VwdGVtYmVyIDIwIDIwMTgsIDM6MzNwbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDFcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMjUucG5nXCJcblx0IyBcdG5hbWU6IFwiTG9sYSBSdWJpb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTFJcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiSW5jb21pbmcgQ2FsbCB8IFNlcHRlbWJlciAxNCAyMDE4LCA3OjMwcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAyXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkp1YW4gUnVpelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiSlJcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlNcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkNvbnN0YW56YSBTdWFyZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkNTXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJUXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTI4LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIkFsZXNzYW5kcmEgVG9ycmVyb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiQVRcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBTZXB0ZW1iZXIgMTEgMjAxOCwgMTE6MTFhbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDNcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlZcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIk1pcmFuZGEgVmVsYXpxdWV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNVlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXVxuXG5leHBvcnRzLnBob3RvR2FsbGVyeSA9IFtcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwOC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwOS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMTMuanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0IyB9LFxuXHQjIHtcblx0IyBcdGltYWdlTmFtZTogXCJpbWcxNC5qcGdcIlxuXHQjIFx0ZGF0ZVN0cmluZzogXCJKdW5lIDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzE1LmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIkp1bmUgMjAxOFwiXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMTYuanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0IyB9LFxuXHQjIHtcblx0IyBcdGltYWdlTmFtZTogXCJpbWcxNy5qcGdcIlxuXHQjIFx0ZGF0ZVN0cmluZzogXCJNYXkgMjAxOFwiXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMTguanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiTWF5IDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzE5LmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIk1heSAyMDE4XCJcblx0IyB9LFxuXHQjIHtcblx0IyBcdGltYWdlTmFtZTogXCJpbWcyMC5qcGdcIlxuXHQjIFx0ZGF0ZVN0cmluZzogXCJNYXkgMjAxOFwiXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMjEuanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiTWF5IDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzIyLmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIkFwcmlsIDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzIzLmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIkFwcmlsIDIwMThcIlxuXHQjIH0sXG5dXG4iLCIjIER5bmFtaWNcbmV4cG9ydHMuY2FsbFN0YXJ0RGF0ZSA9IG5ldyBEYXRlXG5leHBvcnRzLmZpcnN0VGltZUJ1dHRvbiA9IHRydWVcbmV4cG9ydHMuZmlyc3RUaW1lS25vYiA9IHRydWVcbmV4cG9ydHMubGFzdElkbGVTdGF0ZSA9IDFcbmV4cG9ydHMubGFzdEludGVyYWN0aW9uRGF0ZSA9IG5ldyBEYXRlXG5leHBvcnRzLm9uQUNhbGwgPSBmYWxzZVxuZXhwb3J0cy5wcm94aW1pdHlTdGF0ZSA9IGZhbHNlXG5leHBvcnRzLnNob3dpbmdJZGxlID0gdHJ1ZVxuZXhwb3J0cy5zd2lwZUFuaW1hdGlvbiA9IGZhbHNlXG5cbiMgU3RhdGljIGNvbmZpZ3VyYXRpb25cbmV4cG9ydHMubGFuZ3VhZ2UgPSBcImVuZ2xpc2hcIlxuZXhwb3J0cy5zY3JlZW5faGVpZ2h0ID0gRnJhbWVyLkRldmljZS5zY3JlZW4uaGVpZ2h0XG5leHBvcnRzLnNjcmVlbl93aWR0aCA9IEZyYW1lci5EZXZpY2Uuc2NyZWVuLndpZHRoXG5cbiMgRGV2ZWxvcG1lbnQgdmFyaWFibGVzXG5leHBvcnRzLmxvYWRJbWFnZXMgPSB0cnVlXG5cbmV4cG9ydHMubG9hZFRlbGVIZWFsdGggPSBmYWxzZVxuXG5leHBvcnRzLmxvYWRQaG9uZSA9IHRydWVcbmV4cG9ydHMubG9hZFBob3RvcyA9IHRydWVcbmV4cG9ydHMubG9hZFNvY2lhbFRhbGsgPSB0cnVlXG5leHBvcnRzLmxvYWRSYWRpbyA9IHRydWVcblxuIyBleHBvcnRzLmxvYWRJbWFnZXMgPSBmYWxzZVxuI1xuIyBleHBvcnRzLmxvYWRUZWxlSGVhbHRoID0gZmFsc2VcbiNcbiMgZXhwb3J0cy5sb2FkUGhvbmUgPSBmYWxzZVxuIyBleHBvcnRzLmxvYWRQaG90b3MgPSBmYWxzZVxuIyBleHBvcnRzLmxvYWRTb2NpYWxUYWxrID0gdHJ1ZVxuIyBleHBvcnRzLmxvYWRSYWRpbyA9IGZhbHNlXG5cbmV4cG9ydHMuc2hvd0xvZ3MgPSBmYWxzZVxuIiwie0ZvbnRGYWNlfSA9IHJlcXVpcmUgXCJGb250RmFjZVwiXG5cblByb3hpbWFOb3ZhQmxhY2tJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUJsYWNrSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIEJsYWNrIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YUJsYWNrID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFCbGFja1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBCbGFjay5vdGZcIlxuXG5Qcm94aW1hTm92YUJvbGRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUJvbGRJdGFsaWNcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgQm9sZCBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFCb2xkID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFCb2xkXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIEJvbGQub3RmXCJcblxuUHJveGltYU5vdmFFeHRyYWJvbGRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUV4dHJhYm9sZEl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBFeHRyYWJvbGQgSXRhbGljLm90ZlwiXG5cblByb3hpbWFOb3ZhRXh0cmFib2xkID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFFeHRyYWJvbGRcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgRXh0cmFib2xkLm90ZlwiXG5cblByb3hpbWFOb3ZhTGlnaHRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUxpZ2h0SXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIExpZ2h0IEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YUxpZ2h0ID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFMaWdodFwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBMaWdodC5vdGZcIlxuXG5Qcm94aW1hTm92YU1lZGl1bUl0YWxpYyA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhTWVkaXVtSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIE1lZGl1bSBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFNZWRpdW0gPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YU1lZGl1bVwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBNZWRpdW0ub3RmXCJcblxuUHJveGltYU5vdmFSZWd1bGFySXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFSZWd1bGFySXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFJlZ3VsYXIgSXRhbGljLm90ZlwiXG5cblByb3hpbWFOb3ZhUmVndWxhciA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhUmVndWxhclwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBSZWd1bGFyLm90ZlwiXG5cblByb3hpbWFOb3ZhU2VtaWJvbGRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVNlbWlib2xkSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFNlbWlib2xkIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YVJlZ3VsYXIgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVNlbWlib2xkXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFNlbWlib2xkLm90ZlwiXG5cblByb3hpbWFOb3ZhVGhpbkl0YWxpYyA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhVGhpbkl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBUaGluIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YVRoaW4gPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVRoaW5cIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgVGhpbi5vdGZcIlxuIiwiIyBEb2N1bWVudGF0aW9uIG9mIHRoaXMgTW9kdWxlOiBodHRwczovL2dpdGh1Yi5jb20vbWFyY2tyZW5uL2ZyYW1lci1GaXJlYmFzZVxuIyAtLS0tLS0gOiAtLS0tLS0tIEZpcmViYXNlIFJFU1QgQVBJOiBodHRwczovL2ZpcmViYXNlLmdvb2dsZS5jb20vZG9jcy9yZWZlcmVuY2UvcmVzdC9kYXRhYmFzZS9cblxuIyBGaXJlYmFzZSBSRVNUIEFQSSBDbGFzcyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbmNsYXNzIGV4cG9ydHMuRmlyZWJhc2UgZXh0ZW5kcyBGcmFtZXIuQmFzZUNsYXNzXG5cblxuXHRALmRlZmluZSBcInN0YXR1c1wiLFxuXHRcdGdldDogLT4gQF9zdGF0dXMgIyByZWFkT25seVxuXG5cdGNvbnN0cnVjdG9yOiAoQG9wdGlvbnM9e30pIC0+XG5cdFx0QHByb2plY3RJRCA9IEBvcHRpb25zLnByb2plY3RJRCA/PSBudWxsXG5cdFx0QHNlY3JldCAgICA9IEBvcHRpb25zLnNlY3JldCAgICA/PSBudWxsXG5cdFx0QGRlYnVnICAgICA9IEBvcHRpb25zLmRlYnVnICAgICA/PSBmYWxzZVxuXHRcdEBfc3RhdHVzICAgICAgICAgICAgICAgICAgICAgICAgPz0gXCJkaXNjb25uZWN0ZWRcIlxuXG5cdFx0QHNlY3JldEVuZFBvaW50ID0gaWYgQHNlY3JldCB0aGVuIFwiP2F1dGg9I3tAc2VjcmV0fVwiIGVsc2UgXCI/XCIgIyBob3RmaXhcblx0XHRzdXBlclxuXG5cdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogQ29ubmVjdGluZyB0byBGaXJlYmFzZSBQcm9qZWN0ICcje0Bwcm9qZWN0SUR9JyAuLi4gXFxuIFVSTDogJ2h0dHBzOi8vI3tAcHJvamVjdElEfS5maXJlYmFzZWlvLmNvbSdcIiBpZiBAZGVidWdcblx0XHRALm9uQ2hhbmdlIFwiY29ubmVjdGlvblwiXG5cblx0cmVxdWVzdCA9IChwcm9qZWN0LCBzZWNyZXQsIHBhdGgsIGNhbGxiYWNrLCBtZXRob2QsIGRhdGEsIHBhcmFtZXRlcnMsIGRlYnVnKSAtPlxuXG5cdFx0dXJsID0gXCJodHRwczovLyN7cHJvamVjdH0uZmlyZWJhc2Vpby5jb20je3BhdGh9Lmpzb24je3NlY3JldH1cIlxuXG5cdFx0aWYgcGFyYW1ldGVycz9cblx0XHRcdGlmIHBhcmFtZXRlcnMuc2hhbGxvdyAgICAgICAgICAgIHRoZW4gdXJsICs9IFwiJnNoYWxsb3c9dHJ1ZVwiXG5cdFx0XHRpZiBwYXJhbWV0ZXJzLmZvcm1hdCBpcyBcImV4cG9ydFwiIHRoZW4gdXJsICs9IFwiJmZvcm1hdD1leHBvcnRcIlxuXG5cdFx0XHRzd2l0Y2ggcGFyYW1ldGVycy5wcmludFxuXHRcdFx0XHR3aGVuIFwicHJldHR5XCIgdGhlbiB1cmwgKz0gXCImcHJpbnQ9cHJldHR5XCJcblx0XHRcdFx0d2hlbiBcInNpbGVudFwiIHRoZW4gdXJsICs9IFwiJnByaW50PXNpbGVudFwiXG5cblx0XHRcdGlmIHR5cGVvZiBwYXJhbWV0ZXJzLmRvd25sb2FkIGlzIFwic3RyaW5nXCJcblx0XHRcdFx0dXJsICs9IFwiJmRvd25sb2FkPSN7cGFyYW1ldGVycy5kb3dubG9hZH1cIlxuXHRcdFx0XHR3aW5kb3cub3Blbih1cmwsXCJfc2VsZlwiKVxuXG5cdFx0XHR1cmwgKz0gXCImb3JkZXJCeT1cIiArICdcIicgKyBwYXJhbWV0ZXJzLm9yZGVyQnkgKyAnXCInIGlmIHR5cGVvZiBwYXJhbWV0ZXJzLm9yZGVyQnkgICAgICBpcyBcInN0cmluZ1wiXG5cdFx0XHR1cmwgKz0gXCImbGltaXRUb0ZpcnN0PSN7cGFyYW1ldGVycy5saW1pdFRvRmlyc3R9XCIgICBpZiB0eXBlb2YgcGFyYW1ldGVycy5saW1pdFRvRmlyc3QgaXMgXCJudW1iZXJcIlxuXHRcdFx0dXJsICs9IFwiJmxpbWl0VG9MYXN0PSN7cGFyYW1ldGVycy5saW1pdFRvTGFzdH1cIiAgICAgaWYgdHlwZW9mIHBhcmFtZXRlcnMubGltaXRUb0xhc3QgIGlzIFwibnVtYmVyXCJcblx0XHRcdHVybCArPSBcIiZzdGFydEF0PSN7cGFyYW1ldGVycy5zdGFydEF0fVwiICAgICAgICAgICAgIGlmIHR5cGVvZiBwYXJhbWV0ZXJzLnN0YXJ0QXQgICAgICBpcyBcIm51bWJlclwiXG5cdFx0XHR1cmwgKz0gXCImZW5kQXQ9I3twYXJhbWV0ZXJzLmVuZEF0fVwiICAgICAgICAgICAgICAgICBpZiB0eXBlb2YgcGFyYW1ldGVycy5lbmRBdCAgICAgICAgaXMgXCJudW1iZXJcIlxuXHRcdFx0dXJsICs9IFwiJmVxdWFsVG89I3twYXJhbWV0ZXJzLmVxdWFsVG99XCIgICAgICAgICAgICAgaWYgdHlwZW9mIHBhcmFtZXRlcnMuZXF1YWxUbyAgICAgIGlzIFwibnVtYmVyXCJcblx0XHRcblx0XHRjb25zb2xlLmxvZyBcIkZpcmViYXNlOiBOZXcgJyN7bWV0aG9kfSctcmVxdWVzdCB3aXRoIGRhdGE6ICcje0pTT04uc3RyaW5naWZ5KGRhdGEpfScgXFxuIFVSTDogJyN7dXJsfSdcIiBpZiBkZWJ1Z1xuXHRcdFxuXHRcdG9wdGlvbnMgPVxuXHRcdFx0bWV0aG9kOiBtZXRob2Rcblx0XHRcdGhlYWRlcnM6XG5cdFx0XHRcdCdjb250ZW50LXR5cGUnOiAnYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOCdcblx0XHRcblx0XHRpZiBkYXRhP1xuXHRcdFx0b3B0aW9ucy5ib2R5ID0gSlNPTi5zdHJpbmdpZnkoZGF0YSlcblxuXHRcdHIgPSBmZXRjaCh1cmwsIG9wdGlvbnMpXG5cdFx0LnRoZW4gKHJlcykgLT5cblx0XHRcdGlmICFyZXMub2sgdGhlbiB0aHJvdyBFcnJvcihyZXMuc3RhdHVzVGV4dClcblx0XHRcdGpzb24gPSByZXMuanNvbigpXG5cdFx0XHRqc29uLnRoZW4gY2FsbGJhY2tcblx0XHRcdHJldHVybiBqc29uXG5cdFx0LmNhdGNoIChlcnJvcikgPT4gY29uc29sZS53YXJuKGVycm9yKVxuXHRcdFxuXHRcdHJldHVybiByXG5cblx0IyBUaGlyZCBhcmd1bWVudCBjYW4gYWxzbyBhY2NlcHQgb3B0aW9ucywgcmF0aGVyIHRoYW4gY2FsbGJhY2tcblx0cGFyc2VBcmdzID0gKGwsIGFyZ3MuLi4sIGNiKSAtPlxuXHRcdGlmIHR5cGVvZiBhcmdzW2wtMV0gaXMgXCJvYmplY3RcIlxuXHRcdFx0YXJnc1tsXSA9IGFyZ3NbbC0xXVxuXHRcdFx0YXJnc1tsLTFdID0gbnVsbFxuXG5cdFx0cmV0dXJuIGNiLmFwcGx5KG51bGwsIGFyZ3MpXG5cblx0IyBBdmFpbGFibGUgbWV0aG9kc1xuXG5cdGdldDogICAgKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAyLCBhcmdzLi4uLCAocGF0aCwgXHRcdCBjYWxsYmFjaywgcGFyYW1ldGVycykgPT4gcmVxdWVzdChAcHJvamVjdElELCBAc2VjcmV0RW5kUG9pbnQsIHBhdGgsIGNhbGxiYWNrLCBcIkdFVFwiLCAgICBudWxsLCBwYXJhbWV0ZXJzLCBAZGVidWcpXG5cdHB1dDogICAgKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAzLCBhcmdzLi4uLCAocGF0aCwgZGF0YSwgY2FsbGJhY2ssIHBhcmFtZXRlcnMpID0+IHJlcXVlc3QoQHByb2plY3RJRCwgQHNlY3JldEVuZFBvaW50LCBwYXRoLCBjYWxsYmFjaywgXCJQVVRcIiwgICAgZGF0YSwgcGFyYW1ldGVycywgQGRlYnVnKVxuXHRwb3N0OiAgIChhcmdzLi4uKSAtPiBwYXJzZUFyZ3MgMywgYXJncy4uLiwgKHBhdGgsIGRhdGEsIGNhbGxiYWNrLCBwYXJhbWV0ZXJzKSA9PiByZXF1ZXN0KEBwcm9qZWN0SUQsIEBzZWNyZXRFbmRQb2ludCwgcGF0aCwgY2FsbGJhY2ssIFwiUE9TVFwiLCAgIGRhdGEsIHBhcmFtZXRlcnMsIEBkZWJ1Zylcblx0cGF0Y2g6ICAoYXJncy4uLikgLT4gcGFyc2VBcmdzIDMsIGFyZ3MuLi4sIChwYXRoLCBkYXRhLCBjYWxsYmFjaywgcGFyYW1ldGVycykgPT4gcmVxdWVzdChAcHJvamVjdElELCBAc2VjcmV0RW5kUG9pbnQsIHBhdGgsIGNhbGxiYWNrLCBcIlBBVENIXCIsICBkYXRhLCBwYXJhbWV0ZXJzLCBAZGVidWcpXG5cdGRlbGV0ZTogKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAyLCBhcmdzLi4uLCAocGF0aCwgXHQgIFx0IGNhbGxiYWNrLCBwYXJhbWV0ZXJzKSA9PiByZXF1ZXN0KEBwcm9qZWN0SUQsIEBzZWNyZXRFbmRQb2ludCwgcGF0aCwgY2FsbGJhY2ssIFwiREVMRVRFXCIsIG51bGwsIHBhcmFtZXRlcnMsIEBkZWJ1ZylcblxuXG5cdG9uQ2hhbmdlOiAocGF0aCwgY2FsbGJhY2spIC0+XG5cblxuXHRcdGlmIHBhdGggaXMgXCJjb25uZWN0aW9uXCJcblxuXHRcdFx0dXJsID0gXCJodHRwczovLyN7QHByb2plY3RJRH0uZmlyZWJhc2Vpby5jb20vLmpzb24je0BzZWNyZXRFbmRQb2ludH1cIlxuXHRcdFx0Y3VycmVudFN0YXR1cyA9IFwiZGlzY29ubmVjdGVkXCJcblx0XHRcdHNvdXJjZSA9IG5ldyBFdmVudFNvdXJjZSh1cmwpXG5cblx0XHRcdHNvdXJjZS5hZGRFdmVudExpc3RlbmVyIFwib3BlblwiLCA9PlxuXHRcdFx0XHRpZiBjdXJyZW50U3RhdHVzIGlzIFwiZGlzY29ubmVjdGVkXCJcblx0XHRcdFx0XHRALl9zdGF0dXMgPSBcImNvbm5lY3RlZFwiXG5cdFx0XHRcdFx0Y2FsbGJhY2soXCJjb25uZWN0ZWRcIikgaWYgY2FsbGJhY2s/XG5cdFx0XHRcdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogQ29ubmVjdGlvbiB0byBGaXJlYmFzZSBQcm9qZWN0ICcje0Bwcm9qZWN0SUR9JyBlc3RhYmxpc2hlZFwiIGlmIEBkZWJ1Z1xuXHRcdFx0XHRjdXJyZW50U3RhdHVzID0gXCJjb25uZWN0ZWRcIlxuXG5cdFx0XHRzb3VyY2UuYWRkRXZlbnRMaXN0ZW5lciBcImVycm9yXCIsID0+XG5cdFx0XHRcdGlmIGN1cnJlbnRTdGF0dXMgaXMgXCJjb25uZWN0ZWRcIlxuXHRcdFx0XHRcdEAuX3N0YXR1cyA9IFwiZGlzY29ubmVjdGVkXCJcblx0XHRcdFx0XHRjYWxsYmFjayhcImRpc2Nvbm5lY3RlZFwiKSBpZiBjYWxsYmFjaz9cblx0XHRcdFx0XHRjb25zb2xlLndhcm4gXCJGaXJlYmFzZTogQ29ubmVjdGlvbiB0byBGaXJlYmFzZSBQcm9qZWN0ICcje0Bwcm9qZWN0SUR9JyBjbG9zZWRcIiBpZiBAZGVidWdcblx0XHRcdFx0Y3VycmVudFN0YXR1cyA9IFwiZGlzY29ubmVjdGVkXCJcblxuXHRcdFx0cmV0dXJuXG5cblx0XHR1cmwgPSBcImh0dHBzOi8vI3tAcHJvamVjdElEfS5maXJlYmFzZWlvLmNvbSN7cGF0aH0uanNvbiN7QHNlY3JldEVuZFBvaW50fVwiXG5cdFx0c291cmNlID0gbmV3IEV2ZW50U291cmNlKHVybClcblx0XHRjb25zb2xlLmxvZyBcIkZpcmViYXNlOiBMaXN0ZW5pbmcgdG8gY2hhbmdlcyBtYWRlIHRvICcje3BhdGh9JyBcXG4gVVJMOiAnI3t1cmx9J1wiIGlmIEBkZWJ1Z1xuXG5cdFx0c291cmNlLmFkZEV2ZW50TGlzdGVuZXIgXCJwdXRcIiwgKGV2KSA9PlxuXHRcdFx0Y2FsbGJhY2soSlNPTi5wYXJzZShldi5kYXRhKS5kYXRhLCBcInB1dFwiLCBKU09OLnBhcnNlKGV2LmRhdGEpLnBhdGgsIF8udGFpbChKU09OLnBhcnNlKGV2LmRhdGEpLnBhdGguc3BsaXQoXCIvXCIpLDEpKSBpZiBjYWxsYmFjaz9cblx0XHRcdGNvbnNvbGUubG9nIFwiRmlyZWJhc2U6IFJlY2VpdmVkIGNoYW5nZXMgbWFkZSB0byAnI3twYXRofScgdmlhICdQVVQnOiAje0pTT04ucGFyc2UoZXYuZGF0YSkuZGF0YX0gXFxuIFVSTDogJyN7dXJsfSdcIiBpZiBAZGVidWdcblxuXHRcdHNvdXJjZS5hZGRFdmVudExpc3RlbmVyIFwicGF0Y2hcIiwgKGV2KSA9PlxuXHRcdFx0Y2FsbGJhY2soSlNPTi5wYXJzZShldi5kYXRhKS5kYXRhLCBcInBhdGNoXCIsIEpTT04ucGFyc2UoZXYuZGF0YSkucGF0aCwgXy50YWlsKEpTT04ucGFyc2UoZXYuZGF0YSkucGF0aC5zcGxpdChcIi9cIiksMSkpIGlmIGNhbGxiYWNrP1xuXHRcdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogUmVjZWl2ZWQgY2hhbmdlcyBtYWRlIHRvICcje3BhdGh9JyB2aWEgJ1BBVENIJzogI3tKU09OLnBhcnNlKGV2LmRhdGEpLmRhdGF9IFxcbiBVUkw6ICcje3VybH0nXCIgaWYgQGRlYnVnXG4iLCIjIEFkZCB0aGUgZm9sbG93aW5nIGxpbmUgdG8geW91ciBwcm9qZWN0IGluIEZyYW1lciBTdHVkaW8uXG4jIG15TW9kdWxlID0gcmVxdWlyZSBcIm15TW9kdWxlXCJcbiMgUmVmZXJlbmNlIHRoZSBjb250ZW50cyBieSBuYW1lLCBsaWtlIG15TW9kdWxlLm15RnVuY3Rpb24oKSBvciBteU1vZHVsZS5teVZhclxuXG4jIGV4cG9ydHMubXlWYXIgPSBcIm15VmFyaWFibGVcIlxuI1xuIyBleHBvcnRzLm15RnVuY3Rpb24gPSAtPlxuIyBcdHByaW50IFwibXlGdW5jdGlvbiBpcyBydW5uaW5nXCJcbiNcbiMgZXhwb3J0cy5teUFycmF5ID0gWzEsIDIsIDNdXG5cbmV4cG9ydHMuYmxhY2tDb2xvciA9IFwiIzFGMUYxRlwiXG5leHBvcnRzLndoaXRlQ29sb3IgPSBcIiNGRkZGRkZcIlxuXG5leHBvcnRzLmdyYXk4MDAgPSBcIiM0QjUxNjFcIlxuZXhwb3J0cy5ncmF5NjAwID0gXCIjNUE2MTc1XCJcbmV4cG9ydHMuZ3JheTQwMCA9IFwiIzgyOERBQlwiXG5leHBvcnRzLmdyYXkyMDAgPSBcIiNCRUM2REFcIlxuXG5leHBvcnRzLnN0b25lODAwID0gXCIjRTFFNUVBXCJcbmV4cG9ydHMuc3RvbmU2MDAgPSBcIiNFN0U5RUZcIlxuZXhwb3J0cy5zdG9uZTQwMCA9IFwiI0VGRjJGNlwiXG5leHBvcnRzLnN0b25lMjAwID0gXCIjRjFGNEZBXCJcblxuZXhwb3J0cy5ibHVlODAwID0gXCIjMDA5NUMyXCJcbmV4cG9ydHMuYmx1ZTYwMCA9IFwiIzAwQjBFNFwiXG5leHBvcnRzLmJsdWU0MDAgPSBcIiM2OEM4RUVcIlxuZXhwb3J0cy5ibHVlMjAwID0gXCIjQURFOEZGXCJcblxuZXhwb3J0cy5ncmVlbjgwMCA9IFwiIzU2QTQ3OVwiXG5leHBvcnRzLmdyZWVuNjAwID0gXCIjNUJDRDhFXCJcbmV4cG9ydHMuZ3JlZW40MDAgPSBcIiNCQUVFRDFcIlxuZXhwb3J0cy5ncmVlbjIwMCA9IFwiI0Q2RjZFNFwiXG5cbmV4cG9ydHMucmVkODAwID0gXCIjQjQzNzM3XCJcbmV4cG9ydHMucmVkNjAwID0gXCIjRTU0RDREXCJcbmV4cG9ydHMucmVkNDAwID0gXCIjRkZDQ0NDXCJcbmV4cG9ydHMucmVkMjAwID0gXCIjRkZGMEYwXCJcblxuZXhwb3J0cy55ZWxsb3c4MDAgPSBcIiNFREI1MURcIlxuZXhwb3J0cy55ZWxsb3c2MDAgPSBcIiNGRkM4NTJcIlxuZXhwb3J0cy55ZWxsb3c0MDAgPSBcIiNGRUQ1OENcIlxuZXhwb3J0cy55ZWxsb3cyMDAgPSBcIiNGRUVEQ0NcIlxuXG5leHBvcnRzLmNpcmNsZUNvbG9yMSA9IFwiI0ZGNzc0NFwiXG5leHBvcnRzLmNpcmNsZUNvbG9yMiA9IFwiIzkxOEJFNVwiXG5cbmV4cG9ydHMucGhvbmVDb250YWN0QmFja2dyb3VuZENvbG9ycyA9IFtcblx0XCIjNEREMEUxXCIsXG5cdFwiIzk1NzVDRFwiLFxuXHRcIiNGMDYyOTJcIixcblx0XCIjQUVENTgxXCIsXG5cdFwiI0ZGQjc0RFwiLFxuXHRcIiNGRjhBNjVcIixcblx0XCIjRkZENTRGXCIsXG5cdFwiIzc5ODZDQlwiLFxuXHRcIiM0RkMzRjdcIlxuXVxuIiwiY2xhc3MgZXhwb3J0cy5DaXJjbGUgZXh0ZW5kcyBMYXllclxuXHRjdXJyZW50VmFsdWU6IG51bGxcblxuXHRjb25zdHJ1Y3RvcjogKEBvcHRpb25zPXt9KSAtPlxuXG5cdFx0QG9wdGlvbnMuY2lyY2xlU2l6ZSA/PSAzMDBcblx0XHRAb3B0aW9ucy5zdHJva2VXaWR0aCA/PSAyNFxuXG5cdFx0QG9wdGlvbnMuc3Ryb2tlQ29sb3IgPz0gXCIjZmMyNDVjXCJcblx0XHRAb3B0aW9ucy50b3BDb2xvciA/PSBudWxsXG5cdFx0QG9wdGlvbnMuYm90dG9tQ29sb3IgPz0gbnVsbFxuXG5cdFx0QG9wdGlvbnMuaGFzQ291bnRlciA/PSBudWxsXG5cdFx0QG9wdGlvbnMuY291bnRlckNvbG9yID89IFwiI2ZmZlwiXG5cdFx0QG9wdGlvbnMuY291bnRlckZvbnRTaXplID89IDYwXG5cdFx0QG9wdGlvbnMuaGFzTGluZWFyRWFzaW5nID89IG51bGxcblxuXHRcdEBvcHRpb25zLnZhbHVlID0gMlxuXG5cdFx0QG9wdGlvbnMudmlld0JveCA9IChAb3B0aW9ucy5jaXJjbGVTaXplKSArIEBvcHRpb25zLnN0cm9rZVdpZHRoXG5cblx0XHRzdXBlciBAb3B0aW9uc1xuXG5cdFx0QC5iYWNrZ3JvdW5kQ29sb3IgPSBcIlwiXG5cdFx0QC5oZWlnaHQgPSBAb3B0aW9ucy52aWV3Qm94XG5cdFx0QC53aWR0aCA9IEBvcHRpb25zLnZpZXdCb3hcblx0XHRALnJvdGF0aW9uID0gLTkwXG5cblxuXHRcdEAucGF0aExlbmd0aCA9IE1hdGguUEkgKiBAb3B0aW9ucy5jaXJjbGVTaXplXG5cblx0XHRALmNpcmNsZUlEID0gXCJjaXJjbGVcIiArIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSoxMDAwKVxuXHRcdEAuZ3JhZGllbnRJRCA9IFwiY2lyY2xlXCIgKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkqMTAwMClcblxuXHRcdCMgUHV0IHRoaXMgaW5zaWRlIGxpbmVhcmdyYWRpZW50XG5cdFx0IyBncmFkaWVudFVuaXRzPVwidXNlclNwYWNlT25Vc2VcIlxuXHRcdCMgICAgeDE9XCIwJVwiIHkxPVwiMCVcIiB4Mj1cIjUwJVwiIHkyPVwiMCVcIiBncmFkaWVudFRyYW5zZm9ybT1cInJvdGF0ZSgxMjApXCJcblxuXG5cdFx0aWYgQG9wdGlvbnMuaGFzQ291bnRlciBpc250IG51bGxcblx0XHRcdGNvdW50ZXIgPSBuZXcgTGF5ZXJcblx0XHRcdFx0cGFyZW50OiBAXG5cdFx0XHRcdGh0bWw6IFwiXCJcblx0XHRcdFx0d2lkdGg6IEAud2lkdGhcblx0XHRcdFx0aGVpZ2h0OiBALmhlaWdodFxuXHRcdFx0XHRiYWNrZ3JvdW5kQ29sb3I6IFwiXCJcblx0XHRcdFx0cm90YXRpb246IDkwXG5cdFx0XHRcdGNvbG9yOiBAb3B0aW9ucy5jb3VudGVyQ29sb3JcblxuXHRcdFx0c3R5bGUgPSB7XG5cdFx0XHRcdHRleHRBbGlnbjogXCJjZW50ZXJcIlxuXHRcdFx0XHRmb250U2l6ZTogXCIje0BvcHRpb25zLmNvdW50ZXJGb250U2l6ZX1weFwiXG5cdFx0XHRcdGxpbmVIZWlnaHQ6IFwiI3tALmhlaWdodH1weFwiXG5cdFx0XHRcdGZvbnRXZWlnaHQ6IFwiNjAwXCJcblx0XHRcdFx0Zm9udEZhbWlseTogXCItYXBwbGUtc3lzdGVtLCBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmXCJcblx0XHRcdFx0Ym94U2l6aW5nOiBcImJvcmRlci1ib3hcIlxuXHRcdFx0XHRoZWlnaHQ6IEAuaGVpZ2h0XG5cdFx0XHR9XG5cblx0XHRcdGNvdW50ZXIuc3R5bGUgPSBzdHlsZVxuXG5cdFx0XHRudW1iZXJTdGFydCA9IDBcblx0XHRcdG51bWJlckVuZCA9IDEwMFxuXHRcdFx0bnVtYmVyRHVyYXRpb24gPSAyXG5cblx0XHRcdG51bWJlck5vdyA9IG51bWJlclN0YXJ0XG5cdFx0XHRudW1iZXJJbnRlcnZhbCA9IG51bWJlckVuZCAtIG51bWJlclN0YXJ0XG5cblxuXHRcdEAuaHRtbCA9IFwiXCJcIlxuXHRcdFx0PHN2ZyB2aWV3Qm94PSctI3tAb3B0aW9ucy5zdHJva2VXaWR0aC8yfSAtI3tAb3B0aW9ucy5zdHJva2VXaWR0aC8yfSAje0BvcHRpb25zLnZpZXdCb3h9ICN7QG9wdGlvbnMudmlld0JveH0nID5cblx0XHRcdFx0PGRlZnM+XG5cdFx0XHRcdCAgICA8bGluZWFyR3JhZGllbnQgaWQ9JyN7QGdyYWRpZW50SUR9JyA+XG5cdFx0XHRcdCAgICAgICAgPHN0b3Agb2Zmc2V0PVwiMCVcIiBzdG9wLWNvbG9yPScje2lmIEBvcHRpb25zLnRvcENvbG9yIGlzbnQgbnVsbCB0aGVuIEBvcHRpb25zLmJvdHRvbUNvbG9yIGVsc2UgQG9wdGlvbnMuc3Ryb2tlQ29sb3J9Jy8+XG5cdFx0XHRcdCAgICAgICAgPHN0b3Agb2Zmc2V0PVwiMTAwJVwiIHN0b3AtY29sb3I9JyN7aWYgQG9wdGlvbnMudG9wQ29sb3IgaXNudCBudWxsIHRoZW4gQG9wdGlvbnMudG9wQ29sb3IgZWxzZSBAb3B0aW9ucy5zdHJva2VDb2xvcn0nIHN0b3Atb3BhY2l0eT1cIjFcIiAvPlxuXHRcdFx0XHQgICAgPC9saW5lYXJHcmFkaWVudD5cblx0XHRcdFx0PC9kZWZzPlxuXHRcdFx0XHQ8Y2lyY2xlIGlkPScje0BjaXJjbGVJRH0nXG5cdFx0XHRcdFx0XHRmaWxsPSdub25lJ1xuXHRcdFx0XHRcdFx0c3Ryb2tlLWxpbmVjYXA9J3JvdW5kJ1xuXHRcdFx0XHRcdFx0c3Ryb2tlLXdpZHRoICAgICAgPSAnI3tAb3B0aW9ucy5zdHJva2VXaWR0aH0nXG5cdFx0XHRcdFx0XHRzdHJva2UtZGFzaGFycmF5ICA9ICcje0AucGF0aExlbmd0aH0nXG5cdFx0XHRcdFx0XHRzdHJva2UtZGFzaG9mZnNldCA9ICcwJ1xuXHRcdFx0XHRcdFx0c3Ryb2tlPVwidXJsKCMje0BncmFkaWVudElEfSlcIlxuXHRcdFx0XHRcdFx0c3Ryb2tlLXdpZHRoPVwiMTBcIlxuXHRcdFx0XHRcdFx0Y3ggPSAnI3tAb3B0aW9ucy5jaXJjbGVTaXplLzJ9J1xuXHRcdFx0XHRcdFx0Y3kgPSAnI3tAb3B0aW9ucy5jaXJjbGVTaXplLzJ9J1xuXHRcdFx0XHRcdFx0ciAgPSAnI3tAb3B0aW9ucy5jaXJjbGVTaXplLzJ9Jz5cblx0XHRcdDwvc3ZnPlwiXCJcIlxuXG5cdFx0c2VsZiA9IEBcblx0XHRVdGlscy5kb21Db21wbGV0ZSAtPlxuXHRcdFx0c2VsZi5wYXRoID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiMje3NlbGYuY2lyY2xlSUR9XCIpXG5cblx0XHRAcHJveHkgPSBuZXcgTGF5ZXJcblx0XHRcdG9wYWNpdHk6IDBcblx0XHRcdHZpc2libGU6IGZhbHNlXG5cblx0XHRAcHJveHkub24gRXZlbnRzLkFuaW1hdGlvbkVuZCwgKGFuaW1hdGlvbiwgbGF5ZXIpIC0+XG5cdFx0XHRzZWxmLm9uRmluaXNoZWQoKVxuXG5cdFx0QHByb3h5Lm9uICdjaGFuZ2U6eCcsIC0+XG5cblx0XHRcdG9mZnNldCA9IFV0aWxzLm1vZHVsYXRlKEAueCwgWzAsIDUwMF0sIFtzZWxmLnBhdGhMZW5ndGgsIDBdKVxuXG5cdFx0XHRzZWxmLnBhdGguc2V0QXR0cmlidXRlICdzdHJva2UtZGFzaG9mZnNldCcsIG9mZnNldFxuXG5cdFx0XHRpZiBzZWxmLm9wdGlvbnMuaGFzQ291bnRlciBpc250IG51bGxcblx0XHRcdFx0bnVtYmVyTm93ID0gVXRpbHMucm91bmQoc2VsZi5wcm94eS54IC8gNSlcblx0XHRcdFx0Y291bnRlci5odG1sID0gbnVtYmVyTm93XG5cblx0XHRVdGlscy5kb21Db21wbGV0ZSAtPlxuXHRcdFx0c2VsZi5wcm94eS54ID0gMC4xXG5cblx0Y2hhbmdlVG86ICh2YWx1ZSwgdGltZSkgLT5cblx0XHRpZiB0aW1lIGlzIHVuZGVmaW5lZFxuXHRcdFx0dGltZSA9IDJcblxuXHRcdGlmIEBvcHRpb25zLmhhc0NvdW50ZXIgaXMgdHJ1ZSBhbmQgQG9wdGlvbnMuaGFzTGluZWFyRWFzaW5nIGlzIG51bGwgIyBvdmVycmlkZSBkZWZhdWx0IFwiZWFzZS1pbi1vdXRcIiB3aGVuIGNvdW50ZXIgaXMgdXNlZFxuXHRcdFx0Y3VzdG9tQ3VydmUgPSBcImxpbmVhclwiXG5cdFx0ZWxzZVxuXHRcdFx0Y3VzdG9tQ3VydmUgPSBcImVhc2UtaW4tb3V0XCJcblxuXHRcdEBwcm94eS5hbmltYXRlXG5cdFx0XHRwcm9wZXJ0aWVzOlxuXHRcdFx0XHR4OiA1MDAgKiAodmFsdWUgLyAxMDApXG5cdFx0XHR0aW1lOiB0aW1lXG5cdFx0XHRjdXJ2ZTogY3VzdG9tQ3VydmVcblxuXG5cblx0XHRAY3VycmVudFZhbHVlID0gdmFsdWVcblxuXHRzdGFydEF0OiAodmFsdWUpIC0+XG5cdFx0QHByb3h5LmFuaW1hdGVcblx0XHRcdHByb3BlcnRpZXM6XG5cdFx0XHRcdHg6IDUwMCAqICh2YWx1ZSAvIDEwMClcblx0XHRcdHRpbWU6IDAuMDAxXG5cblx0XHRAY3VycmVudFZhbHVlID0gdmFsdWVcblxuXG5cblx0aGlkZTogLT5cblx0XHRALm9wYWNpdHkgPSAwXG5cblx0c2hvdzogLT5cblx0XHRALm9wYWNpdHkgPSAxXG5cblx0b25GaW5pc2hlZDogLT5cbiIsIiMgZG9jdW1lbnRhdGlvbjogaHR0cHM6Ly9kZXZlbG9wZXJzLmdvb2dsZS5jb20veW91dHViZS9pZnJhbWVfYXBpX3JlZmVyZW5jZVxuXG4jIHdpbGwgcmVzb2x2ZSB3aGVuIHdpbmRvdy5vbllvdVR1YmVJZnJhbWVBUElSZWFkeSBpcyBjYWxsZWRcbnlvdVR1YmVSZWFkeSA9IG5ldyBQcm9taXNlIChyZXNvbHZlLCByZWplY3QpIC0+XG4gICAgd2luZG93Lm9uWW91VHViZUlmcmFtZUFQSVJlYWR5ID0gLT4gcmVzb2x2ZSgpXG5cbiMgc3RhbmRhcmQgeW91dHViZSBpZnJhbWUgYXBpIGluaXRpYWxpemF0aW9uXG50YWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50ICdzY3JpcHQnXG50YWcuc3JjID0gJ2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2lmcmFtZV9hcGknXG4jIFRPRE86IHNjcmlwdCBhc3luYyBkZWZlcj9cbmZpcnN0U2NyaXB0VGFnID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NjcmlwdCcpWzBdXG5maXJzdFNjcmlwdFRhZy5wYXJlbnROb2RlLmluc2VydEJlZm9yZSB0YWcsIGZpcnN0U2NyaXB0VGFnXG5cbmNsYXNzIGV4cG9ydHMuWW91VHViZVBsYXllciBleHRlbmRzIExheWVyXG5cbiAgICAjIGV2ZW50cywgc2VlIGh0dHBzOi8vZGV2ZWxvcGVycy5nb29nbGUuY29tL3lvdXR1YmUvaWZyYW1lX2FwaV9yZWZlcmVuY2UjRXZlbnRzXG4gICAgQEV2ZW50czpcbiAgICAgICAgTG9hZGVkOiAneXQtbG9hZGVkJyAjIG9jY3VycyB3aGVuIHZpZGVvIGlzIHF1ZXVlZCBhbmQgcmVhZHkgdG8gcGxheS4gd2lsbCBwcm92aWRlIHRoZSBwbGF5ZXIgYXMgcGFyYW1ldGVyLlxuICAgICAgICBSZWFkeTogJ3l0LXJlYWR5J1xuICAgICAgICBTdGF0ZUNoYW5nZTogJ3l0LXN0YXRlQ2hhbmdlJ1xuICAgICAgICBQbGF5YmFja1F1YWxpdHlDaGFuZ2U6ICd5dC1wbGF5YmFja1F1YWxpdHlDaGFuZ2UnXG4gICAgICAgIFBsYXliYWNrUmF0ZUNoYW5nZTogJ3l0LXBsYXliYWNrUmF0ZUNoYW5nZSdcbiAgICAgICAgRXJyb3I6ICd5dC1lcnJvcidcbiAgICAgICAgQXBpQ2hhbmdlOiAneXQtYXBpQ2hhbmdlJ1xuXG4gICAgIyBvcHRpb25zOiB7IHZpZGVvLCBwbGF5ZXJWYXJzIH1cbiAgICAjIGZvciBwbGF5ZXJWYXJzLCBzZWUgaHR0cHM6Ly9kZXZlbG9wZXJzLmdvb2dsZS5jb20veW91dHViZS9wbGF5ZXJfcGFyYW1ldGVyc1xuICAgIGNvbnN0cnVjdG9yOiAob3B0aW9ucz17fSkgLT5cblxuICAgICAgICAjIHRoaXMgZGl2IHdpbGwgYmUgcmVwbGFjZWQgd2l0aCB5b3V0dWJlIGlmcmFtZVxuICAgICAgICBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50ICdkaXYnXG5cbiAgICAgICAgQF9wbGF5ZXJSZWFkeSA9IG5ldyBQcm9taXNlIChwbGF5ZXJSZXNvbHZlLCBwbGF5ZXJSZWplY3QpID0+XG5cbiAgICAgICAgICAgIHlvdVR1YmVSZWFkeS50aGVuID0+XG5cbiAgICAgICAgICAgICAgICAjIHBsYXllciBpcyBvbmx5IGFjY2Vzc2libGUgb24gcmVhZHkgZXZlbnRcbiAgICAgICAgICAgICAgICBAX3BsYXllciA9IG5ldyBZVC5QbGF5ZXIoZGl2LFxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogQHdpZHRoXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogQGhlaWdodFxuICAgICAgICAgICAgICAgICAgICBwbGF5ZXJWYXJzOiBvcHRpb25zLnBsYXllclZhcnNcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRzOlxuICAgICAgICAgICAgICAgICAgICAgICAgJ29uUmVhZHknOiAoZXZlbnQpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxheWVyUmVzb2x2ZSBldmVudC50YXJnZXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBAZW1pdCBZb3VUdWJlUGxheWVyLkV2ZW50cy5SZWFkeSwgZXZlbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICdvblN0YXRlQ2hhbmdlJzogKGV2ZW50KSA9PiBAZW1pdCBZb3VUdWJlUGxheWVyLkV2ZW50cy5TdGF0ZUNoYW5nZSwgZXZlbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICdvblBsYXliYWNrUXVhbGl0eUNoYW5nZSc6IChldmVudCkgPT4gQGVtaXQgWW91VHViZVBsYXllci5FdmVudHMuUGxheWJhY2tRdWFsaXR5Q2hhbmdlLCBldmVudFxuICAgICAgICAgICAgICAgICAgICAgICAgJ29uUGxheWJhY2tSYXRlQ2hhbmdlJzogKGV2ZW50KSA9PiBAZW1pdCBZb3VUdWJlUGxheWVyLkV2ZW50cy5QbGF5YmFja1JhdGVDaGFuZ2UsIGV2ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAnb25FcnJvcic6IChldmVudCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwbGF5ZXJSZWplY3QgZXZlbnQuZGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIEBlbWl0IFlvdVR1YmVQbGF5ZXIuRXZlbnRzLkVycm9yLCBldmVudFxuICAgICAgICAgICAgICAgICAgICAgICAgJ29uQXBpQ2hhbmdlJzogKGV2ZW50KSA9PiBAZW1pdCBZb3VUdWJlUGxheWVyLkV2ZW50cy5BcGlDaGFuZ2UsIGV2ZW50XG4gICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgICMgb24gc2l6ZSBjaGFuZ2Ugb2YgdGhlIGxheWVyLCByZXNpemUgdGhlIGlmcmFtZVxuICAgICAgICAgICAgICAgIEBvbiBcImNoYW5nZTp3aWR0aFwiLCAtPiBAX3BsYXllci53aWR0aCA9IEB3aWR0aFxuICAgICAgICAgICAgICAgIEBvbiBcImNoYW5nZTpoZWlnaHRcIiwgLT4gQF9wbGF5ZXIuaGVpZ2h0ID0gQGhlaWdodFxuXG4gICAgICAgICMgY2FsbGluZyBzdXBlciBjYXVzZXMgQGRlZmluZSBwcm9wZXJ0aWVzIGJlaW5nIGFzc2lnbmVkXG4gICAgICAgIHN1cGVyIG9wdGlvbnNcblxuICAgICAgICBAX2VsZW1lbnQuYXBwZW5kQ2hpbGQgZGl2XG5cbiAgICBAZGVmaW5lIFwidmlkZW9cIixcbiAgICAgICAgZ2V0OiAtPiBAX3ZpZGVvXG4gICAgICAgIHNldDogKHZpZGVvKSAtPlxuICAgICAgICAgICAgQF92aWRlbyA9IHZpZGVvXG4gICAgICAgICAgICBAX3BsYXllclJlYWR5LnRoZW4gPT5cbiAgICAgICAgICAgICAgICBAX3BsYXllci5jdWVWaWRlb0J5SWQgdmlkZW9cbiAgICAgICAgICAgICAgICBAX3BsYXllci5wbGF5VmlkZW8oKSBpZiBAcGxheWVyVmFycz8uYXV0b3BsYXlcbiAgICAgICAgICAgICAgICBAZW1pdCBZb3VUdWJlUGxheWVyLkV2ZW50cy5Mb2FkZWQsIEBfcGxheWVyXG5cbiAgICBAZGVmaW5lIFwicGxheWVyVmFyc1wiLFxuICAgICAgICBnZXQ6IC0+IEBfcGxheWVyVmFyc1xuICAgICAgICBzZXQ6ICh2YWx1ZSkgLT4gQF9wbGF5ZXJWYXJzID0gdmFsdWUiLCIjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuIyBDcmVhdGVkIGJ5IEpvcmRhbiBSb2JlcnQgRG9ic29uIG9uIDA1IE9jdG9iZXIgMjAxNVxuIyBcbiMgVXNlIHRvIGFkZCBmb250IGZpbGVzIGFuZCByZWZlcmVuY2UgdGhlbSBpbiB5b3VyIENTUyBzdHlsZSBzZXR0aW5ncy5cbiNcbiMgVG8gR2V0IFN0YXJ0ZWQuLi5cbiNcbiMgMS4gUGxhY2UgdGhlIEZvbnRGYWNlLmNvZmZlZSBmaWxlIGluIEZyYW1lciBTdHVkaW8gbW9kdWxlcyBkaXJlY3RvcnlcbiNcbiMgMi4gSW4geW91ciBwcm9qZWN0IGluY2x1ZGU6XG4jICAgICB7Rm9udEZhY2V9ID0gcmVxdWlyZSBcIkZvbnRGYWNlXCJcbiNcbiMgMy4gVG8gYWRkIGEgZm9udCBmYWNlOiBcbiMgICAgIGdvdGhhbSA9IG5ldyBGb250RmFjZSBuYW1lOiBcIkdvdGhhbVwiLCBmaWxlOiBcIkdvdGhhbS50dGZcIlxuIyBcbiMgNC4gSXQgY2hlY2tzIHRoYXQgdGhlIGZvbnQgd2FzIGxvYWRlZC4gRXJyb3JzIGNhbiBiZSBzdXBwcmVzc2VkIGxpa2Ugc28uLi5cbiMgICAgZ290aGFtID0gbmV3IEZvbnRGYWNlIG5hbWU6IFwiR290aGFtXCIsIGZpbGU6IFwiR290aGFtLnR0ZlwiLCBoaWRlRXJyb3JzOiB0cnVlIFxuI1xuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblxuY2xhc3MgZXhwb3J0cy5Gb250RmFjZVxuXG5cdFRFU1QgPVxuXHRcdGZhY2U6IFwibW9ub3NwYWNlXCJcblx0XHR0ZXh0OiBcImZvb1wiXG5cdFx0dGltZTogLjAxXG5cdFx0bWF4TG9hZEF0dGVtcHRzOiA1MFxuXHRcdGhpZGVFcnJvck1lc3NhZ2VzOiB0cnVlXG5cdFx0XG5cdFRFU1Quc3R5bGUgPSBcblx0XHR3aWR0aDogXCJhdXRvXCJcblx0XHRmb250U2l6ZTogXCIxNTBweFwiXG5cdFx0Zm9udEZhbWlseTogVEVTVC5mYWNlXG5cdFx0XG5cdFRFU1QubGF5ZXIgPSBuZXcgTGF5ZXJcblx0XHRuYW1lOlwiRm9udEZhY2UgVGVzdGVyXCJcblx0XHR3aWR0aDogMFxuXHRcdGhlaWdodDogMVxuXHRcdG1heFg6IC0oU2NyZWVuLndpZHRoKVxuXHRcdHZpc2libGU6IGZhbHNlXG5cdFx0aHRtbDogVEVTVC50ZXh0XG5cdFx0c3R5bGU6IFRFU1Quc3R5bGVcblx0XHRcblx0XG5cdCMgU0VUVVAgRk9SIEVWRVJZIElOU1RBTkNFXG5cdGNvbnN0cnVjdG9yOiAob3B0aW9ucykgLT5cblx0XG5cdFx0QG5hbWUgPSBAZmlsZSA9IEB0ZXN0TGF5ZXIgPSBAaXNMb2FkZWQgPSBAbG9hZEZhaWxlZCA9IEBsb2FkQXR0ZW1wdHMgPSBAb3JpZ2luYWxTaXplID0gQGhpZGVFcnJvcnMgPSAgbnVsbFxuXHRcdFxuXHRcdGlmIG9wdGlvbnM/XG5cdFx0XHRAbmFtZSA9IG9wdGlvbnMubmFtZSB8fCBudWxsXG5cdFx0XHRAZmlsZSA9IG9wdGlvbnMuZmlsZSB8fCBudWxsXG5cdFx0XG5cdFx0cmV0dXJuIG1pc3NpbmdBcmd1bWVudEVycm9yKCkgdW5sZXNzIEBuYW1lPyBhbmQgQGZpbGU/XG5cdFx0XG5cdFx0QHRlc3RMYXllciAgICAgICAgID0gVEVTVC5sYXllci5jb3B5KClcblx0XHRAdGVzdExheWVyLnN0eWxlICAgPSBURVNULnN0eWxlXG5cdFx0QHRlc3RMYXllci5tYXhYICAgID0gLShTY3JlZW4ud2lkdGgpXG5cdFx0QHRlc3RMYXllci52aXNpYmxlID0gdHJ1ZVxuXHRcdFxuXHRcdEBpc0xvYWRlZCAgICAgPSBmYWxzZVxuXHRcdEBsb2FkRmFpbGVkICAgPSBmYWxzZVxuXHRcdEBsb2FkQXR0ZW1wdHMgPSAwXG5cdFx0QGhpZGVFcnJvcnMgICA9IG9wdGlvbnMuaGlkZUVycm9yc1xuXG5cdFx0cmV0dXJuIGFkZEZvbnRGYWNlIEBuYW1lLCBAZmlsZSwgQFxuXHRcdFxuXHQjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblx0IyBQcml2YXRlIEhlbHBlciBNZXRob2RzICMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cdFx0XG5cdGFkZEZvbnRGYWNlID0gKG5hbWUsIGZpbGUsIG9iamVjdCkgLT5cblx0XHQjIENyZWF0ZSBvdXIgRWxlbWVudCAmIE5vZGVcblx0XHRzdHlsZVRhZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQgJ3N0eWxlJ1xuXHRcdGZhY2VDU1MgID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUgXCJAZm9udC1mYWNlIHsgZm9udC1mYW1pbHk6ICcje25hbWV9Jzsgc3JjOiB1cmwoJyN7ZmlsZX0nKSBmb3JtYXQoJ3RydWV0eXBlJyk7IH1cIlxuXHRcdCMgQWRkIHRoZSBFbGVtZW50ICYgTm9kZSB0byB0aGUgZG9jdW1lbnRcblx0XHRzdHlsZVRhZy5hcHBlbmRDaGlsZCBmYWNlQ1NTXG5cdFx0ZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZCBzdHlsZVRhZ1xuXHRcdCMgVGVzdCBvdXQgdGhlIEZhc3QgdG8gc2VlIGlmIGl0IGNoYW5nZWRcblx0XHR0ZXN0TmV3RmFjZSBuYW1lLCBvYmplY3Rcblx0XHRcblx0IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cdFx0XG5cdHJlbW92ZVRlc3RMYXllciA9IChvYmplY3QpIC0+XG5cdFx0b2JqZWN0LnRlc3RMYXllci5kZXN0cm95KClcblx0XHRvYmplY3QudGVzdExheWVyID0gbnVsbFxuXHRcdFxuXHQjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblx0XHRcblx0dGVzdE5ld0ZhY2UgPSAobmFtZSwgb2JqZWN0KSAtPlxuXHRcdFxuXHRcdGluaXRpYWxXaWR0aCA9IG9iamVjdC50ZXN0TGF5ZXIuX2VsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGhcblx0XHRcblx0XHQjIENoZWNrIHRvIHNlZSBpZiBpdCdzIHJlYWR5IHlldFxuXHRcdGlmIGluaXRpYWxXaWR0aCBpcyAwXG5cdFx0XHRpZiBvYmplY3QuaGlkZUVycm9ycyBpcyBmYWxzZSBvciBURVNULmhpZGVFcnJvck1lc3NhZ2VzIGlzIGZhbHNlXG5cdFx0XHRcdHByaW50IFwiTG9hZCB0ZXN0aW5nIGZhaWxlZC4gQXR0ZW1wdGluZyBhZ2Fpbi5cIlxuXHRcdFx0cmV0dXJuIFV0aWxzLmRlbGF5IFRFU1QudGltZSwgLT4gdGVzdE5ld0ZhY2UgbmFtZSwgb2JqZWN0XG5cdFx0XG5cdFx0b2JqZWN0LmxvYWRBdHRlbXB0cysrXG5cdFx0XG5cdFx0aWYgb2JqZWN0Lm9yaWdpbmFsU2l6ZSBpcyBudWxsXG5cdFx0XHRvYmplY3Qub3JpZ2luYWxTaXplID0gaW5pdGlhbFdpZHRoXG5cdFx0XHRvYmplY3QudGVzdExheWVyLnN0eWxlID0gZm9udEZhbWlseTogXCIje25hbWV9LCAje1RFU1QuZmFjZX1cIlxuXHRcdFxuXHRcdHdpZHRoVXBkYXRlID0gb2JqZWN0LnRlc3RMYXllci5fZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aFxuXG5cdFx0aWYgb2JqZWN0Lm9yaWdpbmFsU2l6ZSBpcyB3aWR0aFVwZGF0ZVxuXHRcdFx0IyBJZiB3ZSBjYW4gYXR0ZW1wdCB0byBjaGVjayBhZ2Fpbi4uLiBEbyBpdFxuXHRcdFx0aWYgb2JqZWN0LmxvYWRBdHRlbXB0cyA8IFRFU1QubWF4TG9hZEF0dGVtcHRzXG5cdFx0XHRcdHJldHVybiBVdGlscy5kZWxheSBURVNULnRpbWUsIC0+IHRlc3ROZXdGYWNlIG5hbWUsIG9iamVjdFxuXHRcdFx0XHRcblx0XHRcdHByaW50IFwi4pqg77iPIEZhaWxlZCBsb2FkaW5nIEZvbnRGYWNlOiAje25hbWV9XCIgdW5sZXNzIG9iamVjdC5oaWRlRXJyb3JzXG5cdFx0XHRvYmplY3QuaXNMb2FkZWQgICA9IGZhbHNlXG5cdFx0XHRvYmplY3QubG9hZEZhaWxlZCA9IHRydWVcblx0XHRcdGxvYWRUZXN0aW5nRmlsZUVycm9yIG9iamVjdCB1bmxlc3Mgb2JqZWN0LmhpZGVFcnJvcnNcblx0XHRcdHJldHVyblxuXHRcdFx0XG5cdFx0ZWxzZVxuXHRcdFx0cHJpbnQgXCJMT0FERUQ6ICN7bmFtZX1cIiB1bmxlc3Mgb2JqZWN0LmhpZGVFcnJvcnMgaXMgZmFsc2Ugb3IgVEVTVC5oaWRlRXJyb3JNZXNzYWdlc1xuXHRcdFx0b2JqZWN0LmlzTG9hZGVkICAgPSB0cnVlXG5cdFx0XHRvYmplY3QubG9hZEZhaWxlZCA9IGZhbHNlXG5cblx0XHRyZW1vdmVUZXN0TGF5ZXIgb2JqZWN0XG5cdFx0cmV0dXJuIG5hbWVcblxuXHQjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblx0IyBFcnJvciBIYW5kbGVyIE1ldGhvZHMgIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cblx0bWlzc2luZ0FyZ3VtZW50RXJyb3IgPSAtPlxuXHRcdGVycm9yIG51bGxcblx0XHRjb25zb2xlLmVycm9yIFwiXCJcIlxuXHRcdFx0RXJyb3I6IFlvdSBtdXN0IHBhc3MgbmFtZSAmIGZpbGUgcHJvcGVyaXRlcyB3aGVuIGNyZWF0aW5nIGEgbmV3IEZvbnRGYWNlLiBcXG5cblx0XHRcdEV4YW1wbGU6IG15RmFjZSA9IG5ldyBGb250RmFjZSBuYW1lOlxcXCJHb3RoYW1cXFwiLCBmaWxlOlxcXCJnb3RoYW0udHRmXFxcIiBcXG5cIlwiXCJcblx0XHRcdFxuXHRsb2FkVGVzdGluZ0ZpbGVFcnJvciA9IChvYmplY3QpIC0+XG5cdFx0ZXJyb3IgbnVsbFxuXHRcdGNvbnNvbGUuZXJyb3IgXCJcIlwiXG5cdFx0XHRFcnJvcjogQ291bGRuJ3QgZGV0ZWN0IHRoZSBmb250OiBcXFwiI3tvYmplY3QubmFtZX1cXFwiIGFuZCBmaWxlOiBcXFwiI3tvYmplY3QuZmlsZX1cXFwiIHdhcyBsb2FkZWQuICBcXG5cblx0XHRcdEVpdGhlciB0aGUgZmlsZSBjb3VsZG4ndCBiZSBmb3VuZCBvciB5b3VyIGJyb3dzZXIgZG9lc24ndCBzdXBwb3J0IHRoZSBmaWxlIHR5cGUgdGhhdCB3YXMgcHJvdmlkZWQuIFxcblxuXHRcdFx0U3VwcHJlc3MgdGhpcyBtZXNzYWdlIGJ5IGFkZGluZyBcXFwiaGlkZUVycm9yczogdHJ1ZVxcXCIgd2hlbiBjcmVhdGluZyBhIG5ldyBGb250RmFjZS4gXFxuXCJcIlwiXG4iLCIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQVVBQTtBRG9CTSxPQUFPLENBQUM7QUFFYixNQUFBOztFQUFBLElBQUEsR0FDQztJQUFBLElBQUEsRUFBTSxXQUFOO0lBQ0EsSUFBQSxFQUFNLEtBRE47SUFFQSxJQUFBLEVBQU0sR0FGTjtJQUdBLGVBQUEsRUFBaUIsRUFIakI7SUFJQSxpQkFBQSxFQUFtQixJQUpuQjs7O0VBTUQsSUFBSSxDQUFDLEtBQUwsR0FDQztJQUFBLEtBQUEsRUFBTyxNQUFQO0lBQ0EsUUFBQSxFQUFVLE9BRFY7SUFFQSxVQUFBLEVBQVksSUFBSSxDQUFDLElBRmpCOzs7RUFJRCxJQUFJLENBQUMsS0FBTCxHQUFpQixJQUFBLEtBQUEsQ0FDaEI7SUFBQSxJQUFBLEVBQUssaUJBQUw7SUFDQSxLQUFBLEVBQU8sQ0FEUDtJQUVBLE1BQUEsRUFBUSxDQUZSO0lBR0EsSUFBQSxFQUFNLENBQUUsTUFBTSxDQUFDLEtBSGY7SUFJQSxPQUFBLEVBQVMsS0FKVDtJQUtBLElBQUEsRUFBTSxJQUFJLENBQUMsSUFMWDtJQU1BLEtBQUEsRUFBTyxJQUFJLENBQUMsS0FOWjtHQURnQjs7RUFXSixrQkFBQyxPQUFEO0lBRVosSUFBQyxDQUFBLElBQUQsR0FBUSxJQUFDLENBQUEsSUFBRCxHQUFRLElBQUMsQ0FBQSxTQUFELEdBQWEsSUFBQyxDQUFBLFFBQUQsR0FBWSxJQUFDLENBQUEsVUFBRCxHQUFjLElBQUMsQ0FBQSxZQUFELEdBQWdCLElBQUMsQ0FBQSxZQUFELEdBQWdCLElBQUMsQ0FBQSxVQUFELEdBQWU7SUFFdEcsSUFBRyxlQUFIO01BQ0MsSUFBQyxDQUFBLElBQUQsR0FBUSxPQUFPLENBQUMsSUFBUixJQUFnQjtNQUN4QixJQUFDLENBQUEsSUFBRCxHQUFRLE9BQU8sQ0FBQyxJQUFSLElBQWdCLEtBRnpCOztJQUlBLElBQUEsQ0FBQSxDQUFxQyxtQkFBQSxJQUFXLG1CQUFoRCxDQUFBO0FBQUEsYUFBTyxvQkFBQSxDQUFBLEVBQVA7O0lBRUEsSUFBQyxDQUFBLFNBQUQsR0FBcUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFYLENBQUE7SUFDckIsSUFBQyxDQUFBLFNBQVMsQ0FBQyxLQUFYLEdBQXFCLElBQUksQ0FBQztJQUMxQixJQUFDLENBQUEsU0FBUyxDQUFDLElBQVgsR0FBcUIsQ0FBRSxNQUFNLENBQUM7SUFDOUIsSUFBQyxDQUFBLFNBQVMsQ0FBQyxPQUFYLEdBQXFCO0lBRXJCLElBQUMsQ0FBQSxRQUFELEdBQWdCO0lBQ2hCLElBQUMsQ0FBQSxVQUFELEdBQWdCO0lBQ2hCLElBQUMsQ0FBQSxZQUFELEdBQWdCO0lBQ2hCLElBQUMsQ0FBQSxVQUFELEdBQWdCLE9BQU8sQ0FBQztBQUV4QixXQUFPLFdBQUEsQ0FBWSxJQUFDLENBQUEsSUFBYixFQUFtQixJQUFDLENBQUEsSUFBcEIsRUFBMEIsSUFBMUI7RUFwQks7O0VBeUJiLFdBQUEsR0FBYyxTQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsTUFBYjtBQUViLFFBQUE7SUFBQSxRQUFBLEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsT0FBdkI7SUFDWCxPQUFBLEdBQVcsUUFBUSxDQUFDLGNBQVQsQ0FBd0IsNkJBQUEsR0FBOEIsSUFBOUIsR0FBbUMsZUFBbkMsR0FBa0QsSUFBbEQsR0FBdUQsMEJBQS9FO0lBRVgsUUFBUSxDQUFDLFdBQVQsQ0FBcUIsT0FBckI7SUFDQSxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQWQsQ0FBMEIsUUFBMUI7V0FFQSxXQUFBLENBQVksSUFBWixFQUFrQixNQUFsQjtFQVJhOztFQVlkLGVBQUEsR0FBa0IsU0FBQyxNQUFEO0lBQ2pCLE1BQU0sQ0FBQyxTQUFTLENBQUMsT0FBakIsQ0FBQTtXQUNBLE1BQU0sQ0FBQyxTQUFQLEdBQW1CO0VBRkY7O0VBTWxCLFdBQUEsR0FBYyxTQUFDLElBQUQsRUFBTyxNQUFQO0FBRWIsUUFBQTtJQUFBLFlBQUEsR0FBZSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBMUIsQ0FBQSxDQUFpRCxDQUFDO0lBR2pFLElBQUcsWUFBQSxLQUFnQixDQUFuQjtNQUNDLElBQUcsTUFBTSxDQUFDLFVBQVAsS0FBcUIsS0FBckIsSUFBOEIsSUFBSSxDQUFDLGlCQUFMLEtBQTBCLEtBQTNEO1FBQ0MsS0FBQSxDQUFNLHdDQUFOLEVBREQ7O0FBRUEsYUFBTyxLQUFLLENBQUMsS0FBTixDQUFZLElBQUksQ0FBQyxJQUFqQixFQUF1QixTQUFBO2VBQUcsV0FBQSxDQUFZLElBQVosRUFBa0IsTUFBbEI7TUFBSCxDQUF2QixFQUhSOztJQUtBLE1BQU0sQ0FBQyxZQUFQO0lBRUEsSUFBRyxNQUFNLENBQUMsWUFBUCxLQUF1QixJQUExQjtNQUNDLE1BQU0sQ0FBQyxZQUFQLEdBQXNCO01BQ3RCLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBakIsR0FBeUI7UUFBQSxVQUFBLEVBQWUsSUFBRCxHQUFNLElBQU4sR0FBVSxJQUFJLENBQUMsSUFBN0I7UUFGMUI7O0lBSUEsV0FBQSxHQUFjLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUExQixDQUFBLENBQWlELENBQUM7SUFFaEUsSUFBRyxNQUFNLENBQUMsWUFBUCxLQUF1QixXQUExQjtNQUVDLElBQUcsTUFBTSxDQUFDLFlBQVAsR0FBc0IsSUFBSSxDQUFDLGVBQTlCO0FBQ0MsZUFBTyxLQUFLLENBQUMsS0FBTixDQUFZLElBQUksQ0FBQyxJQUFqQixFQUF1QixTQUFBO2lCQUFHLFdBQUEsQ0FBWSxJQUFaLEVBQWtCLE1BQWxCO1FBQUgsQ0FBdkIsRUFEUjs7TUFHQSxJQUFBLENBQW1ELE1BQU0sQ0FBQyxVQUExRDtRQUFBLEtBQUEsQ0FBTSw4QkFBQSxHQUErQixJQUFyQyxFQUFBOztNQUNBLE1BQU0sQ0FBQyxRQUFQLEdBQW9CO01BQ3BCLE1BQU0sQ0FBQyxVQUFQLEdBQW9CO01BQ3BCLElBQUEsQ0FBbUMsTUFBTSxDQUFDLFVBQTFDO1FBQUEsb0JBQUEsQ0FBcUIsTUFBckIsRUFBQTs7QUFDQSxhQVREO0tBQUEsTUFBQTtNQVlDLElBQUEsQ0FBQSxDQUErQixNQUFNLENBQUMsVUFBUCxLQUFxQixLQUFyQixJQUE4QixJQUFJLENBQUMsaUJBQWxFLENBQUE7UUFBQSxLQUFBLENBQU0sVUFBQSxHQUFXLElBQWpCLEVBQUE7O01BQ0EsTUFBTSxDQUFDLFFBQVAsR0FBb0I7TUFDcEIsTUFBTSxDQUFDLFVBQVAsR0FBb0IsTUFkckI7O0lBZ0JBLGVBQUEsQ0FBZ0IsTUFBaEI7QUFDQSxXQUFPO0VBbkNNOztFQXdDZCxvQkFBQSxHQUF1QixTQUFBO0lBQ3RCLEtBQUEsQ0FBTSxJQUFOO1dBQ0EsT0FBTyxDQUFDLEtBQVIsQ0FBYyxzSkFBZDtFQUZzQjs7RUFNdkIsb0JBQUEsR0FBdUIsU0FBQyxNQUFEO0lBQ3RCLEtBQUEsQ0FBTSxJQUFOO1dBQ0EsT0FBTyxDQUFDLEtBQVIsQ0FBYyxxQ0FBQSxHQUN3QixNQUFNLENBQUMsSUFEL0IsR0FDb0MsaUJBRHBDLEdBQ3FELE1BQU0sQ0FBQyxJQUQ1RCxHQUNpRSxrTkFEL0U7RUFGc0I7Ozs7Ozs7O0FEbkl4QixJQUFBLGlDQUFBO0VBQUE7OztBQUFBLFlBQUEsR0FBbUIsSUFBQSxPQUFBLENBQVEsU0FBQyxPQUFELEVBQVUsTUFBVjtTQUN2QixNQUFNLENBQUMsdUJBQVAsR0FBaUMsU0FBQTtXQUFHLE9BQUEsQ0FBQTtFQUFIO0FBRFYsQ0FBUjs7QUFJbkIsR0FBQSxHQUFNLFFBQVEsQ0FBQyxhQUFULENBQXVCLFFBQXZCOztBQUNOLEdBQUcsQ0FBQyxHQUFKLEdBQVU7O0FBRVYsY0FBQSxHQUFpQixRQUFRLENBQUMsb0JBQVQsQ0FBOEIsUUFBOUIsQ0FBd0MsQ0FBQSxDQUFBOztBQUN6RCxjQUFjLENBQUMsVUFBVSxDQUFDLFlBQTFCLENBQXVDLEdBQXZDLEVBQTRDLGNBQTVDOztBQUVNLE9BQU8sQ0FBQzs7O0VBR1YsYUFBQyxDQUFBLE1BQUQsR0FDSTtJQUFBLE1BQUEsRUFBUSxXQUFSO0lBQ0EsS0FBQSxFQUFPLFVBRFA7SUFFQSxXQUFBLEVBQWEsZ0JBRmI7SUFHQSxxQkFBQSxFQUF1QiwwQkFIdkI7SUFJQSxrQkFBQSxFQUFvQix1QkFKcEI7SUFLQSxLQUFBLEVBQU8sVUFMUDtJQU1BLFNBQUEsRUFBVyxjQU5YOzs7RUFVUyx1QkFBQyxPQUFEO0FBR1QsUUFBQTs7TUFIVSxVQUFROztJQUdsQixHQUFBLEdBQU0sUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7SUFFTixJQUFDLENBQUEsWUFBRCxHQUFvQixJQUFBLE9BQUEsQ0FBUSxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsYUFBRCxFQUFnQixZQUFoQjtlQUV4QixZQUFZLENBQUMsSUFBYixDQUFrQixTQUFBO1VBR2QsS0FBQyxDQUFBLE9BQUQsR0FBZSxJQUFBLEVBQUUsQ0FBQyxNQUFILENBQVUsR0FBVixFQUNYO1lBQUEsS0FBQSxFQUFPLEtBQUMsQ0FBQSxLQUFSO1lBQ0EsTUFBQSxFQUFRLEtBQUMsQ0FBQSxNQURUO1lBRUEsVUFBQSxFQUFZLE9BQU8sQ0FBQyxVQUZwQjtZQUdBLE1BQUEsRUFDSTtjQUFBLFNBQUEsRUFBVyxTQUFDLEtBQUQ7Z0JBQ1AsYUFBQSxDQUFjLEtBQUssQ0FBQyxNQUFwQjt1QkFDQSxLQUFDLENBQUEsSUFBRCxDQUFNLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBM0IsRUFBa0MsS0FBbEM7Y0FGTyxDQUFYO2NBR0EsZUFBQSxFQUFpQixTQUFDLEtBQUQ7dUJBQVcsS0FBQyxDQUFBLElBQUQsQ0FBTSxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQTNCLEVBQXdDLEtBQXhDO2NBQVgsQ0FIakI7Y0FJQSx5QkFBQSxFQUEyQixTQUFDLEtBQUQ7dUJBQVcsS0FBQyxDQUFBLElBQUQsQ0FBTSxhQUFhLENBQUMsTUFBTSxDQUFDLHFCQUEzQixFQUFrRCxLQUFsRDtjQUFYLENBSjNCO2NBS0Esc0JBQUEsRUFBd0IsU0FBQyxLQUFEO3VCQUFXLEtBQUMsQ0FBQSxJQUFELENBQU0sYUFBYSxDQUFDLE1BQU0sQ0FBQyxrQkFBM0IsRUFBK0MsS0FBL0M7Y0FBWCxDQUx4QjtjQU1BLFNBQUEsRUFBVyxTQUFDLEtBQUQ7Z0JBQ1AsWUFBQSxDQUFhLEtBQUssQ0FBQyxJQUFuQjt1QkFDQSxLQUFDLENBQUEsSUFBRCxDQUFNLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBM0IsRUFBa0MsS0FBbEM7Y0FGTyxDQU5YO2NBU0EsYUFBQSxFQUFlLFNBQUMsS0FBRDt1QkFBVyxLQUFDLENBQUEsSUFBRCxDQUFNLGFBQWEsQ0FBQyxNQUFNLENBQUMsU0FBM0IsRUFBc0MsS0FBdEM7Y0FBWCxDQVRmO2FBSko7V0FEVztVQWtCZixLQUFDLENBQUEsRUFBRCxDQUFJLGNBQUosRUFBb0IsU0FBQTttQkFBRyxJQUFDLENBQUEsT0FBTyxDQUFDLEtBQVQsR0FBaUIsSUFBQyxDQUFBO1VBQXJCLENBQXBCO2lCQUNBLEtBQUMsQ0FBQSxFQUFELENBQUksZUFBSixFQUFxQixTQUFBO21CQUFHLElBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxHQUFrQixJQUFDLENBQUE7VUFBdEIsQ0FBckI7UUF0QmMsQ0FBbEI7TUFGd0I7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQVI7SUEyQnBCLCtDQUFNLE9BQU47SUFFQSxJQUFDLENBQUEsUUFBUSxDQUFDLFdBQVYsQ0FBc0IsR0FBdEI7RUFsQ1M7O0VBb0NiLGFBQUMsQ0FBQSxNQUFELENBQVEsT0FBUixFQUNJO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUE7SUFBSixDQUFMO0lBQ0EsR0FBQSxFQUFLLFNBQUMsS0FBRDtNQUNELElBQUMsQ0FBQSxNQUFELEdBQVU7YUFDVixJQUFDLENBQUEsWUFBWSxDQUFDLElBQWQsQ0FBbUIsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO0FBQ2YsY0FBQTtVQUFBLEtBQUMsQ0FBQSxPQUFPLENBQUMsWUFBVCxDQUFzQixLQUF0QjtVQUNBLDBDQUFtQyxDQUFFLGlCQUFyQztZQUFBLEtBQUMsQ0FBQSxPQUFPLENBQUMsU0FBVCxDQUFBLEVBQUE7O2lCQUNBLEtBQUMsQ0FBQSxJQUFELENBQU0sYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUEzQixFQUFtQyxLQUFDLENBQUEsT0FBcEM7UUFIZTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBbkI7SUFGQyxDQURMO0dBREo7O0VBU0EsYUFBQyxDQUFBLE1BQUQsQ0FBUSxZQUFSLEVBQ0k7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQTtJQUFKLENBQUw7SUFDQSxHQUFBLEVBQUssU0FBQyxLQUFEO2FBQVcsSUFBQyxDQUFBLFdBQUQsR0FBZTtJQUExQixDQURMO0dBREo7Ozs7R0EzRGdDOzs7O0FEYnBDLElBQUE7OztBQUFNLE9BQU8sQ0FBQzs7O21CQUNiLFlBQUEsR0FBYzs7RUFFRCxnQkFBQyxPQUFEO0FBRVosUUFBQTtJQUZhLElBQUMsQ0FBQSw0QkFBRCxVQUFTOztVQUVkLENBQUMsYUFBYzs7O1dBQ2YsQ0FBQyxjQUFlOzs7V0FFaEIsQ0FBQyxjQUFlOzs7V0FDaEIsQ0FBQyxXQUFZOzs7V0FDYixDQUFDLGNBQWU7OztXQUVoQixDQUFDLGFBQWM7OztXQUNmLENBQUMsZUFBZ0I7OztXQUNqQixDQUFDLGtCQUFtQjs7O1dBQ3BCLENBQUMsa0JBQW1COztJQUU1QixJQUFDLENBQUEsT0FBTyxDQUFDLEtBQVQsR0FBaUI7SUFFakIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUFULEdBQW9CLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVixHQUF3QixJQUFDLENBQUEsT0FBTyxDQUFDO0lBRXBELHdDQUFNLElBQUMsQ0FBQSxPQUFQO0lBRUEsSUFBQyxDQUFDLGVBQUYsR0FBb0I7SUFDcEIsSUFBQyxDQUFDLE1BQUYsR0FBVyxJQUFDLENBQUEsT0FBTyxDQUFDO0lBQ3BCLElBQUMsQ0FBQyxLQUFGLEdBQVUsSUFBQyxDQUFBLE9BQU8sQ0FBQztJQUNuQixJQUFDLENBQUMsUUFBRixHQUFhLENBQUM7SUFHZCxJQUFDLENBQUMsVUFBRixHQUFlLElBQUksQ0FBQyxFQUFMLEdBQVUsSUFBQyxDQUFBLE9BQU8sQ0FBQztJQUVsQyxJQUFDLENBQUMsUUFBRixHQUFhLFFBQUEsR0FBVyxJQUFJLENBQUMsS0FBTCxDQUFXLElBQUksQ0FBQyxNQUFMLENBQUEsQ0FBQSxHQUFjLElBQXpCO0lBQ3hCLElBQUMsQ0FBQyxVQUFGLEdBQWUsUUFBQSxHQUFXLElBQUksQ0FBQyxLQUFMLENBQVcsSUFBSSxDQUFDLE1BQUwsQ0FBQSxDQUFBLEdBQWMsSUFBekI7SUFPMUIsSUFBRyxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsS0FBeUIsSUFBNUI7TUFDQyxPQUFBLEdBQWMsSUFBQSxLQUFBLENBQ2I7UUFBQSxNQUFBLEVBQVEsSUFBUjtRQUNBLElBQUEsRUFBTSxFQUROO1FBRUEsS0FBQSxFQUFPLElBQUMsQ0FBQyxLQUZUO1FBR0EsTUFBQSxFQUFRLElBQUMsQ0FBQyxNQUhWO1FBSUEsZUFBQSxFQUFpQixFQUpqQjtRQUtBLFFBQUEsRUFBVSxFQUxWO1FBTUEsS0FBQSxFQUFPLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFOaEI7T0FEYTtNQVNkLEtBQUEsR0FBUTtRQUNQLFNBQUEsRUFBVyxRQURKO1FBRVAsUUFBQSxFQUFhLElBQUMsQ0FBQSxPQUFPLENBQUMsZUFBVixHQUEwQixJQUYvQjtRQUdQLFVBQUEsRUFBZSxJQUFDLENBQUMsTUFBSCxHQUFVLElBSGpCO1FBSVAsVUFBQSxFQUFZLEtBSkw7UUFLUCxVQUFBLEVBQVksNkNBTEw7UUFNUCxTQUFBLEVBQVcsWUFOSjtRQU9QLE1BQUEsRUFBUSxJQUFDLENBQUMsTUFQSDs7TUFVUixPQUFPLENBQUMsS0FBUixHQUFnQjtNQUVoQixXQUFBLEdBQWM7TUFDZCxTQUFBLEdBQVk7TUFDWixjQUFBLEdBQWlCO01BRWpCLFNBQUEsR0FBWTtNQUNaLGNBQUEsR0FBaUIsU0FBQSxHQUFZLFlBM0I5Qjs7SUE4QkEsSUFBQyxDQUFDLElBQUYsR0FBUyxpQkFBQSxHQUNRLENBQUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULEdBQXFCLENBQXRCLENBRFIsR0FDZ0MsSUFEaEMsR0FDbUMsQ0FBQyxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsR0FBcUIsQ0FBdEIsQ0FEbkMsR0FDMkQsR0FEM0QsR0FDOEQsSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUR2RSxHQUMrRSxHQUQvRSxHQUNrRixJQUFDLENBQUEsT0FBTyxDQUFDLE9BRDNGLEdBQ21HLHlDQURuRyxHQUdtQixJQUFDLENBQUEsVUFIcEIsR0FHK0IsZ0RBSC9CLEdBSWdDLENBQUksSUFBQyxDQUFBLE9BQU8sQ0FBQyxRQUFULEtBQXVCLElBQTFCLEdBQW9DLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBN0MsR0FBOEQsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUF4RSxDQUpoQyxHQUlvSCxrREFKcEgsR0FLa0MsQ0FBSSxJQUFDLENBQUEsT0FBTyxDQUFDLFFBQVQsS0FBdUIsSUFBMUIsR0FBb0MsSUFBQyxDQUFBLE9BQU8sQ0FBQyxRQUE3QyxHQUEyRCxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQXJFLENBTGxDLEdBS21ILDBFQUxuSCxHQVFPLElBQUMsQ0FBQSxRQVJSLEdBUWlCLHdFQVJqQixHQVdrQixJQUFDLENBQUEsT0FBTyxDQUFDLFdBWDNCLEdBV3VDLDZCQVh2QyxHQVlrQixJQUFDLENBQUMsVUFacEIsR0FZK0Isa0RBWi9CLEdBY1UsSUFBQyxDQUFBLFVBZFgsR0Fjc0Isd0NBZHRCLEdBZ0JFLENBQUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUFULEdBQW9CLENBQXJCLENBaEJGLEdBZ0J5QixjQWhCekIsR0FpQkUsQ0FBQyxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsR0FBb0IsQ0FBckIsQ0FqQkYsR0FpQnlCLGNBakJ6QixHQWtCRSxDQUFDLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVCxHQUFvQixDQUFyQixDQWxCRixHQWtCeUI7SUFHbEMsSUFBQSxHQUFPO0lBQ1AsS0FBSyxDQUFDLFdBQU4sQ0FBa0IsU0FBQTthQUNqQixJQUFJLENBQUMsSUFBTCxHQUFZLFFBQVEsQ0FBQyxhQUFULENBQXVCLEdBQUEsR0FBSSxJQUFJLENBQUMsUUFBaEM7SUFESyxDQUFsQjtJQUdBLElBQUMsQ0FBQSxLQUFELEdBQWEsSUFBQSxLQUFBLENBQ1o7TUFBQSxPQUFBLEVBQVMsQ0FBVDtNQUNBLE9BQUEsRUFBUyxLQURUO0tBRFk7SUFJYixJQUFDLENBQUEsS0FBSyxDQUFDLEVBQVAsQ0FBVSxNQUFNLENBQUMsWUFBakIsRUFBK0IsU0FBQyxTQUFELEVBQVksS0FBWjthQUM5QixJQUFJLENBQUMsVUFBTCxDQUFBO0lBRDhCLENBQS9CO0lBR0EsSUFBQyxDQUFBLEtBQUssQ0FBQyxFQUFQLENBQVUsVUFBVixFQUFzQixTQUFBO0FBRXJCLFVBQUE7TUFBQSxNQUFBLEdBQVMsS0FBSyxDQUFDLFFBQU4sQ0FBZSxJQUFDLENBQUMsQ0FBakIsRUFBb0IsQ0FBQyxDQUFELEVBQUksR0FBSixDQUFwQixFQUE4QixDQUFDLElBQUksQ0FBQyxVQUFOLEVBQWtCLENBQWxCLENBQTlCO01BRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFWLENBQXVCLG1CQUF2QixFQUE0QyxNQUE1QztNQUVBLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFiLEtBQTZCLElBQWhDO1FBQ0MsU0FBQSxHQUFZLEtBQUssQ0FBQyxLQUFOLENBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFYLEdBQWUsQ0FBM0I7ZUFDWixPQUFPLENBQUMsSUFBUixHQUFlLFVBRmhCOztJQU5xQixDQUF0QjtJQVVBLEtBQUssQ0FBQyxXQUFOLENBQWtCLFNBQUE7YUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFYLEdBQWU7SUFERSxDQUFsQjtFQTVHWTs7bUJBK0diLFFBQUEsR0FBVSxTQUFDLEtBQUQsRUFBUSxJQUFSO0FBQ1QsUUFBQTtJQUFBLElBQUcsSUFBQSxLQUFRLE1BQVg7TUFDQyxJQUFBLEdBQU8sRUFEUjs7SUFHQSxJQUFHLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVCxLQUF1QixJQUF2QixJQUFnQyxJQUFDLENBQUEsT0FBTyxDQUFDLGVBQVQsS0FBNEIsSUFBL0Q7TUFDQyxXQUFBLEdBQWMsU0FEZjtLQUFBLE1BQUE7TUFHQyxXQUFBLEdBQWMsY0FIZjs7SUFLQSxJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FDQztNQUFBLFVBQUEsRUFDQztRQUFBLENBQUEsRUFBRyxHQUFBLEdBQU0sQ0FBQyxLQUFBLEdBQVEsR0FBVCxDQUFUO09BREQ7TUFFQSxJQUFBLEVBQU0sSUFGTjtNQUdBLEtBQUEsRUFBTyxXQUhQO0tBREQ7V0FRQSxJQUFDLENBQUEsWUFBRCxHQUFnQjtFQWpCUDs7bUJBbUJWLE9BQUEsR0FBUyxTQUFDLEtBQUQ7SUFDUixJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FDQztNQUFBLFVBQUEsRUFDQztRQUFBLENBQUEsRUFBRyxHQUFBLEdBQU0sQ0FBQyxLQUFBLEdBQVEsR0FBVCxDQUFUO09BREQ7TUFFQSxJQUFBLEVBQU0sS0FGTjtLQUREO1dBS0EsSUFBQyxDQUFBLFlBQUQsR0FBZ0I7RUFOUjs7bUJBVVQsSUFBQSxHQUFNLFNBQUE7V0FDTCxJQUFDLENBQUMsT0FBRixHQUFZO0VBRFA7O21CQUdOLElBQUEsR0FBTSxTQUFBO1dBQ0wsSUFBQyxDQUFDLE9BQUYsR0FBWTtFQURQOzttQkFHTixVQUFBLEdBQVksU0FBQSxHQUFBOzs7O0dBckpnQjs7OztBRFc3QixPQUFPLENBQUMsVUFBUixHQUFxQjs7QUFDckIsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBRXJCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUNsQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBQ2xCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUVsQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFDbkIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFFbkIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBQ2xCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUNsQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBRWxCLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFDbkIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUVuQixPQUFPLENBQUMsTUFBUixHQUFpQjs7QUFDakIsT0FBTyxDQUFDLE1BQVIsR0FBaUI7O0FBQ2pCLE9BQU8sQ0FBQyxNQUFSLEdBQWlCOztBQUNqQixPQUFPLENBQUMsTUFBUixHQUFpQjs7QUFFakIsT0FBTyxDQUFDLFNBQVIsR0FBb0I7O0FBQ3BCLE9BQU8sQ0FBQyxTQUFSLEdBQW9COztBQUNwQixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFDcEIsT0FBTyxDQUFDLFNBQVIsR0FBb0I7O0FBRXBCLE9BQU8sQ0FBQyxZQUFSLEdBQXVCOztBQUN2QixPQUFPLENBQUMsWUFBUixHQUF1Qjs7QUFFdkIsT0FBTyxDQUFDLDRCQUFSLEdBQXVDLENBQ3RDLFNBRHNDLEVBRXRDLFNBRnNDLEVBR3RDLFNBSHNDLEVBSXRDLFNBSnNDLEVBS3RDLFNBTHNDLEVBTXRDLFNBTnNDLEVBT3RDLFNBUHNDLEVBUXRDLFNBUnNDLEVBU3RDLFNBVHNDOzs7O0FEMUN2QyxJQUFBOzs7O0FBQU0sT0FBTyxDQUFDO0FBR2IsTUFBQTs7OztFQUFBLFFBQUMsQ0FBQyxNQUFGLENBQVMsUUFBVCxFQUNDO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUE7SUFBSixDQUFMO0dBREQ7O0VBR2Esa0JBQUMsUUFBRDtBQUNaLFFBQUE7SUFEYSxJQUFDLENBQUEsNkJBQUQsV0FBUztJQUN0QixJQUFDLENBQUEsU0FBRCxpREFBcUIsQ0FBQyxnQkFBRCxDQUFDLFlBQWE7SUFDbkMsSUFBQyxDQUFBLE1BQUQsZ0RBQXFCLENBQUMsY0FBRCxDQUFDLFNBQWE7SUFDbkMsSUFBQyxDQUFBLEtBQUQsK0NBQXFCLENBQUMsYUFBRCxDQUFDLFFBQWE7O01BQ25DLElBQUMsQ0FBQSxVQUFrQzs7SUFFbkMsSUFBQyxDQUFBLGNBQUQsR0FBcUIsSUFBQyxDQUFBLE1BQUosR0FBZ0IsUUFBQSxHQUFTLElBQUMsQ0FBQSxNQUExQixHQUF3QztJQUMxRCwyQ0FBQSxTQUFBO0lBRUEsSUFBNkgsSUFBQyxDQUFBLEtBQTlIO01BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSw0Q0FBQSxHQUE2QyxJQUFDLENBQUEsU0FBOUMsR0FBd0QseUJBQXhELEdBQWlGLElBQUMsQ0FBQSxTQUFsRixHQUE0RixrQkFBeEcsRUFBQTs7SUFDQSxJQUFDLENBQUMsUUFBRixDQUFXLFlBQVg7RUFWWTs7RUFZYixPQUFBLEdBQVUsU0FBQyxPQUFELEVBQVUsTUFBVixFQUFrQixJQUFsQixFQUF3QixRQUF4QixFQUFrQyxNQUFsQyxFQUEwQyxJQUExQyxFQUFnRCxVQUFoRCxFQUE0RCxLQUE1RDtBQUVULFFBQUE7SUFBQSxHQUFBLEdBQU0sVUFBQSxHQUFXLE9BQVgsR0FBbUIsaUJBQW5CLEdBQW9DLElBQXBDLEdBQXlDLE9BQXpDLEdBQWdEO0lBRXRELElBQUcsa0JBQUg7TUFDQyxJQUFHLFVBQVUsQ0FBQyxPQUFkO1FBQXNDLEdBQUEsSUFBTyxnQkFBN0M7O01BQ0EsSUFBRyxVQUFVLENBQUMsTUFBWCxLQUFxQixRQUF4QjtRQUFzQyxHQUFBLElBQU8saUJBQTdDOztBQUVBLGNBQU8sVUFBVSxDQUFDLEtBQWxCO0FBQUEsYUFDTSxRQUROO1VBQ29CLEdBQUEsSUFBTztBQUFyQjtBQUROLGFBRU0sUUFGTjtVQUVvQixHQUFBLElBQU87QUFGM0I7TUFJQSxJQUFHLE9BQU8sVUFBVSxDQUFDLFFBQWxCLEtBQThCLFFBQWpDO1FBQ0MsR0FBQSxJQUFPLFlBQUEsR0FBYSxVQUFVLENBQUM7UUFDL0IsTUFBTSxDQUFDLElBQVAsQ0FBWSxHQUFaLEVBQWdCLE9BQWhCLEVBRkQ7O01BSUEsSUFBdUQsT0FBTyxVQUFVLENBQUMsT0FBbEIsS0FBa0MsUUFBekY7UUFBQSxHQUFBLElBQU8sV0FBQSxHQUFjLEdBQWQsR0FBb0IsVUFBVSxDQUFDLE9BQS9CLEdBQXlDLElBQWhEOztNQUNBLElBQXVELE9BQU8sVUFBVSxDQUFDLFlBQWxCLEtBQWtDLFFBQXpGO1FBQUEsR0FBQSxJQUFPLGdCQUFBLEdBQWlCLFVBQVUsQ0FBQyxhQUFuQzs7TUFDQSxJQUF1RCxPQUFPLFVBQVUsQ0FBQyxXQUFsQixLQUFrQyxRQUF6RjtRQUFBLEdBQUEsSUFBTyxlQUFBLEdBQWdCLFVBQVUsQ0FBQyxZQUFsQzs7TUFDQSxJQUF1RCxPQUFPLFVBQVUsQ0FBQyxPQUFsQixLQUFrQyxRQUF6RjtRQUFBLEdBQUEsSUFBTyxXQUFBLEdBQVksVUFBVSxDQUFDLFFBQTlCOztNQUNBLElBQXVELE9BQU8sVUFBVSxDQUFDLEtBQWxCLEtBQWtDLFFBQXpGO1FBQUEsR0FBQSxJQUFPLFNBQUEsR0FBVSxVQUFVLENBQUMsTUFBNUI7O01BQ0EsSUFBdUQsT0FBTyxVQUFVLENBQUMsT0FBbEIsS0FBa0MsUUFBekY7UUFBQSxHQUFBLElBQU8sV0FBQSxHQUFZLFVBQVUsQ0FBQyxRQUE5QjtPQWpCRDs7SUFtQkEsSUFBeUcsS0FBekc7TUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLGlCQUFBLEdBQWtCLE1BQWxCLEdBQXlCLHdCQUF6QixHQUFnRCxDQUFDLElBQUksQ0FBQyxTQUFMLENBQWUsSUFBZixDQUFELENBQWhELEdBQXNFLGFBQXRFLEdBQW1GLEdBQW5GLEdBQXVGLEdBQW5HLEVBQUE7O0lBRUEsT0FBQSxHQUNDO01BQUEsTUFBQSxFQUFRLE1BQVI7TUFDQSxPQUFBLEVBQ0M7UUFBQSxjQUFBLEVBQWdCLGlDQUFoQjtPQUZEOztJQUlELElBQUcsWUFBSDtNQUNDLE9BQU8sQ0FBQyxJQUFSLEdBQWUsSUFBSSxDQUFDLFNBQUwsQ0FBZSxJQUFmLEVBRGhCOztJQUdBLENBQUEsR0FBSSxLQUFBLENBQU0sR0FBTixFQUFXLE9BQVgsQ0FDSixDQUFDLElBREcsQ0FDRSxTQUFDLEdBQUQ7QUFDTCxVQUFBO01BQUEsSUFBRyxDQUFDLEdBQUcsQ0FBQyxFQUFSO0FBQWdCLGNBQU0sS0FBQSxDQUFNLEdBQUcsQ0FBQyxVQUFWLEVBQXRCOztNQUNBLElBQUEsR0FBTyxHQUFHLENBQUMsSUFBSixDQUFBO01BQ1AsSUFBSSxDQUFDLElBQUwsQ0FBVSxRQUFWO0FBQ0EsYUFBTztJQUpGLENBREYsQ0FNSixFQUFDLEtBQUQsRUFOSSxDQU1HLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxLQUFEO2VBQVcsT0FBTyxDQUFDLElBQVIsQ0FBYSxLQUFiO01BQVg7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBTkg7QUFRSixXQUFPO0VBekNFOztFQTRDVixTQUFBLEdBQVksU0FBQTtBQUNYLFFBQUE7SUFEWSxrQkFBRyxpR0FBUztJQUN4QixJQUFHLE9BQU8sSUFBSyxDQUFBLENBQUEsR0FBRSxDQUFGLENBQVosS0FBb0IsUUFBdkI7TUFDQyxJQUFLLENBQUEsQ0FBQSxDQUFMLEdBQVUsSUFBSyxDQUFBLENBQUEsR0FBRSxDQUFGO01BQ2YsSUFBSyxDQUFBLENBQUEsR0FBRSxDQUFGLENBQUwsR0FBWSxLQUZiOztBQUlBLFdBQU8sRUFBRSxDQUFDLEtBQUgsQ0FBUyxJQUFULEVBQWUsSUFBZjtFQUxJOztxQkFTWixHQUFBLEdBQVEsU0FBQTtBQUFhLFFBQUE7SUFBWjtXQUFZLFNBQUEsYUFBVSxDQUFBLENBQUcsU0FBQSxXQUFBLElBQUEsQ0FBQSxFQUFTLENBQUEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLElBQUQsRUFBVSxRQUFWLEVBQW9CLFVBQXBCO2VBQW1DLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFvQixLQUFDLENBQUEsY0FBckIsRUFBcUMsSUFBckMsRUFBMkMsUUFBM0MsRUFBcUQsS0FBckQsRUFBK0QsSUFBL0QsRUFBcUUsVUFBckUsRUFBaUYsS0FBQyxDQUFBLEtBQWxGO01BQW5DO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsQ0FBdEI7RUFBYjs7cUJBQ1IsR0FBQSxHQUFRLFNBQUE7QUFBYSxRQUFBO0lBQVo7V0FBWSxTQUFBLGFBQVUsQ0FBQSxDQUFHLFNBQUEsV0FBQSxJQUFBLENBQUEsRUFBUyxDQUFBLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLFFBQWIsRUFBdUIsVUFBdkI7ZUFBc0MsT0FBQSxDQUFRLEtBQUMsQ0FBQSxTQUFULEVBQW9CLEtBQUMsQ0FBQSxjQUFyQixFQUFxQyxJQUFyQyxFQUEyQyxRQUEzQyxFQUFxRCxLQUFyRCxFQUErRCxJQUEvRCxFQUFxRSxVQUFyRSxFQUFpRixLQUFDLENBQUEsS0FBbEY7TUFBdEM7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQSxDQUF0QjtFQUFiOztxQkFDUixJQUFBLEdBQVEsU0FBQTtBQUFhLFFBQUE7SUFBWjtXQUFZLFNBQUEsYUFBVSxDQUFBLENBQUcsU0FBQSxXQUFBLElBQUEsQ0FBQSxFQUFTLENBQUEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsUUFBYixFQUF1QixVQUF2QjtlQUFzQyxPQUFBLENBQVEsS0FBQyxDQUFBLFNBQVQsRUFBb0IsS0FBQyxDQUFBLGNBQXJCLEVBQXFDLElBQXJDLEVBQTJDLFFBQTNDLEVBQXFELE1BQXJELEVBQStELElBQS9ELEVBQXFFLFVBQXJFLEVBQWlGLEtBQUMsQ0FBQSxLQUFsRjtNQUF0QztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLENBQXRCO0VBQWI7O3FCQUNSLEtBQUEsR0FBUSxTQUFBO0FBQWEsUUFBQTtJQUFaO1dBQVksU0FBQSxhQUFVLENBQUEsQ0FBRyxTQUFBLFdBQUEsSUFBQSxDQUFBLEVBQVMsQ0FBQSxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxRQUFiLEVBQXVCLFVBQXZCO2VBQXNDLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFvQixLQUFDLENBQUEsY0FBckIsRUFBcUMsSUFBckMsRUFBMkMsUUFBM0MsRUFBcUQsT0FBckQsRUFBK0QsSUFBL0QsRUFBcUUsVUFBckUsRUFBaUYsS0FBQyxDQUFBLEtBQWxGO01BQXRDO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsQ0FBdEI7RUFBYjs7c0JBQ1IsUUFBQSxHQUFRLFNBQUE7QUFBYSxRQUFBO0lBQVo7V0FBWSxTQUFBLGFBQVUsQ0FBQSxDQUFHLFNBQUEsV0FBQSxJQUFBLENBQUEsRUFBUyxDQUFBLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxJQUFELEVBQVksUUFBWixFQUFzQixVQUF0QjtlQUFxQyxPQUFBLENBQVEsS0FBQyxDQUFBLFNBQVQsRUFBb0IsS0FBQyxDQUFBLGNBQXJCLEVBQXFDLElBQXJDLEVBQTJDLFFBQTNDLEVBQXFELFFBQXJELEVBQStELElBQS9ELEVBQXFFLFVBQXJFLEVBQWlGLEtBQUMsQ0FBQSxLQUFsRjtNQUFyQztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLENBQXRCO0VBQWI7O3FCQUdSLFFBQUEsR0FBVSxTQUFDLElBQUQsRUFBTyxRQUFQO0FBR1QsUUFBQTtJQUFBLElBQUcsSUFBQSxLQUFRLFlBQVg7TUFFQyxHQUFBLEdBQU0sVUFBQSxHQUFXLElBQUMsQ0FBQSxTQUFaLEdBQXNCLHVCQUF0QixHQUE2QyxJQUFDLENBQUE7TUFDcEQsYUFBQSxHQUFnQjtNQUNoQixNQUFBLEdBQWEsSUFBQSxXQUFBLENBQVksR0FBWjtNQUViLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixNQUF4QixFQUFnQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDL0IsSUFBRyxhQUFBLEtBQWlCLGNBQXBCO1lBQ0MsS0FBQyxDQUFDLE9BQUYsR0FBWTtZQUNaLElBQXlCLGdCQUF6QjtjQUFBLFFBQUEsQ0FBUyxXQUFULEVBQUE7O1lBQ0EsSUFBc0YsS0FBQyxDQUFBLEtBQXZGO2NBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSw0Q0FBQSxHQUE2QyxLQUFDLENBQUEsU0FBOUMsR0FBd0QsZUFBcEUsRUFBQTthQUhEOztpQkFJQSxhQUFBLEdBQWdCO1FBTGU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhDO01BT0EsTUFBTSxDQUFDLGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUNoQyxJQUFHLGFBQUEsS0FBaUIsV0FBcEI7WUFDQyxLQUFDLENBQUMsT0FBRixHQUFZO1lBQ1osSUFBNEIsZ0JBQTVCO2NBQUEsUUFBQSxDQUFTLGNBQVQsRUFBQTs7WUFDQSxJQUFrRixLQUFDLENBQUEsS0FBbkY7Y0FBQSxPQUFPLENBQUMsSUFBUixDQUFhLDRDQUFBLEdBQTZDLEtBQUMsQ0FBQSxTQUE5QyxHQUF3RCxVQUFyRSxFQUFBO2FBSEQ7O2lCQUlBLGFBQUEsR0FBZ0I7UUFMZ0I7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpDO0FBT0EsYUFwQkQ7O0lBc0JBLEdBQUEsR0FBTSxVQUFBLEdBQVcsSUFBQyxDQUFBLFNBQVosR0FBc0IsaUJBQXRCLEdBQXVDLElBQXZDLEdBQTRDLE9BQTVDLEdBQW1ELElBQUMsQ0FBQTtJQUMxRCxNQUFBLEdBQWEsSUFBQSxXQUFBLENBQVksR0FBWjtJQUNiLElBQW1GLElBQUMsQ0FBQSxLQUFwRjtNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksMENBQUEsR0FBMkMsSUFBM0MsR0FBZ0QsYUFBaEQsR0FBNkQsR0FBN0QsR0FBaUUsR0FBN0UsRUFBQTs7SUFFQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsS0FBeEIsRUFBK0IsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLEVBQUQ7UUFDOUIsSUFBc0gsZ0JBQXRIO1VBQUEsUUFBQSxDQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUE3QixFQUFtQyxLQUFuQyxFQUEwQyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBOUQsRUFBb0UsQ0FBQyxDQUFDLElBQUYsQ0FBTyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBSSxDQUFDLEtBQXpCLENBQStCLEdBQS9CLENBQVAsRUFBMkMsQ0FBM0MsQ0FBcEUsRUFBQTs7UUFDQSxJQUFzSCxLQUFDLENBQUEsS0FBdkg7aUJBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxzQ0FBQSxHQUF1QyxJQUF2QyxHQUE0QyxlQUE1QyxHQUEwRCxDQUFDLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUFyQixDQUExRCxHQUFvRixZQUFwRixHQUFnRyxHQUFoRyxHQUFvRyxHQUFoSCxFQUFBOztNQUY4QjtJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBL0I7V0FJQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLEVBQUQ7UUFDaEMsSUFBd0gsZ0JBQXhIO1VBQUEsUUFBQSxDQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUE3QixFQUFtQyxPQUFuQyxFQUE0QyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBaEUsRUFBc0UsQ0FBQyxDQUFDLElBQUYsQ0FBTyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBSSxDQUFDLEtBQXpCLENBQStCLEdBQS9CLENBQVAsRUFBMkMsQ0FBM0MsQ0FBdEUsRUFBQTs7UUFDQSxJQUF3SCxLQUFDLENBQUEsS0FBekg7aUJBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxzQ0FBQSxHQUF1QyxJQUF2QyxHQUE0QyxpQkFBNUMsR0FBNEQsQ0FBQyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBckIsQ0FBNUQsR0FBc0YsWUFBdEYsR0FBa0csR0FBbEcsR0FBc0csR0FBbEgsRUFBQTs7TUFGZ0M7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpDO0VBakNTOzs7O0dBOUVvQixNQUFNLENBQUM7Ozs7QURMdEMsSUFBQTs7QUFBQyxXQUFZLE9BQUEsQ0FBUSxVQUFSOztBQUViLHNCQUFBLEdBQTZCLElBQUEsUUFBQSxDQUM1QjtFQUFBLElBQUEsRUFBTSx3QkFBTjtFQUNBLElBQUEsRUFBTSxrREFETjtDQUQ0Qjs7QUFJN0IsZ0JBQUEsR0FBdUIsSUFBQSxRQUFBLENBQ3RCO0VBQUEsSUFBQSxFQUFNLGtCQUFOO0VBQ0EsSUFBQSxFQUFNLDJDQUROO0NBRHNCOztBQUl2QixxQkFBQSxHQUE0QixJQUFBLFFBQUEsQ0FDM0I7RUFBQSxJQUFBLEVBQU0sdUJBQU47RUFDQSxJQUFBLEVBQU0saURBRE47Q0FEMkI7O0FBSTVCLGVBQUEsR0FBc0IsSUFBQSxRQUFBLENBQ3JCO0VBQUEsSUFBQSxFQUFNLGlCQUFOO0VBQ0EsSUFBQSxFQUFNLDBDQUROO0NBRHFCOztBQUl0QiwwQkFBQSxHQUFpQyxJQUFBLFFBQUEsQ0FDaEM7RUFBQSxJQUFBLEVBQU0sNEJBQU47RUFDQSxJQUFBLEVBQU0sc0RBRE47Q0FEZ0M7O0FBSWpDLG9CQUFBLEdBQTJCLElBQUEsUUFBQSxDQUMxQjtFQUFBLElBQUEsRUFBTSxzQkFBTjtFQUNBLElBQUEsRUFBTSwrQ0FETjtDQUQwQjs7QUFJM0Isc0JBQUEsR0FBNkIsSUFBQSxRQUFBLENBQzVCO0VBQUEsSUFBQSxFQUFNLHdCQUFOO0VBQ0EsSUFBQSxFQUFNLGtEQUROO0NBRDRCOztBQUk3QixnQkFBQSxHQUF1QixJQUFBLFFBQUEsQ0FDdEI7RUFBQSxJQUFBLEVBQU0sa0JBQU47RUFDQSxJQUFBLEVBQU0sMkNBRE47Q0FEc0I7O0FBSXZCLHVCQUFBLEdBQThCLElBQUEsUUFBQSxDQUM3QjtFQUFBLElBQUEsRUFBTSx5QkFBTjtFQUNBLElBQUEsRUFBTSxtREFETjtDQUQ2Qjs7QUFJOUIsaUJBQUEsR0FBd0IsSUFBQSxRQUFBLENBQ3ZCO0VBQUEsSUFBQSxFQUFNLG1CQUFOO0VBQ0EsSUFBQSxFQUFNLDRDQUROO0NBRHVCOztBQUl4Qix3QkFBQSxHQUErQixJQUFBLFFBQUEsQ0FDOUI7RUFBQSxJQUFBLEVBQU0sMEJBQU47RUFDQSxJQUFBLEVBQU0sb0RBRE47Q0FEOEI7O0FBSS9CLGtCQUFBLEdBQXlCLElBQUEsUUFBQSxDQUN4QjtFQUFBLElBQUEsRUFBTSxvQkFBTjtFQUNBLElBQUEsRUFBTSw2Q0FETjtDQUR3Qjs7QUFJekIseUJBQUEsR0FBZ0MsSUFBQSxRQUFBLENBQy9CO0VBQUEsSUFBQSxFQUFNLDJCQUFOO0VBQ0EsSUFBQSxFQUFNLHFEQUROO0NBRCtCOztBQUloQyxrQkFBQSxHQUF5QixJQUFBLFFBQUEsQ0FDeEI7RUFBQSxJQUFBLEVBQU0scUJBQU47RUFDQSxJQUFBLEVBQU0sOENBRE47Q0FEd0I7O0FBSXpCLHFCQUFBLEdBQTRCLElBQUEsUUFBQSxDQUMzQjtFQUFBLElBQUEsRUFBTSx1QkFBTjtFQUNBLElBQUEsRUFBTSxpREFETjtDQUQyQjs7QUFJNUIsZUFBQSxHQUFzQixJQUFBLFFBQUEsQ0FDckI7RUFBQSxJQUFBLEVBQU0saUJBQU47RUFDQSxJQUFBLEVBQU0sMENBRE47Q0FEcUI7Ozs7QUQ3RHRCLE9BQU8sQ0FBQyxhQUFSLEdBQXdCLElBQUk7O0FBQzVCLE9BQU8sQ0FBQyxlQUFSLEdBQTBCOztBQUMxQixPQUFPLENBQUMsYUFBUixHQUF3Qjs7QUFDeEIsT0FBTyxDQUFDLGFBQVIsR0FBd0I7O0FBQ3hCLE9BQU8sQ0FBQyxtQkFBUixHQUE4QixJQUFJOztBQUNsQyxPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLGNBQVIsR0FBeUI7O0FBQ3pCLE9BQU8sQ0FBQyxXQUFSLEdBQXNCOztBQUN0QixPQUFPLENBQUMsY0FBUixHQUF5Qjs7QUFHekIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxhQUFSLEdBQXdCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDOztBQUM3QyxPQUFPLENBQUMsWUFBUixHQUF1QixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQzs7QUFHNUMsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBRXJCLE9BQU8sQ0FBQyxjQUFSLEdBQXlCOztBQUV6QixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFDcEIsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBQ3JCLE9BQU8sQ0FBQyxjQUFSLEdBQXlCOztBQUN6QixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFXcEIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7Ozs7QURuQ25CLE9BQU8sQ0FBQyxTQUFSLEdBQW9CO0VBQ25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQURtQixFQU1uQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLE1BSFA7R0FObUIsRUFXbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxRQUhQO0dBWG1CLEVBZ0JuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFNBSFA7R0FoQm1CLEVBcUJuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFdBSFA7R0FyQm1CLEVBMEJuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFNBSFA7R0ExQm1CLEVBK0JuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFVBSFA7R0EvQm1CLEVBb0NuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLGdCQUhQO0dBcENtQixFQXlDbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxTQUhQO0dBekNtQixFQThDbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxlQUhQO0dBOUNtQixFQW1EbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxRQUhQO0dBbkRtQixFQXdEbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxZQUhQO0dBeERtQixFQTZEbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxRQUhQO0dBN0RtQixFQWtFbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxnQkFIUDtHQWxFbUIsRUF1RW5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sU0FIUDtHQXZFbUI7OztBQThFcEIsT0FBTyxDQUFDLHlCQUFSLEdBQW9DO0VBQ25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sU0FGUDtJQUdDLHdCQUFBLEVBQTBCLDhCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FEbUMsRUFRbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxRQUZQO0lBR0Msd0JBQUEsRUFBMEIsOEJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQVJtQyxFQWVuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBZm1DLEVBc0JuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBdEJtQyxFQTZCbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQTdCbUMsRUFvQ25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0FwQ21DLEVBMkNuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBM0NtQyxFQWtEbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxRQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQWxEbUMsRUF5RG5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sS0FGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0F6RG1DLEVBZ0VuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBaEVtQyxFQXVFbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXZFbUMsRUE4RW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sU0FGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0E5RW1DLEVBcUZuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFVBRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBckZtQyxFQTRGbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxRQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQTVGbUMsRUFtR25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLCtCQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0FuR21DLEVBMEduQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiwrQkFIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBMUdtQyxFQWlIbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxTQUZQO0lBR0Msd0JBQUEsRUFBMEIsOEJBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQWpIbUMsRUF3SG5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sS0FGUDtJQUdDLHdCQUFBLEVBQTBCLCtCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0F4SG1DLEVBK0huQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBL0htQyxFQXNJbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxTQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXRJbUM7OztBQStJcEMsT0FBTyxDQUFDLHlCQUFSLEdBQW9DO0VBQ25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLDhCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FEbUMsRUFRbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxRQUZQO0lBR0Msd0JBQUEsRUFBMEIsOEJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQVJtQyxFQWVuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFNBRlA7SUFHQyx3QkFBQSxFQUEwQiw4QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBZm1DLEVBc0JuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE1BRlA7SUFHQyx3QkFBQSxFQUEwQiw4QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBdEJtQyxFQTZCbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxTQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQTdCbUMsRUFvQ25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sS0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FwQ21DLEVBMkNuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFFBRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBM0NtQzs7O0FBb0RwQyxPQUFPLENBQUMsb0JBQVIsR0FBK0I7RUFDOUI7SUFDQyxJQUFBLEVBQU0sV0FEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLGdDQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FEOEIsRUFTOUI7SUFDQyxJQUFBLEVBQU0sYUFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLDZEQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FUOEIsRUFpQjlCO0lBQ0MsSUFBQSxFQUFNLFNBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQiw4Q0FKakI7SUFLQyxPQUFBLEVBQVMsQ0FMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBakI4QixFQXlCOUI7SUFDQyxJQUFBLEVBQU0sYUFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLHdDQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0F6QjhCLEVBaUM5QjtJQUNDLElBQUEsRUFBTSxXQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0IsMERBSmpCO0lBS0MsT0FBQSxFQUFTLENBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQWpDOEIsRUF5QzlCO0lBQ0MsSUFBQSxFQUFNLFlBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxLQUhYO0lBSUMsY0FBQSxFQUFnQix3Q0FKakI7SUFLQyxPQUFBLEVBQVMsQ0FMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBekM4QixFQWlEOUI7SUFDQyxJQUFBLEVBQU0sU0FEUDtJQUVDLE1BQUEsRUFBUSxXQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLGdEQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FqRDhCLEVBeUQ5QjtJQUNDLElBQUEsRUFBTSxZQURQO0lBRUMsTUFBQSxFQUFRLFdBRlQ7SUFHQyxRQUFBLEVBQVUsS0FIWDtJQUlDLGNBQUEsRUFBZ0IseUNBSmpCO0lBS0MsT0FBQSxFQUFTLENBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQXpEOEIsRUFpRTlCO0lBQ0MsSUFBQSxFQUFNLDRCQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsS0FIWDtJQUlDLGNBQUEsRUFBZ0IsZ0JBSmpCO0lBS0MsT0FBQSxFQUFTLENBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQWpFOEI7OztBQTJFL0IsT0FBTyxDQUFDLG9CQUFSLEdBQStCO0VBQzlCO0lBQ0MsSUFBQSxFQUFNLEtBRFA7SUFFQyxNQUFBLEVBQVEsU0FGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQiw2QkFKakI7SUFLQyxPQUFBLEVBQVMsRUFMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBRDhCLEVBUzlCO0lBQ0MsSUFBQSxFQUFNLFlBRFA7SUFFQyxNQUFBLEVBQVEsU0FGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQiwwQ0FKakI7SUFLQyxPQUFBLEVBQVMsRUFMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBVDhCLEVBaUI5QjtJQUNDLElBQUEsRUFBTSxLQURQO0lBRUMsTUFBQSxFQUFRLFNBRlQ7SUFHQyxRQUFBLEVBQVUsS0FIWDtJQUlDLGNBQUEsRUFBZ0Isa0VBSmpCO0lBS0MsT0FBQSxFQUFTLEVBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQWpCOEIsRUF5QjlCO0lBQ0MsSUFBQSxFQUFNLGFBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQixzREFKakI7SUFLQyxPQUFBLEVBQVMsRUFMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBekI4QixFQWlDOUI7SUFDQyxJQUFBLEVBQU0sWUFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLHNEQUpqQjtJQUtDLE9BQUEsRUFBUyxFQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FqQzhCLEVBeUM5QjtJQUNDLElBQUEsRUFBTSxTQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0IsOENBSmpCO0lBS0MsT0FBQSxFQUFTLEVBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQXpDOEIsRUFpRDlCO0lBQ0MsSUFBQSxFQUFNLDRCQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsS0FIWDtJQUlDLGNBQUEsRUFBZ0IsZ0JBSmpCO0lBS0MsT0FBQSxFQUFTLENBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQWpEOEI7OztBQTJEL0IsT0FBTyxDQUFDLG9CQUFSLEdBQStCO0VBQzlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxlQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQiwwQ0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBRDhCLEVBVTlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxtQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBVjhCOzs7QUF3US9CLE9BQU8sQ0FBQyxvQkFBUixHQUErQjtFQUM5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0saUJBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLDBDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FEOEIsRUFVOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGdCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FWOEIsRUFtQjlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsYUFGWjtJQUdDLElBQUEsRUFBTSxjQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0FuQjhCLEVBNEI5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sWUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBNUI4QixFQXFDOUI7SUFDQyxPQUFBLEVBQVMsRUFEVjtJQUVDLFNBQUEsRUFBVyxhQUZaO0lBR0MsSUFBQSxFQUFNLFlBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLHlDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0FyQzhCLEVBOEM5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sa0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQTlDOEIsRUF1RDlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxlQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQiwwQ0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBdkQ4QixFQWdFOUI7SUFDQyxPQUFBLEVBQVMsRUFEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLFdBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLHVDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FoRThCLEVBeUU5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sZUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBekU4QixFQWtGOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGlCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FsRjhCLEVBMkY5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sZ0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLHlDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0EzRjhCOzs7QUF3US9CLE9BQU8sQ0FBQyxZQUFSLEdBQXVCO0VBQ3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQURzQixFQUt0QjtJQUNDLFNBQUEsRUFBVyxXQURaO0lBRUMsVUFBQSxFQUFZLFdBRmI7R0FMc0IsRUFTdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBVHNCLEVBYXRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQWJzQixFQWlCdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBakJzQixFQXFCdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBckJzQixFQXlCdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBekJzQixFQTZCdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBN0JzQixFQWlDdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBakNzQixFQXFDdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBckNzQixFQXlDdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBekNzQixFQTZDdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBN0NzQjs7Ozs7QUR0NkJ2QixJQUFBOztBQUFBLE1BQUEsR0FBUyxPQUFBLENBQVEsUUFBUjs7QUFFVCxPQUFPLENBQUMsT0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLG1CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE9BRlY7OztBQUlILE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksb0JBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxpQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsT0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLGlCQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxXQUFSLEdBQ0M7RUFBQSxVQUFBLEVBQVksb0JBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUQsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxpQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsUUFBUixHQUNDO0VBQUEsVUFBQSxFQUFZLG1CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlELE9BQU8sQ0FBQyxZQUFSLEdBQ0M7RUFBQSxVQUFBLEVBQVksb0JBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUQsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxxQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsU0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxTQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVkscUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLE1BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxpQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsT0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLG1CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7Ozs7O0FEaEVILE9BQU8sQ0FBQyxhQUFSLEdBQXdCLFNBQUE7QUFDdEIsTUFBQTtFQUFBLEdBQUEsR0FBTSxJQUFJO0VBV1YsSUFBQSxHQUFPLEdBQUcsQ0FBQyxXQUFKLENBQUEsQ0FBQSxHQUFvQjtFQUMzQixJQUFHLElBQUEsSUFBUSxFQUFYO0lBQ0MsSUFBQSxHQUFPLElBQUEsR0FBTyxHQURmOztFQUdBLE1BQUEsR0FBUyxHQUFHLENBQUMsYUFBSixDQUFBO0VBQ1QsTUFBQSxHQUFTLEdBQUcsQ0FBQyxhQUFKLENBQUE7RUFDVCxPQUFBLEdBQWEsSUFBQSxJQUFRLEVBQVgsR0FBbUIsSUFBbkIsR0FBNkI7RUFDdkMsSUFBQSxHQUFVLElBQUEsSUFBUSxFQUFYLEdBQW1CLElBQUEsR0FBTyxFQUExQixHQUFrQztFQUN6QyxJQUFHLElBQUEsS0FBUSxDQUFYO0lBQ0MsSUFBQSxHQUFPLEdBRFI7O0VBRUEsTUFBQSxHQUFZLE1BQUEsR0FBUyxFQUFaLEdBQW9CLEdBQUEsR0FBTSxNQUExQixHQUFzQztFQUMvQyxVQUFBLEdBQWEsSUFBQSxHQUFPLEdBQVAsR0FBYSxNQUFiLEdBQXNCO0FBQ25DLFNBQU87QUF4QmU7O0FBMEJ4QixPQUFPLENBQUMsdUJBQVIsR0FBa0MsU0FBQyxVQUFEO0FBQ2hDLE1BQUE7RUFBQSxVQUFBLEdBQWEsVUFBQSxHQUFXO0VBQ3hCLE1BQUEsR0FBUyxJQUFJLENBQUMsS0FBTCxDQUFXLFVBQUEsR0FBYSxFQUF4QjtFQUNULFVBQUEsR0FBYSxVQUFBLEdBQVc7RUFDeEIsTUFBQSxHQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsVUFBQSxHQUFhLEVBQXhCO0VBQ1QsVUFBQSxHQUFhLFVBQUEsR0FBVztFQUN4QixJQUFBLEdBQU8sSUFBSSxDQUFDLEtBQUwsQ0FBVyxVQUFBLEdBQWEsRUFBeEI7RUFFUCxJQUFBLEdBQVUsSUFBQSxHQUFPLEVBQVYsR0FBa0IsR0FBQSxHQUFNLElBQXhCLEdBQWtDO0VBQ3pDLE1BQUEsR0FBWSxNQUFBLEdBQVMsRUFBWixHQUFvQixHQUFBLEdBQU0sTUFBMUIsR0FBc0M7RUFDL0MsTUFBQSxHQUFZLE1BQUEsR0FBUyxFQUFaLEdBQW9CLEdBQUEsR0FBTSxNQUExQixHQUFzQztFQUUvQyxVQUFBLEdBQWEsSUFBQSxHQUFPLEdBQVAsR0FBYSxNQUFiLEdBQXNCLEdBQXRCLEdBQTRCO0FBRXpDLFNBQU87QUFkeUIifQ==
