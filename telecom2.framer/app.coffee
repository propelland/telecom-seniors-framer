# MODULE REQUIREMENTS

{Firebase} = require "firebase"
{Fonts} = require "fonts"
{Circle} = require "circleModule"
{YouTubePlayer} = require 'YouTubePlayer'
colors = require "colors"
styles = require "styles"
protoFakeInfo = require "protoFakeInfo"
general = require "general"
time = require "time"

# GET GENERAL VARIABLES

lastVolumeChangeDate = new Date
# firebase = 0

firebase = new Firebase
	projectID: "framer-test-1-ff664"
	secret: "FeoF0DXgGEFMxI7p63P5ao77WT7WTdIhpKlQaZvQ"

passwordCounter = 0
incomingCall = false


if general.language == "spanish"
	phoneContactsDictionary = protoFakeInfo.phoneContactsSpanish
	socialTalkContactsDictionary = protoFakeInfo.socialTalkContactsSpanish
	radioStationsDictionary = protoFakeInfo.radioStationsSpanish
else if general.language == "english"
	phoneContactsDictionary = protoFakeInfo.phoneContactsEnglish
	socialTalkContactsDictionary = protoFakeInfo.socialTalkContactsEnglish
	radioStationsDictionary = protoFakeInfo.radioStationsEnglish
photoGalleryDictionary = protoFakeInfo.photoGallery

imagesPathSuffix = "images/"
# imagesPathSuffix = "https://s3.eu-west-3.amazonaws.com/telecom-framer-images/images/"
# imagesPathSuffix = "http://127.0.0.1:8887/"

# print(imagesPathSuffix)

# TEST

if general.showLogs
	print("FIRST LINE OF CODE")

if general.showLogs
	print(general.screen_width)
	print(general.screen_height)

# EXTRA - SETUP


# lastFirebaseGet = new Date
# lastFirebasePut = new Date
# 
# checkFirebasePutConnection = () ->
# 	print("checkFirebasePutConnection")
# 	if lastFirebasePut > lastFirebaseGet
# 		print("casi")
# 		firebase = new Firebase
# 			projectID: "framer-test-1-ff664"
# 			secret: "FeoF0DXgGEFMxI7p63P5ao77WT7WTdIhpKlQaZvQ"
# 		setTimeout(setupFirebase, 3000)

firebasePut = (path, value) ->
# 	print("firebase put: " + path + " " + value)
	firebase.put(path, value)
# 	if path = "/button"
# 		lastFirebasePut = new Date
# 		setTimeout(checkFirebasePutConnection, 3000)

setupFirebaseListeners = () ->
	
	firebase.onChange "/button", (buttonNumber) ->
		
		lastFirebaseGet = new Date
		
		if general.showLogs
			print("buttonNumber changed to " + buttonNumber)
		
		lastInteraction()
	

		if general.firstTimeButton
			general.firstTimeButton = false
		else
			if general.showingIdle
				showDashboard()
			else 
				if buttonNumber == "3" && passwordCounter == 0
					passwordCounter = 1
				else if buttonNumber == "7" && passwordCounter == 1
					passwordCounter = 2
				else if buttonNumber == "4" && passwordCounter == 2
					passwordCounter = 3
				else if buttonNumber == "6" && passwordCounter == 3
					passwordCounter = 4
				else
					passwordCounter = 0
					
				if passwordCounter > 3
					softResetFramer()
				else
					if buttonNumber == "1"
						if flow.current != SOSScreen
							showSOS()
					else if buttonNumber == "2"
						if flow.current != healthScreen
							if general.loadTeleHealth
								showHealth()
					else if buttonNumber == "3"
						if flow.current != socialTalkMain
							showSocialTalk()
					else if buttonNumber == "4"
						if flow.current != phoneCallScreen
							showPhone()
					else if buttonNumber == "5"
						if flow.current != videoCallScreen
							showVideoCall()
					else if buttonNumber == "6"
						if flow.current != leisureScreen
							showLeisure()
					else if buttonNumber == "7"
						if flow.current != radioScreen
							showRadio()	
				
				
	
	firebase.onChange "/knobValue", (knobValue) ->

		if general.firstTimeKnob
			general.firstTimeKnob = false
		else
			if general.showLogs
				print("knobValue changed to " + knobValue)
			volumeSlider.value = parseInt(knobValue)
			lastVolumeChangeDate = new Date
	
			if volumeSlider.value <= 0
				radioIconVolumeOn.opacity = 0
				radioIconVolumeOff.opacity = 1
				volumeControllerIconOn.opacity = 0
				volumeControllerIconOff.opacity = 1
			else
				radioIconVolumeOn.opacity = 1
				radioIconVolumeOff.opacity = 0
				volumeControllerIconOn.opacity = 1
				volumeControllerIconOff.opacity = 0
	
			if !showingVolumePopUp
				showVolume()
			setTimeout ( ->
				now = new Date
				substraction = now - lastVolumeChangeDate
				if substraction >= 2000
					hideVolume()
	
			), 2000
	
	firebase.onChange "/proximityState", (proximityStateString) ->
		
		if general.showLogs
			print("proximity changed to " + proximityStateString)
		lastInteraction()
		if proximityStateString == "true"
			general.proximityState = true
			if general.showingIdle
				showDashboard()
		else
			general.proximityState = false
	
# 	firebase.onChange "/radioStation", (radioStation) ->
# 		if general.showLogs
# 			print("radioStation changed to " + radioStation)

setupFirebase = () ->
	
	if general.showLogs
		print("setupFirebase")
	
	# Setting up Firebase
	firebase = new Firebase
		projectID: "framer-test-1-ff664"
		secret: "FeoF0DXgGEFMxI7p63P5ao77WT7WTdIhpKlQaZvQ"
	
# 	firebase.onChange "connection", (status) ->
# 		print(status)
	
	firebase.id = 1

	setTimeout(setupFirebaseListeners, 3000)

checkFirebase = () ->
	if firebase.status == "disconnected"
		if general.showLogs
			print("Firebase disconnected")
		setTimeout(setupFirebase, 5000)
# 	else
# 		if general.showLogs
# 			print("Firebase connected")

# Reset button
resetButton = () ->
	firebasePut("/button", "0")

setIdle = (state) ->
	general.showingIdle = state
	if state
		firebasePut("/idle", "true")
	else
		firebasePut("/idle", "false")

# Navigation
flow = new FlowComponent

# EXTRA - AUDIO

silence = () ->
	playAudio("silence")

playAudio = (audio) ->

	if general.showLogs
		print("Playing audio: " + audio)
		
# 	print(firebase)

	if audio == "silence"
		firebasePut("/radioStation", "0")
	else if audio == "socialTalkCall"
		firebasePut("/radioStation", "10")
	else if audio == "connecting"
		firebasePut("/radioStation", "12")
	else if audio == "call"
		firebasePut("/radioStation", "9")
	else if audio == "incoming"
		firebasePut("/radioStation", "11")
	else if audio == "teleHealth"
		firebasePut("/radioStation", "13")
	else if audio == "videoCall"
		firebasePut("/radioStation", "14")
	else if audio == "SOSSiren"
		firebasePut("/radioStation", "21")
	else if audio == "SOSAudio"
		firebasePut("/radioStation", "22")
	else
		firebasePut("/radioStation", audio)

radioIconVolumeOn = iconVolumeOn.copy()
radioIconVolumeOn.parent = radioIconVolume
radioIconVolumeOn.x = 0
radioIconVolumeOn.y = 0
radioIconVolumeOn.opacity = 1

radioIconVolumeOff = iconVolumeOff.copy()
radioIconVolumeOff.parent = radioIconVolume
radioIconVolumeOff.x = 0
radioIconVolumeOff.y = 0
radioIconVolumeOff.opacity = 0




# IDLE SCREEN

idleScreen.width = general.screen_width
idleScreen.height = general.screen_height

idleTimeFrame.style = styles.header0
idleTimeFrame.style.lineHeight = idleTimeFrame.height+"px"
idleTimeFrame.style.textAlign = "left"
idleTimeFrame.style.color = colors.whiteColor
idleTimeFrame.style.textShadow = "0px 4px 4px rgba(0,0,0,0.25)"

idleTemperatureFrame.style = styles.header1
idleTemperatureFrame.style.lineHeight = idleTemperatureFrame.height+"px"
idleTemperatureFrame.style.textAlign = "right"
idleTemperatureFrame.style.color = colors.whiteColor
idleTemperatureFrame.style.textShadow = "0px 4px 4px rgba(0,0,0,0.25)"
idleTemperatureFrame.html = "32°"

idleInfoOverlay1.parent = idleBottomOverlay
idleInfoOverlay1.x = 0
idleInfoOverlay1.y = 0

idleInfoOverlay2.parent = idleBottomOverlay
idleInfoOverlay2.x = 0
idleInfoOverlay2.y = 0

idleInfoOverlay2.opacity = 0

if general.loadImages
	idleImageFrame.image = imagesPathSuffix + "General/idleImage.png"


# Animate

idleInfoOverlay1.states = 
	appear: {scale:1, opacity:1}
	disappear: {scale: 1, opacity: 0}

idleInfoOverlay2.states =
	appear: {scale:1, opacity:1}
	disappear: {scale: 1, opacity: 0}

idleInfoOverlay1Appear = new Animation idleInfoOverlay1,
	idleInfoOverlay1.states.appear
	time: 1.5

idleInfoOverlay1Disappear = new Animation idleInfoOverlay1,
	idleInfoOverlay1.states.disappear
	time: 1.5

idleInfoOverlay2Appear = new Animation idleInfoOverlay2,
	idleInfoOverlay2.states.appear
	time: 1.5

idleInfoOverlay2Disappear = new Animation idleInfoOverlay2,
	idleInfoOverlay2.states.disappear
	time: 1.5

# MAIN NAVIGATION BAR

flow.header = mainNavBar
mainNavBar.width = general.screen_width

navBarSectionName.html = "Section Name"
navBarSectionName.style = styles.subhead
navBarSectionName.style.lineHeight = navBarSectionName.height+"px"
navBarSectionName.style.textAlign = "left"
navBarSectionName.style.color = colors.whiteColor

timeLabelContainer.style = styles.header3
timeLabelContainer.style.textAlign = "right"
timeLabelContainer.style.fontFamily = "ProximaNovaRegular"
timeLabelContainer.style.lineHeight = timeLabelContainer.height+"px"
timeLabelContainer.color = colors.gray800

changeToDashboardMainBar = () ->
	mainNavBar.backgroundColor = colors.whiteColor
	dashboardTabNavBar.visible = true
	navBarSectionName.visible = false
	timeLabelContainer.color = colors.gray600
	burgerButtonContainer.children[0].color = colors.blue600

	backButton.visible = false
	mainNavBarSearchBar.visible = false

	burgerButtonContainer.x = 86
	navBarSectionName.x = 190

changeToSectionMainBar = (searchBarHidden) ->

	mainNavBar.backgroundColor = colors.blue600
	dashboardTabNavBar.visible = false
	timeLabelContainer.color = colors.whiteColor
	burgerButtonContainer.children[0].color = colors.whiteColor

	backButton.visible = true

	if searchBarHidden
		mainNavBarSearchBar.visible = false
	else
		mainNavBarSearchBar.visible = true

	burgerButtonContainer.x = 190

	navBarSectionName.visible = true
	navBarSectionName.x = 294

mainNavBar.states =
	hide:
		y: -mainNavBar.height
	show:
		y: 0

showMainBar = (state) ->
	if state
		mainNavBar.animate "show",
			time: 0.5
	else
		mainNavBar.animate "hide",
			time: 0.5

# MENU

sideMenu.height = general.screen_height
burgerButtonContainer.y = mainNavBar.height/2 - burgerButtonContainer.height/2

backButton.onTap ->

	if flow.previous == photosGridScreen and flow.current == singlePhotoScreen
		selectedScrollPhoto.animate("unselected", {instant:true})
		flow.showPrevious()
	else if flow.previous == socialTalkMain and flow.current == editProfileScreen
		flow.showPrevious()
	else if flow.previous == mentalActivityMainScreen and flow.current == settingsGoal
		navBarSectionName.html = "Mental Activity"
		flow.showPrevious()
	else if flow.previous == physicalActivityMainScreen and flow.current == settingsGoal
		navBarSectionName.html = "Physical Activity"
		flow.showPrevious()
	else if flow.previous == dashboardPageComponent
		flow.showPrevious()
		showMainBar(true)
		changeToDashboardMainBar()
		resetButton()
		silence()
	else
		showDashboard()

# DASHBOARD

# if general.loadImages
# 	cityImageFrame.image = imagesPathSuffix + "General/madridImage.png"

# Creating tab nav bar for page component
dashboardTabNavBar.parent = mainNavBar
dashboardTabNavBar.x = 474
dashboardTabNavBar.y = 0
dashboardTabNavBar.height = mainNavBar.height

# Dashboard tab nav indicator states
dashboardIndicator.states.state1 =
	x: 0
dashboardIndicator.states.state2 =
	x: dashBoardTab1.width

dashboardIndicator.states.state3 =
	x: dashBoardTab1.width + dashBoardTab2.width

dashboardTabs = []

animateDashboardTabs = (tabNumber) ->
	# Indicator animation
	if tabNumber == 0
		loadingCircle1.changeTo(0,0)
		loadingCircle2.changeTo(0,0)
		dashboardIndicator.animate "state1",
			time: 0.5
	else if tabNumber == 1
		loadingCircle1.changeTo(45, 2)
		loadingCircle2.changeTo(30, 2)
		dashboardIndicator.animate "state2",
			time: 0.5
	else if tabNumber == 2
		loadingCircle1.changeTo(0,0)
		loadingCircle2.changeTo(0,0)
		dashboardIndicator.animate "state3",
			time: 0.5
	# Tab style animation
	index = 0
	for tab in dashboardTabs
		if index == tabNumber
			tab.animate("active")
		else
			tab.animate("inactive")
		index++

dashboardPageComponent = new PageComponent
	width: general.screen_width
	height: general.screen_height
	backgroundColor: colors.whiteColor
	scrollVertical: false
	scrollHorizontal: false
	x: 0
	y: 0

# Create each tab with each page content

dashboardScrolls = []
currentDashboardPageIndex = 0

for i in [0...3]
	# Creating tabs
	if i == 0
		dashboardTab = dashBoardTab1
		dashboardTab.html = "Comfort"
	else if i == 1
		dashboardTab = dashBoardTab2
		dashboardTab.html = "Activity"
	else if i == 2
		dashboardTab = dashBoardTab3
		dashboardTab.html = "Schedule"
	dashboardTab.number = i
	dashboardTab.style = styles.subhead
	dashboardTab.style.textAlign = "center"
	dashboardTab.style.lineHeight = dashboardTab.height+"px"

	# Tab states
	dashboardTab.states =
		active:
			color: colors.blue600
		inactive:
			color: colors.gray600

	dashboardTab.animate("inactive", {instant:true})

	# Pushing tab into tabs array
	dashboardTabs.push (dashboardTab)

	# Detect tab taps
	dashboardTab.onTap ->
		dashboardPageComponent.snapToPage(@page, true)
		currentDashboardPageIndex = @number
		animateDashboardTabs(currentDashboardPageIndex)

for i in [0...3]

	page = 0

	if i == 0
	
		page = dashboardComfort
		
		# Weather
		
		buttonWeatherApp.onTap ->
			showWeather()
			
		# Scroll component
		weatherScroll = new ScrollComponent
			width: general.screen_width
			height: general.screen_height
			x: 0
			y: 0
# 			backgroundColor: false
			backgroundLayer: 0
			parent: weatherScreen
			name: "weatherScroll"
			scrollHorizontal: false
		
		weatherScroll.content.draggable.overdrag = false
		
		weatherContent.x = 0
		weatherContent.y = 0
		weatherContent.parent = weatherScroll.content
		weatherContent.x = 0
		weatherContent.y = 0
		
		# Home Comfort
		
		homeComfortButton.onTap ->
			showHomeComfort()
		
		# Scroll component
		homeComfortScroll = new ScrollComponent
			width: homeComfortScrollContainer.width
			height: homeComfortScrollContainer.height
			x: 0
			y: 0
			backgroundLayer: 0
			parent: homeComfortScrollContainer
			name: "homeComfortScroll"
			scrollHorizontal: false
		
		homeComfortScroll.content.draggable.overdrag = false
		
		homeComfortContent.x = 0
		homeComfortContent.y = 0
		homeComfortContent.parent = homeComfortScroll.content
		homeComfortContent.x = 0
		homeComfortContent.y = 0
		
		
	else if i == 1
		page = dashboardActivity
		strokeWidth = 70
		circle1Size = 560

		loadingCircle1 = new Circle
			circleSize: circle1Size
			strokeWidth: strokeWidth
			topColor: colors.circleColor1
			bottomColor: colors.circleColor1
		loadingCircle1.parent = dashboardGoalsProgress
		loadingCircle1.x = dashboardGoalsProgress.width/2 - loadingCircle1.width / 2
		loadingCircle1.y = 253

		circle2Size = 400

		loadingCircle2 = new Circle
			circleSize: circle2Size
			strokeWidth: strokeWidth
			topColor: colors.circleColor2
			bottomColor: colors.circleColor2
		loadingCircle2.parent = dashboardGoalsProgress
		loadingCircle2.x = dashboardGoalsProgress.width/2 - loadingCircle2.width / 2
		loadingCircle2.y = loadingCircle1.y + (circle1Size - circle2Size)/2

		iconHeart.placeBefore(loadingCircle1)
		iconBrain.placeBefore(loadingCircle2)
		
		physicalActivityButton.onTap ->
			showPhysicalActivity()
		
		mentalActivityButton.onTap ->
			showMentalActivity()
		
		physicalEditButton.onTap ->
			showSettingsGoal()
		
		mentalEditButton.onTap ->
			showSettingsGoal()
		
		
		# Scroll component
		graphContainerScroll = new ScrollComponent
			width: graphsContainer.width
			height: graphsContainer.height
			x: 0
			y: 0
			backgroundLayer: false
			parent: graphsContainer
			name: "graphContainerScroll"
			scrollHorizontal: false
		graphContainerScroll.content.draggable.overdrag = false
		graphsContent.x = 0 # Necessary...
		graphsContent.y = 0 # Necessary...
		graphsContent.parent = graphContainerScroll.content
		graphsContent.x = 0
		graphsContent.y = 0

	else if i == 2
		page = dashboardSchedule

		checkbox1.states =
			default:
				backgroundColor: colors.stone400
				borderWidth: 2
			checked:
				backgroundColor: colors.green200
				borderWidth: 0

		checkbox2.states =
			default:
				backgroundColor: colors.stone400
				borderWidth: 2
			checked:
				backgroundColor: colors.green200
				borderWidth: 0

		checkbox3.states =
			default:
				backgroundColor: colors.stone400
				borderWidth: 2
			checked:
				backgroundColor: colors.green200
				borderWidth: 0

		checkFill1.states =
			default:
				visible: false
			checked:
				visibe: true

		checkFill2.states =
			default:
				visible: false
			checked:
				visibe: true

		checkFill3.states =
			default:
				visible: false
			checked:
				visibe: true

		checkbox1.animate("default", {instant:true})
		checkFill1.visible = false
# 		checkFill1.animate("default", {instant:true})

		checkbox2.animate("default", {instant:true})
		checkFill2.visible = false
# 		checkFill2.animate("default", {instant:true})

		checkbox3.animate("default", {instant:true})
		checkFill3.visible = false
# 		checkFill3.animate("default", {instant:true})

		checkbox1.onClick ->

			this.stateCycle(["checked", "default"], time:0)

			lastState = checkFill1.visible
			newState = !lastState
			checkFill1.visible = newState

			for child in this.parent.children
				if child.name == "taskCellTopLabel"
					if !newState
						child.color = colors.red600
					else
						child.color = colors.gray200
				else if child.name == "taskCellMiddleLabel"
					if !newState
						child.color = colors.blackColor
					else
						child.color = colors.gray200
				else if child.name == "taskCellBottomLabel"
					if !newState
						child.color = colors.gray400
					else
						child.color = colors.gray200

		checkbox2.onClick ->
			this.stateCycle(["checked", "default"], time:0)

			lastState = checkFill2.visible
			newState = !lastState
			checkFill2.visible = newState

			for child in this.parent.children
				if child.name == "taskCellTopLabel"
					if !newState
						child.color = colors.blue600
					else
						child.color = colors.gray200
				else if child.name == "taskCellMiddleLabel"
					if !newState
						child.color = colors.blackColor
					else
						child.color = colors.gray200
				else if child.name == "taskCellBottomLabel"
					if !newState
						child.color = colors.gray400
					else
						child.color = colors.gray200

		checkbox3.onClick ->
			this.stateCycle(["checked", "default"], time:0)

			lastState = checkFill3.visible
			newState = !lastState
			checkFill3.visible = newState

			for child in this.parent.children
				if child.name == "taskCellTopLabel"
					if !newState
						child.color = colors.blue600
					else
						child.color = colors.gray200
				else if child.name == "taskCellMiddleLabel"
					if !newState
						child.color = colors.blackColor
					else
						child.color = colors.gray200
				else if child.name == "taskCellBottomLabel"
					if !newState
						child.color = colors.gray400
					else
						child.color = colors.gray200

	page.width = dashboardPageComponent.width
	page.height = dashboardPageComponent.height
	page.x = dashboardPageComponent.width * i
	page.y = dashboardPageComponent.y
	page.parent = dashboardPageComponent.content
	page.name = "page" + i
	page.number = i

	page.onSwipeLeft ->
		if general.swipeAnimation == false
			if general.showLogs
				print("page.onSwipeLeft")
			dashboardPageComponent.snapToNextPage("right", animationOptions = time : 1000)
			if @number == 0
				currentDashboardPageIndex = 1
			else if @number == 1
				currentDashboardPageIndex = 2
			animateDashboardTabs(currentDashboardPageIndex)
		general.swipeAnimation = true

	page.onSwipeRight ->
		if general.swipeAnimation == false
			if general.showLogs
				print("page.onSwipeRight")
			dashboardPageComponent.snapToNextPage("left", animationOptions = time : 1000)
			if @number == 1
				currentDashboardPageIndex = 0
			else if @number == 2
				currentDashboardPageIndex = 1
			animateDashboardTabs(currentDashboardPageIndex)
		general.swipeAnimation = true

	page.onSwipeLeftEnd ->
		general.swipeAnimation = false
		if general.showLogs
			print("page.onSwipeLeftEnd")

	page.onSwipeRightEnd ->
		general.swipeAnimation = false
		if general.showLogs
			print("page.onSwipeRightEnd")

	dashboardTabs[i].page = page
	
	
	
# Dashboard - Calendar

buttonCalendarApp.onTap ->
	showCalendar()
	
calendarCreateButton.onTap ->
	showCreateCalendar()
	
calendarTopBar.onTap ->
	flow.showPrevious()
	
calendarScroll = new ScrollComponent
	width: calendarScrollContainer.width
	height: calendarScrollContainer.height
	x: 0
	y: 0
# 	backgroundColor: colors.gray200
	parent: calendarScrollContainer
	name: "calendarScroll"
	scrollHorizontal: false

calendarScroll.content.draggable.overdrag = false

calendarContent.x = 0
calendarContent.y = 0
calendarContent.parent = calendarScroll.content
calendarContent.x = 0
calendarContent.y = 0
calendarContent.visible = true


calendarDatePickerButton1.onTap ->
	showCalendarDatePicker()
	
calendarDatePickerButton2.onTap ->
	showCalendarDatePicker()
	
calendarTimePickerButton1.onTap ->
	showCalendarTimePicker()

calendarTimePickerButton2.onTap ->
	showCalendarTimePicker()
	
# Dashboard - Reminder

manageTasksButton.onTap ->
	showReminders()

createReminderButton.onTap ->
	showCreateReminder()
	
reminderTopBar.onTap ->
	flow.showPrevious()

reminderDatePickerButton.onTap ->
	showReminderDatePicker()

reminderRepeatButton.onTap ->
	showReminderRepeatPicker()

reminderTimePickerButton.onTap ->
	showReminderTimePicker()
	
datePicker.onTap ->
	this.visible = false

timePicker.onTap ->
	this.visible = false

reminderCreateScreen.addSubLayer(reminderRepeatPicker)
reminderRepeatPicker.visible = false
reminderRepeatPicker.x = 0
reminderRepeatPicker.y = 0
reminderRepeatPicker.onTap ->
	this.visible = false
	
# Reminder menu

remindersMenuText1.style = styles.reminderMenu
remindersMenuText1.style.textAlign = "left"
remindersMenuText1.style.lineHeight = remindersMenuText1.height+"px"
remindersMenuText1.html = "Scheduled"

remindersMenuText2.style = styles.reminderMenu
remindersMenuText2.style.textAlign = "left"
remindersMenuText2.style.lineHeight = remindersMenuText2.height+"px"
remindersMenuText2.html = "Anytime"

remindersMenuText3.style = styles.reminderMenu
remindersMenuText3.style.textAlign = "left"
remindersMenuText3.style.lineHeight = remindersMenuText1.height+"px"
remindersMenuText3.html = "Completed"

remindersMenuText1.states =
		active:
			color: new Color(colors.gray800).alpha(1)
		inactive:
			color: new Color(colors.gray800).alpha(0.5)
			
remindersMenuText2.states =
		active:
			color: new Color(colors.gray800).alpha(1)
		inactive:
			color: new Color(colors.gray800).alpha(0.5)
			
remindersMenuText3.states =
		active:
			color: new Color(colors.gray800).alpha(1)
		inactive:
			color: new Color(colors.gray800).alpha(0.5)


remindersMenuText1.animate("active")
remindersMenuText2.animate("inactive")
remindersMenuText3.animate("inactive")

remindersMenuButton1.onTap ->
	
	remindersMenuText1.animate("active")
	remindersMenuText2.animate("inactive")
	remindersMenuText3.animate("inactive")
	
	remindersScheduled.visible = true
	remindersAnytime.visible = false
	remindersCompleted.visible = false

remindersMenuButton2.onTap ->
	
	remindersMenuText1.animate("inactive")
	remindersMenuText2.animate("active")
	remindersMenuText3.animate("inactive")
	
	remindersScheduled.visible = false
	remindersAnytime.visible = true
	remindersCompleted.visible = false

remindersMenuButton3.onTap ->
	
	remindersMenuText1.animate("inactive")
	remindersMenuText2.animate("inactive")
	remindersMenuText3.animate("active")
	
	remindersScheduled.visible = false
	remindersAnytime.visible = false
	remindersCompleted.visible = true


# Scroll component
remindersListContainerScroll = new ScrollComponent
	width: remindersListContainer.width
	height: remindersListContainer.height
	x: 0
	y: 0
	backgroundColor: colors.gray200
	parent: remindersListContainer
	name: "remindersListContainerScroll"
	scrollHorizontal: false

remindersListContainerScroll.content.draggable.overdrag = false

remindersScheduled.parent = remindersListContainerScroll.content
remindersScheduled.x = 0
remindersScheduled.y = 0
remindersScheduled.visible = true

remindersAnytime.parent = remindersListContainerScroll.content
remindersAnytime.x = 0
remindersAnytime.y = 0
remindersAnytime.visible = false

remindersCompleted.parent = remindersListContainerScroll.content
remindersCompleted.x = 0
remindersCompleted.y = 0
remindersCompleted.visible = false


reminderEditLayer.parent = remindersScheduled
reminderEditLayer.x = 970 + 24
reminderEditLayer.y = 16 + 44
reminderEditLayer.visible = false

tapReminderEdit = false
reminderEditButton.onTap ->
	tapReminderEdit = true
	reminderEditLayer.visible = true

remindersScreen.onTap ->
	if !tapReminderEdit
		reminderEditLayer.visible = false
	tapReminderEdit = false

reminderModalDelete.onTap ->
	showReminderDelete()

deleteReminderDismissButton.onTap ->
	flow.showPrevious()







# LEISURE

# Scroll component
leisureContainerScroll = new ScrollComponent
	width: general.screen_width
	height: leisureScrollContainer.height
	x: 0
	y: 0
# 	backgroundLayer: false
	parent: leisureScrollContainer
	name: "leisureContainerScroll"
	scrollVertical: false
# leisureContainerScroll.content.draggable.overdrag = false
leisureContent.x = 0 # Necessary...
leisureContent.y = 0 # Necessary...
leisureContent.parent = leisureContainerScroll.content
leisureContent.x = 0
leisureContent.y = 0








# Tele health

teleHealthVideoLayer = new VideoLayer
	video: "videos/teleHealthVideo.mp4"
	width: healthVideoContainer.width
	height: healthVideoContainer.height
	x: 0
	y: 0
	parent: healthVideoContainer

reallyPlayHealthVideo = () ->
	teleHealthVideoLayer.player.play()

playHealthVideo = () ->
	teleHealthVideoLayer.player.currentTime = 0
	setTimeout(reallyPlayHealthVideo, 200)
	playAudio("teleHealth")
	setTimeout(checkToEndHealthCall, 72000)

endHealthCall = () ->
	teleHealthVideoLayer.player.pause()
	showDashboard()

checkToEndHealthCall = () ->
	if teleHealthVideoLayer.player.currentTime > 70 && flow.current == healthScreen
		endHealthCall()

teleHealthEndButton.onTap ->
	endHealthCall()
	
		


	






# Video Call

videoCallVideoLayer = new VideoLayer
	video: "videos/videoCallVideo.mp4"
	width: videoCallVideoContainer.width
	height: videoCallVideoContainer.height
	x: 0
	y: 0
	parent: videoCallVideoContainer

reallyPlayVideoCallVideo = () ->
	videoCallVideoLayer.player.play()
	
playVideoCallVideo = () ->
	videoCallVideoLayer.player.currentTime = 0
	setTimeout(reallyPlayVideoCallVideo, 200)
	playAudio("videoCall")
	setTimeout(checkToEndVideoCall, 33000)

endVideoCall = () ->
	videoCallVideoLayer.player.pause()
	showDashboard()

checkToEndVideoCall = () ->
	if videoCallVideoLayer.player.currentTime > 30 && flow.current == videoCallScreen
		endVideoCall()

videoCallEndButton.onTap ->
	endVideoCall()








# SOS
SOSTimeLabel.style = styles.callTime
SOSTimeLabel.style.textAlign = "center"
SOSTimeLabel.style.lineHeight = SOSTimeLabel.height+"px"
SOSTimeLabel.color = colors.whiteColor

SOSRotateAnim = new Animation
	layer: SOSSpinnerLayer
	properties:
		rotationZ: -360
	time: 1
	curve: Bezier.linear

SOSRotateAnim.start() # kick if off once

SOSRotateAnim.on "end", ->
	SOSSpinnerLayer.rotationZ = 0 # need to reset to zero so we can animate to 360 again
	SOSRotateAnim.start()

SOSEndCallButton.onTap ->
	endSOSCall()

startSOSCall = () ->
	playAudio("SOSAudio")
	general.callStartDate = new Date
	general.onACall = true
	setTimeout(checkToEndSOSCall, 28000)

endSOSCall = () ->
	general.onACall = false
	showDashboard()

checkToEndSOSCall = () ->
	if flow.current == SOSCall
		endVideoCall()

SOSCancelButton.onTap ->
	backButton.emit Events.Tap




# SOCIAL TALK

if general.loadSocialTalk
	
	socialTalkMain.width = general.screen_width
	socialTalkMain.height = general.screen_height
	
	connectingScreen.width = general.screen_width
	connectingScreen.height = general.screen_height
	
	editProfileButton.onClick ->
		showEditProfile()
		
		
	# Scroll component
	happeningNowScroll = new ScrollComponent
		width: happeningNowScrollContainer.width
		height: happeningNowScrollContainer.height
		x: 0
		y: 0
# 		backgroundColor: colors.gray200
		parent: happeningNowScrollContainer
		name: "happeningNowScroll"
		scrollVertical: false
	
	happeningNowScroll.content.draggable.overdrag = false
	
	happeningNowContent.x = 0
	happeningNowContent.y = 0
	happeningNowContent.parent = happeningNowScroll.content
	happeningNowContent.x = 0
	happeningNowContent.y = 0
	happeningNowContent.visible = true
# 	
# 	# Scroll component
# 	socialTalkMainScroll = new ScrollComponent
# 		width: socialTalkMain.width - dashboardScheduleCalendar.width
# 		height: socialTalkMain.height - mainNavBar.height 
# 		backgroundColor: colors.stone400
# 		x: dashboardScheduleCalendar.width
# 		y: mainNavBar.heigh
# 		parent: socialTalkMain
# 		name: "socialTalkMainScroll"
# 		scrollHorizontal: false
# 	
# 	socialTalkMainScroll.content.draggable.overdrag = false
# 	
# 	dashboardScheduleCalendar.placeBefore(socialTalkMainScroll)
# 	
# 	# Content for scroll
# 	for j in [0...socialTalkContactsDictionary.length]
# 	
# 		card = cellSocialCall.copy()
# 		card.parent = socialTalkMainScroll.content
# 		card.width = socialTalkMainScroll.width
# 		card.height = 208
# 		card.x = 0
# 		card.y = 208 * j
# 		card.backgroundColor = colors.whiteColor
# 		card.name = "card " + j + " at tab " + i
# 	
# 		for child in card.children
# 			if child.name == "cellSocialCallPhoto"
# 				if general.loadImages
# 					child.image = imagesPathSuffix + "Avatars/" + socialTalkContactsDictionary[j]["photoName"]
# 			else if child.name == "cellSocialTalkName"
# 				child.style = styles.subhead
# 				child.style.color = colors.blackColor
# 				child.style.textAlign = "left"
# 				child.style.lineHeight = child.height+"px"
# 				child.html = socialTalkContactsDictionary[j]["name"]
# 			else if child.name == "cellSocialTalkDate"
# 				child.style = styles.paragraph
# 				child.style.color = colors.blackColor
# 				child.style.textAlign = "left"
# 				child.style.lineHeight = child.height+"px"
# 				child.html = socialTalkContactsDictionary[j]["lastConnectionDateString"]
# 			else if child.name == "cellSocialTalkStatus"
# 				child.style = styles.button
# 				if socialTalkContactsDictionary[j]["status"] == "Unavailable"
# 					child.style.color = colors.red600
# 				else if socialTalkContactsDictionary[j]["status"] == "Available"
# 					child.style.color = colors.green600
# 				child.style.textAlign = "right"
# 				child.style.lineHeight = child.height+"px"
# 				child.html = socialTalkContactsDictionary[j]["status"]
# 			else if child.name == "cellSocialTalkButton"
# 				if socialTalkContactsDictionary[j]["friend"]
# 					icon = iconPhone.copy()
# 					icon.parent = child
# 					icon.x = Align.center
# 					icon.y = Align.center
# 				else
# 					icon = iconAddContact.copy()
# 					icon.parent = child
# 					icon.x = Align.center
# 					icon.y = Align.center
					
					
					
	
	# Social talk Toggle
	
# 	toggleAvailable.style = styles.paragraph
# 	toggleAvailable.style.textAlign = "center"
# 	toggleAvailable.style.lineHeight = toggleAvailable.height+"px"
# 	toggleAvailable.html = "AVAILABLE"
# 	
# 	toggleUnavailable.style = styles.paragraph
# 	toggleUnavailable.style.textAlign = "center"
# 	toggleUnavailable.style.lineHeight = toggleUnavailable.height+"px"
# 	toggleUnavailable.html = "UNAVAILABLE"
# 	
# 	toggleAvailable.states =
# 		active:
# 			backgroundColor: colors.whiteColor
# 			color: colors.green600
# 		inactive:
# 			backgroundColor: colors.stone800
# 			color: colors.gray400
# 	
# 	toggleUnavailable.states =
# 		active:
# 			backgroundColor: colors.whiteColor
# 			color: colors.red600
# 		inactive:
# 			backgroundColor: colors.stone800
# 			color: colors.gray400
# 	
# 	toggleAvailable.onClick ->
# 		toggleAvailable.animate("active", {instant:true})
# 		toggleUnavailable.animate("inactive", {instant:true})
# 	
# 	toggleUnavailable.onClick ->
# 		toggleAvailable.animate("inactive", {instant:true})
# 		toggleUnavailable.animate("active", {instant:true})
# 	
# 	# Initial state
# 	toggleAvailable.animate("active", {instant:true})
# 	toggleUnavailable.animate("inactive", {instant:true})
	
	# SOCIAL TALK - EDIT PROFILE TABS
	
	editProfilePageComponent = new PageComponent
		parent: editProfileScreen
		width: general.screen_width
		height: general.screen_height
		backgroundColor: "#FFFFFF"
		scrollHorizontal: false
		scrollVertical: false
	
	socialTalkTabNavBarContainer = new Layer
		width: general.screen_width
		height: socialTalkTabNavBar.height
		parent: editProfilePageComponent
		y: flow.header.height
		x: 0
		backgroundColor: colors.stone200
	
	socialTalkTabNavBar.parent = socialTalkTabNavBarContainer
	socialTalkTabNavBar.y = 0
	socialTalkTabNavBar.x = socialTalkTabNavBarContainer.width / 2 - socialTalkTabNavBar.width/2
	
	# Create tabs
	
	editProfileTabNumber = 3
	socialTalkTabs = []
	currentSocialTalkPageIndex = 0
	
	socialTalkIndicator.states.state1 =
		x: 0
	socialTalkIndicator.states.state2 =
		x: socialTalkTab1.width
	socialTalkIndicator.states.state3 =
		x: socialTalkTab1.width + socialTalkTab2.width
	
	animateSocialTalkTabs = (tabNumber) ->
		# Indicator animation
		if tabNumber == 0
			socialTalkIndicator.animate "state1",
				time: 0.5
		else if tabNumber == 1
			socialTalkIndicator.animate "state2",
				time: 0.5
		else if tabNumber == 2
			socialTalkIndicator.animate "state3",
				time: 0.5
		# Tab style animation
		index = 0
		for tab in socialTalkTabs
			if index == tabNumber
				tab.animate("active")
			else
				tab.animate("inactive")
			index++
	
	for i in [0...editProfileTabNumber]
	
		# Creating tabs
		if i == 0
			socialTalkTab = socialTalkTab1
			socialTalkTab.html = "My Profile"
		else if i == 1
			socialTalkTab = socialTalkTab2
			socialTalkTab.html = "My Avatar"
		else if i == 2
			socialTalkTab = socialTalkTab3
			socialTalkTab.html = "My Availability"
	
		socialTalkTab.number = i
		socialTalkTab.style = styles.button
		socialTalkTab.style.textAlign = "center"
		socialTalkTab.style.lineHeight = socialTalkTab.height+"px"
		socialTalkTab.color = colors.blue800
	
		# Tab states
		socialTalkTab.states =
			active:
				opacity: 1
			inactive:
				opacity: 0.7
	
		# Pushing tab into tabs array
		socialTalkTabs.push (socialTalkTab)
	
		# Making it so that the tabs navigate to correct page
		socialTalkTab.onTap ->
			editProfilePageComponent.snapToPage(@page, true)
			currentSocialTalkPageIndex = @number
			animateSocialTalkTabs(currentSocialTalkPageIndex)
	
	editProfilePages = []
	
	lastAvatarChosen = avatarImageContainer11
	
	for i in [0...editProfileTabNumber]
	
		editProfilePage = new ScrollComponent
			backgroundColor: colors.whiteColor
			parent: editProfilePageComponent.content
			height: editProfilePageComponent.height
			width: editProfilePageComponent.width
			name: "editProfilePage" + i
			x: editProfilePageComponent.width * i
			y: 0
			scrollHorizontal: false
			scrollVertical: true
	
		editProfilePage.content.draggable.overdrag = false
		editProfilePage.content.draggable.bounce = false
	
		editProfilePage.number = i
	
		if i == 0
	
			socialTalkEditProfile.width = general.screen_width
			socialTalkEditProfile.x = 0
			socialTalkEditProfile.y = mainNavBar.height
			socialTalkEditProfile.parent = editProfilePage.content
			socialTalkEditProfile.x = 0
			socialTalkEditProfile.y = 0
	
# 			array = [inputField0, inputField1, inputField2]
# 	
# 			emptyInputFieldTexts = ["Enter Your Name", "", ""]
# 	
# 			for inputField, j in array
# 	
# 				inputField.html = inputField.children[0].text
# 				inputField.style.lineHeight = inputField.height+"px"
# 				inputField.style.textAlign = "left"
# 				inputField.number = j
# 	
# 				inputField.states =
# 					empty:
# 						style: styles.paragraph
# 						color: colors.gray200
# 						html: "&nbsp " + emptyInputFieldTexts[j]
# 					filled:
# 						style: styles.paragraph
# 						color: colors.blackColor
# 						html: "&nbsp " + inputField.children[0].text
# 	
# 				inputField.children[0].visible = false
# 	
# 				if inputField.number < 2
# 					inputField.animate("empty", {instant:true})
# 				else
# 					inputField.animate("filled", {instant:true})
# 	
# 				inputField.onTap ->
# 					if @number < 2
# 						this.stateCycle(["empty", "filled"], time:0)
	
			editProfileInfoCancelButton.onTap ->
				flow.showPrevious()
	
			editProfileInfoSaveButton.onTap ->
				flow.showPrevious()
	
		else if i == 1
			socialTalkEditInterests.width = general.screen_width
			socialTalkEditInterests.x = 0
			socialTalkEditInterests.y = mainNavBar.height
			socialTalkEditInterests.parent = editProfilePage.content
			socialTalkEditInterests.x = 0
			socialTalkEditInterests.y = 0
			
			for child in avatarsContainer.subLayers
				if child.name.indexOf("avatarImageContainer") == 0
# 					if general.loadImages
# 						child.image = imagesPathSuffix + "Avatars/avatar"+ child.name[child.name.length - 2] + child.name[child.name.length - 1] + ".jpg"
					
					if child == lastAvatarChosen
						child.opacity = 1
					else
						child.opacity = 0.5
					
					child.onTap ->
						
# 						if lastAvatarChosen.children[0]
# 							lastAvatarChosen.children[0].destroy()
# 	
# 						overlayLayer = avatarPhotoOverlay.copy()
# 						overlayLayer.parent = this
# 						overlayLayer.x = 0
# 						overlayLayer.y = 0
						lastAvatarChosen.opacity = 0.5
						this.opacity = 1
	
						lastAvatarChosen = this
	
# 			for layer, j in interestsWrapper.children
# 				if layer.name.indexOf("interest") == 0
# 					for child in layer.children
# 						if child.name == "photo"
# 							child.states =
# 								inactive:
# 									image: imagesPathSuffix + "Interests/" + protoFakeInfo.interests[j]["photoNameInactive"]
# 								active:
# 									image: imagesPathSuffix + "Interests/" + protoFakeInfo.interests[j]["photoNameActive"]
# 	# 						child.animate("inactive", {instant:true})
# 							if general.loadImages
# 								child.image = imagesPathSuffix + "Interests/" + protoFakeInfo.interests[j]["photoNameInactive"]
# 							child.onTap ->
# 								this.stateCycle(["active", "inactive"], time:0)
# 						else if child.name == "text"
# 							child.text = protoFakeInfo.interests[j]["text"]
	
			editProfileInterestsCancelButton.onTap ->
				flow.showPrevious()
	
			editProfileInterestsSaveButton.onTap ->
				flow.showPrevious()
	
		else if i == 2
		
			socialTalkEditAvailability.width = general.screen_width
			socialTalkEditAvailability.x = 0
			socialTalkEditAvailability.y = mainNavBar.height
			socialTalkEditAvailability.parent = editProfilePage.content
			socialTalkEditAvailability.x = 0
			socialTalkEditAvailability.y = 0
			
			availabilityCounter = 0
			
			for availability in availabilities.children
			
				availabilityCounter = availabilityCounter + 1
				
				availability.states =
					inactive:
						backgroundColor: colors.stone800
						borderWidth: 0
					active:
						backgroundColor: colors.whiteColor
						borderWidth: 2
						borderColor: colors.blue600
				
				for child in availability.children
					child.states =
						inactive:
							color: colors.gray400
						active:
							color: colors.blue600
					if availabilityCounter < 4
						child.animate("inactive", {instant:true})
					else 
						child.animate("active", {instant:true})
						
				if availabilityCounter < 4
						availability.animate("inactive", {instant:true})
					else 
						availability.animate("active", {instant:true})
						
				availability.onTap ->
					this.stateCycle(["active", "inactive"], time:0)
					for child in this.children
						child.stateCycle(["active", "inactive"], time:0)
	
# 			array = nightContainer.subLayers.concat dayContainer.subLayers
# 			array = array.concat morningContainer.subLayers
# 			array = array.concat toggleWeekDays.subLayers
# 	
# 			for layer in array
# 	
# 				if layer.name.indexOf("time") == 0 or layer.name.indexOf("day") == 0
# 	
# 					layer.children[0].visible = false
# 	
# 					layer.html = layer.children[0].text
# 					layer.style.lineHeight = layer.height+"px"
# 					layer.style.textAlign = "center"
# 	
# 					layer.states =
# 						inactive:
# 							backgroundColor: colors.stone800
# 							style: styles.paragraph
# 							color: colors.gray400
# 						active:
# 							backgroundColor: colors.whiteColor
# 							style: styles.tabActive
# 							color: colors.blue600
# 	
# 					layer.animate("inactive", {instant:true})
# 	
# 					layer.onTap ->
# 						this.stateCycle(["active", "inactive"], time:0)
	
			editProfileAvailabilityCancelButton.onTap ->
				flow.showPrevious()
	
			editProfileAvailabilitySaveButton.onTap ->
				flow.showPrevious()
	
		socialTalkTabs[i].page = editProfilePage
	
		editProfilePage.onSwipeLeft ->
			if general.swipeAnimation == false
				if general.showLogs
					print("editProfilePage.onSwipeLeft")
				editProfilePageComponent.snapToNextPage("right", animationOptions = time : 1000)
				if @number == 0
					currentSocialTalkPageIndex = 1
				else if @number == 1
					currentSocialTalkPageIndex = 2
				animateSocialTalkTabs(currentSocialTalkPageIndex)
			general.swipeAnimation = true
	
		editProfilePage.onSwipeRight ->
			if general.swipeAnimation == false
				if general.showLogs
					print("editProfilePage.onSwipeRight")
				editProfilePageComponent.snapToNextPage("left", animationOptions = time : 1000)
				if @number == 1
					currentSocialTalkPageIndex = 0
				else if @number == 2
					currentSocialTalkPageIndex = 1
				animateSocialTalkTabs(currentSocialTalkPageIndex)
			general.swipeAnimation = true
	
		editProfilePage.onSwipeLeftEnd ->
			general.swipeAnimation = false
			if general.showLogs
				print("editProfilePage.onSwipeLeftEnd")
	
		editProfilePage.onSwipeRightEnd ->
			general.swipeAnimation = false
			if general.showLogs
				print("editProfilePage.onSwipeRightEnd")
	
		editProfilePages.push(editProfilePage)
	
	# SOCIAL TALK - CALL
	
	findingFriendsCancelButton.onTap ->
		flow.showPrevious()
		showMainBar(true)
	
	connectingFriendsCancelButton.onTap ->
		flow.showPrevious()
		flow.showPrevious()
		showMainBar(true)
		silence()
	
# Social Talk Animations - Finding friends
	
	findingFriends = [findingFriend1, findingFriend2, findingFriend3, findingFriend4, findingFriend5, findingFriend6, findingFriend7, findingFriend8]
	
	currentFindingFriendIndex = 7
	
	resetFriendsStyle = () ->
		for findingFriend in findingFriends
			findingFriend.animate("inactive")
	
	for findingFriend in findingFriends
	
		findingFriend.states = 
			inactive: {scale:1, opacity:0.75}
			active: {scale: 1.33, opacity: 1}
	
	findingFriendsAnimation = () ->
		findingFriends[currentFindingFriendIndex].animate("inactive")
		currentFindingFriendIndex = currentFindingFriendIndex + 1
		if currentFindingFriendIndex >= 8
			currentFindingFriendIndex = 0
		findingFriends[currentFindingFriendIndex].animate("active")
		if flow.current == findingFriendsScreen
			setTimeout(findingFriendsAnimation, 500)
		else
			setTimeout(resetFriendsStyle, 1000)
			

# 		findingFriendsOval1.states.bigCircle
# 		time: 1.5
	
# 	findingFriendsOval1.states = 
# 		smallCircle: {scale:1, opacity:0.5}
# 		bigCircle: {scale: 1.3, opacity: 0}
# 	
# 	findingFriendsOval2.states = 
# 		smallCircle: {scale:1, opacity:0.25}
# 		bigCircle: {scale: 1.3, opacity: 0}
# 	
# 	findingFriendsOval3.states = 
# 		smallCircle: {scale:1, opacity:0.10}
# 		bigCircle: {scale: 1.3, opacity: 0}
# 	
# 	findingFriendsOval1GetBig = new Animation findingFriendsOval1,
# 		findingFriendsOval1.states.bigCircle
# 		time: 1.5
# 	
# 	findingFriendsOval2GetBig = new Animation findingFriendsOval2,
# 		findingFriendsOval2.states.bigCircle
# 		time: 1.5
# 	
# 	findingFriendsOval3GetBig = new Animation findingFriendsOval3,
# 		findingFriendsOval3.states.bigCircle
# 		time: 1.5
# 	
# 	findingFriendsOval1GetSmall = new Animation findingFriendsOval1,
# 		findingFriendsOval1.states.smallCircle
# 		time: 0
# 	
# 	findingFriendsOval2GetSmall = new Animation findingFriendsOval2,
# 		findingFriendsOval2.states.smallCircle
# 		time: 0
# 	
# 	findingFriendsOval3GetSmall = new Animation findingFriendsOval3,
# 		findingFriendsOval3.states.smallCircle
# 		time: 0
# 	
# 	findingFriendsOval1GetBig.on Events.AnimationEnd, ->
# 		findingFriendsOval1GetSmall.start()
# 	
# 	findingFriendsOval1GetSmall.on Events.AnimationEnd, ->
# 		findingFriendsOval1GetBig.start()
# 	
# 	findingFriendsOval2GetBig.on Events.AnimationEnd, ->
# 		findingFriendsOval2GetSmall.start()
# 	
# 	findingFriendsOval2GetSmall.on Events.AnimationEnd, ->
# 		findingFriendsOval2GetBig.start()
# 	
# 	findingFriendsOval3GetBig.on Events.AnimationEnd, ->
# 		findingFriendsOval3GetSmall.start()
# 	
# 	findingFriendsOval3GetSmall.on Events.AnimationEnd, ->
# 		findingFriendsOval3GetBig.start()
	
	# Social Talk Animations - Connecting
	
	wiggleLimit = 8
	wiggleCounter = 0
	
	connectingPhoneIcon.states = 
		center: {x: 15}
		left: {x: 10}
		right: {x: 20}
	
	connectingOval1.states = 
		smallCircle: {scale:1}
		bigCircle: {scale: 1.1}
	
	connectingOval2.states = 
		smallCircle: {scale:1, opacity:0.5}
		bigCircle: {scale: 1.2, opacity: 0}
	
	connectingPhoneIconCenter = new Animation connectingPhoneIcon,
		connectingPhoneIcon.states.center
		time: 0.07
	
	connectingPhoneIconLeft = new Animation connectingPhoneIcon,
		connectingPhoneIcon.states.left
		time: 0.07
	
	connectingPhoneIconRight = new Animation connectingPhoneIcon,
		connectingPhoneIcon.states.right
		time: 0.07
	
	connectingOval1GetBig = new Animation connectingOval1,
		connectingOval1.states.bigCircle
		time: 1
	
	connectingOval2GetBig = new Animation connectingOval2,
		connectingOval2.states.bigCircle
		time: 1
	
	connectingOval1GetSmall = new Animation connectingOval1,
		connectingOval1.states.smallCircle
		time: 1
	
	connectingOval2GetSmall = new Animation connectingOval2,
		connectingOval2.states.smallCircle
		time: 1
	
	startConnectingAnimations = () ->
		connectingPhoneIconRight.start()
	
	connectingPhoneIconLeft.on Events.AnimationEnd, ->
		if wiggleCounter > wiggleLimit
			wiggleCounter = 0
			connectingPhoneIconCenter.start()
			setTimeout(startConnectingAnimations, 1000)
		else
			wiggleCounter++
			connectingPhoneIconRight.start()
	
	connectingPhoneIconRight.on Events.AnimationEnd, ->
		connectingPhoneIconLeft.start()
	
	connectingPhoneIconRight.start()
	
	connectingOval1GetBig.on Events.AnimationEnd, ->
		connectingOval1GetSmall.start()
	
	connectingOval1GetSmall.on Events.AnimationEnd, ->
		connectingOval1GetBig.start()
	
	connectingOval2GetBig.on Events.AnimationEnd, ->
		connectingOval2GetSmall.start()
	
	connectingOval2GetSmall.on Events.AnimationEnd, ->
		connectingOval2GetBig.start()
	
	connectingOval1GetBig.start()
	connectingOval2GetBig.start()
	
	# Social Talk Animations - Talking
	
	socialTalkOval1.states = 
		smallCircle: {scale:1, opacity:0.2}
		bigCircle: {scale: 1.1, opacity: 0.05}
	
	socialTalkOval2.states = 
		smallCircle: {scale:1, opacity:0.2}
		bigCircle: {scale: 1.1, opacity: 0.05}
	
	socialTalkOval1GetBig = new Animation socialTalkOval1,
		socialTalkOval1.states.bigCircle
		time: 1.5
	
	socialTalkOval2GetBig = new Animation socialTalkOval2,
		socialTalkOval2.states.bigCircle
		time: 1.5
	
	socialTalkOval1GetSmall = new Animation socialTalkOval1,
		socialTalkOval1.states.smallCircle
		time: 0
	
	socialTalkOval2GetSmall = new Animation socialTalkOval2,
		socialTalkOval2.states.smallCircle
		time: 0
	
	socialTalkOval1GetBig.on Events.AnimationEnd, ->
		socialTalkOval1GetSmall.start()
	
	socialTalkOval1GetSmall.on Events.AnimationEnd, ->
		socialTalkOval1GetBig.start()
	
	socialTalkOval2GetBig.on Events.AnimationEnd, ->
		socialTalkOval2GetSmall.start()
	
	socialTalkOval2GetSmall.on Events.AnimationEnd, ->
		socialTalkOval2GetBig.start()
	
	# Social Talk Call Buttons
	
	socialTalkIncomingButtonEndCall.onTap ->
		general.onACall = false
		flow.showPrevious()
		incomingCall = false
		silence()
	
	socialTalkIncomingButtonAcceptCall.onTap ->
		showSocialTalkCallScreen()
	
	startSocialTalkCallButton.onTap ->
		showSocialTalkFriends()
	
	socialTalkButtonEndCall.onTap ->
		general.onACall = false
		if general.showLogs
			print("showNext(socialTalkMain)")
		flow.showNext(socialTalkMain, true)
		showMainBar(true)
		flow.showOverlayCenter(socialTalkFeedback)
		silence()
		feedbackTimeLabel.text = socialTalkTimeLabel.html
	
	socialTalkFeedbackReportButton.onTap ->
		flow.showPrevious()
		flow.showOverlayCenter(socialTalkFeedbackReportScreen)
	
	socialTalkFeedbackPositiveButton.onTap ->
		flow.showPrevious()
		flow.showOverlayCenter(socialTalkFeedbackPositiveScreen)
	
	socialTalkFeedbackPositiveButtonWrapper.onTap ->
		flow.showPrevious()
	
	socialTalkFeedbackReportButtonWrapper.onTap ->
		flow.showPrevious()
	
	socialTalkTimeLabel.style = styles.callTime
	socialTalkTimeLabel.style.textAlign = "center"
	socialTalkTimeLabel.style.lineHeight = socialTalkTimeLabel.height+"px"
	socialTalkTimeLabel.color = colors.whiteColor









# RADIO
if general.loadRadio
	# Radio Left
	
	radioLeft.height = general.screen_height - mainNavBar.height
	radioLeft.x = 0
	radioLeft.y = mainNavBar.height
	
	# Creating tab nav bar for page component
	radioTabNavBar.parent = radioLeft
	radioTabNavBar.x = 0
	radioTabNavBar.y = 0
	radioTabNavBar.width = radioLeft.width
	radioTab1.width = radioLeft.width/2
	radioTab2.width = radioLeft.width/2
	radioIndicator.width = radioLeft.width/2
	
	# Dashboard tab nav indicator states
	radioIndicator.states.state1 =
		x: 0
	radioIndicator.states.state2 =
		x: radioTab1.width
	
	radioTabs = []
	
	animateRadioTabs = (tabNumber) ->
		if tabNumber == 0
			radioIndicator.animate "state1",
				time: 0.5
		else if tabNumber == 1
			radioIndicator.animate "state2",
				time: 0.5
		index = 0
		for tab in radioTabs
			if index == tabNumber
				tab.animate("active")
			else
				tab.animate("inactive")
			index++
	
	radioPageComponent = new PageComponent
		width: radioLeft.width
		height: radioLeft.height
		backgroundColor: colors.whiteColor
		scrollVertical: false
		scrollHorizontal: false
		parent: radioLeft
		x: 0
		y: radioTabNavBar.height
	
	# Create each tab with each page content
	
	radioScrolls = []
	currentRadioPageIndex = 0
	currentRadioStationID = -1 # Will be set to 0 when it runs
	selectedRadioCell0 = 0
	selectedRadioCell1 = 0
	
	selectRadioCell = (number, soundState) ->
	
		if general.showLogs
			print("SelectRadioCell number: " + number + " SoundState: " + soundState)
	
		radioStationName.text = radioStationsDictionary[number]["name"]
		radioStationNumber.text = radioStationsDictionary[number]["number"]
		artistSongName.text = radioStationsDictionary[number]["artistSongName"]
		radioProtocolID = radioStationsDictionary[number]["audioID"]
	
		if selectedRadioCell0 != 0
			selectedRadioCell0.backgroundColor = colors.whiteColor
	
		if selectedRadioCell1 != 0
			selectedRadioCell1.backgroundColor = colors.whiteColor
	
		radioScroll0 = radioPageComponent.content.subLayersByName("radioScroll0")[0]
		cell0 = radioScroll0.content.subLayersByName("Radio" + number)[0]
		if cell0
			cell0.backgroundColor = new Color(colors.blue800).alpha(0.25)
			selectedRadioCell0 = cell0
	
		radioScroll1 = radioPageComponent.content.subLayersByName("radioScroll1")[0]
		cell1 = radioScroll1.content.subLayersByName("Radio" + number)[0]
		cell1.backgroundColor = new Color(colors.blue800).alpha(0.25)
		selectedRadioCell1 = cell1
	
		if radioStationsDictionary[number]["favorite"]
			iconStarActive.children[0].animate("active", {instant:true})
		else
			iconStarActive.children[0].animate("inactive", {instant:true})
	
		if currentRadioStationID != number
			if soundState
				playAudio(radioProtocolID.toString())
		currentRadioStationID = number
	
	createRadioCell = (scroll, radioID, cellNumber) ->
		
		if general.showLogs
			print("Create radio cell for radioID: " + radioID)
	
		card = cellRadio.copy()
		card.width = scroll.width - (144)
		card.x = 0
		card.y = 0
		card.parent = scroll.content
		card.x = 144
		card.cellNumber = radioID
		card.radioID = radioStationsDictionary[radioID]["id"]
		card.backgroundColor = colors.whiteColor
	
		if i == 0
			card.y = card.height * (cellNumber - 1)
			card.name = "Radio" + radioID
			card.favorite = true
		else if i == 1
			card.y = card.height * radioID
			card.name = "Radio" + radioID
			card.favorite = false
	
		card.onTap ->
			selectRadioCell(@cellNumber, true)
	
		if radioID == currentRadioStationID
			card.backgroundColor = new Color(colors.blue800).alpha(0.25)
			if i == 0
				selectedRadioCell0 = card
			else if i == 1
				selectedRadioCell1 = card
	
	
		for child in card.children
			if child.name == "radioStationNameLabel"
				child.text = radioStationsDictionary[radioID]["name"]
			if child.name == "radioStationNumberLabel"
				child.text = radioStationsDictionary[radioID]["number"]
			if child.name == "radioIconStar"
	
				child.children[0].states =
					active:
						backgroundColor: colors.yellow600
					inactive:
						backgroundColor: colors.stone800
	
				if radioStationsDictionary[radioID]["favorite"]
					child.children[0].animate("active", {instant:true})
				else
					child.children[0].animate("inactive", {instant:true})
	
				child.onTap ->
					radioStationsDictionary[radioID]["favorite"] = !radioStationsDictionary[radioID]["favorite"]
					@children[0].stateCycle(["inactive", "active"], time:0)
					updateRadioScrolls()
	
	sliderFavoritesValue = 0
	scrollFavoritesValue = 0
	sliderStationsValue = 0
	scrollStationsValue = 0
	
	updateRadioScrolls = () ->
	
		if general.showLogs
			print("updateRadioScrolls")
	
		for radioScroll, i in radioScrolls
	
			if i == 0
				sliderFavoritesValue = radioScroll.slider.value
				scrollFavoritesValue = radioScroll.content.y
			else
				sliderStationsValue = radioScroll.slider.value
				scrollStationsValue = radioScroll.content.y
	
			for layer in radioScroll.content.subLayers
				layer.destroy()
	
			# Content for scroll
			numberOfFavorites = 0
			for j in [0...radioStationsDictionary.length]
	
				if i == 0
					if !radioStationsDictionary[j]["favorite"]
						continue
					else
						numberOfFavorites++
	
				createRadioCell(radioScroll, j, numberOfFavorites)
	
			# Hide slider if not enough cells
			if i == 0
				if numberOfFavorites < 5
					radioScroll.slider.visible = false
				else
					radioScroll.slider.visible = true
					# Ghost cell
					createRadioCell(radioScroll, radioStationsDictionary.length - 1, numberOfFavorites + 1)
					radioScroll.slider.value = sliderFavoritesValue
					radioScroll.content.y = scrollFavoritesValue
			else
				if radioStationsDictionary.length < 5
					radioScroll.slider.visible = false
				else
					radioScroll.slider.visible = true
					radioScroll.slider.value = sliderStationsValue
					radioScroll.content.y = scrollStationsValue
	
	for i in [0...2]
	
		# Creating tabs
		if i == 0
			radioTab = radioTab1
			radioTab.html = "Favorites"
		else if i == 1
			radioTab = radioTab2
			radioTab.html = "Stations"
		radioTab.number = i
		radioTab.style = styles.subhead
		radioTab.style.textAlign = "center"
		radioTab.style.lineHeight = radioTab.height+"px"
		radioTab.color = colors.blue800
	
		# Tab states
		radioTab.states =
			active:
				opacity: 1
			inactive:
				opacity: 0.7
	
		radioTab.animate("inactive", {instant:true})
	
		# Pushing tab into tabs array
		radioTabs.push (radioTab)
	
		# Detect tab taps
		radioTab.onTap ->
			radioPageComponent.snapToPage(@page, true)
			currentRadioPageIndex = @number
			animateRadioTabs(currentRadioPageIndex)
	
		# Creating scrolling pages
		radioScroll = new ScrollComponent
			width: radioPageComponent.width
			height: radioPageComponent.height - radioTabNavBar.height
			backgroundColor: colors.whiteColor
			x: 0
			y: 0
			parent: radioPageComponent.content
			x: radioPageComponent.width * i
			y: 0
			name: "radioScroll" + i
			scrollHorizontal: false
		radioScroll.number = i
		radioScroll.content.draggable.overdrag = false
	
		radioScrolls.push(radioScroll)
		radioTab.page = radioScroll
	
		slider = new SliderComponent
			width: 10
			height: radioScroll.height - 2*(48 + 16)
			x: 68
			y: 48 + 16
			backgroundColor: colors.stone800
			parent: radioScroll
			min: 0
			max: 1
	
		radioScroll.slider = slider
	
		slider.knob.cornerRadius = 4
		slider.knob.width = 32
		slider.knob.height = 96
		slider.knob.backgroundColor = colors.gray800
	
		scrollerLinesCopy = scrollerLines.copy()
		scrollerLinesCopy.width = 24
		scrollerLinesCopy.height = 24
		scrollerLinesCopy.x = slider.knob.width /2 - 12
		scrollerLinesCopy.y = slider.knob.height /2 - 12
		scrollerLinesCopy.parent = slider.knob
		slider.knob.addSubLayer(scrollerLinesCopy)
	
		# Change slider and scroll values
		slider.onValueChange ->
			@parent.content.y =  (@parent.content.height - @parent.height) * (- this.value )
	
		radioScroll.onMove ->
			for radioScroll in radioScrolls
				if this.id == (radioScroll.id + 1)
					radioScroll.slider.value = - this.y / (radioScroll.content.height - radioScroll.height)
	
		radioScroll.onSwipeLeft ->
			if general.swipeAnimation == false
				if general.showLogs
					print("scroll.onSwipeLeft")
				radioPageComponent.snapToNextPage("right", animationOptions = time : 1000)
				if @number == 0
					currentRadioPageIndex = 1
				else if @number == 1
					currentRadioPageIndex = 2
				animateRadioTabs(currentRadioPageIndex)
			general.swipeAnimation = true
	
		radioScroll.onSwipeRight ->
			if general.swipeAnimation == false
				if general.showLogs
					print("scroll.onSwipeRight")
				radioPageComponent.snapToNextPage("left", animationOptions = time : 1000)
				if @number == 1
					currentRadioPageIndex = 0
				else if @number == 2
					currentRadioPageIndex = 1
				animateRadioTabs(currentRadioPageIndex)
			general.swipeAnimation = true
	
		radioScroll.onSwipeLeftEnd ->
			general.swipeAnimation = false
			if general.showLogs
				print("scroll.onSwipeLeftEnd")
	
		radioScroll.onSwipeRightEnd ->
			general.swipeAnimation = false
			if general.showLogs
				print("scroll.onSwipeRightEnd")
	
		updateRadioScrolls()
	
	# Making it so that tabs change on page change
	
	radioPageComponent.on "change:currentPage", (currentPage) ->
		currentDashboardPageIndex = radioPageComponent.horizontalPageIndex(currentPage)
		index = 0
		for tab in radioTabs
			if index == currentDashboardPageIndex
				tab.animate("active")
			else
				tab.animate("inactive")
			index++
	
	# Radio Right
	
	radioRight.height = general.screen_height - mainNavBar.height
	radioRight.x = general.screen_width - radioRight.width
	radioRight.y = mainNavBar.height
	
	containerNowPlaying.parent = radioRight
	containerNowPlaying.x = 0
	containerNowPlaying.y = 0
	
	iconStarActive.children[0].states =
		active:
			backgroundColor: colors.yellow600
		inactive:
			backgroundColor: colors.stone800
	
	iconStarActive.onTap ->
		if general.showLogs
			print("iconStarActive tapped")
		radioStationsDictionary[currentRadioStationID]["favorite"] = !radioStationsDictionary[currentRadioStationID]["favorite"]
		iconStarActive.children[0].stateCycle(["active", "inactive"], time:0)
	
		updateRadioScrolls()
	
	# Radio Toggle
	
	toggleAM.style = styles.paragraph
	toggleAM.style.textAlign = "center"
	toggleAM.style.lineHeight = toggleAM.height+"px"
	toggleAM.html = "AM"
	
	toggleFM.style = styles.paragraph
	toggleFM.style.textAlign = "center"
	toggleFM.style.lineHeight = toggleFM.height+"px"
	toggleFM.html = "FM"
	
	toggleAM.states =
		active:
			backgroundColor: colors.whiteColor
			color: colors.blue600
		inactive:
			backgroundColor: colors.stone800
			color: colors.gray400
	
	toggleFM.states =
		active:
			backgroundColor: colors.whiteColor
			color: colors.blue600
		inactive:
			backgroundColor: colors.stone800
			color: colors.gray400
	
	toggleAM.onClick ->
		toggleFM.animate("inactive", {instant:true})
		toggleAM.animate("active", {instant:true})
	
	toggleFM.onClick ->
		toggleFM.animate("active", {instant:true})
		toggleAM.animate("inactive", {instant:true})
	
	# Initial state
	toggleFM.animate("active", {instant:true})
	toggleAM.animate("inactive", {instant:true})











# PHONE & CONTACTS
if general.loadPhone

	phoneButtonEndCall.onTap ->
		if general.showLogs
			print("Ending call")
		general.onACall = false
		showDashboard()
	
	phonePageComponent = new PageComponent
		parent: phoneCallScreen
		width: general.screen_width
		height: general.screen_height
		backgroundColor: "#FFFFFF"
		scrollHorizontal: false
		scrollVertical: false
	
	phoneTabNavBar.parent = phoneCallScreen
	phoneTabNavBar.y = mainNavBar.height
	phoneTabNavBar.x = 0
	
	# Create tabs
	
	phoneTabNumber = 5
	phoneTabs = []
	phoneInnerTabs = []
	currentPhonePageIndex = 1
	
	phoneIndicator.states.state1 =
		x: 0
	phoneIndicator.states.state2 =
		x: phoneTab1.width
	phoneIndicator.states.state3 =
		x: phoneTab1.width + phoneTab2.width
	phoneIndicator.states.state4 =
		x: phoneTab1.width + phoneTab2.width + phoneTab3.width
	phoneIndicator.states.state5 =
		x: phoneTab1.width + phoneTab2.width + phoneTab3.width + phoneTab4.width
	
	animatePhoneTabs = (tabNumber) ->
		# Indicator animation
		if tabNumber == 0
			phoneIndicator.animate "state1",
				time: 0.5
		else if tabNumber == 1
			phoneIndicator.animate "state2",
				time: 0.5
		else if tabNumber == 2
			phoneIndicator.animate "state3",
				time: 0.5
		else if tabNumber == 3
			phoneIndicator.animate "state4",
				time: 0.5
		else if tabNumber == 4
			phoneIndicator.animate "state5",
				time: 0.5
		# Tab style animation
		index = 0
		for tab in phoneTabs
			if index == tabNumber
				tab.animate("active")
			else
				tab.animate("inactive")
			index++
	
	for i in [0...phoneTabNumber]
	
		# Creating tabs
		if i == 0
			phoneTab = phoneTab1
			phoneInnerTab1.html = "Contacts"
			phoneInnerTab = phoneInnerTab1
		else if i == 1
			phoneTab = phoneTab2
			phoneInnerTab2.html = "Recents"
			phoneInnerTab = phoneInnerTab2
		else if i == 2
			phoneTab = phoneTab3
			phoneInnerTab3.html = "Favorites"
			phoneInnerTab = phoneInnerTab3
		else if i == 3
			phoneTab = phoneTab4
			phoneInnerTab4.html = "Dialer"
			phoneInnerTab = phoneInnerTab4
		else if i == 4
			phoneTab = phoneTab5
			phoneInnerTab5.html = "Messages"
			phoneInnerTab = phoneInnerTab5
	
		phoneTab.number = i
		phoneInnerTab.style = styles.button
		phoneInnerTab.style.textAlign = "left"
		phoneInnerTab.style.lineHeight = phoneTab.height+"px"
		phoneInnerTab.color = colors.blue800
	
			# Tab states
		phoneTab.states =
			active:
				opacity: 1
			inactive:
				opacity: 0.7
	
		# Pushing tab into tabs array
		phoneTabs.push (phoneTab)
		phoneInnerTabs.push (phoneInnerTab)
	
		# Making it so that the tabs navigate to correct page
		phoneTab.onTap ->
			phonePageComponent.snapToPage(@page, true)
			currentPhonePageIndex = @number
			animatePhoneTabs(currentPhonePageIndex)
	
	phonePages = []
	
	callAnswered = () ->
		
		if flow.current == phoneMainTalking
			if general.showLogs
				print("Starting call")
				
			general.onACall = true
			general.callStartDate = new Date
			
			playAudio("call")
	
	cellMobilePhoneContainer.onTap ->
		showPhoneCall()
	
	createPhoneCell = (scroll, cellNumber,contactIndex, type) ->
	
		if type == 0
	
			cell = cellContact.copy()
	
			for child in cell.children
	
				if child.name == "cellContactOpenFrame"
					for grandson in child.children
						if grandson.name == "cellContactName"
							if cellNumber > 0
								grandson.html = phoneContactsDictionary[contactIndex]["name"]
							else
								grandson.html = "Create New Contact"
							grandson.style = styles.subhead
							grandson.style.lineHeight = grandson.height+"px"
							grandson.style.textAlign = "left"
							grandson.style.color = colors.blackColor
						else if grandson.name == "cellContactInitial"
							if cellNumber > 0
								grandson.html = phoneContactsDictionary[contactIndex]["initial"]
							else
								grandson.html = ""
							grandson.style = styles.subhead
							grandson.style.color = colors.gray600
							grandson.style.lineHeight = grandson.height+"px"
							grandson.style.textAlign = "center"
						else if grandson.name == "cellContactPhoto"
							if cellNumber > 0
								photoName = phoneContactsDictionary[contactIndex]["photoName"]
								if photoName == ""
									photoColorIndex = cellNumber % colors.phoneContactBackgroundColors.length
									color = colors.phoneContactBackgroundColors[photoColorIndex]
									grandson.backgroundColor = color
									grandson.children[0].text = phoneContactsDictionary[contactIndex]["nameInitials"]
									grandson.children[0].style.textAlign = "center"
								else
									if general.loadImages
										grandson.image = imagesPathSuffix + "PhoneContacts/" + photoName
									grandson.children[0].text = ""
							else
								if general.loadImages
									grandson.image = imagesPathSuffix + "PhoneContacts/" + "image00.png"
								grandson.children[0].text = ""
					if cellNumber > 0
						child.onTap ->
							flow.showOverlayBottom(phoneContactInfoCard, true)
				else if child.name == "cellContactStarIcon"
					if cellNumber == 0
						child.children[0].visible = false
					else if phoneContactsDictionary[contactIndex]["favorite"]
						child.children[0].color = colors.yellow600
					else
						child.children[0].color = colors.stone800
				else if child.name == "cellContactCallButton"
					if cellNumber > 0
						child.onTap ->
							flow.showOverlayCenter(phoneSelectModal, true)
					else
						child.visible = false
	
		else if type == 1
	
			cell = cellContactRecent.copy()
	
			for child in cell.children
	
				if child.name == "cellContactRecentName"
					child.html = phoneContactsDictionary[contactIndex]["name"]
					child.style = styles.subhead
					if phoneContactsDictionary[contactIndex]["lastCallType"] == 2
						child.style.color = colors.red600
					else
						child.style.color = colors.blackColor
					child.style.lineHeight = child.height+"px"
					child.style.textAlign = "left"
				else if child.name == "cellContactRecentPhoto"
					photoName = phoneContactsDictionary[contactIndex]["photoName"]
					if photoName == ""
						photoColorIndex = contactIndex % colors.phoneContactBackgroundColors.length
						color = colors.phoneContactBackgroundColors[photoColorIndex]
						child.backgroundColor = color
						child.children[0].text = phoneContactsDictionary[contactIndex]["nameInitials"]
						child.children[0].style.textAlign = "center"
					else
						child.children[0].text = ""
						if general.loadImages
							child.image = imagesPathSuffix + "PhoneContacts/" + photoName
				else if child.name == "cellContactRecentDate"
					child.html = phoneContactsDictionary[contactIndex]["lastCalledDateString"]
					child.style = styles.paragraph
					child.style.color = colors.blackColor
					child.style.lineHeight = child.height+"px"
					child.style.textAlign = "left"
				else if child.name == "cellContactRecentCallType"
					if phoneContactsDictionary[contactIndex]["lastCallType"] == 1
						icon = iconOutgoingCall.copy()
						icon.parent = child
						icon.x = Align.center
						icon.y = Align.center
					else if phoneContactsDictionary[contactIndex]["lastCallType"] == 2
						icon = iconIncomingCall.copy()
						icon.parent = child
						icon.x = Align.center
						icon.y = Align.center
					else if phoneContactsDictionary[contactIndex]["lastCallType"] == 3
						icon = iconMissedCall.copy()
						icon.parent = child
						icon.x = Align.center
						icon.y = Align.center
				else if child.name == "cellContactRecentStarIcon"
					if phoneContactsDictionary[contactIndex]["favorite"]
						child.children[0].color = colors.yellow600
					else
						child.children[0].color = colors.stone800
				else if child.name == "cellRecentCallButton"
					child.onTap ->
						flow.showOverlayCenter(phoneSelectModal, true)
	
		else if type == 2
	
			cell = cellContactFavorite.copy()
	
			for child in cell.children
	
				if child.name == "cellContactFavoriteName"
					child.html = phoneContactsDictionary[contactIndex]["name"]
					child.style = styles.subhead
					child.style.lineHeight = child.height+"px"
					child.style.textAlign = "left"
					child.style.color = colors.blackColor
				else if child.name == "cellContactFavoritePhoto"
					photoName = phoneContactsDictionary[contactIndex]["photoName"]
					if photoName == ""
						photoColorIndex = contactIndex % colors.phoneContactBackgroundColors.length
						color = colors.phoneContactBackgroundColors[photoColorIndex]
						child.backgroundColor = color
						child.children[0].text = phoneContactsDictionary[contactIndex]["nameInitials"]
						child.children[0].style.textAlign = "center"
					else
						if general.loadImages
							child.image = imagesPathSuffix + "PhoneContacts/" + photoName
						child.children[0].text = ""
				else if child.name == "cellFavoriteCallButton"
					child.onTap ->
						flow.showOverlayCenter(phoneSelectModal, true)
						
	
		cell.width = scroll.width
		cell.parent = scroll.content
		cell.x = 0
		cell.cellNumber = j
	
		cell.y = cell.height * (cellNumber) + phoneTabNavBar.height
		cell.name = "Phone cell " + (cellNumber)
	
	phoneFavoritesCounter = 0
	phoneLastCallCounter = 0
	
	for i in [0...phoneTabNumber]
	
		phonePage = new ScrollComponent
			backgroundColor: colors.whiteColor
			parent: phonePageComponent.content
			height: phonePageComponent.height - mainNavBar.height
			width: phonePageComponent.width
			name: "phonePage" + i
			x: phonePageComponent.width * (i)
			y: 0
			scrollHorizontal: false
			scrollVertical: true
	
		phonePage.content.draggable.overdrag = false
		phonePage.content.draggable.bounce = false
	
		phonePage.number = i
	
		if i == 0
			# Create contact cell
			createPhoneCell(phonePage, 0, null, 0)
			for j in [0...phoneContactsDictionary.length]
				createPhoneCell(phonePage, j+1, j, 0)
			# Create ghost cell
			createPhoneCell(phonePage, phoneContactsDictionary.length + 1, 0, 0)
		else if i == 1
			for j in [0...phoneContactsDictionary.length]
				if phoneContactsDictionary[j]["lastCallType"] != 0
					createPhoneCell(phonePage, phoneLastCallCounter, j, 1)
					phoneLastCallCounter++
			# Create ghost cell
			createPhoneCell(phonePage, phoneLastCallCounter, 0, 1)
		else if i == 2
			for j in [0...phoneContactsDictionary.length]
				if phoneContactsDictionary[j]["favorite"]
					createPhoneCell(phonePage, phoneFavoritesCounter, j, 2)
					phoneFavoritesCounter++
			# Create ghost cell
			createPhoneCell(phonePage, phoneFavoritesCounter, 0, 2)
		else if i == 3
	
			phoneDialer.width = general.screen_width
			phoneDialer.x = 0
			phoneDialer.y = mainNavBar.height
			phoneDialer.parent = phonePage.content
			phoneDialer.x = 0
			phoneDialer.y = 0
	
			phonePage.scrollVertical = false
	
			addNumberButton.onTap ->
				if dialerNumberContainer.html != ""
					flow.showOverlayCenter(phoneAddModal, true)
	
			addNumberCancelButton.onTap ->
				flow.showPrevious()
	
			dialerNumberContainer.style = styles.numberStyle
			dialerNumberContainer.style.textAlign = "center"
			dialerNumberContainer.style.lineHeight = dialerNumberContainer.height+"px"
			dialerNumberContainer.color = colors.blackColor
	
			dialerNumbersContainer.onTap ->
				dialerNumberContainer.html = "860 - 068 - 1998"
			
			phoneDialerCallButton.onTap ->
				showPhoneCall()
				
		else if i == 4
	
			phoneMessages.width = general.screen_width
			phoneMessages.x = 0
			phoneMessages.y = mainNavBar.height
			phoneMessages.parent = phonePage.content
			phoneMessages.x = 0
			phoneMessages.y = 0
			
# 			chatLayer.x = 0
# 			chatLayer.y = 0
			chatLayer.addSubLayer(newChatLayer)
			newChatLayer.x = 0
			newChatLayer.y = 0
			newChatLayer.visible = false
			
			startChatLayer.onTap ->
				newChatLayer.visible = true
			
			backChatButton.onTap ->
				newChatLayer.visible = false
			
			phonePage.scrollVertical = false

	
		phoneTabs[i].page = phonePage
	
		phonePage.onSwipeLeft ->
			if general.swipeAnimation == false
				if general.showLogs
					print("editProfilePage.onSwipeLeft")
				phonePageComponent.snapToNextPage("right", animationOptions = time : 1000)
				if @number == 0
					currentPhonePageIndex = 1
				else if @number == 1
					currentPhonePageIndex = 2
				else if @number == 2
					currentPhonePageIndex = 3
				else if @number == 3
					currentPhonePageIndex = 4
				animatePhoneTabs(currentPhonePageIndex)
			general.swipeAnimation = true
	
		phonePage.onSwipeRight ->
			if general.swipeAnimation == false
				if general.showLogs
					print("editProfilePage.onSwipeRight")
				phonePageComponent.snapToNextPage("left", animationOptions = time : 1000)
				if @number == 1
					currentPhonePageIndex = 0
				else if @number == 2
					currentPhonePageIndex = 1
				else if @number == 3
					currentPhonePageIndex = 2
				else if @number == 4
					currentPhonePageIndex = 3
				animatePhoneTabs(currentPhonePageIndex)
			general.swipeAnimation = true
	
		phonePage.onSwipeLeftEnd ->
			general.swipeAnimation = false
			if general.showLogs
				print("phonePage.onSwipeLeftEnd")
	
		phonePage.onSwipeRightEnd ->
			general.swipeAnimation = false
			if general.showLogs
				print("phonePage.onSwipeRightEnd")
	
		phonePages.push(phonePage)
	
	phoneContactInfoCardEditButton.onTap ->
		flow.showPrevious(false)
		flow.showOverlayBottom(phoneEditContactCard, false)
	
	phoneEditContactContainerButtons.onTap ->
		flow.showPrevious(false)
		flow.showOverlayBottom(phoneContactInfoCard, false)
	
	profilePicture.onTap ->
		flow.showPrevious(false)
		flow.showOverlayBottom(phoneEditContactProfilePictureCard, false)
	
	photoEditContactProfilePictureContainerButtons.onTap ->
		flow.showPrevious(false)
		flow.showOverlayBottom(phoneEditContactCard, false)
	
	phoneMainTalkingTimeLabel.style = styles.callTime
	phoneMainTalkingTimeLabel.style.textAlign = "center"
	phoneMainTalkingTimeLabel.style.lineHeight = phoneMainTalkingTimeLabel.height+"px"
	phoneMainTalkingTimeLabel.color = colors.whiteColor
	
	# Phone - Contact Photo
	
	if general.loadImages
		phoneContactPhoto1.image = imagesPathSuffix + "Photos/img01.jpg"
		phoneContactPhoto2.image = imagesPathSuffix + "Photos/img02.jpg"
		phoneContactPhoto3.image = imagesPathSuffix + "Photos/img03.jpg"
		phoneContactPhoto4.image = imagesPathSuffix + "Photos/img04.jpg"
		phoneContactPhoto5.image = imagesPathSuffix + "Photos/img05.jpg"
		phoneContactPhoto6.image = imagesPathSuffix + "Photos/img06.jpg"
		









# PHOTOS
if general.loadPhotos

	selectedScrollPhoto = new Layer
		width: 0
		height: 0
	
	photoLayersArray = []
	
	photosGridScreen.x = 0
	photosGridScreen.y = 0
	
	# Scroll component
	photoScreenScroll = new ScrollComponent
		x: 0
		y: 0
		parent: photosGridScreen
		width: general.screen_width
		height: general.screen_height
		x: 0
		y: 0
		name: "photoScreenScroll"
		scrollHorizontal: false
	
	photoScreenScroll.content.draggable.overdrag = false
	
	# Add slider
	
	
	photoScreenSlider = new SliderComponent
		width: 10
		height: photoScreenScroll.height - 2*(48 + 16) - mainNavBar.height
		x: photoScreenScroll.width - 60
		y: mainNavBar.height + 48 + 16
		backgroundColor: colors.stone800
		parent: photoScreenScroll
		min: 0
		max: 1
	
	photoScreenScroll.slider = photoScreenSlider
	
	photoScreenSlider.knob.cornerRadius = 4
	photoScreenSlider.knob.width = 32
	photoScreenSlider.knob.height = 96
	photoScreenSlider.knob.backgroundColor = colors.gray800
	
	scrollerLinesCopy = scrollerLines.copy()
	scrollerLinesCopy.width = 24
	scrollerLinesCopy.height = 24
	scrollerLinesCopy.x = photoScreenSlider.knob.width /2 - 12
	scrollerLinesCopy.y = photoScreenSlider.knob.height /2 - 12
	scrollerLinesCopy.parent = photoScreenSlider.knob
	photoScreenSlider.knob.addSubLayer(scrollerLinesCopy)
	
	# Change slider and scroll values
	photoScreenSlider.onValueChange ->
		scrollValue = - (- mainNavBar.height + (@parent.content.height - @parent.height + mainNavBar.height) * this.value)
		@parent.content.y =  scrollValue
	
	photoScreenScroll.onMove ->
		sliderValue = - (this.y - mainNavBar.height) / (photoScreenScroll.content.height - photoScreenScroll.height + mainNavBar.height)
		photoScreenScroll.slider.value = sliderValue
	
	
	
	
	# Add photos
	
	currentDateString = 0
	currentGridPosition = 0
	numberOfMonths = 0
	lastYPosition = 0
	totalYPositions = 0
	xPosition = 0
	yPosition = 0
	
	for photo, i in photoGalleryDictionary
	
		if currentDateString != photo["dateString"]
	
			totalYPositions += lastYPosition + 1
	
			dateLayer = new Layer
				width: 600
				height: 64
				parent: photoScreenScroll.content
				x: 80
				y: 42 + (totalYPositions - 1)*392 + numberOfMonths*122
				html: photo["dateString"]
				backgroundColor: null
			dateLayer.style = styles.header4
			dateLayer.style.textAlign = "left"
			dateLayer.style.lineHeight = dateLayer.height+"px"
			dateLayer.color = colors.blackColor
	
			currentDateString = photo["dateString"]
			currentGridPosition = 0
	
			numberOfMonths++
	
		xPosition =  currentGridPosition %% 3
		yPosition = parseInt(currentGridPosition / 3)
		lastYPosition = yPosition

		photoLayer = new Layer
			width: 544
			height: 368
			parent: photoScreenScroll.content
			x: 80 + xPosition*568
			y: (totalYPositions + yPosition - 1)*392 + numberOfMonths*122 + 20
		photoLayer.number = i
		if general.loadImages
			photoLayer.image = imagesPathSuffix + "Photos/" + photo["imageName"]
	
		photoLayer.onTap ->

			if general.showLogs
				print("showNext(singlePhotoScreen)")
			selectedScrollPhoto = photoLayersArray[@number]
			selectedScrollPhoto.animate("selected", {instant:true})
			flow.showNext(singlePhotoScreen, animate: true)
			if general.loadImages
				singlePhotoScreen.image = @image
	
		currentGridPosition += 1
	
	# Add last white layer
	marginLayer = new Layer
		width: general.screen_width
		height: 80
		parent: photoScreenScroll.content
		x: 0
		y: photoLayer.y + photoLayer.height
		backgroundColor: null
	
	
	# SINGLE PHOTO
	
	tapFront = false
	
	bottomScrollContainer.onTap ->
		tapFront = true
	
	bottomScrollContainer.states =
		hide:
			y: general.screen_height
		show:
			y: general.screen_height - bottomScrollContainer.height
	
	singlePhotoScreen.onTap ->
		if !tapFront
			if mainNavBar.y == 0
				showMainBar(false)
				bottomScrollContainer.animate "hide",
					time: 0.5
			else
				showMainBar(true)
				bottomScrollContainer.animate "show",
					time: 0.5
		tapFront = false
	
	photosAllContainer.onTap ->
		flow.showPrevious()
	
	# Scroll component
	photoSingleScreenScroll = new ScrollComponent
		width: photoSingleScrollBase.width
		height: general.screen_height
		x: 0
		y: 0
		parent: photoSingleScrollBase
		name: "photoSingleScreenScroll"
		scrollVertical: false
	
	photoSingleScreenScroll.content.draggable.overdrag = false
	
	photoSingleMaskRight.onTap ->
		nothing = "happens"
	
	photoSingleMaskLeft.onTap ->
		nothing = "happens"
	
	photoSingleScrollBase.onTap ->
		nothing = "happens"
	
	# Add photos
	
	selectedScrollPhoto.states = 
		selected: {borderWidth:4, shadowBlur:8}
		unselected: {borderWidth: 0, shadowBlur: 0}
	
	for photo, i in photoGalleryDictionary
	
		photoLayer = new Layer
			width: 96
			height: 96
			parent: photoSingleScreenScroll.content
			x: 5 + 152 * i
			y: 24
			borderRadius: 4
			borderColor: colors.yellow600
		if general.loadImages
			photoLayer.image = imagesPathSuffix + "Photos/" + photo["imageName"]
	
		photoLayer.states =
			selected: {borderWidth:4, shadowBlur:8}
			unselected: {borderWidth: 0, shadowBlur: 0}
	
		photoLayer.animate("unselected", {instant:true})
	
		photoLayer.onTap ->
			if general.loadImages
				singlePhotoScreen.image = @image
			this.animate("selected", {instant:true})
			selectedScrollPhoto.animate("unselected", {instant:true})
			selectedScrollPhoto = this
	
		photoLayersArray.push(photoLayer)


# EXTRA - UPDATE TIME STRINGS

updateTimeStrings = () ->
	timeString = time.getTimeString()
	timeLabelContainer.html = timeString
	idleTimeFrame.html = timeString

updateTimeDifferenceStrings = () ->
	now = new Date
	difference = now - general.callStartDate
	timeString = time.getTimeDifferenceString(difference)
	phoneMainTalkingTimeLabel.html = timeString
	socialTalkTimeLabel.html = timeString
	SOSTimeLabel.html = timeString

# EXTRA - LAST INTERACTION

lastInteraction = () ->
	general.lastInteractionDate = new Date

flow.onTap ->
	lastInteraction()

checkInteraction = () ->
	now = new Date
	difference = now - general.lastInteractionDate
	if general.showLogs
		print("checkInteraction. Miliseconds difference: " + difference)
	if difference > 90000 and !general.proximityState
		showIdleScreen()

# EXTRA - TIMERS

timerShortFunction = () ->
	if general.onACall
		updateTimeDifferenceStrings()
	updateTimeStrings()
	checkInteraction()

timerMediumFunction = () ->
	checkFirebase()

timerLongFunction = () ->
	if general.lastIdleState == 2
		showIdleScreen1()
	else if general.lastIdleState == 1
		showIdleScreen2()

setInterval(timerShortFunction, 1000)
setInterval(timerMediumFunction, 20000)
setInterval(timerLongFunction, 30000)

# EXTRA - VOLUME

volumeSlider = new SliderComponent
   parent: volumeController2
   x: 48
   y: 168
   width: 768
   height: 8
   backgroundColor: colors.stone800
   max: 100
   min: 0

volumeSlider.sliderOverlay.off Events.TapStart
volumeSlider.knob.draggable = false
volumeSlider.knob.visible = false
volumeSlider.fill.backgroundColor = colors.blue600

volumeControllerIconOff = iconVolumeOff.copy()
volumeControllerIconOff.parent = volumeControllerIcon
volumeControllerIconOff.x = 0
volumeControllerIconOff.y = 0

volumeControllerIconOn = iconVolumeOn.copy()
volumeControllerIconOn.parent = volumeControllerIcon
volumeControllerIconOn.x = 0
volumeControllerIconOn.y = 0

volumeControllerIconOn.opacity = 1
volumeControllerIconOff.opacity = 0

# NAVIGATION

hardResetFramer = () ->
	if general.showLogs
		print("Reset")
	window.location.reload(false)

softResetFramer = () ->
	if general.showLogs
		print("SoftReset")
	passwordCounter = 0
	showDashboard() # Includes silence
	showMainBar(true)
	changeToDashboardMainBar()
	
showIdleScreen = () ->
	if general.showLogs
		print("showNext(idleScreen)")
	flow.showNext(idleScreen, animate: true)
	showMainBar(false)
	setIdle(true)
	silence()


showSettingsGoal = () ->
	if general.showLogs
		print("showNext(settingsGoal)")
	flow.showNext(settingsGoal, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Settings"
	setIdle(false)



showIdleScreen1 = () ->
	setTimeout(idleInfoOverlay1Appear.start, 1500)
	idleInfoOverlay2Disappear.start()
	general.lastIdleState = 1

showIdleScreen2 = () ->
	setTimeout(idleInfoOverlay2Appear.start, 1500)
	idleInfoOverlay1Disappear.start()
	general.lastIdleState = 2

showDashboard = () ->
	if general.showLogs
		print("showNext(dashboardPageComponent)")
	flow.showNext(dashboardPageComponent, animate: true)
	showMainBar(true)
	changeToDashboardMainBar()
	resetButton() # Set button to 0
	silence()
	setIdle(false)
	
showWeather = () ->
	if general.showLogs
		print("showNext(weatherScreen)")
	flow.showNext(weatherScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Weather"
	setIdle(false)

showHomeComfort = () ->
	if general.showLogs
		print("showNext(homeComfort)")
	flow.showNext(homeComfort, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Home Comfort"
	setIdle(false)

showPhysicalActivity = () ->
	if general.showLogs
		print("showNext(physicalActivityMainScreen)")
	flow.showNext(physicalActivityMainScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Physical Activity"
	setIdle(false)
	
showMentalActivity = () ->
	if general.showLogs
		print("showNext(mentalActivityMainScreen)")
	flow.showNext(mentalActivityMainScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Mental Activity"
	setIdle(false)

showCalendar = () ->
	if general.showLogs
		print("showNext(calendar)")
	flow.showNext(calendarScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Calendar"
	setIdle(false)
	
showCreateCalendar = () ->
	if general.showLogs
		print("showOverlayCenter(calendarCreateScreen)")
	flow.showOverlayBottom(calendarCreateScreen, true)

showCalendarDatePicker = () ->
	datePicker.parent = calendarCreateScreen
	datePicker.x = 0
	datePicker.y = 0
	datePicker.visible = true

showCalendarTimePicker = () ->
	timePicker.parent = calendarCreateScreen
	timePicker.x = 0
	timePicker.y = 0
	timePicker.visible = true
	
showReminders = () ->
	if general.showLogs
		print("showNext(remindersScreen)")
	flow.showNext(remindersScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Reminders"
	setIdle(false)
	
showCreateReminder = () ->
	if general.showLogs
		print("showOverlayCenter(reminderCreateScreen)")
	timePicker.visible = false
	datePicker.visible = false
	reminderRepeatPicker.visible = false
	flow.showOverlayBottom(reminderCreateScreen, true)
	
showReminderDatePicker = () ->
# 	reminderCreateScreen.addSubLayer(datePicker)
	datePicker.parent = reminderCreateScreen
# 	datePicker.visible = false
	datePicker.x = 0
	datePicker.y = 0
	datePicker.visible = true

showReminderTimePicker = () ->
# 	reminderCreateScreen.addSubLayer()
	timePicker.parent = reminderCreateScreen
	timePicker.x = 0
	timePicker.y = 0
	timePicker.visible = true
	
showReminderRepeatPicker = () ->
	reminderRepeatPicker.visible = true
	
showReminderDelete = () ->
	if general.showLogs
		print("showOverlayCenter(deleteReminderModal)")
	flow.showOverlayCenter(deleteReminderModal, true)



showPhotos = () ->
	if general.showLogs
		print("showNext(photosGridScreen)")
	flow.showNext(photosGridScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Photos"

showApps = () ->
	if general.showLogs
		print("showNext(appsMainScreen)")
	flow.showNext(appsMainScreen, animate: true)
	changeToSectionMainBar(true)
	navBarSectionName.html = "Apps"

showHealth = () ->
	
	playHealthVideo()
# 	setTimeout(playHealthVideo, 2000)
	
	if general.showLogs
		print("showNext(healthScreen)")
		
	flow.showNext(healthScreen, animate: true)
	showMainBar(false)
# 	changeToSectionMainBar(true)
# 	navBarSectionName.html = "Tele Health"
# 	healthScreen.y = -mainNavBar.height

showVideoCall = () ->
	
	playVideoCallVideo()
	
	if general.showLogs
		print("showNext(videoCallScreen)")
		
	flow.showNext(videoCallScreen, animate: true)
	showMainBar(false)
# 	changeToSectionMainBar(true)
# 	navBarSectionName.html = "Video Call"
# 	healthScreen.y = -mainNavBar.height
	
	
# 	youtubeLayer = new YouTubePlayer
# 		video: "FIsXGw6osKQ"
# 		width: healthScreen.width
# 		height: healthScreen.height
# 		playerVars: # see https://developers.google.com/youtube/player_parameters
# 			autoplay: 1
# 			controls: 0
# 	healthScreen.addSubLayer(youtubeLayer)
	
showSOS = () ->
	if general.showLogs
		print("showNext(SOSScreen)")
	flow.showNext(SOSScreen, animate: true)
	showMainBar(false)
	SOSTimerNumber.text = "3"
# 	print("YO")
	setTimeout(silence, 2800) # Prevent the 4th siren to sound
	setTimeout(showSOS2, 1000)
# 	print("YA")
	playAudio("SOSSiren")

showSOS2 = () ->
# 	print("2")
	SOSTimerNumber.text = "2"
	setTimeout(showSOS1, 1000)

showSOS1 = () ->
	SOSTimerNumber.text = "1"
	setTimeout(showSOSConnecting, 1000)

showSOSConnecting = () ->
	if flow.current == SOSScreen
		flow.showNext(SOSConnecting, animate: true)
		SOSTimeLabel.html = "00:00:00"
		setTimeout(showSOSCall, 5000)

showSOSCall = () ->
	if flow.current == SOSConnecting
		flow.showNext(SOSCall, animate: true)
		startSOSCall()



showSocialTalk = () ->
	if general.showLogs
		print("showNext(socialTalkMain)")
	flow.showNext(socialTalkMain, animate: true)
	changeToSectionMainBar(true)
	showMainBar(true)
	navBarSectionName.html = "Social Talk"
	silence()

showEditProfile = () ->
	if general.showLogs
		print("showNext(editProfileScreen)")
	flow.showNext(editProfileScreen, animate: true)
	currentSocialTalkPageIndex = 0
	animateSocialTalkTabs(currentSocialTalkPageIndex)

showPhone = () ->
	if general.showLogs
		print("showNext(phoneCallScreen)")
	flow.showNext(phoneCallScreen, animate: true)
	changeToSectionMainBar(false)
	showMainBar(true)
	navBarSectionName.html = "Phone"
	silence()
	# Preselect tab
	currentPhonePageIndex = 1
	phoneTabs[currentPhonePageIndex].emit Events.Tap

showPhoneCall = () ->
	if general.showLogs
			print("showNext(phoneMainTalking)")
	flow.showNext(phoneMainTalking, animate: false)
	showMainBar(false)
	playAudio("connecting")
	phoneMainTalkingTimeLabel.html = "Dialing"
	setTimeout(callAnswered, 2000)
	
showSocialTalkCallScreen = () ->
	
	if flow.current == connectingScreen or incomingCall == true
	
		incomingCall = false
	
		if general.showLogs
			print("showNext(socialTalkCallScreen)")
		showMainBar(false)
	
		general.callStartDate = new Date
		general.onACall = true
	
		flow.showNext(socialTalkCallScreen, animate: true)
		playAudio("socialTalkCall")
		socialTalkOval1GetBig.start()
		socialTalkOval2GetBig.start()

showSocialTalkFriends = () ->
	if general.showLogs
		print("showNext(findingFriendsScreen)")
	flow.showNext(findingFriendsScreen, animate: true)
	showMainBar(false)
# 		findingFriendsOval1GetBig.start()
# 		findingFriendsOval2GetBig.start()
# 		findingFriendsOval3GetBig.start()
	currentFindingFriendIndex = 7
	findingFriendsAnimation()
	setTimeout(showConnectingScreen, 7000)

showConnectingScreen = () ->

	if flow.current == findingFriendsScreen
		if general.showLogs
			print("showNext(connectingScreen)")
		flow.showNext(connectingScreen, animate: false)
		playAudio("connecting")
		setTimeout(showSocialTalkCallScreen, 8000)

showLeisure = () ->
	if general.showLogs
		print("showNext(leisureScreen)")
	flow.showNext(leisureScreen, animate: true)
	changeToSectionMainBar(true)
	showMainBar(true)
	navBarSectionName.html = "Leisure"
	silence()

showRadio = () ->
	if general.showLogs
		print("showNext(radioScreen)")
	flow.showNext(radioScreen, animate: true)
	changeToSectionMainBar(true)
	showMainBar(true)
	navBarSectionName.html = "Radio"
	selectRadioCell(0, true)



showIncomingCall = () ->
	if general.showLogs
		print("showOverlayCenter(socialTalkIincomingCallScreen)")
	incomingCall = true
	flow.showOverlayCenter(socialTalkIincomingCallScreen)
	playAudio("incoming")

hideVolume = () ->
# 	print("HIDING VOLUME")
	volumeController.visible = false
	showingVolumePopUp = false

showVolume = () ->
# 	print("SHOWING VOLUME")
	showingVolumePopUp = true
# 	if general.showLogs
# 		print("showOverlayCenter(volumeController)")
# 	flow.showOverlayCenter(volumeController, true)
	volumeController.x = 0
	volumeController.y = 0
	volumeController.bringToFront()
	volumeController.visible = true

# Configure navigation buttons

burgerButtonContainer.onTap ->
	if general.showLogs
		print("Show sideMenu")
	flow.showOverlayLeft(sideMenu)

menuBackButton.onTap ->
	flow.showPrevious() #dismiss

menuButton1.onClick ->
	dashboardPageComponent.snapToPage(dashboardComfort, true)
	currentDashboardPageIndex = 0
	animateDashboardTabs(currentDashboardPageIndex)
	if flow.previous == dashboardPageComponent
		flow.showPrevious() #dismiss
	else
		flow.showPrevious()
		showDashboard()

menuButton2.onClick ->
	dashboardPageComponent.snapToPage(dashboardActivity, true)
	currentDashboardPageIndex = 1
	animateDashboardTabs(currentDashboardPageIndex)
	if flow.previous == dashboardPageComponent
		flow.showPrevious() #dismiss
	else
		flow.showPrevious()
		showDashboard()

menuButton3.onClick ->
	dashboardPageComponent.snapToPage(dashboardSchedule, true)
	currentDashboardPageIndex = 2
	animateDashboardTabs(currentDashboardPageIndex)
	if flow.previous == dashboardPageComponent
		flow.showPrevious() #dismiss
	else
		flow.showPrevious()
		showDashboard()

menuButton4.onClick ->
	if flow.previous == photosGridScreen
		flow.showPrevious() #dismiss
	else
		flow.showPrevious()
		showPhotos()

menuButton5.onClick ->
	if flow.previous == appsMainScreen
		flow.showPrevious() #dismiss
	else
		flow.showPrevious()
		showApps()

menuButton6.onClick ->
	flow.showPrevious()
	hardResetFramer()

menuButton7.onClick ->
	flow.showPrevious()
	setTimeout(showIncomingCall, 2000)

#Making the first tabs active
animateDashboardTabs(currentDashboardPageIndex)
if general.loadSocialTalk
	animateSocialTalkTabs(currentSocialTalkPageIndex)
if general.loadRadio
	animateRadioTabs(currentRadioPageIndex)
# if general.loadRadio
# 	animatePhoneTabs(currentPhonePageIndex)
# 	phoneTab2.emit Events.Tap

# Firebase listeners

showingVolumePopUp = false

setupFirebase()

# Refresh firebase values
silence()
resetButton()
# First setup for time strings
updateTimeStrings()
# Initial navigation, must do both calls
showDashboard()
showIdleScreen()

if general.showLogs
	print("LAST LINE OF CODE")
