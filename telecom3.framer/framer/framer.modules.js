require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"FontFace":[function(require,module,exports){
exports.FontFace = (function() {
  var TEST, addFontFace, loadTestingFileError, missingArgumentError, removeTestLayer, testNewFace;

  TEST = {
    face: "monospace",
    text: "foo",
    time: .01,
    maxLoadAttempts: 50,
    hideErrorMessages: true
  };

  TEST.style = {
    width: "auto",
    fontSize: "150px",
    fontFamily: TEST.face
  };

  TEST.layer = new Layer({
    name: "FontFace Tester",
    width: 0,
    height: 1,
    maxX: -Screen.width,
    visible: false,
    html: TEST.text,
    style: TEST.style
  });

  function FontFace(options) {
    this.name = this.file = this.testLayer = this.isLoaded = this.loadFailed = this.loadAttempts = this.originalSize = this.hideErrors = null;
    if (options != null) {
      this.name = options.name || null;
      this.file = options.file || null;
    }
    if (!((this.name != null) && (this.file != null))) {
      return missingArgumentError();
    }
    this.testLayer = TEST.layer.copy();
    this.testLayer.style = TEST.style;
    this.testLayer.maxX = -Screen.width;
    this.testLayer.visible = true;
    this.isLoaded = false;
    this.loadFailed = false;
    this.loadAttempts = 0;
    this.hideErrors = options.hideErrors;
    return addFontFace(this.name, this.file, this);
  }

  addFontFace = function(name, file, object) {
    var faceCSS, styleTag;
    styleTag = document.createElement('style');
    faceCSS = document.createTextNode("@font-face { font-family: '" + name + "'; src: url('" + file + "') format('truetype'); }");
    styleTag.appendChild(faceCSS);
    document.head.appendChild(styleTag);
    return testNewFace(name, object);
  };

  removeTestLayer = function(object) {
    object.testLayer.destroy();
    return object.testLayer = null;
  };

  testNewFace = function(name, object) {
    var initialWidth, widthUpdate;
    initialWidth = object.testLayer._element.getBoundingClientRect().width;
    if (initialWidth === 0) {
      if (object.hideErrors === false || TEST.hideErrorMessages === false) {
        print("Load testing failed. Attempting again.");
      }
      return Utils.delay(TEST.time, function() {
        return testNewFace(name, object);
      });
    }
    object.loadAttempts++;
    if (object.originalSize === null) {
      object.originalSize = initialWidth;
      object.testLayer.style = {
        fontFamily: name + ", " + TEST.face
      };
    }
    widthUpdate = object.testLayer._element.getBoundingClientRect().width;
    if (object.originalSize === widthUpdate) {
      if (object.loadAttempts < TEST.maxLoadAttempts) {
        return Utils.delay(TEST.time, function() {
          return testNewFace(name, object);
        });
      }
      if (!object.hideErrors) {
        print("⚠️ Failed loading FontFace: " + name);
      }
      object.isLoaded = false;
      object.loadFailed = true;
      if (!object.hideErrors) {
        loadTestingFileError(object);
      }
      return;
    } else {
      if (!(object.hideErrors === false || TEST.hideErrorMessages)) {
        print("LOADED: " + name);
      }
      object.isLoaded = true;
      object.loadFailed = false;
    }
    removeTestLayer(object);
    return name;
  };

  missingArgumentError = function() {
    error(null);
    return console.error("Error: You must pass name & file properites when creating a new FontFace. \n\nExample: myFace = new FontFace name:\"Gotham\", file:\"gotham.ttf\" \n");
  };

  loadTestingFileError = function(object) {
    error(null);
    return console.error("Error: Couldn't detect the font: \"" + object.name + "\" and file: \"" + object.file + "\" was loaded.  \n\nEither the file couldn't be found or your browser doesn't support the file type that was provided. \n\nSuppress this message by adding \"hideErrors: true\" when creating a new FontFace. \n");
  };

  return FontFace;

})();


},{}],"YouTubePlayer":[function(require,module,exports){
var firstScriptTag, tag, youTubeReady,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

youTubeReady = new Promise(function(resolve, reject) {
  return window.onYouTubeIframeAPIReady = function() {
    return resolve();
  };
});

tag = document.createElement('script');

tag.src = 'https://www.youtube.com/iframe_api';

firstScriptTag = document.getElementsByTagName('script')[0];

firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

exports.YouTubePlayer = (function(superClass) {
  extend(YouTubePlayer, superClass);

  YouTubePlayer.Events = {
    Loaded: 'yt-loaded',
    Ready: 'yt-ready',
    StateChange: 'yt-stateChange',
    PlaybackQualityChange: 'yt-playbackQualityChange',
    PlaybackRateChange: 'yt-playbackRateChange',
    Error: 'yt-error',
    ApiChange: 'yt-apiChange'
  };

  function YouTubePlayer(options) {
    var div;
    if (options == null) {
      options = {};
    }
    div = document.createElement('div');
    this._playerReady = new Promise((function(_this) {
      return function(playerResolve, playerReject) {
        return youTubeReady.then(function() {
          _this._player = new YT.Player(div, {
            width: _this.width,
            height: _this.height,
            playerVars: options.playerVars,
            events: {
              'onReady': function(event) {
                playerResolve(event.target);
                return _this.emit(YouTubePlayer.Events.Ready, event);
              },
              'onStateChange': function(event) {
                return _this.emit(YouTubePlayer.Events.StateChange, event);
              },
              'onPlaybackQualityChange': function(event) {
                return _this.emit(YouTubePlayer.Events.PlaybackQualityChange, event);
              },
              'onPlaybackRateChange': function(event) {
                return _this.emit(YouTubePlayer.Events.PlaybackRateChange, event);
              },
              'onError': function(event) {
                playerReject(event.data);
                return _this.emit(YouTubePlayer.Events.Error, event);
              },
              'onApiChange': function(event) {
                return _this.emit(YouTubePlayer.Events.ApiChange, event);
              }
            }
          });
          _this.on("change:width", function() {
            return this._player.width = this.width;
          });
          return _this.on("change:height", function() {
            return this._player.height = this.height;
          });
        });
      };
    })(this));
    YouTubePlayer.__super__.constructor.call(this, options);
    this._element.appendChild(div);
  }

  YouTubePlayer.define("video", {
    get: function() {
      return this._video;
    },
    set: function(video) {
      this._video = video;
      return this._playerReady.then((function(_this) {
        return function() {
          var ref;
          _this._player.cueVideoById(video);
          if ((ref = _this.playerVars) != null ? ref.autoplay : void 0) {
            _this._player.playVideo();
          }
          return _this.emit(YouTubePlayer.Events.Loaded, _this._player);
        };
      })(this));
    }
  });

  YouTubePlayer.define("playerVars", {
    get: function() {
      return this._playerVars;
    },
    set: function(value) {
      return this._playerVars = value;
    }
  });

  return YouTubePlayer;

})(Layer);


},{}],"circleModule":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

exports.Circle = (function(superClass) {
  extend(Circle, superClass);

  Circle.prototype.currentValue = null;

  function Circle(options) {
    var base, base1, base2, base3, base4, base5, base6, base7, base8, counter, numberDuration, numberEnd, numberInterval, numberNow, numberStart, self, style;
    this.options = options != null ? options : {};
    if ((base = this.options).circleSize == null) {
      base.circleSize = 300;
    }
    if ((base1 = this.options).strokeWidth == null) {
      base1.strokeWidth = 24;
    }
    if ((base2 = this.options).strokeColor == null) {
      base2.strokeColor = "#fc245c";
    }
    if ((base3 = this.options).topColor == null) {
      base3.topColor = null;
    }
    if ((base4 = this.options).bottomColor == null) {
      base4.bottomColor = null;
    }
    if ((base5 = this.options).hasCounter == null) {
      base5.hasCounter = null;
    }
    if ((base6 = this.options).counterColor == null) {
      base6.counterColor = "#fff";
    }
    if ((base7 = this.options).counterFontSize == null) {
      base7.counterFontSize = 60;
    }
    if ((base8 = this.options).hasLinearEasing == null) {
      base8.hasLinearEasing = null;
    }
    this.options.value = 2;
    this.options.viewBox = this.options.circleSize + this.options.strokeWidth;
    Circle.__super__.constructor.call(this, this.options);
    this.backgroundColor = "";
    this.height = this.options.viewBox;
    this.width = this.options.viewBox;
    this.rotation = -90;
    this.pathLength = Math.PI * this.options.circleSize;
    this.circleID = "circle" + Math.floor(Math.random() * 1000);
    this.gradientID = "circle" + Math.floor(Math.random() * 1000);
    if (this.options.hasCounter !== null) {
      counter = new Layer({
        parent: this,
        html: "",
        width: this.width,
        height: this.height,
        backgroundColor: "",
        rotation: 90,
        color: this.options.counterColor
      });
      style = {
        textAlign: "center",
        fontSize: this.options.counterFontSize + "px",
        lineHeight: this.height + "px",
        fontWeight: "600",
        fontFamily: "-apple-system, Helvetica, Arial, sans-serif",
        boxSizing: "border-box",
        height: this.height
      };
      counter.style = style;
      numberStart = 0;
      numberEnd = 100;
      numberDuration = 2;
      numberNow = numberStart;
      numberInterval = numberEnd - numberStart;
    }
    this.html = "<svg viewBox='-" + (this.options.strokeWidth / 2) + " -" + (this.options.strokeWidth / 2) + " " + this.options.viewBox + " " + this.options.viewBox + "' >\n	<defs>\n	    <linearGradient id='" + this.gradientID + "' >\n	        <stop offset=\"0%\" stop-color='" + (this.options.topColor !== null ? this.options.bottomColor : this.options.strokeColor) + "'/>\n	        <stop offset=\"100%\" stop-color='" + (this.options.topColor !== null ? this.options.topColor : this.options.strokeColor) + "' stop-opacity=\"1\" />\n	    </linearGradient>\n	</defs>\n	<circle id='" + this.circleID + "'\n			fill='none'\n			stroke-linecap='round'\n			stroke-width      = '" + this.options.strokeWidth + "'\n			stroke-dasharray  = '" + this.pathLength + "'\n			stroke-dashoffset = '0'\n			stroke=\"url(#" + this.gradientID + ")\"\n			stroke-width=\"10\"\n			cx = '" + (this.options.circleSize / 2) + "'\n			cy = '" + (this.options.circleSize / 2) + "'\n			r  = '" + (this.options.circleSize / 2) + "'>\n</svg>";
    self = this;
    Utils.domComplete(function() {
      return self.path = document.querySelector("#" + self.circleID);
    });
    this.proxy = new Layer({
      opacity: 0,
      visible: false
    });
    this.proxy.on(Events.AnimationEnd, function(animation, layer) {
      return self.onFinished();
    });
    this.proxy.on('change:x', function() {
      var offset;
      offset = Utils.modulate(this.x, [0, 500], [self.pathLength, 0]);
      self.path.setAttribute('stroke-dashoffset', offset);
      if (self.options.hasCounter !== null) {
        numberNow = Utils.round(self.proxy.x / 5);
        return counter.html = numberNow;
      }
    });
    Utils.domComplete(function() {
      return self.proxy.x = 0.1;
    });
  }

  Circle.prototype.changeTo = function(value, time) {
    var customCurve;
    if (time === void 0) {
      time = 2;
    }
    if (this.options.hasCounter === true && this.options.hasLinearEasing === null) {
      customCurve = "linear";
    } else {
      customCurve = "ease-in-out";
    }
    this.proxy.animate({
      properties: {
        x: 500 * (value / 100)
      },
      time: time,
      curve: customCurve
    });
    return this.currentValue = value;
  };

  Circle.prototype.startAt = function(value) {
    this.proxy.animate({
      properties: {
        x: 500 * (value / 100)
      },
      time: 0.001
    });
    return this.currentValue = value;
  };

  Circle.prototype.hide = function() {
    return this.opacity = 0;
  };

  Circle.prototype.show = function() {
    return this.opacity = 1;
  };

  Circle.prototype.onFinished = function() {};

  return Circle;

})(Layer);


},{}],"colors":[function(require,module,exports){
exports.blackColor = "#1F1F1F";

exports.whiteColor = "#FFFFFF";

exports.gray800 = "#4B5161";

exports.gray600 = "#5A6175";

exports.gray400 = "#828DAB";

exports.gray200 = "#BEC6DA";

exports.stone800 = "#E1E5EA";

exports.stone600 = "#E7E9EF";

exports.stone400 = "#EFF2F6";

exports.stone200 = "#F1F4FA";

exports.blue800 = "#0095C2";

exports.blue600 = "#00B0E4";

exports.blue400 = "#68C8EE";

exports.blue200 = "#ADE8FF";

exports.green800 = "#56A479";

exports.green600 = "#5BCD8E";

exports.green400 = "#BAEED1";

exports.green200 = "#D6F6E4";

exports.red800 = "#B43737";

exports.red600 = "#E54D4D";

exports.red400 = "#FFCCCC";

exports.red200 = "#FFF0F0";

exports.yellow800 = "#EDB51D";

exports.yellow600 = "#FFC852";

exports.yellow400 = "#FED58C";

exports.yellow200 = "#FEEDCC";

exports.circleColor1 = "#FF7744";

exports.circleColor2 = "#918BE5";

exports.circleColor3 = "#4ECDC4";

exports.phoneContactBackgroundColors = ["#4DD0E1", "#9575CD", "#F06292", "#AED581", "#FFB74D", "#FF8A65", "#FFD54F", "#7986CB", "#4FC3F7"];


},{}],"firebase":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  slice = [].slice;

exports.Firebase = (function(superClass) {
  var parseArgs, request;

  extend(Firebase, superClass);

  Firebase.define("status", {
    get: function() {
      return this._status;
    }
  });

  function Firebase(options1) {
    var base, base1, base2;
    this.options = options1 != null ? options1 : {};
    this.projectID = (base = this.options).projectID != null ? base.projectID : base.projectID = null;
    this.secret = (base1 = this.options).secret != null ? base1.secret : base1.secret = null;
    this.debug = (base2 = this.options).debug != null ? base2.debug : base2.debug = false;
    if (this._status == null) {
      this._status = "disconnected";
    }
    this.secretEndPoint = this.secret ? "?auth=" + this.secret : "?";
    Firebase.__super__.constructor.apply(this, arguments);
    if (this.debug) {
      console.log("Firebase: Connecting to Firebase Project '" + this.projectID + "' ... \n URL: 'https://" + this.projectID + ".firebaseio.com'");
    }
    this.onChange("connection");
  }

  request = function(project, secret, path, callback, method, data, parameters, debug) {
    var options, r, url;
    url = "https://" + project + ".firebaseio.com" + path + ".json" + secret;
    if (parameters != null) {
      if (parameters.shallow) {
        url += "&shallow=true";
      }
      if (parameters.format === "export") {
        url += "&format=export";
      }
      switch (parameters.print) {
        case "pretty":
          url += "&print=pretty";
          break;
        case "silent":
          url += "&print=silent";
      }
      if (typeof parameters.download === "string") {
        url += "&download=" + parameters.download;
        window.open(url, "_self");
      }
      if (typeof parameters.orderBy === "string") {
        url += "&orderBy=" + '"' + parameters.orderBy + '"';
      }
      if (typeof parameters.limitToFirst === "number") {
        url += "&limitToFirst=" + parameters.limitToFirst;
      }
      if (typeof parameters.limitToLast === "number") {
        url += "&limitToLast=" + parameters.limitToLast;
      }
      if (typeof parameters.startAt === "number") {
        url += "&startAt=" + parameters.startAt;
      }
      if (typeof parameters.endAt === "number") {
        url += "&endAt=" + parameters.endAt;
      }
      if (typeof parameters.equalTo === "number") {
        url += "&equalTo=" + parameters.equalTo;
      }
    }
    if (debug) {
      console.log("Firebase: New '" + method + "'-request with data: '" + (JSON.stringify(data)) + "' \n URL: '" + url + "'");
    }
    options = {
      method: method,
      headers: {
        'content-type': 'application/json; charset=utf-8'
      }
    };
    if (data != null) {
      options.body = JSON.stringify(data);
    }
    r = fetch(url, options).then(function(res) {
      var json;
      if (!res.ok) {
        throw Error(res.statusText);
      }
      json = res.json();
      json.then(callback);
      return json;
    })["catch"]((function(_this) {
      return function(error) {
        return console.warn(error);
      };
    })(this));
    return r;
  };

  parseArgs = function() {
    var args, cb, i, l;
    l = arguments[0], args = 3 <= arguments.length ? slice.call(arguments, 1, i = arguments.length - 1) : (i = 1, []), cb = arguments[i++];
    if (typeof args[l - 1] === "object") {
      args[l] = args[l - 1];
      args[l - 1] = null;
    }
    return cb.apply(null, args);
  };

  Firebase.prototype.get = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [2].concat(slice.call(args), [(function(_this) {
      return function(path, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "GET", null, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.put = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "PUT", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.post = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "POST", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.patch = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "PATCH", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype["delete"] = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [2].concat(slice.call(args), [(function(_this) {
      return function(path, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "DELETE", null, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.onChange = function(path, callback) {
    var currentStatus, source, url;
    if (path === "connection") {
      url = "https://" + this.projectID + ".firebaseio.com/.json" + this.secretEndPoint;
      currentStatus = "disconnected";
      source = new EventSource(url);
      source.addEventListener("open", (function(_this) {
        return function() {
          if (currentStatus === "disconnected") {
            _this._status = "connected";
            if (callback != null) {
              callback("connected");
            }
            if (_this.debug) {
              console.log("Firebase: Connection to Firebase Project '" + _this.projectID + "' established");
            }
          }
          return currentStatus = "connected";
        };
      })(this));
      source.addEventListener("error", (function(_this) {
        return function() {
          if (currentStatus === "connected") {
            _this._status = "disconnected";
            if (callback != null) {
              callback("disconnected");
            }
            if (_this.debug) {
              console.warn("Firebase: Connection to Firebase Project '" + _this.projectID + "' closed");
            }
          }
          return currentStatus = "disconnected";
        };
      })(this));
      return;
    }
    url = "https://" + this.projectID + ".firebaseio.com" + path + ".json" + this.secretEndPoint;
    source = new EventSource(url);
    if (this.debug) {
      console.log("Firebase: Listening to changes made to '" + path + "' \n URL: '" + url + "'");
    }
    source.addEventListener("put", (function(_this) {
      return function(ev) {
        if (callback != null) {
          callback(JSON.parse(ev.data).data, "put", JSON.parse(ev.data).path, _.tail(JSON.parse(ev.data).path.split("/"), 1));
        }
        if (_this.debug) {
          return console.log("Firebase: Received changes made to '" + path + "' via 'PUT': " + (JSON.parse(ev.data).data) + " \n URL: '" + url + "'");
        }
      };
    })(this));
    return source.addEventListener("patch", (function(_this) {
      return function(ev) {
        if (callback != null) {
          callback(JSON.parse(ev.data).data, "patch", JSON.parse(ev.data).path, _.tail(JSON.parse(ev.data).path.split("/"), 1));
        }
        if (_this.debug) {
          return console.log("Firebase: Received changes made to '" + path + "' via 'PATCH': " + (JSON.parse(ev.data).data) + " \n URL: '" + url + "'");
        }
      };
    })(this));
  };

  return Firebase;

})(Framer.BaseClass);


},{}],"fonts":[function(require,module,exports){
var FontFace, ProximaNovaBlack, ProximaNovaBlackItalic, ProximaNovaBold, ProximaNovaBoldItalic, ProximaNovaExtrabold, ProximaNovaExtraboldItalic, ProximaNovaLight, ProximaNovaLightItalic, ProximaNovaMedium, ProximaNovaMediumItalic, ProximaNovaRegular, ProximaNovaRegularItalic, ProximaNovaSemiboldItalic, ProximaNovaThin, ProximaNovaThinItalic;

FontFace = require("FontFace").FontFace;

ProximaNovaBlackItalic = new FontFace({
  name: "ProximaNovaBlackItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Black Italic.otf"
});

ProximaNovaBlack = new FontFace({
  name: "ProximaNovaBlack",
  file: "Fonts/Proxima Nova/Proxima Nova Black.otf"
});

ProximaNovaBoldItalic = new FontFace({
  name: "ProximaNovaBoldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Bold Italic.otf"
});

ProximaNovaBold = new FontFace({
  name: "ProximaNovaBold",
  file: "Fonts/Proxima Nova/Proxima Nova Bold.otf"
});

ProximaNovaExtraboldItalic = new FontFace({
  name: "ProximaNovaExtraboldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Extrabold Italic.otf"
});

ProximaNovaExtrabold = new FontFace({
  name: "ProximaNovaExtrabold",
  file: "Fonts/Proxima Nova/Proxima Nova Extrabold.otf"
});

ProximaNovaLightItalic = new FontFace({
  name: "ProximaNovaLightItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Light Italic.otf"
});

ProximaNovaLight = new FontFace({
  name: "ProximaNovaLight",
  file: "Fonts/Proxima Nova/Proxima Nova Light.otf"
});

ProximaNovaMediumItalic = new FontFace({
  name: "ProximaNovaMediumItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Medium Italic.otf"
});

ProximaNovaMedium = new FontFace({
  name: "ProximaNovaMedium",
  file: "Fonts/Proxima Nova/Proxima Nova Medium.otf"
});

ProximaNovaRegularItalic = new FontFace({
  name: "ProximaNovaRegularItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Regular Italic.otf"
});

ProximaNovaRegular = new FontFace({
  name: "ProximaNovaRegular",
  file: "Fonts/Proxima Nova/Proxima Nova Regular.otf"
});

ProximaNovaSemiboldItalic = new FontFace({
  name: "ProximaNovaSemiboldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Semibold Italic.otf"
});

ProximaNovaRegular = new FontFace({
  name: "ProximaNovaSemibold",
  file: "Fonts/Proxima Nova/Proxima Nova Semibold.otf"
});

ProximaNovaThinItalic = new FontFace({
  name: "ProximaNovaThinItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Thin Italic.otf"
});

ProximaNovaThin = new FontFace({
  name: "ProximaNovaThin",
  file: "Fonts/Proxima Nova/Proxima Nova Thin.otf"
});


},{"FontFace":"FontFace"}],"general":[function(require,module,exports){
exports.callStartDate = new Date;

exports.lastIdleState = 1;

exports.lastInteractionDate = new Date;

exports.onACall = false;

exports.proximityState = false;

exports.showingIdle = true;

exports.swipeAnimation = false;

exports.language = "english";

exports.screen_height = Framer.Device.screen.height;

exports.screen_width = Framer.Device.screen.width;

exports.loadImages = true;

exports.loadTeleHealth = true;

exports.loadPhone = true;

exports.loadPhotos = true;

exports.loadSocialTalk = true;

exports.loadRadio = true;

exports.showLogs = false;


},{}],"protoFakeInfo":[function(require,module,exports){
exports.interests = [
  {
    photoNameActive: "icon01Active.png",
    photoNameInactive: "icon01Inactive.png",
    text: "Family"
  }, {
    photoNameActive: "icon02Active.png",
    photoNameInactive: "icon02Inactive.png",
    text: "Pets"
  }, {
    photoNameActive: "icon03Active.png",
    photoNameInactive: "icon03Inactive.png",
    text: "Travel"
  }, {
    photoNameActive: "icon04Active.png",
    photoNameInactive: "icon04Inactive.png",
    text: "Reading"
  }, {
    photoNameActive: "icon05Active.png",
    photoNameInactive: "icon05Inactive.png",
    text: "Gardening"
  }, {
    photoNameActive: "icon07Active.png",
    photoNameInactive: "icon07Inactive.png",
    text: "Cooking"
  }, {
    photoNameActive: "icon06Active.png",
    photoNameInactive: "icon06Inactive.png",
    text: "Outdoors"
  }, {
    photoNameActive: "icon08Active.png",
    photoNameInactive: "icon08Inactive.png",
    text: "Current Events"
  }, {
    photoNameActive: "icon09Active.png",
    photoNameInactive: "icon09Inactive.png",
    text: "Economy"
  }, {
    photoNameActive: "icon10Active.png",
    photoNameInactive: "icon10Inactive.png",
    text: "Art & Culture"
  }, {
    photoNameActive: "icon11Active.png",
    photoNameInactive: "icon11Inactive.png",
    text: "Movies"
  }, {
    photoNameActive: "icon12Active.png",
    photoNameInactive: "icon12Inactive.png",
    text: "Television"
  }, {
    photoNameActive: "icon13Active.png",
    photoNameInactive: "icon13Inactive.png",
    text: "Sports"
  }, {
    photoNameActive: "icon14Active.png",
    photoNameInactive: "icon14Inactive.png",
    text: "Science & Tech"
  }, {
    photoNameActive: "icon15Active.png",
    photoNameInactive: "icon15Inactive.png",
    text: "Fashion"
  }
];

exports.socialTalkContactsSpanish = [
  {
    photoName: "avatar12.jpg",
    name: "Miranda",
    lastConnectionDateString: "October 15 2018 | 47 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar08.jpg",
    name: "Olivia",
    lastConnectionDateString: "October 13 2018 | 98 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar02.jpg",
    name: "Simón",
    lastConnectionDateString: "October 9 2018 | 27 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar01.jpg",
    name: "Elías",
    lastConnectionDateString: "October 8 2018 | 42 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar15.jpg",
    name: "Dante",
    lastConnectionDateString: "October 7 2018 | 37 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar05.jpg",
    name: "Irene",
    lastConnectionDateString: "October 2 2018 | 47 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar04.jpg",
    name: "Luana",
    lastConnectionDateString: "September 30 2018 | 57 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar07.jpg",
    name: "Isabel",
    lastConnectionDateString: "September 27 2018 | 72 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar09.jpg",
    name: "Ana",
    lastConnectionDateString: "September 24 2018 | 53 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Dylan",
    lastConnectionDateString: "September 23 2018 | 36 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar10.jpg",
    name: "Ángel",
    lastConnectionDateString: "September 19 2018 | 27 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar14.jpg",
    name: "Martina",
    lastConnectionDateString: "September 16 2018 | 14 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar11.jpg",
    name: "Victoria",
    lastConnectionDateString: "September 12 2018 | 22 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar01.jpg",
    name: "Camila",
    lastConnectionDateString: "September 10 2018 | 39 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Sofia",
    lastConnectionDateString: "September 7 2018 | 10 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar06.jpg",
    name: "Tomas",
    lastConnectionDateString: "September 6 2018 | 27 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar08.jpg",
    name: "Joaquín",
    lastConnectionDateString: "September 3 2018 | 8 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar07.jpg",
    name: "Mía",
    lastConnectionDateString: "September 1 2018 | 48 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar11.jpg",
    name: "Lucas",
    lastConnectionDateString: "August 29 2018 | 18 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar13.jpg",
    name: "Natalia",
    lastConnectionDateString: "August 26 2018 | 53 minutes",
    status: "Available",
    friend: true
  }
];

exports.socialTalkContactsEnglish = [
  {
    photoName: "avatar01.jpg",
    name: "Henry",
    lastConnectionDateString: "October 15 2018 | 47 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar02.jpg",
    name: "Olivia",
    lastConnectionDateString: "October 13 2018 | 98 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Miranda",
    lastConnectionDateString: "October 12 2018 | 32 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar04.jpg",
    name: "Fred",
    lastConnectionDateString: "October 11 2018 | 57 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar05.jpg",
    name: "Hermine",
    lastConnectionDateString: "October 7 2018 | 48 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar06.jpg",
    name: "Leo",
    lastConnectionDateString: "October 6 2018 | 23 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar07.jpg",
    name: "Thomas",
    lastConnectionDateString: "October 5 2018 | 53 minutes",
    status: "Available",
    friend: true
  }
];

exports.radioStationsSpanish = [
  {
    name: "Europa FM",
    number: "91.00 FM",
    favorite: true,
    artistSongName: "Estopa en Levántate y Cárdenas",
    audioID: 1,
    id: 0
  }, {
    name: "Cadena Dial",
    number: "91.70 FM",
    favorite: true,
    artistSongName: "Ana Guerra y Manel Fuentes despiertan a todos los Atrevidos",
    audioID: 2,
    id: 1
  }, {
    name: "Radiole",
    number: "92.40 FM",
    favorite: true,
    artistSongName: "El Duende Callejero - La Jungla de Alquitrán",
    audioID: 3,
    id: 2
  }, {
    name: "Cadena COPE",
    number: "94.80 FM",
    favorite: false,
    artistSongName: "#COPEhaceHistoria en la radio española",
    audioID: 4,
    id: 3
  }, {
    name: "Onda Cero",
    number: "98.00 FM",
    favorite: true,
    artistSongName: "La última pregunta a David Calle, el profesor de Youtube",
    audioID: 5,
    id: 4
  }, {
    name: "Cadena 100",
    number: "99.50 FM",
    favorite: false,
    artistSongName: "Los niños y Jimeno - ¿Playa o montaña?",
    audioID: 6,
    id: 5
  }, {
    name: "Kiss FM",
    number: "102.70 FM",
    favorite: true,
    artistSongName: "KISS FM, lo mejor de los 80 y los 90 hasta hoy",
    audioID: 7,
    id: 6
  }, {
    name: "Cadena Ser",
    number: "105.40 FM",
    favorite: false,
    artistSongName: "Por qué soy feminista y voy a la huelga",
    audioID: 8,
    id: 7
  }, {
    name: "Ghost radio, figure it out",
    number: "110.5 FM",
    favorite: false,
    artistSongName: "artistSongName",
    audioID: 0,
    id: 8
  }
];

exports.radioStationsEnglish = [
  {
    name: "BBC",
    number: "94.9 FM",
    favorite: true,
    artistSongName: "Grimmy chats to Emma Watson",
    audioID: 15,
    id: 0
  }, {
    name: "Capital FM",
    number: "95.8 FM",
    favorite: true,
    artistSongName: "Martin Garrix Capital FM Radio Interview",
    audioID: 16,
    id: 1
  }, {
    name: "LBC",
    number: "97.3 FM",
    favorite: false,
    artistSongName: "James O'Brien Rails At Brexiteers Who Want To Leave At All Costs",
    audioID: 17,
    id: 2
  }, {
    name: "Magic Radio",
    number: "105.4 FM",
    favorite: true,
    artistSongName: "Take That talk spy gadgets with Magic in the Morning",
    audioID: 18,
    id: 3
  }, {
    name: "Talk Radio",
    number: "102.7 FM",
    favorite: true,
    artistSongName: "How has Amazon halved their UK corporation tax bill?",
    audioID: 19,
    id: 4
  }, {
    name: "Radio X",
    number: "104.9 FM",
    favorite: true,
    artistSongName: "Dom's 'Off the Cuff' Knowledge is Remarkable",
    audioID: 20,
    id: 5
  }, {
    name: "Ghost radio, figure it out",
    number: "110.5 FM",
    favorite: false,
    artistSongName: "artistSongName",
    audioID: 0,
    id: 6
  }
];

exports.phoneContactsSpanish = [
  {
    initial: "A",
    photoName: "",
    name: "Sofía Alcaldo",
    nameInitials: "SA",
    lastCalledDateString: "Outgoing Call | October 15 2018, 12:34pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "B",
    photoName: "",
    name: "Valentina Barbero",
    nameInitials: "VB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }
];

exports.phoneContactsEnglish = [
  {
    initial: "A",
    photoName: "",
    name: "Sophie Anderson",
    nameInitials: "SA",
    lastCalledDateString: "Outgoing Call | October 15 2018, 12:34pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "B",
    photoName: "",
    name: "Veronica Brown",
    nameInitials: "VB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "image03.png",
    name: "Steven Baker",
    nameInitials: "SB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "C",
    photoName: "image04.png",
    name: "Milton Cox",
    nameInitials: "MC",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "",
    photoName: "image05.png",
    name: "Mary Clark",
    nameInitials: "MC",
    lastCalledDateString: "Incoming Call | October 14 2018, 5:16pm",
    lastCallType: 2,
    favorite: true
  }, {
    initial: "",
    photoName: "",
    name: "Alexander Carter",
    nameInitials: "AC",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Daryl Coleman",
    nameInitials: "DC",
    lastCalledDateString: "Outgoing Call | October 14 2018, 10:07am",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Lisa Cook",
    nameInitials: "LC",
    lastCalledDateString: "Missed Call | October 13 2018, 2:30pm",
    lastCallType: 3,
    favorite: false
  }, {
    initial: "D",
    photoName: "image09.png",
    name: "Matthew Davis",
    nameInitials: "MD",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "T",
    photoName: "",
    name: "Alasdair Taylor",
    nameInitials: "AT",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "W",
    photoName: "image10.png",
    name: "Eliza Williams",
    nameInitials: "EW",
    lastCalledDateString: "Outgoing Call | October 9 2018, 11:11am",
    lastCallType: 1,
    favorite: true
  }
];

exports.photoGallery = [
  {
    imageName: "img01.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img02.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img03.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img04.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img05.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img06.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img07.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img08.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img09.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img10.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img11.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img12.jpg",
    dateString: "June 2018"
  }
];


},{}],"styles":[function(require,module,exports){
var colors;

colors = require('colors');

exports.header0 = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "144px"
};

exports.header1 = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "92px"
};

exports.header2 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "74px"
};

exports.header3 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "56px"
};

exports.numberStyle = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "56px"
};

exports.header4 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.callTime = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.reminderMenu = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.cellNames = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.subhead = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "38px"
};

exports.cellDetailInfo = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "38px"
};

exports.paragraph = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.tabActive = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.button = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.caption = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "25px"
};


},{"colors":"colors"}],"time":[function(require,module,exports){
exports.getTimeString = function() {
  var hour, minute, now, prepand, second, timeString;
  now = new Date;
  hour = now.getUTCHours() + 2;
  if (hour >= 24) {
    hour = hour - 24;
  }
  minute = now.getUTCMinutes();
  second = now.getUTCSeconds();
  prepand = hour >= 12 ? 'pm' : 'am';
  hour = hour >= 12 ? hour - 12 : hour;
  if (hour === 0) {
    hour = 12;
  }
  minute = minute < 10 ? "0" + minute : minute;
  timeString = hour + ":" + minute + prepand;
  return timeString;
};

exports.getTimeDifferenceString = function(difference) {
  var hour, minute, second, timeString;
  difference = difference / 1000;
  second = Math.floor(difference % 60);
  difference = difference / 60;
  minute = Math.floor(difference % 60);
  difference = difference / 60;
  hour = Math.floor(difference % 24);
  hour = hour < 10 ? "0" + hour : hour;
  minute = minute < 10 ? "0" + minute : minute;
  second = second < 10 ? "0" + second : second;
  timeString = hour + ":" + minute + ":" + second;
  return timeString;
};


},{}]},{},[])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJhbWVyLm1vZHVsZXMuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbTMuZnJhbWVyL21vZHVsZXMvdGltZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20zLmZyYW1lci9tb2R1bGVzL3N0eWxlcy5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20zLmZyYW1lci9tb2R1bGVzL3Byb3RvRmFrZUluZm8uY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tMy5mcmFtZXIvbW9kdWxlcy9nZW5lcmFsLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbTMuZnJhbWVyL21vZHVsZXMvZm9udHMuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tMy5mcmFtZXIvbW9kdWxlcy9maXJlYmFzZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20zLmZyYW1lci9tb2R1bGVzL2NvbG9ycy5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20zLmZyYW1lci9tb2R1bGVzL2NpcmNsZU1vZHVsZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20zLmZyYW1lci9tb2R1bGVzL1lvdVR1YmVQbGF5ZXIuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tMy5mcmFtZXIvbW9kdWxlcy9Gb250RmFjZS5jb2ZmZWUiLCJub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMuZ2V0VGltZVN0cmluZyA9ICgpIC0+XG4gIG5vdyA9IG5ldyBEYXRlXG4gICMgXHRkYXkgPSBub3cuZ2V0VVRDRGF5KClcbiAgIyBcdGRheWxpc3QgPSBbXG4gICMgXHRcdCdTdW5kYXknXG4gICMgXHRcdCdNb25kYXknXG4gICMgXHRcdCdUdWVzZGF5J1xuICAjIFx0XHQnV2VkbmVzZGF5J1xuICAjIFx0XHQnVGh1cnNkYXknXG4gICMgXHRcdCdGcmlkYXknXG4gICMgXHRcdCdTYXR1cmRheSdcbiAgIyBcdF1cbiAgaG91ciA9IG5vdy5nZXRVVENIb3VycygpICsgMlxuICBpZiBob3VyID49IDI0XG4gIFx0aG91ciA9IGhvdXIgLSAyNFxuICAjIFx0XHRkYXkgKz0gMVxuICBtaW51dGUgPSBub3cuZ2V0VVRDTWludXRlcygpXG4gIHNlY29uZCA9IG5vdy5nZXRVVENTZWNvbmRzKClcbiAgcHJlcGFuZCA9IGlmIGhvdXIgPj0gMTIgdGhlbiAncG0nIGVsc2UgJ2FtJ1xuICBob3VyID0gaWYgaG91ciA+PSAxMiB0aGVuIGhvdXIgLSAxMiBlbHNlIGhvdXJcbiAgaWYgaG91ciA9PSAwXG4gIFx0aG91ciA9IDEyXG4gIG1pbnV0ZSA9IGlmIG1pbnV0ZSA8IDEwIHRoZW4gXCIwXCIgKyBtaW51dGUgZWxzZSBtaW51dGVcbiAgdGltZVN0cmluZyA9IGhvdXIgKyBcIjpcIiArIG1pbnV0ZSArIHByZXBhbmRcbiAgcmV0dXJuIHRpbWVTdHJpbmdcblxuZXhwb3J0cy5nZXRUaW1lRGlmZmVyZW5jZVN0cmluZyA9IChkaWZmZXJlbmNlKSAtPlxuICBkaWZmZXJlbmNlID0gZGlmZmVyZW5jZS8xMDAwO1xuICBzZWNvbmQgPSBNYXRoLmZsb29yKGRpZmZlcmVuY2UgJSA2MCk7XG4gIGRpZmZlcmVuY2UgPSBkaWZmZXJlbmNlLzYwO1xuICBtaW51dGUgPSBNYXRoLmZsb29yKGRpZmZlcmVuY2UgJSA2MCk7XG4gIGRpZmZlcmVuY2UgPSBkaWZmZXJlbmNlLzYwO1xuICBob3VyID0gTWF0aC5mbG9vcihkaWZmZXJlbmNlICUgMjQpO1xuXG4gIGhvdXIgPSBpZiBob3VyIDwgMTAgdGhlbiBcIjBcIiArIGhvdXIgZWxzZSBob3VyXG4gIG1pbnV0ZSA9IGlmIG1pbnV0ZSA8IDEwIHRoZW4gXCIwXCIgKyBtaW51dGUgZWxzZSBtaW51dGVcbiAgc2Vjb25kID0gaWYgc2Vjb25kIDwgMTAgdGhlbiBcIjBcIiArIHNlY29uZCBlbHNlIHNlY29uZFxuXG4gIHRpbWVTdHJpbmcgPSBob3VyICsgXCI6XCIgKyBtaW51dGUgKyBcIjpcIiArIHNlY29uZFxuXG4gIHJldHVybiB0aW1lU3RyaW5nXG4iLCIjIHtGb250c30gPSByZXF1aXJlIFwiZm9udHNcIlxuY29sb3JzID0gcmVxdWlyZSAnY29sb3JzJ1xuXG5leHBvcnRzLmhlYWRlcjAgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YU1lZGl1bVwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjE0NHB4XCJcblxuZXhwb3J0cy5oZWFkZXIxID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFSZWd1bGFyXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiOTJweFwiXG5cbmV4cG9ydHMuaGVhZGVyMiA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhQm9sZFwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjc0cHhcIlxuXG5leHBvcnRzLmhlYWRlcjMgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YUJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCI1NnB4XCJcblxuZXhwb3J0cy5udW1iZXJTdHlsZSA9XG5cdGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFSZWd1bGFyXCJcblx0Y29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG5cdGZvbnRTaXplOiBcIjU2cHhcIlxuXG5leHBvcnRzLmhlYWRlcjQgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YUJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCI0NnB4XCJcblxuZXhwb3J0cy5jYWxsVGltZSA9XG5cdGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFNZWRpdW1cIlxuXHRjb2xvcjogY29sb3JzLmJsYWNrQ29sb3Jcblx0Zm9udFNpemU6IFwiNDZweFwiXG5cbmV4cG9ydHMucmVtaW5kZXJNZW51ID1cblx0Zm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuXHRjb2xvcjogY29sb3JzLmJsYWNrQ29sb3Jcblx0Zm9udFNpemU6IFwiNDZweFwiXG5cbmV4cG9ydHMuY2VsbE5hbWVzID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFTZW1pYm9sZFwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjQ2cHhcIlxuXG5leHBvcnRzLnN1YmhlYWQgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVNlbWlib2xkXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMzhweFwiXG5cbmV4cG9ydHMuY2VsbERldGFpbEluZm8gPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIzOHB4XCJcblxuZXhwb3J0cy5wYXJhZ3JhcGggPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIzMHB4XCJcblxuZXhwb3J0cy50YWJBY3RpdmUgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVNlbWlib2xkXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMzBweFwiXG5cbmV4cG9ydHMuYnV0dG9uID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFCb2xkXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMzBweFwiXG5cbmV4cG9ydHMuY2FwdGlvbiA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhTWVkaXVtXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMjVweFwiXG4iLCJleHBvcnRzLmludGVyZXN0cyA9IFtcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDFBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDFJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiRmFtaWx5XCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDJBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDJJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiUGV0c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjAzQWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjAzSW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlRyYXZlbFwiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjA0QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjA0SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlJlYWRpbmdcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wNUFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wNUluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJHYXJkZW5pbmdcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wN0FjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wN0luYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJDb29raW5nXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDZBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDZJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiT3V0ZG9vcnNcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wOEFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wOEluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJDdXJyZW50IEV2ZW50c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjA5QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjA5SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIkVjb25vbXlcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24xMEFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24xMEluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJBcnQgJiBDdWx0dXJlXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTFBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTFJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiTW92aWVzXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTJBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTJJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiVGVsZXZpc2lvblwiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjEzQWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjEzSW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlNwb3J0c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjE0QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjE0SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlNjaWVuY2UgJiBUZWNoXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTVBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTVJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiRmFzaGlvblwiXG5cdH0sXG5dXG5cbmV4cG9ydHMuc29jaWFsVGFsa0NvbnRhY3RzU3BhbmlzaCA9IFtcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMi5qcGdcIlxuXHRcdG5hbWU6IFwiTWlyYW5kYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTUgMjAxOCB8IDQ3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA4LmpwZ1wiXG5cdFx0bmFtZTogXCJPbGl2aWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDEzIDIwMTggfCA5OCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMi5qcGdcIlxuXHRcdG5hbWU6IFwiU2ltw7NuXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiT2N0b2JlciA5IDIwMTggfCAyNyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDEuanBnXCJcblx0XHRuYW1lOiBcIkVsw61hc1wiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgOCAyMDE4IHwgNDIgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxNS5qcGdcIlxuXHRcdG5hbWU6IFwiRGFudGVcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDcgMjAxOCB8IDM3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJVbmF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA1LmpwZ1wiXG5cdFx0bmFtZTogXCJJcmVuZVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMiAyMDE4IHwgNDcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA0LmpwZ1wiXG5cdFx0bmFtZTogXCJMdWFuYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAzMCAyMDE4IHwgNTcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwNy5qcGdcIlxuXHRcdG5hbWU6IFwiSXNhYmVsXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDI3IDIwMTggfCA3MiBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwOS5qcGdcIlxuXHRcdG5hbWU6IFwiQW5hXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDI0IDIwMTggfCA1MyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMy5qcGdcIlxuXHRcdG5hbWU6IFwiRHlsYW5cIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMjMgMjAxOCB8IDM2IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMC5qcGdcIlxuXHRcdG5hbWU6IFwiw4FuZ2VsXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDE5IDIwMTggfCAyNyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxNC5qcGdcIlxuXHRcdG5hbWU6IFwiTWFydGluYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAxNiAyMDE4IHwgMTQgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMTEuanBnXCJcblx0XHRuYW1lOiBcIlZpY3RvcmlhXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDEyIDIwMTggfCAyMiBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMS5qcGdcIlxuXHRcdG5hbWU6IFwiQ2FtaWxhXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDEwIDIwMTggfCAzOSBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMy5qcGdcIlxuXHRcdG5hbWU6IFwiU29maWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgNyAyMDE4IHwgMTAgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDYuanBnXCJcblx0XHRuYW1lOiBcIlRvbWFzXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDYgMjAxOCB8IDI3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJVbmF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDguanBnXCJcblx0XHRuYW1lOiBcIkpvYXF1w61uXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDMgMjAxOCB8IDggbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDcuanBnXCJcblx0XHRuYW1lOiBcIk3DrWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMSAyMDE4IHwgNDggbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMTEuanBnXCJcblx0XHRuYW1lOiBcIkx1Y2FzXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiQXVndXN0IDI5IDIwMTggfCAxOCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiVW5hdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMy5qcGdcIlxuXHRcdG5hbWU6IFwiTmF0YWxpYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIkF1Z3VzdCAyNiAyMDE4IHwgNTMgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH1cbl1cblxuZXhwb3J0cy5zb2NpYWxUYWxrQ29udGFjdHNFbmdsaXNoID0gW1xuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAxLmpwZ1wiXG5cdFx0bmFtZTogXCJIZW5yeVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTUgMjAxOCB8IDQ3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAyLmpwZ1wiXG5cdFx0bmFtZTogXCJPbGl2aWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDEzIDIwMTggfCA5OCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMy5qcGdcIlxuXHRcdG5hbWU6IFwiTWlyYW5kYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTIgMjAxOCB8IDMyIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA0LmpwZ1wiXG5cdFx0bmFtZTogXCJGcmVkXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiT2N0b2JlciAxMSAyMDE4IHwgNTcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDUuanBnXCJcblx0XHRuYW1lOiBcIkhlcm1pbmVcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDcgMjAxOCB8IDQ4IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA2LmpwZ1wiXG5cdFx0bmFtZTogXCJMZW9cIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDYgMjAxOCB8IDIzIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA3LmpwZ1wiXG5cdFx0bmFtZTogXCJUaG9tYXNcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDUgMjAxOCB8IDUzIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXVxuXG5leHBvcnRzLnJhZGlvU3RhdGlvbnNTcGFuaXNoID0gW1xuXHR7XG5cdFx0bmFtZTogXCJFdXJvcGEgRk1cIlxuXHRcdG51bWJlcjogXCI5MS4wMCBGTVwiXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0XHRhcnRpc3RTb25nTmFtZTogXCJFc3RvcGEgZW4gTGV2w6FudGF0ZSB5IEPDoXJkZW5hc1wiXG5cdFx0YXVkaW9JRDogMVxuXHRcdGlkOiAwXG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIkNhZGVuYSBEaWFsXCJcblx0XHRudW1iZXI6IFwiOTEuNzAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiQW5hIEd1ZXJyYSB5IE1hbmVsIEZ1ZW50ZXMgZGVzcGllcnRhbiBhIHRvZG9zIGxvcyBBdHJldmlkb3NcIlxuXHRcdGF1ZGlvSUQ6IDJcblx0XHRpZDogMVxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJSYWRpb2xlXCJcblx0XHRudW1iZXI6IFwiOTIuNDAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiRWwgRHVlbmRlIENhbGxlamVybyAtIExhIEp1bmdsYSBkZSBBbHF1aXRyw6FuXCJcblx0XHRhdWRpb0lEOiAzXG5cdFx0aWQ6IDJcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FkZW5hIENPUEVcIlxuXHRcdG51bWJlcjogXCI5NC44MCBGTVwiXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiI0NPUEVoYWNlSGlzdG9yaWEgZW4gbGEgcmFkaW8gZXNwYcOxb2xhXCJcblx0XHRhdWRpb0lEOiA0XG5cdFx0aWQ6IDNcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiT25kYSBDZXJvXCJcblx0XHRudW1iZXI6IFwiOTguMDAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiTGEgw7psdGltYSBwcmVndW50YSBhIERhdmlkIENhbGxlLCBlbCBwcm9mZXNvciBkZSBZb3V0dWJlXCJcblx0XHRhdWRpb0lEOiA1XG5cdFx0aWQ6IDRcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FkZW5hIDEwMFwiXG5cdFx0bnVtYmVyOiBcIjk5LjUwIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJMb3MgbmnDsW9zIHkgSmltZW5vIC0gwr9QbGF5YSBvIG1vbnRhw7FhP1wiXG5cdFx0YXVkaW9JRDogNlxuXHRcdGlkOiA1XG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIktpc3MgRk1cIlxuXHRcdG51bWJlcjogXCIxMDIuNzAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiS0lTUyBGTSwgbG8gbWVqb3IgZGUgbG9zIDgwIHkgbG9zIDkwIGhhc3RhIGhveVwiXG5cdFx0YXVkaW9JRDogN1xuXHRcdGlkOiA2XG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIkNhZGVuYSBTZXJcIlxuXHRcdG51bWJlcjogXCIxMDUuNDAgRk1cIlxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHRcdGFydGlzdFNvbmdOYW1lOiBcIlBvciBxdcOpIHNveSBmZW1pbmlzdGEgeSB2b3kgYSBsYSBodWVsZ2FcIlxuXHRcdGF1ZGlvSUQ6IDhcblx0XHRpZDogN1xuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJHaG9zdCByYWRpbywgZmlndXJlIGl0IG91dFwiXG5cdFx0bnVtYmVyOiBcIjExMC41IEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJhcnRpc3RTb25nTmFtZVwiXG5cdFx0YXVkaW9JRDogMFxuXHRcdGlkOiA4XG5cdH1cbl1cblxuZXhwb3J0cy5yYWRpb1N0YXRpb25zRW5nbGlzaCA9IFtcblx0e1xuXHRcdG5hbWU6IFwiQkJDXCJcblx0XHRudW1iZXI6IFwiOTQuOSBGTVwiXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0XHRhcnRpc3RTb25nTmFtZTogXCJHcmltbXkgY2hhdHMgdG8gRW1tYSBXYXRzb25cIlxuXHRcdGF1ZGlvSUQ6IDE1XG5cdFx0aWQ6IDBcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FwaXRhbCBGTVwiXG5cdFx0bnVtYmVyOiBcIjk1LjggRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiTWFydGluIEdhcnJpeCBDYXBpdGFsIEZNIFJhZGlvIEludGVydmlld1wiXG5cdFx0YXVkaW9JRDogMTZcblx0XHRpZDogMVxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJMQkNcIlxuXHRcdG51bWJlcjogXCI5Ny4zIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJKYW1lcyBPJ0JyaWVuIFJhaWxzIEF0IEJyZXhpdGVlcnMgV2hvIFdhbnQgVG8gTGVhdmUgQXQgQWxsIENvc3RzXCJcblx0XHRhdWRpb0lEOiAxN1xuXHRcdGlkOiAyXG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIk1hZ2ljIFJhZGlvXCJcblx0XHRudW1iZXI6IFwiMTA1LjQgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiVGFrZSBUaGF0IHRhbGsgc3B5IGdhZGdldHMgd2l0aCBNYWdpYyBpbiB0aGUgTW9ybmluZ1wiXG5cdFx0YXVkaW9JRDogMThcblx0XHRpZDogM1xuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJUYWxrIFJhZGlvXCJcblx0XHRudW1iZXI6IFwiMTAyLjcgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiSG93IGhhcyBBbWF6b24gaGFsdmVkIHRoZWlyIFVLIGNvcnBvcmF0aW9uIHRheCBiaWxsP1wiXG5cdFx0YXVkaW9JRDogMTlcblx0XHRpZDogNFxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJSYWRpbyBYXCJcblx0XHRudW1iZXI6IFwiMTA0LjkgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiRG9tJ3MgJ09mZiB0aGUgQ3VmZicgS25vd2xlZGdlIGlzIFJlbWFya2FibGVcIlxuXHRcdGF1ZGlvSUQ6IDIwXG5cdFx0aWQ6IDVcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiR2hvc3QgcmFkaW8sIGZpZ3VyZSBpdCBvdXRcIlxuXHRcdG51bWJlcjogXCIxMTAuNSBGTVwiXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiYXJ0aXN0U29uZ05hbWVcIlxuXHRcdGF1ZGlvSUQ6IDBcblx0XHRpZDogNlxuXHR9XG5dXG5cbmV4cG9ydHMucGhvbmVDb250YWN0c1NwYW5pc2ggPSBbXG5cdHtcblx0XHRpbml0aWFsOiBcIkFcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiU29mw61hIEFsY2FsZG9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJTQVwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTUgMjAxOCwgMTI6MzRwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiVmFsZW50aW5hIEJhcmJlcm9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJWQlwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UwMy5wbmdcIlxuXHQjIFx0bmFtZTogXCJTZWJhc3Rpw6FuIEJyYXZvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJTQlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJDXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTA0LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIk1hdGVvIENhYnJlcm9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIk1DXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UwNS5wbmdcIlxuXHQjIFx0bmFtZTogXCJNYXLDrWEgSm9zw6kgQ2FudG9yXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNQ1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJJbmNvbWluZyBDYWxsIHwgT2N0b2JlciAxNCAyMDE4LCA1OjE2cG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAyXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkFsZWphbmRybyBDYXN0aWxsZWpvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJBQ1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJEYW5pZWwgQ29sYVwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiRENcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTQgMjAxOCwgMTA6MDdhbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDFcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkx1Y8OtYSBDb3J0w6lzXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJMQ1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IE9jdG9iZXIgMTMgMjAxOCwgMjozMHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogM1xuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIkRcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMDkucG5nXCJcblx0IyBcdG5hbWU6IFwiTWF0ZW8gRG9taW5ndWV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNRFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTAucG5nXCJcblx0IyBcdG5hbWU6IFwiTWFydGEgRGVsZ2Fkb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiRURcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgOSAyMDE4LCAxMToxMWFtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMVxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiR1wiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiRW1tYW51ZWwgR2FyemFcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkVHXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTIucG5nXCJcblx0IyBcdG5hbWU6IFwiUGF1bGEgR3JhbmRcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIlBHXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIkhcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIk1hcmlhbmEgSGVybmFuZGV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNSFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJMdWNpYSBIZXJyZXJhXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJMSFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTE1LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIlpvZSBIaWRhbGdvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJaSFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJJbmNvbWluZyBDYWxsIHwgT2N0b2JlciAyIDIwMTgsIDk6MTVhbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDJcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIkxcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTYucG5nXCJcblx0IyBcdG5hbWU6IFwiVGhpYWdvIExvcGV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJUTFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IFNlcHRlbWJlciAyOSAyMDE4LCAxMDozMHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogM1xuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTE3LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIlJlbmF0YSBMb3BlelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiUkxcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiTVwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiQ2F0YWxpbmEgTWFyaW5cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkNNXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk91dGdvaW5nIENhbGwgfCBTZXB0ZW1iZXIgMjYgMjAxOCwgMTo0N3BtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMVxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiTWFyw61hIE1hcnTDrW5lelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTU1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiRmVybmFuZG8gTWFycXVlc1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiRk1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBTZXB0ZW1iZXIgMjUgMjAxOCwgNzo1OHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogM1xuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiSnVhbiBKb3PDqSBNb2xpbmVyb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiSk1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiTmF0YWxpYSBNb3Jlbm9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIk5NXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk91dGdvaW5nIENhbGwgfCBTZXB0ZW1iZXIgMjAgMjAxOCwgMzozM3BtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMVxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkFsb25zbyBSYW1pcmV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJBUlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJPbGl2aWEgUm9kcsOtZ3VlelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiT1JcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyMCAyMDE4LCAzOjMzcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAxXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTI1LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIkxvbGEgUnViaW9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkxSXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBTZXB0ZW1iZXIgMTQgMjAxOCwgNzozMHBtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMlxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJKdWFuIFJ1aXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkpSXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJTXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJDb25zdGFuemEgU3VhcmV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJDU1wiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiVFwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UyOC5wbmdcIlxuXHQjIFx0bmFtZTogXCJBbGVzc2FuZHJhIFRvcnJlcm9cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkFUXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk1pc3NlZCBDYWxsIHwgU2VwdGVtYmVyIDExIDIwMTgsIDExOjExYW1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAzXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJWXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJNaXJhbmRhIFZlbGF6cXVlelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTVZcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcbl1cblxuZXhwb3J0cy5waG9uZUNvbnRhY3RzRW5nbGlzaCA9IFtcblx0e1xuXHRcdGluaXRpYWw6IFwiQVwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJTb3BoaWUgQW5kZXJzb25cIlxuXHRcdG5hbWVJbml0aWFsczogXCJTQVwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTUgMjAxOCwgMTI6MzRwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiVmVyb25pY2EgQnJvd25cIlxuXHRcdG5hbWVJbml0aWFsczogXCJWQlwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiaW1hZ2UwMy5wbmdcIlxuXHRcdG5hbWU6IFwiU3RldmVuIEJha2VyXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiU0JcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiQ1wiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMDQucG5nXCJcblx0XHRuYW1lOiBcIk1pbHRvbiBDb3hcIlxuXHRcdG5hbWVJbml0aWFsczogXCJNQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTA1LnBuZ1wiXG5cdFx0bmFtZTogXCJNYXJ5IENsYXJrXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTUNcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBPY3RvYmVyIDE0IDIwMTgsIDU6MTZwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAyXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkFsZXhhbmRlciBDYXJ0ZXJcIlxuXHRcdG5hbWVJbml0aWFsczogXCJBQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkRhcnlsIENvbGVtYW5cIlxuXHRcdG5hbWVJbml0aWFsczogXCJEQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IE9jdG9iZXIgMTQgMjAxOCwgMTA6MDdhbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJMaXNhIENvb2tcIlxuXHRcdG5hbWVJbml0aWFsczogXCJMQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBPY3RvYmVyIDEzIDIwMTgsIDI6MzBwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAzXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkRcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTA5LnBuZ1wiXG5cdFx0bmFtZTogXCJNYXR0aGV3IERhdmlzXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTURcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiVFwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJBbGFzZGFpciBUYXlsb3JcIlxuXHRcdG5hbWVJbml0aWFsczogXCJBVFwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiV1wiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMTAucG5nXCJcblx0XHRuYW1lOiBcIkVsaXphIFdpbGxpYW1zXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiRVdcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk91dGdvaW5nIENhbGwgfCBPY3RvYmVyIDkgMjAxOCwgMTE6MTFhbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UxMi5wbmdcIlxuXHQjIFx0bmFtZTogXCJQYXVsYSBHcmFuZFwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiUEdcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiSFwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiTWFyaWFuYSBIZXJuYW5kZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIk1IXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkx1Y2lhIEhlcnJlcmFcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkxIXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTUucG5nXCJcblx0IyBcdG5hbWU6IFwiWm9lIEhpZGFsZ29cIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIlpIXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBPY3RvYmVyIDIgMjAxOCwgOToxNWFtXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMlxuXHQjIFx0ZmF2b3JpdGU6IHRydWVcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiTFwiXG5cdCMgXHRwaG90b05hbWU6IFwiaW1hZ2UxNi5wbmdcIlxuXHQjIFx0bmFtZTogXCJUaGlhZ28gTG9wZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIlRMXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk1pc3NlZCBDYWxsIHwgU2VwdGVtYmVyIDI5IDIwMTgsIDEwOjMwcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAzXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMTcucG5nXCJcblx0IyBcdG5hbWU6IFwiUmVuYXRhIExvcGV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJSTFwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJNXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJDYXRhbGluYSBNYXJpblwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiQ01cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyNiAyMDE4LCAxOjQ3cG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAxXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJNYXLDrWEgTWFydMOtbmV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNTVwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJGZXJuYW5kbyBNYXJxdWVzXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJGTVwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IFNlcHRlbWJlciAyNSAyMDE4LCA3OjU4cG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAzXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJKdWFuIEpvc8OpIE1vbGluZXJvXCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJKTVwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiXCJcblx0IyBcdHBob3RvTmFtZTogXCJcIlxuXHQjIFx0bmFtZTogXCJOYXRhbGlhIE1vcmVub1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTk1cIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyMCAyMDE4LCAzOjMzcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAxXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXHQjIHtcblx0IyBcdGluaXRpYWw6IFwiUlwiXG5cdCMgXHRwaG90b05hbWU6IFwiXCJcblx0IyBcdG5hbWU6IFwiQWxvbnNvIFJhbWlyZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkFSXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIk9saXZpYSBSb2Ryw61ndWV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJPUlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJPdXRnb2luZyBDYWxsIHwgU2VwdGVtYmVyIDIwIDIwMTgsIDM6MzNwbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDFcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcImltYWdlMjUucG5nXCJcblx0IyBcdG5hbWU6IFwiTG9sYSBSdWJpb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiTFJcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiSW5jb21pbmcgQ2FsbCB8IFNlcHRlbWJlciAxNCAyMDE4LCA3OjMwcG1cIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAyXG5cdCMgXHRmYXZvcml0ZTogdHJ1ZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkp1YW4gUnVpelwiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiSlJcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0IyBcdGxhc3RDYWxsVHlwZTogMFxuXHQjIFx0ZmF2b3JpdGU6IGZhbHNlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlNcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIkNvbnN0YW56YSBTdWFyZXpcIlxuXHQjIFx0bmFtZUluaXRpYWxzOiBcIkNTXCJcblx0IyBcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDBcblx0IyBcdGZhdm9yaXRlOiBmYWxzZVxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW5pdGlhbDogXCJUXCJcblx0IyBcdHBob3RvTmFtZTogXCJpbWFnZTI4LnBuZ1wiXG5cdCMgXHRuYW1lOiBcIkFsZXNzYW5kcmEgVG9ycmVyb1wiXG5cdCMgXHRuYW1lSW5pdGlhbHM6IFwiQVRcIlxuXHQjIFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBTZXB0ZW1iZXIgMTEgMjAxOCwgMTE6MTFhbVwiXG5cdCMgXHRsYXN0Q2FsbFR5cGU6IDNcblx0IyBcdGZhdm9yaXRlOiB0cnVlXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbml0aWFsOiBcIlZcIlxuXHQjIFx0cGhvdG9OYW1lOiBcIlwiXG5cdCMgXHRuYW1lOiBcIk1pcmFuZGEgVmVsYXpxdWV6XCJcblx0IyBcdG5hbWVJbml0aWFsczogXCJNVlwiXG5cdCMgXHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHQjIFx0bGFzdENhbGxUeXBlOiAwXG5cdCMgXHRmYXZvcml0ZTogZmFsc2Vcblx0IyB9LFxuXVxuXG5leHBvcnRzLnBob3RvR2FsbGVyeSA9IFtcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwOC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwOS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMTMuanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0IyB9LFxuXHQjIHtcblx0IyBcdGltYWdlTmFtZTogXCJpbWcxNC5qcGdcIlxuXHQjIFx0ZGF0ZVN0cmluZzogXCJKdW5lIDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzE1LmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIkp1bmUgMjAxOFwiXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMTYuanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0IyB9LFxuXHQjIHtcblx0IyBcdGltYWdlTmFtZTogXCJpbWcxNy5qcGdcIlxuXHQjIFx0ZGF0ZVN0cmluZzogXCJNYXkgMjAxOFwiXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMTguanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiTWF5IDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzE5LmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIk1heSAyMDE4XCJcblx0IyB9LFxuXHQjIHtcblx0IyBcdGltYWdlTmFtZTogXCJpbWcyMC5qcGdcIlxuXHQjIFx0ZGF0ZVN0cmluZzogXCJNYXkgMjAxOFwiXG5cdCMgfSxcblx0IyB7XG5cdCMgXHRpbWFnZU5hbWU6IFwiaW1nMjEuanBnXCJcblx0IyBcdGRhdGVTdHJpbmc6IFwiTWF5IDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzIyLmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIkFwcmlsIDIwMThcIlxuXHQjIH0sXG5cdCMge1xuXHQjIFx0aW1hZ2VOYW1lOiBcImltZzIzLmpwZ1wiXG5cdCMgXHRkYXRlU3RyaW5nOiBcIkFwcmlsIDIwMThcIlxuXHQjIH0sXG5dXG4iLCIjIER5bmFtaWNcbmV4cG9ydHMuY2FsbFN0YXJ0RGF0ZSA9IG5ldyBEYXRlXG5leHBvcnRzLmxhc3RJZGxlU3RhdGUgPSAxXG5leHBvcnRzLmxhc3RJbnRlcmFjdGlvbkRhdGUgPSBuZXcgRGF0ZVxuZXhwb3J0cy5vbkFDYWxsID0gZmFsc2VcbmV4cG9ydHMucHJveGltaXR5U3RhdGUgPSBmYWxzZVxuZXhwb3J0cy5zaG93aW5nSWRsZSA9IHRydWVcbmV4cG9ydHMuc3dpcGVBbmltYXRpb24gPSBmYWxzZVxuXG4jIFN0YXRpYyBjb25maWd1cmF0aW9uXG5leHBvcnRzLmxhbmd1YWdlID0gXCJlbmdsaXNoXCJcbmV4cG9ydHMuc2NyZWVuX2hlaWdodCA9IEZyYW1lci5EZXZpY2Uuc2NyZWVuLmhlaWdodFxuZXhwb3J0cy5zY3JlZW5fd2lkdGggPSBGcmFtZXIuRGV2aWNlLnNjcmVlbi53aWR0aFxuXG4jIERldmVsb3BtZW50IHZhcmlhYmxlc1xuXG5cblxuZXhwb3J0cy5sb2FkSW1hZ2VzID0gdHJ1ZVxuXG5leHBvcnRzLmxvYWRUZWxlSGVhbHRoID0gdHJ1ZVxuXG5leHBvcnRzLmxvYWRQaG9uZSA9IHRydWVcbmV4cG9ydHMubG9hZFBob3RvcyA9IHRydWVcbmV4cG9ydHMubG9hZFNvY2lhbFRhbGsgPSB0cnVlXG5leHBvcnRzLmxvYWRSYWRpbyA9IHRydWVcblxuXG5cblxuIyBleHBvcnRzLmxvYWRJbWFnZXMgPSBmYWxzZVxuI1xuIyBleHBvcnRzLmxvYWRUZWxlSGVhbHRoID0gZmFsc2VcbiNcbiMgZXhwb3J0cy5sb2FkUGhvbmUgPSBmYWxzZVxuIyBleHBvcnRzLmxvYWRQaG90b3MgPSBmYWxzZVxuIyBleHBvcnRzLmxvYWRTb2NpYWxUYWxrID0gZmFsc2VcbiMgZXhwb3J0cy5sb2FkUmFkaW8gPSBmYWxzZVxuXG5cblxuXG5leHBvcnRzLnNob3dMb2dzID0gZmFsc2VcbiIsIntGb250RmFjZX0gPSByZXF1aXJlIFwiRm9udEZhY2VcIlxuXG5Qcm94aW1hTm92YUJsYWNrSXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFCbGFja0l0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBCbGFjayBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFCbGFjayA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhQmxhY2tcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgQmxhY2sub3RmXCJcblxuUHJveGltYU5vdmFCb2xkSXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFCb2xkSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIEJvbGQgSXRhbGljLm90ZlwiXG5cblByb3hpbWFOb3ZhQm9sZCA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhQm9sZFwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBCb2xkLm90ZlwiXG5cblByb3hpbWFOb3ZhRXh0cmFib2xkSXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFFeHRyYWJvbGRJdGFsaWNcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgRXh0cmFib2xkIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YUV4dHJhYm9sZCA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhRXh0cmFib2xkXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIEV4dHJhYm9sZC5vdGZcIlxuXG5Qcm94aW1hTm92YUxpZ2h0SXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFMaWdodEl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBMaWdodCBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFMaWdodCA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhTGlnaHRcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgTGlnaHQub3RmXCJcblxuUHJveGltYU5vdmFNZWRpdW1JdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YU1lZGl1bUl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBNZWRpdW0gSXRhbGljLm90ZlwiXG5cblByb3hpbWFOb3ZhTWVkaXVtID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFNZWRpdW1cIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgTWVkaXVtLm90ZlwiXG5cblByb3hpbWFOb3ZhUmVndWxhckl0YWxpYyA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhUmVndWxhckl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBSZWd1bGFyIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YVJlZ3VsYXIgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgUmVndWxhci5vdGZcIlxuXG5Qcm94aW1hTm92YVNlbWlib2xkSXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFTZW1pYm9sZEl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBTZW1pYm9sZCBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFSZWd1bGFyID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFTZW1pYm9sZFwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBTZW1pYm9sZC5vdGZcIlxuXG5Qcm94aW1hTm92YVRoaW5JdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVRoaW5JdGFsaWNcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgVGhpbiBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFUaGluID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFUaGluXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFRoaW4ub3RmXCJcbiIsIiMgRG9jdW1lbnRhdGlvbiBvZiB0aGlzIE1vZHVsZTogaHR0cHM6Ly9naXRodWIuY29tL21hcmNrcmVubi9mcmFtZXItRmlyZWJhc2VcbiMgLS0tLS0tIDogLS0tLS0tLSBGaXJlYmFzZSBSRVNUIEFQSTogaHR0cHM6Ly9maXJlYmFzZS5nb29nbGUuY29tL2RvY3MvcmVmZXJlbmNlL3Jlc3QvZGF0YWJhc2UvXG5cbiMgRmlyZWJhc2UgUkVTVCBBUEkgQ2xhc3MgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5jbGFzcyBleHBvcnRzLkZpcmViYXNlIGV4dGVuZHMgRnJhbWVyLkJhc2VDbGFzc1xuXG5cblx0QC5kZWZpbmUgXCJzdGF0dXNcIixcblx0XHRnZXQ6IC0+IEBfc3RhdHVzICMgcmVhZE9ubHlcblxuXHRjb25zdHJ1Y3RvcjogKEBvcHRpb25zPXt9KSAtPlxuXHRcdEBwcm9qZWN0SUQgPSBAb3B0aW9ucy5wcm9qZWN0SUQgPz0gbnVsbFxuXHRcdEBzZWNyZXQgICAgPSBAb3B0aW9ucy5zZWNyZXQgICAgPz0gbnVsbFxuXHRcdEBkZWJ1ZyAgICAgPSBAb3B0aW9ucy5kZWJ1ZyAgICAgPz0gZmFsc2Vcblx0XHRAX3N0YXR1cyAgICAgICAgICAgICAgICAgICAgICAgID89IFwiZGlzY29ubmVjdGVkXCJcblxuXHRcdEBzZWNyZXRFbmRQb2ludCA9IGlmIEBzZWNyZXQgdGhlbiBcIj9hdXRoPSN7QHNlY3JldH1cIiBlbHNlIFwiP1wiICMgaG90Zml4XG5cdFx0c3VwZXJcblxuXHRcdGNvbnNvbGUubG9nIFwiRmlyZWJhc2U6IENvbm5lY3RpbmcgdG8gRmlyZWJhc2UgUHJvamVjdCAnI3tAcHJvamVjdElEfScgLi4uIFxcbiBVUkw6ICdodHRwczovLyN7QHByb2plY3RJRH0uZmlyZWJhc2Vpby5jb20nXCIgaWYgQGRlYnVnXG5cdFx0QC5vbkNoYW5nZSBcImNvbm5lY3Rpb25cIlxuXG5cdHJlcXVlc3QgPSAocHJvamVjdCwgc2VjcmV0LCBwYXRoLCBjYWxsYmFjaywgbWV0aG9kLCBkYXRhLCBwYXJhbWV0ZXJzLCBkZWJ1ZykgLT5cblxuXHRcdHVybCA9IFwiaHR0cHM6Ly8je3Byb2plY3R9LmZpcmViYXNlaW8uY29tI3twYXRofS5qc29uI3tzZWNyZXR9XCJcblxuXHRcdGlmIHBhcmFtZXRlcnM/XG5cdFx0XHRpZiBwYXJhbWV0ZXJzLnNoYWxsb3cgICAgICAgICAgICB0aGVuIHVybCArPSBcIiZzaGFsbG93PXRydWVcIlxuXHRcdFx0aWYgcGFyYW1ldGVycy5mb3JtYXQgaXMgXCJleHBvcnRcIiB0aGVuIHVybCArPSBcIiZmb3JtYXQ9ZXhwb3J0XCJcblxuXHRcdFx0c3dpdGNoIHBhcmFtZXRlcnMucHJpbnRcblx0XHRcdFx0d2hlbiBcInByZXR0eVwiIHRoZW4gdXJsICs9IFwiJnByaW50PXByZXR0eVwiXG5cdFx0XHRcdHdoZW4gXCJzaWxlbnRcIiB0aGVuIHVybCArPSBcIiZwcmludD1zaWxlbnRcIlxuXG5cdFx0XHRpZiB0eXBlb2YgcGFyYW1ldGVycy5kb3dubG9hZCBpcyBcInN0cmluZ1wiXG5cdFx0XHRcdHVybCArPSBcIiZkb3dubG9hZD0je3BhcmFtZXRlcnMuZG93bmxvYWR9XCJcblx0XHRcdFx0d2luZG93Lm9wZW4odXJsLFwiX3NlbGZcIilcblxuXHRcdFx0dXJsICs9IFwiJm9yZGVyQnk9XCIgKyAnXCInICsgcGFyYW1ldGVycy5vcmRlckJ5ICsgJ1wiJyBpZiB0eXBlb2YgcGFyYW1ldGVycy5vcmRlckJ5ICAgICAgaXMgXCJzdHJpbmdcIlxuXHRcdFx0dXJsICs9IFwiJmxpbWl0VG9GaXJzdD0je3BhcmFtZXRlcnMubGltaXRUb0ZpcnN0fVwiICAgaWYgdHlwZW9mIHBhcmFtZXRlcnMubGltaXRUb0ZpcnN0IGlzIFwibnVtYmVyXCJcblx0XHRcdHVybCArPSBcIiZsaW1pdFRvTGFzdD0je3BhcmFtZXRlcnMubGltaXRUb0xhc3R9XCIgICAgIGlmIHR5cGVvZiBwYXJhbWV0ZXJzLmxpbWl0VG9MYXN0ICBpcyBcIm51bWJlclwiXG5cdFx0XHR1cmwgKz0gXCImc3RhcnRBdD0je3BhcmFtZXRlcnMuc3RhcnRBdH1cIiAgICAgICAgICAgICBpZiB0eXBlb2YgcGFyYW1ldGVycy5zdGFydEF0ICAgICAgaXMgXCJudW1iZXJcIlxuXHRcdFx0dXJsICs9IFwiJmVuZEF0PSN7cGFyYW1ldGVycy5lbmRBdH1cIiAgICAgICAgICAgICAgICAgaWYgdHlwZW9mIHBhcmFtZXRlcnMuZW5kQXQgICAgICAgIGlzIFwibnVtYmVyXCJcblx0XHRcdHVybCArPSBcIiZlcXVhbFRvPSN7cGFyYW1ldGVycy5lcXVhbFRvfVwiICAgICAgICAgICAgIGlmIHR5cGVvZiBwYXJhbWV0ZXJzLmVxdWFsVG8gICAgICBpcyBcIm51bWJlclwiXG5cdFx0XG5cdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogTmV3ICcje21ldGhvZH0nLXJlcXVlc3Qgd2l0aCBkYXRhOiAnI3tKU09OLnN0cmluZ2lmeShkYXRhKX0nIFxcbiBVUkw6ICcje3VybH0nXCIgaWYgZGVidWdcblx0XHRcblx0XHRvcHRpb25zID1cblx0XHRcdG1ldGhvZDogbWV0aG9kXG5cdFx0XHRoZWFkZXJzOlxuXHRcdFx0XHQnY29udGVudC10eXBlJzogJ2FwcGxpY2F0aW9uL2pzb247IGNoYXJzZXQ9dXRmLTgnXG5cdFx0XG5cdFx0aWYgZGF0YT9cblx0XHRcdG9wdGlvbnMuYm9keSA9IEpTT04uc3RyaW5naWZ5KGRhdGEpXG5cblx0XHRyID0gZmV0Y2godXJsLCBvcHRpb25zKVxuXHRcdC50aGVuIChyZXMpIC0+XG5cdFx0XHRpZiAhcmVzLm9rIHRoZW4gdGhyb3cgRXJyb3IocmVzLnN0YXR1c1RleHQpXG5cdFx0XHRqc29uID0gcmVzLmpzb24oKVxuXHRcdFx0anNvbi50aGVuIGNhbGxiYWNrXG5cdFx0XHRyZXR1cm4ganNvblxuXHRcdC5jYXRjaCAoZXJyb3IpID0+IGNvbnNvbGUud2FybihlcnJvcilcblx0XHRcblx0XHRyZXR1cm4gclxuXG5cdCMgVGhpcmQgYXJndW1lbnQgY2FuIGFsc28gYWNjZXB0IG9wdGlvbnMsIHJhdGhlciB0aGFuIGNhbGxiYWNrXG5cdHBhcnNlQXJncyA9IChsLCBhcmdzLi4uLCBjYikgLT5cblx0XHRpZiB0eXBlb2YgYXJnc1tsLTFdIGlzIFwib2JqZWN0XCJcblx0XHRcdGFyZ3NbbF0gPSBhcmdzW2wtMV1cblx0XHRcdGFyZ3NbbC0xXSA9IG51bGxcblxuXHRcdHJldHVybiBjYi5hcHBseShudWxsLCBhcmdzKVxuXG5cdCMgQXZhaWxhYmxlIG1ldGhvZHNcblxuXHRnZXQ6ICAgIChhcmdzLi4uKSAtPiBwYXJzZUFyZ3MgMiwgYXJncy4uLiwgKHBhdGgsIFx0XHQgY2FsbGJhY2ssIHBhcmFtZXRlcnMpID0+IHJlcXVlc3QoQHByb2plY3RJRCwgQHNlY3JldEVuZFBvaW50LCBwYXRoLCBjYWxsYmFjaywgXCJHRVRcIiwgICAgbnVsbCwgcGFyYW1ldGVycywgQGRlYnVnKVxuXHRwdXQ6ICAgIChhcmdzLi4uKSAtPiBwYXJzZUFyZ3MgMywgYXJncy4uLiwgKHBhdGgsIGRhdGEsIGNhbGxiYWNrLCBwYXJhbWV0ZXJzKSA9PiByZXF1ZXN0KEBwcm9qZWN0SUQsIEBzZWNyZXRFbmRQb2ludCwgcGF0aCwgY2FsbGJhY2ssIFwiUFVUXCIsICAgIGRhdGEsIHBhcmFtZXRlcnMsIEBkZWJ1Zylcblx0cG9zdDogICAoYXJncy4uLikgLT4gcGFyc2VBcmdzIDMsIGFyZ3MuLi4sIChwYXRoLCBkYXRhLCBjYWxsYmFjaywgcGFyYW1ldGVycykgPT4gcmVxdWVzdChAcHJvamVjdElELCBAc2VjcmV0RW5kUG9pbnQsIHBhdGgsIGNhbGxiYWNrLCBcIlBPU1RcIiwgICBkYXRhLCBwYXJhbWV0ZXJzLCBAZGVidWcpXG5cdHBhdGNoOiAgKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAzLCBhcmdzLi4uLCAocGF0aCwgZGF0YSwgY2FsbGJhY2ssIHBhcmFtZXRlcnMpID0+IHJlcXVlc3QoQHByb2plY3RJRCwgQHNlY3JldEVuZFBvaW50LCBwYXRoLCBjYWxsYmFjaywgXCJQQVRDSFwiLCAgZGF0YSwgcGFyYW1ldGVycywgQGRlYnVnKVxuXHRkZWxldGU6IChhcmdzLi4uKSAtPiBwYXJzZUFyZ3MgMiwgYXJncy4uLiwgKHBhdGgsIFx0ICBcdCBjYWxsYmFjaywgcGFyYW1ldGVycykgPT4gcmVxdWVzdChAcHJvamVjdElELCBAc2VjcmV0RW5kUG9pbnQsIHBhdGgsIGNhbGxiYWNrLCBcIkRFTEVURVwiLCBudWxsLCBwYXJhbWV0ZXJzLCBAZGVidWcpXG5cblxuXHRvbkNoYW5nZTogKHBhdGgsIGNhbGxiYWNrKSAtPlxuXG5cblx0XHRpZiBwYXRoIGlzIFwiY29ubmVjdGlvblwiXG5cblx0XHRcdHVybCA9IFwiaHR0cHM6Ly8je0Bwcm9qZWN0SUR9LmZpcmViYXNlaW8uY29tLy5qc29uI3tAc2VjcmV0RW5kUG9pbnR9XCJcblx0XHRcdGN1cnJlbnRTdGF0dXMgPSBcImRpc2Nvbm5lY3RlZFwiXG5cdFx0XHRzb3VyY2UgPSBuZXcgRXZlbnRTb3VyY2UodXJsKVxuXG5cdFx0XHRzb3VyY2UuYWRkRXZlbnRMaXN0ZW5lciBcIm9wZW5cIiwgPT5cblx0XHRcdFx0aWYgY3VycmVudFN0YXR1cyBpcyBcImRpc2Nvbm5lY3RlZFwiXG5cdFx0XHRcdFx0QC5fc3RhdHVzID0gXCJjb25uZWN0ZWRcIlxuXHRcdFx0XHRcdGNhbGxiYWNrKFwiY29ubmVjdGVkXCIpIGlmIGNhbGxiYWNrP1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nIFwiRmlyZWJhc2U6IENvbm5lY3Rpb24gdG8gRmlyZWJhc2UgUHJvamVjdCAnI3tAcHJvamVjdElEfScgZXN0YWJsaXNoZWRcIiBpZiBAZGVidWdcblx0XHRcdFx0Y3VycmVudFN0YXR1cyA9IFwiY29ubmVjdGVkXCJcblxuXHRcdFx0c291cmNlLmFkZEV2ZW50TGlzdGVuZXIgXCJlcnJvclwiLCA9PlxuXHRcdFx0XHRpZiBjdXJyZW50U3RhdHVzIGlzIFwiY29ubmVjdGVkXCJcblx0XHRcdFx0XHRALl9zdGF0dXMgPSBcImRpc2Nvbm5lY3RlZFwiXG5cdFx0XHRcdFx0Y2FsbGJhY2soXCJkaXNjb25uZWN0ZWRcIikgaWYgY2FsbGJhY2s/XG5cdFx0XHRcdFx0Y29uc29sZS53YXJuIFwiRmlyZWJhc2U6IENvbm5lY3Rpb24gdG8gRmlyZWJhc2UgUHJvamVjdCAnI3tAcHJvamVjdElEfScgY2xvc2VkXCIgaWYgQGRlYnVnXG5cdFx0XHRcdGN1cnJlbnRTdGF0dXMgPSBcImRpc2Nvbm5lY3RlZFwiXG5cblx0XHRcdHJldHVyblxuXG5cdFx0dXJsID0gXCJodHRwczovLyN7QHByb2plY3RJRH0uZmlyZWJhc2Vpby5jb20je3BhdGh9Lmpzb24je0BzZWNyZXRFbmRQb2ludH1cIlxuXHRcdHNvdXJjZSA9IG5ldyBFdmVudFNvdXJjZSh1cmwpXG5cdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogTGlzdGVuaW5nIHRvIGNoYW5nZXMgbWFkZSB0byAnI3twYXRofScgXFxuIFVSTDogJyN7dXJsfSdcIiBpZiBAZGVidWdcblxuXHRcdHNvdXJjZS5hZGRFdmVudExpc3RlbmVyIFwicHV0XCIsIChldikgPT5cblx0XHRcdGNhbGxiYWNrKEpTT04ucGFyc2UoZXYuZGF0YSkuZGF0YSwgXCJwdXRcIiwgSlNPTi5wYXJzZShldi5kYXRhKS5wYXRoLCBfLnRhaWwoSlNPTi5wYXJzZShldi5kYXRhKS5wYXRoLnNwbGl0KFwiL1wiKSwxKSkgaWYgY2FsbGJhY2s/XG5cdFx0XHRjb25zb2xlLmxvZyBcIkZpcmViYXNlOiBSZWNlaXZlZCBjaGFuZ2VzIG1hZGUgdG8gJyN7cGF0aH0nIHZpYSAnUFVUJzogI3tKU09OLnBhcnNlKGV2LmRhdGEpLmRhdGF9IFxcbiBVUkw6ICcje3VybH0nXCIgaWYgQGRlYnVnXG5cblx0XHRzb3VyY2UuYWRkRXZlbnRMaXN0ZW5lciBcInBhdGNoXCIsIChldikgPT5cblx0XHRcdGNhbGxiYWNrKEpTT04ucGFyc2UoZXYuZGF0YSkuZGF0YSwgXCJwYXRjaFwiLCBKU09OLnBhcnNlKGV2LmRhdGEpLnBhdGgsIF8udGFpbChKU09OLnBhcnNlKGV2LmRhdGEpLnBhdGguc3BsaXQoXCIvXCIpLDEpKSBpZiBjYWxsYmFjaz9cblx0XHRcdGNvbnNvbGUubG9nIFwiRmlyZWJhc2U6IFJlY2VpdmVkIGNoYW5nZXMgbWFkZSB0byAnI3twYXRofScgdmlhICdQQVRDSCc6ICN7SlNPTi5wYXJzZShldi5kYXRhKS5kYXRhfSBcXG4gVVJMOiAnI3t1cmx9J1wiIGlmIEBkZWJ1Z1xuIiwiIyBBZGQgdGhlIGZvbGxvd2luZyBsaW5lIHRvIHlvdXIgcHJvamVjdCBpbiBGcmFtZXIgU3R1ZGlvLlxuIyBteU1vZHVsZSA9IHJlcXVpcmUgXCJteU1vZHVsZVwiXG4jIFJlZmVyZW5jZSB0aGUgY29udGVudHMgYnkgbmFtZSwgbGlrZSBteU1vZHVsZS5teUZ1bmN0aW9uKCkgb3IgbXlNb2R1bGUubXlWYXJcblxuIyBleHBvcnRzLm15VmFyID0gXCJteVZhcmlhYmxlXCJcbiNcbiMgZXhwb3J0cy5teUZ1bmN0aW9uID0gLT5cbiMgXHRwcmludCBcIm15RnVuY3Rpb24gaXMgcnVubmluZ1wiXG4jXG4jIGV4cG9ydHMubXlBcnJheSA9IFsxLCAyLCAzXVxuXG5leHBvcnRzLmJsYWNrQ29sb3IgPSBcIiMxRjFGMUZcIlxuZXhwb3J0cy53aGl0ZUNvbG9yID0gXCIjRkZGRkZGXCJcblxuZXhwb3J0cy5ncmF5ODAwID0gXCIjNEI1MTYxXCJcbmV4cG9ydHMuZ3JheTYwMCA9IFwiIzVBNjE3NVwiXG5leHBvcnRzLmdyYXk0MDAgPSBcIiM4MjhEQUJcIlxuZXhwb3J0cy5ncmF5MjAwID0gXCIjQkVDNkRBXCJcblxuZXhwb3J0cy5zdG9uZTgwMCA9IFwiI0UxRTVFQVwiXG5leHBvcnRzLnN0b25lNjAwID0gXCIjRTdFOUVGXCJcbmV4cG9ydHMuc3RvbmU0MDAgPSBcIiNFRkYyRjZcIlxuZXhwb3J0cy5zdG9uZTIwMCA9IFwiI0YxRjRGQVwiXG5cbmV4cG9ydHMuYmx1ZTgwMCA9IFwiIzAwOTVDMlwiXG5leHBvcnRzLmJsdWU2MDAgPSBcIiMwMEIwRTRcIlxuZXhwb3J0cy5ibHVlNDAwID0gXCIjNjhDOEVFXCJcbmV4cG9ydHMuYmx1ZTIwMCA9IFwiI0FERThGRlwiXG5cbmV4cG9ydHMuZ3JlZW44MDAgPSBcIiM1NkE0NzlcIlxuZXhwb3J0cy5ncmVlbjYwMCA9IFwiIzVCQ0Q4RVwiXG5leHBvcnRzLmdyZWVuNDAwID0gXCIjQkFFRUQxXCJcbmV4cG9ydHMuZ3JlZW4yMDAgPSBcIiNENkY2RTRcIlxuXG5leHBvcnRzLnJlZDgwMCA9IFwiI0I0MzczN1wiXG5leHBvcnRzLnJlZDYwMCA9IFwiI0U1NEQ0RFwiXG5leHBvcnRzLnJlZDQwMCA9IFwiI0ZGQ0NDQ1wiXG5leHBvcnRzLnJlZDIwMCA9IFwiI0ZGRjBGMFwiXG5cbmV4cG9ydHMueWVsbG93ODAwID0gXCIjRURCNTFEXCJcbmV4cG9ydHMueWVsbG93NjAwID0gXCIjRkZDODUyXCJcbmV4cG9ydHMueWVsbG93NDAwID0gXCIjRkVENThDXCJcbmV4cG9ydHMueWVsbG93MjAwID0gXCIjRkVFRENDXCJcblxuZXhwb3J0cy5jaXJjbGVDb2xvcjEgPSBcIiNGRjc3NDRcIlxuZXhwb3J0cy5jaXJjbGVDb2xvcjIgPSBcIiM5MThCRTVcIlxuZXhwb3J0cy5jaXJjbGVDb2xvcjMgPSBcIiM0RUNEQzRcIlxuXG5leHBvcnRzLnBob25lQ29udGFjdEJhY2tncm91bmRDb2xvcnMgPSBbXG5cdFwiIzRERDBFMVwiLFxuXHRcIiM5NTc1Q0RcIixcblx0XCIjRjA2MjkyXCIsXG5cdFwiI0FFRDU4MVwiLFxuXHRcIiNGRkI3NERcIixcblx0XCIjRkY4QTY1XCIsXG5cdFwiI0ZGRDU0RlwiLFxuXHRcIiM3OTg2Q0JcIixcblx0XCIjNEZDM0Y3XCJcbl1cbiIsImNsYXNzIGV4cG9ydHMuQ2lyY2xlIGV4dGVuZHMgTGF5ZXJcblx0Y3VycmVudFZhbHVlOiBudWxsXG5cblx0Y29uc3RydWN0b3I6IChAb3B0aW9ucz17fSkgLT5cblxuXHRcdEBvcHRpb25zLmNpcmNsZVNpemUgPz0gMzAwXG5cdFx0QG9wdGlvbnMuc3Ryb2tlV2lkdGggPz0gMjRcblxuXHRcdEBvcHRpb25zLnN0cm9rZUNvbG9yID89IFwiI2ZjMjQ1Y1wiXG5cdFx0QG9wdGlvbnMudG9wQ29sb3IgPz0gbnVsbFxuXHRcdEBvcHRpb25zLmJvdHRvbUNvbG9yID89IG51bGxcblxuXHRcdEBvcHRpb25zLmhhc0NvdW50ZXIgPz0gbnVsbFxuXHRcdEBvcHRpb25zLmNvdW50ZXJDb2xvciA/PSBcIiNmZmZcIlxuXHRcdEBvcHRpb25zLmNvdW50ZXJGb250U2l6ZSA/PSA2MFxuXHRcdEBvcHRpb25zLmhhc0xpbmVhckVhc2luZyA/PSBudWxsXG5cblx0XHRAb3B0aW9ucy52YWx1ZSA9IDJcblxuXHRcdEBvcHRpb25zLnZpZXdCb3ggPSAoQG9wdGlvbnMuY2lyY2xlU2l6ZSkgKyBAb3B0aW9ucy5zdHJva2VXaWR0aFxuXG5cdFx0c3VwZXIgQG9wdGlvbnNcblxuXHRcdEAuYmFja2dyb3VuZENvbG9yID0gXCJcIlxuXHRcdEAuaGVpZ2h0ID0gQG9wdGlvbnMudmlld0JveFxuXHRcdEAud2lkdGggPSBAb3B0aW9ucy52aWV3Qm94XG5cdFx0QC5yb3RhdGlvbiA9IC05MFxuXG5cblx0XHRALnBhdGhMZW5ndGggPSBNYXRoLlBJICogQG9wdGlvbnMuY2lyY2xlU2l6ZVxuXG5cdFx0QC5jaXJjbGVJRCA9IFwiY2lyY2xlXCIgKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkqMTAwMClcblx0XHRALmdyYWRpZW50SUQgPSBcImNpcmNsZVwiICsgTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpKjEwMDApXG5cblx0XHQjIFB1dCB0aGlzIGluc2lkZSBsaW5lYXJncmFkaWVudFxuXHRcdCMgZ3JhZGllbnRVbml0cz1cInVzZXJTcGFjZU9uVXNlXCJcblx0XHQjICAgIHgxPVwiMCVcIiB5MT1cIjAlXCIgeDI9XCI1MCVcIiB5Mj1cIjAlXCIgZ3JhZGllbnRUcmFuc2Zvcm09XCJyb3RhdGUoMTIwKVwiXG5cblxuXHRcdGlmIEBvcHRpb25zLmhhc0NvdW50ZXIgaXNudCBudWxsXG5cdFx0XHRjb3VudGVyID0gbmV3IExheWVyXG5cdFx0XHRcdHBhcmVudDogQFxuXHRcdFx0XHRodG1sOiBcIlwiXG5cdFx0XHRcdHdpZHRoOiBALndpZHRoXG5cdFx0XHRcdGhlaWdodDogQC5oZWlnaHRcblx0XHRcdFx0YmFja2dyb3VuZENvbG9yOiBcIlwiXG5cdFx0XHRcdHJvdGF0aW9uOiA5MFxuXHRcdFx0XHRjb2xvcjogQG9wdGlvbnMuY291bnRlckNvbG9yXG5cblx0XHRcdHN0eWxlID0ge1xuXHRcdFx0XHR0ZXh0QWxpZ246IFwiY2VudGVyXCJcblx0XHRcdFx0Zm9udFNpemU6IFwiI3tAb3B0aW9ucy5jb3VudGVyRm9udFNpemV9cHhcIlxuXHRcdFx0XHRsaW5lSGVpZ2h0OiBcIiN7QC5oZWlnaHR9cHhcIlxuXHRcdFx0XHRmb250V2VpZ2h0OiBcIjYwMFwiXG5cdFx0XHRcdGZvbnRGYW1pbHk6IFwiLWFwcGxlLXN5c3RlbSwgSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZlwiXG5cdFx0XHRcdGJveFNpemluZzogXCJib3JkZXItYm94XCJcblx0XHRcdFx0aGVpZ2h0OiBALmhlaWdodFxuXHRcdFx0fVxuXG5cdFx0XHRjb3VudGVyLnN0eWxlID0gc3R5bGVcblxuXHRcdFx0bnVtYmVyU3RhcnQgPSAwXG5cdFx0XHRudW1iZXJFbmQgPSAxMDBcblx0XHRcdG51bWJlckR1cmF0aW9uID0gMlxuXG5cdFx0XHRudW1iZXJOb3cgPSBudW1iZXJTdGFydFxuXHRcdFx0bnVtYmVySW50ZXJ2YWwgPSBudW1iZXJFbmQgLSBudW1iZXJTdGFydFxuXG5cblx0XHRALmh0bWwgPSBcIlwiXCJcblx0XHRcdDxzdmcgdmlld0JveD0nLSN7QG9wdGlvbnMuc3Ryb2tlV2lkdGgvMn0gLSN7QG9wdGlvbnMuc3Ryb2tlV2lkdGgvMn0gI3tAb3B0aW9ucy52aWV3Qm94fSAje0BvcHRpb25zLnZpZXdCb3h9JyA+XG5cdFx0XHRcdDxkZWZzPlxuXHRcdFx0XHQgICAgPGxpbmVhckdyYWRpZW50IGlkPScje0BncmFkaWVudElEfScgPlxuXHRcdFx0XHQgICAgICAgIDxzdG9wIG9mZnNldD1cIjAlXCIgc3RvcC1jb2xvcj0nI3tpZiBAb3B0aW9ucy50b3BDb2xvciBpc250IG51bGwgdGhlbiBAb3B0aW9ucy5ib3R0b21Db2xvciBlbHNlIEBvcHRpb25zLnN0cm9rZUNvbG9yfScvPlxuXHRcdFx0XHQgICAgICAgIDxzdG9wIG9mZnNldD1cIjEwMCVcIiBzdG9wLWNvbG9yPScje2lmIEBvcHRpb25zLnRvcENvbG9yIGlzbnQgbnVsbCB0aGVuIEBvcHRpb25zLnRvcENvbG9yIGVsc2UgQG9wdGlvbnMuc3Ryb2tlQ29sb3J9JyBzdG9wLW9wYWNpdHk9XCIxXCIgLz5cblx0XHRcdFx0ICAgIDwvbGluZWFyR3JhZGllbnQ+XG5cdFx0XHRcdDwvZGVmcz5cblx0XHRcdFx0PGNpcmNsZSBpZD0nI3tAY2lyY2xlSUR9J1xuXHRcdFx0XHRcdFx0ZmlsbD0nbm9uZSdcblx0XHRcdFx0XHRcdHN0cm9rZS1saW5lY2FwPSdyb3VuZCdcblx0XHRcdFx0XHRcdHN0cm9rZS13aWR0aCAgICAgID0gJyN7QG9wdGlvbnMuc3Ryb2tlV2lkdGh9J1xuXHRcdFx0XHRcdFx0c3Ryb2tlLWRhc2hhcnJheSAgPSAnI3tALnBhdGhMZW5ndGh9J1xuXHRcdFx0XHRcdFx0c3Ryb2tlLWRhc2hvZmZzZXQgPSAnMCdcblx0XHRcdFx0XHRcdHN0cm9rZT1cInVybCgjI3tAZ3JhZGllbnRJRH0pXCJcblx0XHRcdFx0XHRcdHN0cm9rZS13aWR0aD1cIjEwXCJcblx0XHRcdFx0XHRcdGN4ID0gJyN7QG9wdGlvbnMuY2lyY2xlU2l6ZS8yfSdcblx0XHRcdFx0XHRcdGN5ID0gJyN7QG9wdGlvbnMuY2lyY2xlU2l6ZS8yfSdcblx0XHRcdFx0XHRcdHIgID0gJyN7QG9wdGlvbnMuY2lyY2xlU2l6ZS8yfSc+XG5cdFx0XHQ8L3N2Zz5cIlwiXCJcblxuXHRcdHNlbGYgPSBAXG5cdFx0VXRpbHMuZG9tQ29tcGxldGUgLT5cblx0XHRcdHNlbGYucGF0aCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjI3tzZWxmLmNpcmNsZUlEfVwiKVxuXG5cdFx0QHByb3h5ID0gbmV3IExheWVyXG5cdFx0XHRvcGFjaXR5OiAwXG5cdFx0XHR2aXNpYmxlOiBmYWxzZVxuXG5cdFx0QHByb3h5Lm9uIEV2ZW50cy5BbmltYXRpb25FbmQsIChhbmltYXRpb24sIGxheWVyKSAtPlxuXHRcdFx0c2VsZi5vbkZpbmlzaGVkKClcblxuXHRcdEBwcm94eS5vbiAnY2hhbmdlOngnLCAtPlxuXG5cdFx0XHRvZmZzZXQgPSBVdGlscy5tb2R1bGF0ZShALngsIFswLCA1MDBdLCBbc2VsZi5wYXRoTGVuZ3RoLCAwXSlcblxuXHRcdFx0c2VsZi5wYXRoLnNldEF0dHJpYnV0ZSAnc3Ryb2tlLWRhc2hvZmZzZXQnLCBvZmZzZXRcblxuXHRcdFx0aWYgc2VsZi5vcHRpb25zLmhhc0NvdW50ZXIgaXNudCBudWxsXG5cdFx0XHRcdG51bWJlck5vdyA9IFV0aWxzLnJvdW5kKHNlbGYucHJveHkueCAvIDUpXG5cdFx0XHRcdGNvdW50ZXIuaHRtbCA9IG51bWJlck5vd1xuXG5cdFx0VXRpbHMuZG9tQ29tcGxldGUgLT5cblx0XHRcdHNlbGYucHJveHkueCA9IDAuMVxuXG5cdGNoYW5nZVRvOiAodmFsdWUsIHRpbWUpIC0+XG5cdFx0aWYgdGltZSBpcyB1bmRlZmluZWRcblx0XHRcdHRpbWUgPSAyXG5cblx0XHRpZiBAb3B0aW9ucy5oYXNDb3VudGVyIGlzIHRydWUgYW5kIEBvcHRpb25zLmhhc0xpbmVhckVhc2luZyBpcyBudWxsICMgb3ZlcnJpZGUgZGVmYXVsdCBcImVhc2UtaW4tb3V0XCIgd2hlbiBjb3VudGVyIGlzIHVzZWRcblx0XHRcdGN1c3RvbUN1cnZlID0gXCJsaW5lYXJcIlxuXHRcdGVsc2Vcblx0XHRcdGN1c3RvbUN1cnZlID0gXCJlYXNlLWluLW91dFwiXG5cblx0XHRAcHJveHkuYW5pbWF0ZVxuXHRcdFx0cHJvcGVydGllczpcblx0XHRcdFx0eDogNTAwICogKHZhbHVlIC8gMTAwKVxuXHRcdFx0dGltZTogdGltZVxuXHRcdFx0Y3VydmU6IGN1c3RvbUN1cnZlXG5cblxuXG5cdFx0QGN1cnJlbnRWYWx1ZSA9IHZhbHVlXG5cblx0c3RhcnRBdDogKHZhbHVlKSAtPlxuXHRcdEBwcm94eS5hbmltYXRlXG5cdFx0XHRwcm9wZXJ0aWVzOlxuXHRcdFx0XHR4OiA1MDAgKiAodmFsdWUgLyAxMDApXG5cdFx0XHR0aW1lOiAwLjAwMVxuXG5cdFx0QGN1cnJlbnRWYWx1ZSA9IHZhbHVlXG5cblxuXG5cdGhpZGU6IC0+XG5cdFx0QC5vcGFjaXR5ID0gMFxuXG5cdHNob3c6IC0+XG5cdFx0QC5vcGFjaXR5ID0gMVxuXG5cdG9uRmluaXNoZWQ6IC0+XG4iLCIjIGRvY3VtZW50YXRpb246IGh0dHBzOi8vZGV2ZWxvcGVycy5nb29nbGUuY29tL3lvdXR1YmUvaWZyYW1lX2FwaV9yZWZlcmVuY2VcblxuIyB3aWxsIHJlc29sdmUgd2hlbiB3aW5kb3cub25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkgaXMgY2FsbGVkXG55b3VUdWJlUmVhZHkgPSBuZXcgUHJvbWlzZSAocmVzb2x2ZSwgcmVqZWN0KSAtPlxuICAgIHdpbmRvdy5vbllvdVR1YmVJZnJhbWVBUElSZWFkeSA9IC0+IHJlc29sdmUoKVxuXG4jIHN0YW5kYXJkIHlvdXR1YmUgaWZyYW1lIGFwaSBpbml0aWFsaXphdGlvblxudGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCAnc2NyaXB0J1xudGFnLnNyYyA9ICdodHRwczovL3d3dy55b3V0dWJlLmNvbS9pZnJhbWVfYXBpJ1xuIyBUT0RPOiBzY3JpcHQgYXN5bmMgZGVmZXI/XG5maXJzdFNjcmlwdFRhZyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdzY3JpcHQnKVswXVxuZmlyc3RTY3JpcHRUYWcucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUgdGFnLCBmaXJzdFNjcmlwdFRhZ1xuXG5jbGFzcyBleHBvcnRzLllvdVR1YmVQbGF5ZXIgZXh0ZW5kcyBMYXllclxuXG4gICAgIyBldmVudHMsIHNlZSBodHRwczovL2RldmVsb3BlcnMuZ29vZ2xlLmNvbS95b3V0dWJlL2lmcmFtZV9hcGlfcmVmZXJlbmNlI0V2ZW50c1xuICAgIEBFdmVudHM6XG4gICAgICAgIExvYWRlZDogJ3l0LWxvYWRlZCcgIyBvY2N1cnMgd2hlbiB2aWRlbyBpcyBxdWV1ZWQgYW5kIHJlYWR5IHRvIHBsYXkuIHdpbGwgcHJvdmlkZSB0aGUgcGxheWVyIGFzIHBhcmFtZXRlci5cbiAgICAgICAgUmVhZHk6ICd5dC1yZWFkeSdcbiAgICAgICAgU3RhdGVDaGFuZ2U6ICd5dC1zdGF0ZUNoYW5nZSdcbiAgICAgICAgUGxheWJhY2tRdWFsaXR5Q2hhbmdlOiAneXQtcGxheWJhY2tRdWFsaXR5Q2hhbmdlJ1xuICAgICAgICBQbGF5YmFja1JhdGVDaGFuZ2U6ICd5dC1wbGF5YmFja1JhdGVDaGFuZ2UnXG4gICAgICAgIEVycm9yOiAneXQtZXJyb3InXG4gICAgICAgIEFwaUNoYW5nZTogJ3l0LWFwaUNoYW5nZSdcblxuICAgICMgb3B0aW9uczogeyB2aWRlbywgcGxheWVyVmFycyB9XG4gICAgIyBmb3IgcGxheWVyVmFycywgc2VlIGh0dHBzOi8vZGV2ZWxvcGVycy5nb29nbGUuY29tL3lvdXR1YmUvcGxheWVyX3BhcmFtZXRlcnNcbiAgICBjb25zdHJ1Y3RvcjogKG9wdGlvbnM9e30pIC0+XG5cbiAgICAgICAgIyB0aGlzIGRpdiB3aWxsIGJlIHJlcGxhY2VkIHdpdGggeW91dHViZSBpZnJhbWVcbiAgICAgICAgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCAnZGl2J1xuXG4gICAgICAgIEBfcGxheWVyUmVhZHkgPSBuZXcgUHJvbWlzZSAocGxheWVyUmVzb2x2ZSwgcGxheWVyUmVqZWN0KSA9PlxuXG4gICAgICAgICAgICB5b3VUdWJlUmVhZHkudGhlbiA9PlxuXG4gICAgICAgICAgICAgICAgIyBwbGF5ZXIgaXMgb25seSBhY2Nlc3NpYmxlIG9uIHJlYWR5IGV2ZW50XG4gICAgICAgICAgICAgICAgQF9wbGF5ZXIgPSBuZXcgWVQuUGxheWVyKGRpdixcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IEB3aWR0aFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IEBoZWlnaHRcbiAgICAgICAgICAgICAgICAgICAgcGxheWVyVmFyczogb3B0aW9ucy5wbGF5ZXJWYXJzXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50czpcbiAgICAgICAgICAgICAgICAgICAgICAgICdvblJlYWR5JzogKGV2ZW50KSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYXllclJlc29sdmUgZXZlbnQudGFyZ2V0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQGVtaXQgWW91VHViZVBsYXllci5FdmVudHMuUmVhZHksIGV2ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAnb25TdGF0ZUNoYW5nZSc6IChldmVudCkgPT4gQGVtaXQgWW91VHViZVBsYXllci5FdmVudHMuU3RhdGVDaGFuZ2UsIGV2ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAnb25QbGF5YmFja1F1YWxpdHlDaGFuZ2UnOiAoZXZlbnQpID0+IEBlbWl0IFlvdVR1YmVQbGF5ZXIuRXZlbnRzLlBsYXliYWNrUXVhbGl0eUNoYW5nZSwgZXZlbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICdvblBsYXliYWNrUmF0ZUNoYW5nZSc6IChldmVudCkgPT4gQGVtaXQgWW91VHViZVBsYXllci5FdmVudHMuUGxheWJhY2tSYXRlQ2hhbmdlLCBldmVudFxuICAgICAgICAgICAgICAgICAgICAgICAgJ29uRXJyb3InOiAoZXZlbnQpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxheWVyUmVqZWN0IGV2ZW50LmRhdGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBAZW1pdCBZb3VUdWJlUGxheWVyLkV2ZW50cy5FcnJvciwgZXZlbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICdvbkFwaUNoYW5nZSc6IChldmVudCkgPT4gQGVtaXQgWW91VHViZVBsYXllci5FdmVudHMuQXBpQ2hhbmdlLCBldmVudFxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgICAgICAjIG9uIHNpemUgY2hhbmdlIG9mIHRoZSBsYXllciwgcmVzaXplIHRoZSBpZnJhbWVcbiAgICAgICAgICAgICAgICBAb24gXCJjaGFuZ2U6d2lkdGhcIiwgLT4gQF9wbGF5ZXIud2lkdGggPSBAd2lkdGhcbiAgICAgICAgICAgICAgICBAb24gXCJjaGFuZ2U6aGVpZ2h0XCIsIC0+IEBfcGxheWVyLmhlaWdodCA9IEBoZWlnaHRcblxuICAgICAgICAjIGNhbGxpbmcgc3VwZXIgY2F1c2VzIEBkZWZpbmUgcHJvcGVydGllcyBiZWluZyBhc3NpZ25lZFxuICAgICAgICBzdXBlciBvcHRpb25zXG5cbiAgICAgICAgQF9lbGVtZW50LmFwcGVuZENoaWxkIGRpdlxuXG4gICAgQGRlZmluZSBcInZpZGVvXCIsXG4gICAgICAgIGdldDogLT4gQF92aWRlb1xuICAgICAgICBzZXQ6ICh2aWRlbykgLT5cbiAgICAgICAgICAgIEBfdmlkZW8gPSB2aWRlb1xuICAgICAgICAgICAgQF9wbGF5ZXJSZWFkeS50aGVuID0+XG4gICAgICAgICAgICAgICAgQF9wbGF5ZXIuY3VlVmlkZW9CeUlkIHZpZGVvXG4gICAgICAgICAgICAgICAgQF9wbGF5ZXIucGxheVZpZGVvKCkgaWYgQHBsYXllclZhcnM/LmF1dG9wbGF5XG4gICAgICAgICAgICAgICAgQGVtaXQgWW91VHViZVBsYXllci5FdmVudHMuTG9hZGVkLCBAX3BsYXllclxuXG4gICAgQGRlZmluZSBcInBsYXllclZhcnNcIixcbiAgICAgICAgZ2V0OiAtPiBAX3BsYXllclZhcnNcbiAgICAgICAgc2V0OiAodmFsdWUpIC0+IEBfcGxheWVyVmFycyA9IHZhbHVlIiwiIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcbiMgQ3JlYXRlZCBieSBKb3JkYW4gUm9iZXJ0IERvYnNvbiBvbiAwNSBPY3RvYmVyIDIwMTVcbiMgXG4jIFVzZSB0byBhZGQgZm9udCBmaWxlcyBhbmQgcmVmZXJlbmNlIHRoZW0gaW4geW91ciBDU1Mgc3R5bGUgc2V0dGluZ3MuXG4jXG4jIFRvIEdldCBTdGFydGVkLi4uXG4jXG4jIDEuIFBsYWNlIHRoZSBGb250RmFjZS5jb2ZmZWUgZmlsZSBpbiBGcmFtZXIgU3R1ZGlvIG1vZHVsZXMgZGlyZWN0b3J5XG4jXG4jIDIuIEluIHlvdXIgcHJvamVjdCBpbmNsdWRlOlxuIyAgICAge0ZvbnRGYWNlfSA9IHJlcXVpcmUgXCJGb250RmFjZVwiXG4jXG4jIDMuIFRvIGFkZCBhIGZvbnQgZmFjZTogXG4jICAgICBnb3RoYW0gPSBuZXcgRm9udEZhY2UgbmFtZTogXCJHb3RoYW1cIiwgZmlsZTogXCJHb3RoYW0udHRmXCJcbiMgXG4jIDQuIEl0IGNoZWNrcyB0aGF0IHRoZSBmb250IHdhcyBsb2FkZWQuIEVycm9ycyBjYW4gYmUgc3VwcHJlc3NlZCBsaWtlIHNvLi4uXG4jICAgIGdvdGhhbSA9IG5ldyBGb250RmFjZSBuYW1lOiBcIkdvdGhhbVwiLCBmaWxlOiBcIkdvdGhhbS50dGZcIiwgaGlkZUVycm9yczogdHJ1ZSBcbiNcbiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cbmNsYXNzIGV4cG9ydHMuRm9udEZhY2VcblxuXHRURVNUID1cblx0XHRmYWNlOiBcIm1vbm9zcGFjZVwiXG5cdFx0dGV4dDogXCJmb29cIlxuXHRcdHRpbWU6IC4wMVxuXHRcdG1heExvYWRBdHRlbXB0czogNTBcblx0XHRoaWRlRXJyb3JNZXNzYWdlczogdHJ1ZVxuXHRcdFxuXHRURVNULnN0eWxlID0gXG5cdFx0d2lkdGg6IFwiYXV0b1wiXG5cdFx0Zm9udFNpemU6IFwiMTUwcHhcIlxuXHRcdGZvbnRGYW1pbHk6IFRFU1QuZmFjZVxuXHRcdFxuXHRURVNULmxheWVyID0gbmV3IExheWVyXG5cdFx0bmFtZTpcIkZvbnRGYWNlIFRlc3RlclwiXG5cdFx0d2lkdGg6IDBcblx0XHRoZWlnaHQ6IDFcblx0XHRtYXhYOiAtKFNjcmVlbi53aWR0aClcblx0XHR2aXNpYmxlOiBmYWxzZVxuXHRcdGh0bWw6IFRFU1QudGV4dFxuXHRcdHN0eWxlOiBURVNULnN0eWxlXG5cdFx0XG5cdFxuXHQjIFNFVFVQIEZPUiBFVkVSWSBJTlNUQU5DRVxuXHRjb25zdHJ1Y3RvcjogKG9wdGlvbnMpIC0+XG5cdFxuXHRcdEBuYW1lID0gQGZpbGUgPSBAdGVzdExheWVyID0gQGlzTG9hZGVkID0gQGxvYWRGYWlsZWQgPSBAbG9hZEF0dGVtcHRzID0gQG9yaWdpbmFsU2l6ZSA9IEBoaWRlRXJyb3JzID0gIG51bGxcblx0XHRcblx0XHRpZiBvcHRpb25zP1xuXHRcdFx0QG5hbWUgPSBvcHRpb25zLm5hbWUgfHwgbnVsbFxuXHRcdFx0QGZpbGUgPSBvcHRpb25zLmZpbGUgfHwgbnVsbFxuXHRcdFxuXHRcdHJldHVybiBtaXNzaW5nQXJndW1lbnRFcnJvcigpIHVubGVzcyBAbmFtZT8gYW5kIEBmaWxlP1xuXHRcdFxuXHRcdEB0ZXN0TGF5ZXIgICAgICAgICA9IFRFU1QubGF5ZXIuY29weSgpXG5cdFx0QHRlc3RMYXllci5zdHlsZSAgID0gVEVTVC5zdHlsZVxuXHRcdEB0ZXN0TGF5ZXIubWF4WCAgICA9IC0oU2NyZWVuLndpZHRoKVxuXHRcdEB0ZXN0TGF5ZXIudmlzaWJsZSA9IHRydWVcblx0XHRcblx0XHRAaXNMb2FkZWQgICAgID0gZmFsc2Vcblx0XHRAbG9hZEZhaWxlZCAgID0gZmFsc2Vcblx0XHRAbG9hZEF0dGVtcHRzID0gMFxuXHRcdEBoaWRlRXJyb3JzICAgPSBvcHRpb25zLmhpZGVFcnJvcnNcblxuXHRcdHJldHVybiBhZGRGb250RmFjZSBAbmFtZSwgQGZpbGUsIEBcblx0XHRcblx0IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cdCMgUHJpdmF0ZSBIZWxwZXIgTWV0aG9kcyAjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXHRcdFxuXHRhZGRGb250RmFjZSA9IChuYW1lLCBmaWxlLCBvYmplY3QpIC0+XG5cdFx0IyBDcmVhdGUgb3VyIEVsZW1lbnQgJiBOb2RlXG5cdFx0c3R5bGVUYWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50ICdzdHlsZSdcblx0XHRmYWNlQ1NTICA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlIFwiQGZvbnQtZmFjZSB7IGZvbnQtZmFtaWx5OiAnI3tuYW1lfSc7IHNyYzogdXJsKCcje2ZpbGV9JykgZm9ybWF0KCd0cnVldHlwZScpOyB9XCJcblx0XHQjIEFkZCB0aGUgRWxlbWVudCAmIE5vZGUgdG8gdGhlIGRvY3VtZW50XG5cdFx0c3R5bGVUYWcuYXBwZW5kQ2hpbGQgZmFjZUNTU1xuXHRcdGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQgc3R5bGVUYWdcblx0XHQjIFRlc3Qgb3V0IHRoZSBGYXN0IHRvIHNlZSBpZiBpdCBjaGFuZ2VkXG5cdFx0dGVzdE5ld0ZhY2UgbmFtZSwgb2JqZWN0XG5cdFx0XG5cdCMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXHRcdFxuXHRyZW1vdmVUZXN0TGF5ZXIgPSAob2JqZWN0KSAtPlxuXHRcdG9iamVjdC50ZXN0TGF5ZXIuZGVzdHJveSgpXG5cdFx0b2JqZWN0LnRlc3RMYXllciA9IG51bGxcblx0XHRcblx0IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cdFx0XG5cdHRlc3ROZXdGYWNlID0gKG5hbWUsIG9iamVjdCkgLT5cblx0XHRcblx0XHRpbml0aWFsV2lkdGggPSBvYmplY3QudGVzdExheWVyLl9lbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoXG5cdFx0XG5cdFx0IyBDaGVjayB0byBzZWUgaWYgaXQncyByZWFkeSB5ZXRcblx0XHRpZiBpbml0aWFsV2lkdGggaXMgMFxuXHRcdFx0aWYgb2JqZWN0LmhpZGVFcnJvcnMgaXMgZmFsc2Ugb3IgVEVTVC5oaWRlRXJyb3JNZXNzYWdlcyBpcyBmYWxzZVxuXHRcdFx0XHRwcmludCBcIkxvYWQgdGVzdGluZyBmYWlsZWQuIEF0dGVtcHRpbmcgYWdhaW4uXCJcblx0XHRcdHJldHVybiBVdGlscy5kZWxheSBURVNULnRpbWUsIC0+IHRlc3ROZXdGYWNlIG5hbWUsIG9iamVjdFxuXHRcdFxuXHRcdG9iamVjdC5sb2FkQXR0ZW1wdHMrK1xuXHRcdFxuXHRcdGlmIG9iamVjdC5vcmlnaW5hbFNpemUgaXMgbnVsbFxuXHRcdFx0b2JqZWN0Lm9yaWdpbmFsU2l6ZSA9IGluaXRpYWxXaWR0aFxuXHRcdFx0b2JqZWN0LnRlc3RMYXllci5zdHlsZSA9IGZvbnRGYW1pbHk6IFwiI3tuYW1lfSwgI3tURVNULmZhY2V9XCJcblx0XHRcblx0XHR3aWR0aFVwZGF0ZSA9IG9iamVjdC50ZXN0TGF5ZXIuX2VsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGhcblxuXHRcdGlmIG9iamVjdC5vcmlnaW5hbFNpemUgaXMgd2lkdGhVcGRhdGVcblx0XHRcdCMgSWYgd2UgY2FuIGF0dGVtcHQgdG8gY2hlY2sgYWdhaW4uLi4gRG8gaXRcblx0XHRcdGlmIG9iamVjdC5sb2FkQXR0ZW1wdHMgPCBURVNULm1heExvYWRBdHRlbXB0c1xuXHRcdFx0XHRyZXR1cm4gVXRpbHMuZGVsYXkgVEVTVC50aW1lLCAtPiB0ZXN0TmV3RmFjZSBuYW1lLCBvYmplY3Rcblx0XHRcdFx0XG5cdFx0XHRwcmludCBcIuKaoO+4jyBGYWlsZWQgbG9hZGluZyBGb250RmFjZTogI3tuYW1lfVwiIHVubGVzcyBvYmplY3QuaGlkZUVycm9yc1xuXHRcdFx0b2JqZWN0LmlzTG9hZGVkICAgPSBmYWxzZVxuXHRcdFx0b2JqZWN0LmxvYWRGYWlsZWQgPSB0cnVlXG5cdFx0XHRsb2FkVGVzdGluZ0ZpbGVFcnJvciBvYmplY3QgdW5sZXNzIG9iamVjdC5oaWRlRXJyb3JzXG5cdFx0XHRyZXR1cm5cblx0XHRcdFxuXHRcdGVsc2Vcblx0XHRcdHByaW50IFwiTE9BREVEOiAje25hbWV9XCIgdW5sZXNzIG9iamVjdC5oaWRlRXJyb3JzIGlzIGZhbHNlIG9yIFRFU1QuaGlkZUVycm9yTWVzc2FnZXNcblx0XHRcdG9iamVjdC5pc0xvYWRlZCAgID0gdHJ1ZVxuXHRcdFx0b2JqZWN0LmxvYWRGYWlsZWQgPSBmYWxzZVxuXG5cdFx0cmVtb3ZlVGVzdExheWVyIG9iamVjdFxuXHRcdHJldHVybiBuYW1lXG5cblx0IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cdCMgRXJyb3IgSGFuZGxlciBNZXRob2RzICMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXG5cdG1pc3NpbmdBcmd1bWVudEVycm9yID0gLT5cblx0XHRlcnJvciBudWxsXG5cdFx0Y29uc29sZS5lcnJvciBcIlwiXCJcblx0XHRcdEVycm9yOiBZb3UgbXVzdCBwYXNzIG5hbWUgJiBmaWxlIHByb3Blcml0ZXMgd2hlbiBjcmVhdGluZyBhIG5ldyBGb250RmFjZS4gXFxuXG5cdFx0XHRFeGFtcGxlOiBteUZhY2UgPSBuZXcgRm9udEZhY2UgbmFtZTpcXFwiR290aGFtXFxcIiwgZmlsZTpcXFwiZ290aGFtLnR0ZlxcXCIgXFxuXCJcIlwiXG5cdFx0XHRcblx0bG9hZFRlc3RpbmdGaWxlRXJyb3IgPSAob2JqZWN0KSAtPlxuXHRcdGVycm9yIG51bGxcblx0XHRjb25zb2xlLmVycm9yIFwiXCJcIlxuXHRcdFx0RXJyb3I6IENvdWxkbid0IGRldGVjdCB0aGUgZm9udDogXFxcIiN7b2JqZWN0Lm5hbWV9XFxcIiBhbmQgZmlsZTogXFxcIiN7b2JqZWN0LmZpbGV9XFxcIiB3YXMgbG9hZGVkLiAgXFxuXG5cdFx0XHRFaXRoZXIgdGhlIGZpbGUgY291bGRuJ3QgYmUgZm91bmQgb3IgeW91ciBicm93c2VyIGRvZXNuJ3Qgc3VwcG9ydCB0aGUgZmlsZSB0eXBlIHRoYXQgd2FzIHByb3ZpZGVkLiBcXG5cblx0XHRcdFN1cHByZXNzIHRoaXMgbWVzc2FnZSBieSBhZGRpbmcgXFxcImhpZGVFcnJvcnM6IHRydWVcXFwiIHdoZW4gY3JlYXRpbmcgYSBuZXcgRm9udEZhY2UuIFxcblwiXCJcIlxuIiwiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFVQUE7QURvQk0sT0FBTyxDQUFDO0FBRWIsTUFBQTs7RUFBQSxJQUFBLEdBQ0M7SUFBQSxJQUFBLEVBQU0sV0FBTjtJQUNBLElBQUEsRUFBTSxLQUROO0lBRUEsSUFBQSxFQUFNLEdBRk47SUFHQSxlQUFBLEVBQWlCLEVBSGpCO0lBSUEsaUJBQUEsRUFBbUIsSUFKbkI7OztFQU1ELElBQUksQ0FBQyxLQUFMLEdBQ0M7SUFBQSxLQUFBLEVBQU8sTUFBUDtJQUNBLFFBQUEsRUFBVSxPQURWO0lBRUEsVUFBQSxFQUFZLElBQUksQ0FBQyxJQUZqQjs7O0VBSUQsSUFBSSxDQUFDLEtBQUwsR0FBaUIsSUFBQSxLQUFBLENBQ2hCO0lBQUEsSUFBQSxFQUFLLGlCQUFMO0lBQ0EsS0FBQSxFQUFPLENBRFA7SUFFQSxNQUFBLEVBQVEsQ0FGUjtJQUdBLElBQUEsRUFBTSxDQUFFLE1BQU0sQ0FBQyxLQUhmO0lBSUEsT0FBQSxFQUFTLEtBSlQ7SUFLQSxJQUFBLEVBQU0sSUFBSSxDQUFDLElBTFg7SUFNQSxLQUFBLEVBQU8sSUFBSSxDQUFDLEtBTlo7R0FEZ0I7O0VBV0osa0JBQUMsT0FBRDtJQUVaLElBQUMsQ0FBQSxJQUFELEdBQVEsSUFBQyxDQUFBLElBQUQsR0FBUSxJQUFDLENBQUEsU0FBRCxHQUFhLElBQUMsQ0FBQSxRQUFELEdBQVksSUFBQyxDQUFBLFVBQUQsR0FBYyxJQUFDLENBQUEsWUFBRCxHQUFnQixJQUFDLENBQUEsWUFBRCxHQUFnQixJQUFDLENBQUEsVUFBRCxHQUFlO0lBRXRHLElBQUcsZUFBSDtNQUNDLElBQUMsQ0FBQSxJQUFELEdBQVEsT0FBTyxDQUFDLElBQVIsSUFBZ0I7TUFDeEIsSUFBQyxDQUFBLElBQUQsR0FBUSxPQUFPLENBQUMsSUFBUixJQUFnQixLQUZ6Qjs7SUFJQSxJQUFBLENBQUEsQ0FBcUMsbUJBQUEsSUFBVyxtQkFBaEQsQ0FBQTtBQUFBLGFBQU8sb0JBQUEsQ0FBQSxFQUFQOztJQUVBLElBQUMsQ0FBQSxTQUFELEdBQXFCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBWCxDQUFBO0lBQ3JCLElBQUMsQ0FBQSxTQUFTLENBQUMsS0FBWCxHQUFxQixJQUFJLENBQUM7SUFDMUIsSUFBQyxDQUFBLFNBQVMsQ0FBQyxJQUFYLEdBQXFCLENBQUUsTUFBTSxDQUFDO0lBQzlCLElBQUMsQ0FBQSxTQUFTLENBQUMsT0FBWCxHQUFxQjtJQUVyQixJQUFDLENBQUEsUUFBRCxHQUFnQjtJQUNoQixJQUFDLENBQUEsVUFBRCxHQUFnQjtJQUNoQixJQUFDLENBQUEsWUFBRCxHQUFnQjtJQUNoQixJQUFDLENBQUEsVUFBRCxHQUFnQixPQUFPLENBQUM7QUFFeEIsV0FBTyxXQUFBLENBQVksSUFBQyxDQUFBLElBQWIsRUFBbUIsSUFBQyxDQUFBLElBQXBCLEVBQTBCLElBQTFCO0VBcEJLOztFQXlCYixXQUFBLEdBQWMsU0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLE1BQWI7QUFFYixRQUFBO0lBQUEsUUFBQSxHQUFXLFFBQVEsQ0FBQyxhQUFULENBQXVCLE9BQXZCO0lBQ1gsT0FBQSxHQUFXLFFBQVEsQ0FBQyxjQUFULENBQXdCLDZCQUFBLEdBQThCLElBQTlCLEdBQW1DLGVBQW5DLEdBQWtELElBQWxELEdBQXVELDBCQUEvRTtJQUVYLFFBQVEsQ0FBQyxXQUFULENBQXFCLE9BQXJCO0lBQ0EsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFkLENBQTBCLFFBQTFCO1dBRUEsV0FBQSxDQUFZLElBQVosRUFBa0IsTUFBbEI7RUFSYTs7RUFZZCxlQUFBLEdBQWtCLFNBQUMsTUFBRDtJQUNqQixNQUFNLENBQUMsU0FBUyxDQUFDLE9BQWpCLENBQUE7V0FDQSxNQUFNLENBQUMsU0FBUCxHQUFtQjtFQUZGOztFQU1sQixXQUFBLEdBQWMsU0FBQyxJQUFELEVBQU8sTUFBUDtBQUViLFFBQUE7SUFBQSxZQUFBLEdBQWUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQTFCLENBQUEsQ0FBaUQsQ0FBQztJQUdqRSxJQUFHLFlBQUEsS0FBZ0IsQ0FBbkI7TUFDQyxJQUFHLE1BQU0sQ0FBQyxVQUFQLEtBQXFCLEtBQXJCLElBQThCLElBQUksQ0FBQyxpQkFBTCxLQUEwQixLQUEzRDtRQUNDLEtBQUEsQ0FBTSx3Q0FBTixFQUREOztBQUVBLGFBQU8sS0FBSyxDQUFDLEtBQU4sQ0FBWSxJQUFJLENBQUMsSUFBakIsRUFBdUIsU0FBQTtlQUFHLFdBQUEsQ0FBWSxJQUFaLEVBQWtCLE1BQWxCO01BQUgsQ0FBdkIsRUFIUjs7SUFLQSxNQUFNLENBQUMsWUFBUDtJQUVBLElBQUcsTUFBTSxDQUFDLFlBQVAsS0FBdUIsSUFBMUI7TUFDQyxNQUFNLENBQUMsWUFBUCxHQUFzQjtNQUN0QixNQUFNLENBQUMsU0FBUyxDQUFDLEtBQWpCLEdBQXlCO1FBQUEsVUFBQSxFQUFlLElBQUQsR0FBTSxJQUFOLEdBQVUsSUFBSSxDQUFDLElBQTdCO1FBRjFCOztJQUlBLFdBQUEsR0FBYyxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBMUIsQ0FBQSxDQUFpRCxDQUFDO0lBRWhFLElBQUcsTUFBTSxDQUFDLFlBQVAsS0FBdUIsV0FBMUI7TUFFQyxJQUFHLE1BQU0sQ0FBQyxZQUFQLEdBQXNCLElBQUksQ0FBQyxlQUE5QjtBQUNDLGVBQU8sS0FBSyxDQUFDLEtBQU4sQ0FBWSxJQUFJLENBQUMsSUFBakIsRUFBdUIsU0FBQTtpQkFBRyxXQUFBLENBQVksSUFBWixFQUFrQixNQUFsQjtRQUFILENBQXZCLEVBRFI7O01BR0EsSUFBQSxDQUFtRCxNQUFNLENBQUMsVUFBMUQ7UUFBQSxLQUFBLENBQU0sOEJBQUEsR0FBK0IsSUFBckMsRUFBQTs7TUFDQSxNQUFNLENBQUMsUUFBUCxHQUFvQjtNQUNwQixNQUFNLENBQUMsVUFBUCxHQUFvQjtNQUNwQixJQUFBLENBQW1DLE1BQU0sQ0FBQyxVQUExQztRQUFBLG9CQUFBLENBQXFCLE1BQXJCLEVBQUE7O0FBQ0EsYUFURDtLQUFBLE1BQUE7TUFZQyxJQUFBLENBQUEsQ0FBK0IsTUFBTSxDQUFDLFVBQVAsS0FBcUIsS0FBckIsSUFBOEIsSUFBSSxDQUFDLGlCQUFsRSxDQUFBO1FBQUEsS0FBQSxDQUFNLFVBQUEsR0FBVyxJQUFqQixFQUFBOztNQUNBLE1BQU0sQ0FBQyxRQUFQLEdBQW9CO01BQ3BCLE1BQU0sQ0FBQyxVQUFQLEdBQW9CLE1BZHJCOztJQWdCQSxlQUFBLENBQWdCLE1BQWhCO0FBQ0EsV0FBTztFQW5DTTs7RUF3Q2Qsb0JBQUEsR0FBdUIsU0FBQTtJQUN0QixLQUFBLENBQU0sSUFBTjtXQUNBLE9BQU8sQ0FBQyxLQUFSLENBQWMsc0pBQWQ7RUFGc0I7O0VBTXZCLG9CQUFBLEdBQXVCLFNBQUMsTUFBRDtJQUN0QixLQUFBLENBQU0sSUFBTjtXQUNBLE9BQU8sQ0FBQyxLQUFSLENBQWMscUNBQUEsR0FDd0IsTUFBTSxDQUFDLElBRC9CLEdBQ29DLGlCQURwQyxHQUNxRCxNQUFNLENBQUMsSUFENUQsR0FDaUUsa05BRC9FO0VBRnNCOzs7Ozs7OztBRG5JeEIsSUFBQSxpQ0FBQTtFQUFBOzs7QUFBQSxZQUFBLEdBQW1CLElBQUEsT0FBQSxDQUFRLFNBQUMsT0FBRCxFQUFVLE1BQVY7U0FDdkIsTUFBTSxDQUFDLHVCQUFQLEdBQWlDLFNBQUE7V0FBRyxPQUFBLENBQUE7RUFBSDtBQURWLENBQVI7O0FBSW5CLEdBQUEsR0FBTSxRQUFRLENBQUMsYUFBVCxDQUF1QixRQUF2Qjs7QUFDTixHQUFHLENBQUMsR0FBSixHQUFVOztBQUVWLGNBQUEsR0FBaUIsUUFBUSxDQUFDLG9CQUFULENBQThCLFFBQTlCLENBQXdDLENBQUEsQ0FBQTs7QUFDekQsY0FBYyxDQUFDLFVBQVUsQ0FBQyxZQUExQixDQUF1QyxHQUF2QyxFQUE0QyxjQUE1Qzs7QUFFTSxPQUFPLENBQUM7OztFQUdWLGFBQUMsQ0FBQSxNQUFELEdBQ0k7SUFBQSxNQUFBLEVBQVEsV0FBUjtJQUNBLEtBQUEsRUFBTyxVQURQO0lBRUEsV0FBQSxFQUFhLGdCQUZiO0lBR0EscUJBQUEsRUFBdUIsMEJBSHZCO0lBSUEsa0JBQUEsRUFBb0IsdUJBSnBCO0lBS0EsS0FBQSxFQUFPLFVBTFA7SUFNQSxTQUFBLEVBQVcsY0FOWDs7O0VBVVMsdUJBQUMsT0FBRDtBQUdULFFBQUE7O01BSFUsVUFBUTs7SUFHbEIsR0FBQSxHQUFNLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO0lBRU4sSUFBQyxDQUFBLFlBQUQsR0FBb0IsSUFBQSxPQUFBLENBQVEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLGFBQUQsRUFBZ0IsWUFBaEI7ZUFFeEIsWUFBWSxDQUFDLElBQWIsQ0FBa0IsU0FBQTtVQUdkLEtBQUMsQ0FBQSxPQUFELEdBQWUsSUFBQSxFQUFFLENBQUMsTUFBSCxDQUFVLEdBQVYsRUFDWDtZQUFBLEtBQUEsRUFBTyxLQUFDLENBQUEsS0FBUjtZQUNBLE1BQUEsRUFBUSxLQUFDLENBQUEsTUFEVDtZQUVBLFVBQUEsRUFBWSxPQUFPLENBQUMsVUFGcEI7WUFHQSxNQUFBLEVBQ0k7Y0FBQSxTQUFBLEVBQVcsU0FBQyxLQUFEO2dCQUNQLGFBQUEsQ0FBYyxLQUFLLENBQUMsTUFBcEI7dUJBQ0EsS0FBQyxDQUFBLElBQUQsQ0FBTSxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQTNCLEVBQWtDLEtBQWxDO2NBRk8sQ0FBWDtjQUdBLGVBQUEsRUFBaUIsU0FBQyxLQUFEO3VCQUFXLEtBQUMsQ0FBQSxJQUFELENBQU0sYUFBYSxDQUFDLE1BQU0sQ0FBQyxXQUEzQixFQUF3QyxLQUF4QztjQUFYLENBSGpCO2NBSUEseUJBQUEsRUFBMkIsU0FBQyxLQUFEO3VCQUFXLEtBQUMsQ0FBQSxJQUFELENBQU0sYUFBYSxDQUFDLE1BQU0sQ0FBQyxxQkFBM0IsRUFBa0QsS0FBbEQ7Y0FBWCxDQUozQjtjQUtBLHNCQUFBLEVBQXdCLFNBQUMsS0FBRDt1QkFBVyxLQUFDLENBQUEsSUFBRCxDQUFNLGFBQWEsQ0FBQyxNQUFNLENBQUMsa0JBQTNCLEVBQStDLEtBQS9DO2NBQVgsQ0FMeEI7Y0FNQSxTQUFBLEVBQVcsU0FBQyxLQUFEO2dCQUNQLFlBQUEsQ0FBYSxLQUFLLENBQUMsSUFBbkI7dUJBQ0EsS0FBQyxDQUFBLElBQUQsQ0FBTSxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQTNCLEVBQWtDLEtBQWxDO2NBRk8sQ0FOWDtjQVNBLGFBQUEsRUFBZSxTQUFDLEtBQUQ7dUJBQVcsS0FBQyxDQUFBLElBQUQsQ0FBTSxhQUFhLENBQUMsTUFBTSxDQUFDLFNBQTNCLEVBQXNDLEtBQXRDO2NBQVgsQ0FUZjthQUpKO1dBRFc7VUFrQmYsS0FBQyxDQUFBLEVBQUQsQ0FBSSxjQUFKLEVBQW9CLFNBQUE7bUJBQUcsSUFBQyxDQUFBLE9BQU8sQ0FBQyxLQUFULEdBQWlCLElBQUMsQ0FBQTtVQUFyQixDQUFwQjtpQkFDQSxLQUFDLENBQUEsRUFBRCxDQUFJLGVBQUosRUFBcUIsU0FBQTttQkFBRyxJQUFDLENBQUEsT0FBTyxDQUFDLE1BQVQsR0FBa0IsSUFBQyxDQUFBO1VBQXRCLENBQXJCO1FBdEJjLENBQWxCO01BRndCO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFSO0lBMkJwQiwrQ0FBTSxPQUFOO0lBRUEsSUFBQyxDQUFBLFFBQVEsQ0FBQyxXQUFWLENBQXNCLEdBQXRCO0VBbENTOztFQW9DYixhQUFDLENBQUEsTUFBRCxDQUFRLE9BQVIsRUFDSTtJQUFBLEdBQUEsRUFBSyxTQUFBO2FBQUcsSUFBQyxDQUFBO0lBQUosQ0FBTDtJQUNBLEdBQUEsRUFBSyxTQUFDLEtBQUQ7TUFDRCxJQUFDLENBQUEsTUFBRCxHQUFVO2FBQ1YsSUFBQyxDQUFBLFlBQVksQ0FBQyxJQUFkLENBQW1CLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtBQUNmLGNBQUE7VUFBQSxLQUFDLENBQUEsT0FBTyxDQUFDLFlBQVQsQ0FBc0IsS0FBdEI7VUFDQSwwQ0FBbUMsQ0FBRSxpQkFBckM7WUFBQSxLQUFDLENBQUEsT0FBTyxDQUFDLFNBQVQsQ0FBQSxFQUFBOztpQkFDQSxLQUFDLENBQUEsSUFBRCxDQUFNLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBM0IsRUFBbUMsS0FBQyxDQUFBLE9BQXBDO1FBSGU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQW5CO0lBRkMsQ0FETDtHQURKOztFQVNBLGFBQUMsQ0FBQSxNQUFELENBQVEsWUFBUixFQUNJO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUE7SUFBSixDQUFMO0lBQ0EsR0FBQSxFQUFLLFNBQUMsS0FBRDthQUFXLElBQUMsQ0FBQSxXQUFELEdBQWU7SUFBMUIsQ0FETDtHQURKOzs7O0dBM0RnQzs7OztBRGJwQyxJQUFBOzs7QUFBTSxPQUFPLENBQUM7OzttQkFDYixZQUFBLEdBQWM7O0VBRUQsZ0JBQUMsT0FBRDtBQUVaLFFBQUE7SUFGYSxJQUFDLENBQUEsNEJBQUQsVUFBUzs7VUFFZCxDQUFDLGFBQWM7OztXQUNmLENBQUMsY0FBZTs7O1dBRWhCLENBQUMsY0FBZTs7O1dBQ2hCLENBQUMsV0FBWTs7O1dBQ2IsQ0FBQyxjQUFlOzs7V0FFaEIsQ0FBQyxhQUFjOzs7V0FDZixDQUFDLGVBQWdCOzs7V0FDakIsQ0FBQyxrQkFBbUI7OztXQUNwQixDQUFDLGtCQUFtQjs7SUFFNUIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxLQUFULEdBQWlCO0lBRWpCLElBQUMsQ0FBQSxPQUFPLENBQUMsT0FBVCxHQUFvQixJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVYsR0FBd0IsSUFBQyxDQUFBLE9BQU8sQ0FBQztJQUVwRCx3Q0FBTSxJQUFDLENBQUEsT0FBUDtJQUVBLElBQUMsQ0FBQyxlQUFGLEdBQW9CO0lBQ3BCLElBQUMsQ0FBQyxNQUFGLEdBQVcsSUFBQyxDQUFBLE9BQU8sQ0FBQztJQUNwQixJQUFDLENBQUMsS0FBRixHQUFVLElBQUMsQ0FBQSxPQUFPLENBQUM7SUFDbkIsSUFBQyxDQUFDLFFBQUYsR0FBYSxDQUFDO0lBR2QsSUFBQyxDQUFDLFVBQUYsR0FBZSxJQUFJLENBQUMsRUFBTCxHQUFVLElBQUMsQ0FBQSxPQUFPLENBQUM7SUFFbEMsSUFBQyxDQUFDLFFBQUYsR0FBYSxRQUFBLEdBQVcsSUFBSSxDQUFDLEtBQUwsQ0FBVyxJQUFJLENBQUMsTUFBTCxDQUFBLENBQUEsR0FBYyxJQUF6QjtJQUN4QixJQUFDLENBQUMsVUFBRixHQUFlLFFBQUEsR0FBVyxJQUFJLENBQUMsS0FBTCxDQUFXLElBQUksQ0FBQyxNQUFMLENBQUEsQ0FBQSxHQUFjLElBQXpCO0lBTzFCLElBQUcsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUFULEtBQXlCLElBQTVCO01BQ0MsT0FBQSxHQUFjLElBQUEsS0FBQSxDQUNiO1FBQUEsTUFBQSxFQUFRLElBQVI7UUFDQSxJQUFBLEVBQU0sRUFETjtRQUVBLEtBQUEsRUFBTyxJQUFDLENBQUMsS0FGVDtRQUdBLE1BQUEsRUFBUSxJQUFDLENBQUMsTUFIVjtRQUlBLGVBQUEsRUFBaUIsRUFKakI7UUFLQSxRQUFBLEVBQVUsRUFMVjtRQU1BLEtBQUEsRUFBTyxJQUFDLENBQUEsT0FBTyxDQUFDLFlBTmhCO09BRGE7TUFTZCxLQUFBLEdBQVE7UUFDUCxTQUFBLEVBQVcsUUFESjtRQUVQLFFBQUEsRUFBYSxJQUFDLENBQUEsT0FBTyxDQUFDLGVBQVYsR0FBMEIsSUFGL0I7UUFHUCxVQUFBLEVBQWUsSUFBQyxDQUFDLE1BQUgsR0FBVSxJQUhqQjtRQUlQLFVBQUEsRUFBWSxLQUpMO1FBS1AsVUFBQSxFQUFZLDZDQUxMO1FBTVAsU0FBQSxFQUFXLFlBTko7UUFPUCxNQUFBLEVBQVEsSUFBQyxDQUFDLE1BUEg7O01BVVIsT0FBTyxDQUFDLEtBQVIsR0FBZ0I7TUFFaEIsV0FBQSxHQUFjO01BQ2QsU0FBQSxHQUFZO01BQ1osY0FBQSxHQUFpQjtNQUVqQixTQUFBLEdBQVk7TUFDWixjQUFBLEdBQWlCLFNBQUEsR0FBWSxZQTNCOUI7O0lBOEJBLElBQUMsQ0FBQyxJQUFGLEdBQVMsaUJBQUEsR0FDUSxDQUFDLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBVCxHQUFxQixDQUF0QixDQURSLEdBQ2dDLElBRGhDLEdBQ21DLENBQUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULEdBQXFCLENBQXRCLENBRG5DLEdBQzJELEdBRDNELEdBQzhELElBQUMsQ0FBQSxPQUFPLENBQUMsT0FEdkUsR0FDK0UsR0FEL0UsR0FDa0YsSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUQzRixHQUNtRyx5Q0FEbkcsR0FHbUIsSUFBQyxDQUFBLFVBSHBCLEdBRytCLGdEQUgvQixHQUlnQyxDQUFJLElBQUMsQ0FBQSxPQUFPLENBQUMsUUFBVCxLQUF1QixJQUExQixHQUFvQyxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQTdDLEdBQThELElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBeEUsQ0FKaEMsR0FJb0gsa0RBSnBILEdBS2tDLENBQUksSUFBQyxDQUFBLE9BQU8sQ0FBQyxRQUFULEtBQXVCLElBQTFCLEdBQW9DLElBQUMsQ0FBQSxPQUFPLENBQUMsUUFBN0MsR0FBMkQsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFyRSxDQUxsQyxHQUttSCwwRUFMbkgsR0FRTyxJQUFDLENBQUEsUUFSUixHQVFpQix3RUFSakIsR0FXa0IsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQVgzQixHQVd1Qyw2QkFYdkMsR0FZa0IsSUFBQyxDQUFDLFVBWnBCLEdBWStCLGtEQVovQixHQWNVLElBQUMsQ0FBQSxVQWRYLEdBY3NCLHdDQWR0QixHQWdCRSxDQUFDLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVCxHQUFvQixDQUFyQixDQWhCRixHQWdCeUIsY0FoQnpCLEdBaUJFLENBQUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUFULEdBQW9CLENBQXJCLENBakJGLEdBaUJ5QixjQWpCekIsR0FrQkUsQ0FBQyxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsR0FBb0IsQ0FBckIsQ0FsQkYsR0FrQnlCO0lBR2xDLElBQUEsR0FBTztJQUNQLEtBQUssQ0FBQyxXQUFOLENBQWtCLFNBQUE7YUFDakIsSUFBSSxDQUFDLElBQUwsR0FBWSxRQUFRLENBQUMsYUFBVCxDQUF1QixHQUFBLEdBQUksSUFBSSxDQUFDLFFBQWhDO0lBREssQ0FBbEI7SUFHQSxJQUFDLENBQUEsS0FBRCxHQUFhLElBQUEsS0FBQSxDQUNaO01BQUEsT0FBQSxFQUFTLENBQVQ7TUFDQSxPQUFBLEVBQVMsS0FEVDtLQURZO0lBSWIsSUFBQyxDQUFBLEtBQUssQ0FBQyxFQUFQLENBQVUsTUFBTSxDQUFDLFlBQWpCLEVBQStCLFNBQUMsU0FBRCxFQUFZLEtBQVo7YUFDOUIsSUFBSSxDQUFDLFVBQUwsQ0FBQTtJQUQ4QixDQUEvQjtJQUdBLElBQUMsQ0FBQSxLQUFLLENBQUMsRUFBUCxDQUFVLFVBQVYsRUFBc0IsU0FBQTtBQUVyQixVQUFBO01BQUEsTUFBQSxHQUFTLEtBQUssQ0FBQyxRQUFOLENBQWUsSUFBQyxDQUFDLENBQWpCLEVBQW9CLENBQUMsQ0FBRCxFQUFJLEdBQUosQ0FBcEIsRUFBOEIsQ0FBQyxJQUFJLENBQUMsVUFBTixFQUFrQixDQUFsQixDQUE5QjtNQUVULElBQUksQ0FBQyxJQUFJLENBQUMsWUFBVixDQUF1QixtQkFBdkIsRUFBNEMsTUFBNUM7TUFFQSxJQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBYixLQUE2QixJQUFoQztRQUNDLFNBQUEsR0FBWSxLQUFLLENBQUMsS0FBTixDQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBWCxHQUFlLENBQTNCO2VBQ1osT0FBTyxDQUFDLElBQVIsR0FBZSxVQUZoQjs7SUFOcUIsQ0FBdEI7SUFVQSxLQUFLLENBQUMsV0FBTixDQUFrQixTQUFBO2FBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBWCxHQUFlO0lBREUsQ0FBbEI7RUE1R1k7O21CQStHYixRQUFBLEdBQVUsU0FBQyxLQUFELEVBQVEsSUFBUjtBQUNULFFBQUE7SUFBQSxJQUFHLElBQUEsS0FBUSxNQUFYO01BQ0MsSUFBQSxHQUFPLEVBRFI7O0lBR0EsSUFBRyxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsS0FBdUIsSUFBdkIsSUFBZ0MsSUFBQyxDQUFBLE9BQU8sQ0FBQyxlQUFULEtBQTRCLElBQS9EO01BQ0MsV0FBQSxHQUFjLFNBRGY7S0FBQSxNQUFBO01BR0MsV0FBQSxHQUFjLGNBSGY7O0lBS0EsSUFBQyxDQUFBLEtBQUssQ0FBQyxPQUFQLENBQ0M7TUFBQSxVQUFBLEVBQ0M7UUFBQSxDQUFBLEVBQUcsR0FBQSxHQUFNLENBQUMsS0FBQSxHQUFRLEdBQVQsQ0FBVDtPQUREO01BRUEsSUFBQSxFQUFNLElBRk47TUFHQSxLQUFBLEVBQU8sV0FIUDtLQUREO1dBUUEsSUFBQyxDQUFBLFlBQUQsR0FBZ0I7RUFqQlA7O21CQW1CVixPQUFBLEdBQVMsU0FBQyxLQUFEO0lBQ1IsSUFBQyxDQUFBLEtBQUssQ0FBQyxPQUFQLENBQ0M7TUFBQSxVQUFBLEVBQ0M7UUFBQSxDQUFBLEVBQUcsR0FBQSxHQUFNLENBQUMsS0FBQSxHQUFRLEdBQVQsQ0FBVDtPQUREO01BRUEsSUFBQSxFQUFNLEtBRk47S0FERDtXQUtBLElBQUMsQ0FBQSxZQUFELEdBQWdCO0VBTlI7O21CQVVULElBQUEsR0FBTSxTQUFBO1dBQ0wsSUFBQyxDQUFDLE9BQUYsR0FBWTtFQURQOzttQkFHTixJQUFBLEdBQU0sU0FBQTtXQUNMLElBQUMsQ0FBQyxPQUFGLEdBQVk7RUFEUDs7bUJBR04sVUFBQSxHQUFZLFNBQUEsR0FBQTs7OztHQXJKZ0I7Ozs7QURXN0IsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBQ3JCLE9BQU8sQ0FBQyxVQUFSLEdBQXFCOztBQUVyQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBQ2xCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUNsQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFFbEIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFDbkIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBRW5CLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUNsQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBQ2xCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUVsQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFDbkIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFFbkIsT0FBTyxDQUFDLE1BQVIsR0FBaUI7O0FBQ2pCLE9BQU8sQ0FBQyxNQUFSLEdBQWlCOztBQUNqQixPQUFPLENBQUMsTUFBUixHQUFpQjs7QUFDakIsT0FBTyxDQUFDLE1BQVIsR0FBaUI7O0FBRWpCLE9BQU8sQ0FBQyxTQUFSLEdBQW9COztBQUNwQixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFDcEIsT0FBTyxDQUFDLFNBQVIsR0FBb0I7O0FBQ3BCLE9BQU8sQ0FBQyxTQUFSLEdBQW9COztBQUVwQixPQUFPLENBQUMsWUFBUixHQUF1Qjs7QUFDdkIsT0FBTyxDQUFDLFlBQVIsR0FBdUI7O0FBQ3ZCLE9BQU8sQ0FBQyxZQUFSLEdBQXVCOztBQUV2QixPQUFPLENBQUMsNEJBQVIsR0FBdUMsQ0FDdEMsU0FEc0MsRUFFdEMsU0FGc0MsRUFHdEMsU0FIc0MsRUFJdEMsU0FKc0MsRUFLdEMsU0FMc0MsRUFNdEMsU0FOc0MsRUFPdEMsU0FQc0MsRUFRdEMsU0FSc0MsRUFTdEMsU0FUc0M7Ozs7QUQzQ3ZDLElBQUE7Ozs7QUFBTSxPQUFPLENBQUM7QUFHYixNQUFBOzs7O0VBQUEsUUFBQyxDQUFDLE1BQUYsQ0FBUyxRQUFULEVBQ0M7SUFBQSxHQUFBLEVBQUssU0FBQTthQUFHLElBQUMsQ0FBQTtJQUFKLENBQUw7R0FERDs7RUFHYSxrQkFBQyxRQUFEO0FBQ1osUUFBQTtJQURhLElBQUMsQ0FBQSw2QkFBRCxXQUFTO0lBQ3RCLElBQUMsQ0FBQSxTQUFELGlEQUFxQixDQUFDLGdCQUFELENBQUMsWUFBYTtJQUNuQyxJQUFDLENBQUEsTUFBRCxnREFBcUIsQ0FBQyxjQUFELENBQUMsU0FBYTtJQUNuQyxJQUFDLENBQUEsS0FBRCwrQ0FBcUIsQ0FBQyxhQUFELENBQUMsUUFBYTs7TUFDbkMsSUFBQyxDQUFBLFVBQWtDOztJQUVuQyxJQUFDLENBQUEsY0FBRCxHQUFxQixJQUFDLENBQUEsTUFBSixHQUFnQixRQUFBLEdBQVMsSUFBQyxDQUFBLE1BQTFCLEdBQXdDO0lBQzFELDJDQUFBLFNBQUE7SUFFQSxJQUE2SCxJQUFDLENBQUEsS0FBOUg7TUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLDRDQUFBLEdBQTZDLElBQUMsQ0FBQSxTQUE5QyxHQUF3RCx5QkFBeEQsR0FBaUYsSUFBQyxDQUFBLFNBQWxGLEdBQTRGLGtCQUF4RyxFQUFBOztJQUNBLElBQUMsQ0FBQyxRQUFGLENBQVcsWUFBWDtFQVZZOztFQVliLE9BQUEsR0FBVSxTQUFDLE9BQUQsRUFBVSxNQUFWLEVBQWtCLElBQWxCLEVBQXdCLFFBQXhCLEVBQWtDLE1BQWxDLEVBQTBDLElBQTFDLEVBQWdELFVBQWhELEVBQTRELEtBQTVEO0FBRVQsUUFBQTtJQUFBLEdBQUEsR0FBTSxVQUFBLEdBQVcsT0FBWCxHQUFtQixpQkFBbkIsR0FBb0MsSUFBcEMsR0FBeUMsT0FBekMsR0FBZ0Q7SUFFdEQsSUFBRyxrQkFBSDtNQUNDLElBQUcsVUFBVSxDQUFDLE9BQWQ7UUFBc0MsR0FBQSxJQUFPLGdCQUE3Qzs7TUFDQSxJQUFHLFVBQVUsQ0FBQyxNQUFYLEtBQXFCLFFBQXhCO1FBQXNDLEdBQUEsSUFBTyxpQkFBN0M7O0FBRUEsY0FBTyxVQUFVLENBQUMsS0FBbEI7QUFBQSxhQUNNLFFBRE47VUFDb0IsR0FBQSxJQUFPO0FBQXJCO0FBRE4sYUFFTSxRQUZOO1VBRW9CLEdBQUEsSUFBTztBQUYzQjtNQUlBLElBQUcsT0FBTyxVQUFVLENBQUMsUUFBbEIsS0FBOEIsUUFBakM7UUFDQyxHQUFBLElBQU8sWUFBQSxHQUFhLFVBQVUsQ0FBQztRQUMvQixNQUFNLENBQUMsSUFBUCxDQUFZLEdBQVosRUFBZ0IsT0FBaEIsRUFGRDs7TUFJQSxJQUF1RCxPQUFPLFVBQVUsQ0FBQyxPQUFsQixLQUFrQyxRQUF6RjtRQUFBLEdBQUEsSUFBTyxXQUFBLEdBQWMsR0FBZCxHQUFvQixVQUFVLENBQUMsT0FBL0IsR0FBeUMsSUFBaEQ7O01BQ0EsSUFBdUQsT0FBTyxVQUFVLENBQUMsWUFBbEIsS0FBa0MsUUFBekY7UUFBQSxHQUFBLElBQU8sZ0JBQUEsR0FBaUIsVUFBVSxDQUFDLGFBQW5DOztNQUNBLElBQXVELE9BQU8sVUFBVSxDQUFDLFdBQWxCLEtBQWtDLFFBQXpGO1FBQUEsR0FBQSxJQUFPLGVBQUEsR0FBZ0IsVUFBVSxDQUFDLFlBQWxDOztNQUNBLElBQXVELE9BQU8sVUFBVSxDQUFDLE9BQWxCLEtBQWtDLFFBQXpGO1FBQUEsR0FBQSxJQUFPLFdBQUEsR0FBWSxVQUFVLENBQUMsUUFBOUI7O01BQ0EsSUFBdUQsT0FBTyxVQUFVLENBQUMsS0FBbEIsS0FBa0MsUUFBekY7UUFBQSxHQUFBLElBQU8sU0FBQSxHQUFVLFVBQVUsQ0FBQyxNQUE1Qjs7TUFDQSxJQUF1RCxPQUFPLFVBQVUsQ0FBQyxPQUFsQixLQUFrQyxRQUF6RjtRQUFBLEdBQUEsSUFBTyxXQUFBLEdBQVksVUFBVSxDQUFDLFFBQTlCO09BakJEOztJQW1CQSxJQUF5RyxLQUF6RztNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksaUJBQUEsR0FBa0IsTUFBbEIsR0FBeUIsd0JBQXpCLEdBQWdELENBQUMsSUFBSSxDQUFDLFNBQUwsQ0FBZSxJQUFmLENBQUQsQ0FBaEQsR0FBc0UsYUFBdEUsR0FBbUYsR0FBbkYsR0FBdUYsR0FBbkcsRUFBQTs7SUFFQSxPQUFBLEdBQ0M7TUFBQSxNQUFBLEVBQVEsTUFBUjtNQUNBLE9BQUEsRUFDQztRQUFBLGNBQUEsRUFBZ0IsaUNBQWhCO09BRkQ7O0lBSUQsSUFBRyxZQUFIO01BQ0MsT0FBTyxDQUFDLElBQVIsR0FBZSxJQUFJLENBQUMsU0FBTCxDQUFlLElBQWYsRUFEaEI7O0lBR0EsQ0FBQSxHQUFJLEtBQUEsQ0FBTSxHQUFOLEVBQVcsT0FBWCxDQUNKLENBQUMsSUFERyxDQUNFLFNBQUMsR0FBRDtBQUNMLFVBQUE7TUFBQSxJQUFHLENBQUMsR0FBRyxDQUFDLEVBQVI7QUFBZ0IsY0FBTSxLQUFBLENBQU0sR0FBRyxDQUFDLFVBQVYsRUFBdEI7O01BQ0EsSUFBQSxHQUFPLEdBQUcsQ0FBQyxJQUFKLENBQUE7TUFDUCxJQUFJLENBQUMsSUFBTCxDQUFVLFFBQVY7QUFDQSxhQUFPO0lBSkYsQ0FERixDQU1KLEVBQUMsS0FBRCxFQU5JLENBTUcsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLEtBQUQ7ZUFBVyxPQUFPLENBQUMsSUFBUixDQUFhLEtBQWI7TUFBWDtJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FOSDtBQVFKLFdBQU87RUF6Q0U7O0VBNENWLFNBQUEsR0FBWSxTQUFBO0FBQ1gsUUFBQTtJQURZLGtCQUFHLGlHQUFTO0lBQ3hCLElBQUcsT0FBTyxJQUFLLENBQUEsQ0FBQSxHQUFFLENBQUYsQ0FBWixLQUFvQixRQUF2QjtNQUNDLElBQUssQ0FBQSxDQUFBLENBQUwsR0FBVSxJQUFLLENBQUEsQ0FBQSxHQUFFLENBQUY7TUFDZixJQUFLLENBQUEsQ0FBQSxHQUFFLENBQUYsQ0FBTCxHQUFZLEtBRmI7O0FBSUEsV0FBTyxFQUFFLENBQUMsS0FBSCxDQUFTLElBQVQsRUFBZSxJQUFmO0VBTEk7O3FCQVNaLEdBQUEsR0FBUSxTQUFBO0FBQWEsUUFBQTtJQUFaO1dBQVksU0FBQSxhQUFVLENBQUEsQ0FBRyxTQUFBLFdBQUEsSUFBQSxDQUFBLEVBQVMsQ0FBQSxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsSUFBRCxFQUFVLFFBQVYsRUFBb0IsVUFBcEI7ZUFBbUMsT0FBQSxDQUFRLEtBQUMsQ0FBQSxTQUFULEVBQW9CLEtBQUMsQ0FBQSxjQUFyQixFQUFxQyxJQUFyQyxFQUEyQyxRQUEzQyxFQUFxRCxLQUFyRCxFQUErRCxJQUEvRCxFQUFxRSxVQUFyRSxFQUFpRixLQUFDLENBQUEsS0FBbEY7TUFBbkM7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQSxDQUF0QjtFQUFiOztxQkFDUixHQUFBLEdBQVEsU0FBQTtBQUFhLFFBQUE7SUFBWjtXQUFZLFNBQUEsYUFBVSxDQUFBLENBQUcsU0FBQSxXQUFBLElBQUEsQ0FBQSxFQUFTLENBQUEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsUUFBYixFQUF1QixVQUF2QjtlQUFzQyxPQUFBLENBQVEsS0FBQyxDQUFBLFNBQVQsRUFBb0IsS0FBQyxDQUFBLGNBQXJCLEVBQXFDLElBQXJDLEVBQTJDLFFBQTNDLEVBQXFELEtBQXJELEVBQStELElBQS9ELEVBQXFFLFVBQXJFLEVBQWlGLEtBQUMsQ0FBQSxLQUFsRjtNQUF0QztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLENBQXRCO0VBQWI7O3FCQUNSLElBQUEsR0FBUSxTQUFBO0FBQWEsUUFBQTtJQUFaO1dBQVksU0FBQSxhQUFVLENBQUEsQ0FBRyxTQUFBLFdBQUEsSUFBQSxDQUFBLEVBQVMsQ0FBQSxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxRQUFiLEVBQXVCLFVBQXZCO2VBQXNDLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFvQixLQUFDLENBQUEsY0FBckIsRUFBcUMsSUFBckMsRUFBMkMsUUFBM0MsRUFBcUQsTUFBckQsRUFBK0QsSUFBL0QsRUFBcUUsVUFBckUsRUFBaUYsS0FBQyxDQUFBLEtBQWxGO01BQXRDO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsQ0FBdEI7RUFBYjs7cUJBQ1IsS0FBQSxHQUFRLFNBQUE7QUFBYSxRQUFBO0lBQVo7V0FBWSxTQUFBLGFBQVUsQ0FBQSxDQUFHLFNBQUEsV0FBQSxJQUFBLENBQUEsRUFBUyxDQUFBLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLFFBQWIsRUFBdUIsVUFBdkI7ZUFBc0MsT0FBQSxDQUFRLEtBQUMsQ0FBQSxTQUFULEVBQW9CLEtBQUMsQ0FBQSxjQUFyQixFQUFxQyxJQUFyQyxFQUEyQyxRQUEzQyxFQUFxRCxPQUFyRCxFQUErRCxJQUEvRCxFQUFxRSxVQUFyRSxFQUFpRixLQUFDLENBQUEsS0FBbEY7TUFBdEM7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQSxDQUF0QjtFQUFiOztzQkFDUixRQUFBLEdBQVEsU0FBQTtBQUFhLFFBQUE7SUFBWjtXQUFZLFNBQUEsYUFBVSxDQUFBLENBQUcsU0FBQSxXQUFBLElBQUEsQ0FBQSxFQUFTLENBQUEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLElBQUQsRUFBWSxRQUFaLEVBQXNCLFVBQXRCO2VBQXFDLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFvQixLQUFDLENBQUEsY0FBckIsRUFBcUMsSUFBckMsRUFBMkMsUUFBM0MsRUFBcUQsUUFBckQsRUFBK0QsSUFBL0QsRUFBcUUsVUFBckUsRUFBaUYsS0FBQyxDQUFBLEtBQWxGO01BQXJDO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsQ0FBdEI7RUFBYjs7cUJBR1IsUUFBQSxHQUFVLFNBQUMsSUFBRCxFQUFPLFFBQVA7QUFHVCxRQUFBO0lBQUEsSUFBRyxJQUFBLEtBQVEsWUFBWDtNQUVDLEdBQUEsR0FBTSxVQUFBLEdBQVcsSUFBQyxDQUFBLFNBQVosR0FBc0IsdUJBQXRCLEdBQTZDLElBQUMsQ0FBQTtNQUNwRCxhQUFBLEdBQWdCO01BQ2hCLE1BQUEsR0FBYSxJQUFBLFdBQUEsQ0FBWSxHQUFaO01BRWIsTUFBTSxDQUFDLGdCQUFQLENBQXdCLE1BQXhCLEVBQWdDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUMvQixJQUFHLGFBQUEsS0FBaUIsY0FBcEI7WUFDQyxLQUFDLENBQUMsT0FBRixHQUFZO1lBQ1osSUFBeUIsZ0JBQXpCO2NBQUEsUUFBQSxDQUFTLFdBQVQsRUFBQTs7WUFDQSxJQUFzRixLQUFDLENBQUEsS0FBdkY7Y0FBQSxPQUFPLENBQUMsR0FBUixDQUFZLDRDQUFBLEdBQTZDLEtBQUMsQ0FBQSxTQUE5QyxHQUF3RCxlQUFwRSxFQUFBO2FBSEQ7O2lCQUlBLGFBQUEsR0FBZ0I7UUFMZTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBaEM7TUFPQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO1VBQ2hDLElBQUcsYUFBQSxLQUFpQixXQUFwQjtZQUNDLEtBQUMsQ0FBQyxPQUFGLEdBQVk7WUFDWixJQUE0QixnQkFBNUI7Y0FBQSxRQUFBLENBQVMsY0FBVCxFQUFBOztZQUNBLElBQWtGLEtBQUMsQ0FBQSxLQUFuRjtjQUFBLE9BQU8sQ0FBQyxJQUFSLENBQWEsNENBQUEsR0FBNkMsS0FBQyxDQUFBLFNBQTlDLEdBQXdELFVBQXJFLEVBQUE7YUFIRDs7aUJBSUEsYUFBQSxHQUFnQjtRQUxnQjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakM7QUFPQSxhQXBCRDs7SUFzQkEsR0FBQSxHQUFNLFVBQUEsR0FBVyxJQUFDLENBQUEsU0FBWixHQUFzQixpQkFBdEIsR0FBdUMsSUFBdkMsR0FBNEMsT0FBNUMsR0FBbUQsSUFBQyxDQUFBO0lBQzFELE1BQUEsR0FBYSxJQUFBLFdBQUEsQ0FBWSxHQUFaO0lBQ2IsSUFBbUYsSUFBQyxDQUFBLEtBQXBGO01BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSwwQ0FBQSxHQUEyQyxJQUEzQyxHQUFnRCxhQUFoRCxHQUE2RCxHQUE3RCxHQUFpRSxHQUE3RSxFQUFBOztJQUVBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixLQUF4QixFQUErQixDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsRUFBRDtRQUM5QixJQUFzSCxnQkFBdEg7VUFBQSxRQUFBLENBQVMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxFQUFFLENBQUMsSUFBZCxDQUFtQixDQUFDLElBQTdCLEVBQW1DLEtBQW5DLEVBQTBDLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUE5RCxFQUFvRSxDQUFDLENBQUMsSUFBRixDQUFPLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUFJLENBQUMsS0FBekIsQ0FBK0IsR0FBL0IsQ0FBUCxFQUEyQyxDQUEzQyxDQUFwRSxFQUFBOztRQUNBLElBQXNILEtBQUMsQ0FBQSxLQUF2SDtpQkFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHNDQUFBLEdBQXVDLElBQXZDLEdBQTRDLGVBQTVDLEdBQTBELENBQUMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxFQUFFLENBQUMsSUFBZCxDQUFtQixDQUFDLElBQXJCLENBQTFELEdBQW9GLFlBQXBGLEdBQWdHLEdBQWhHLEdBQW9HLEdBQWhILEVBQUE7O01BRjhCO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEvQjtXQUlBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixPQUF4QixFQUFpQyxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsRUFBRDtRQUNoQyxJQUF3SCxnQkFBeEg7VUFBQSxRQUFBLENBQVMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxFQUFFLENBQUMsSUFBZCxDQUFtQixDQUFDLElBQTdCLEVBQW1DLE9BQW5DLEVBQTRDLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUFoRSxFQUFzRSxDQUFDLENBQUMsSUFBRixDQUFPLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUFJLENBQUMsS0FBekIsQ0FBK0IsR0FBL0IsQ0FBUCxFQUEyQyxDQUEzQyxDQUF0RSxFQUFBOztRQUNBLElBQXdILEtBQUMsQ0FBQSxLQUF6SDtpQkFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHNDQUFBLEdBQXVDLElBQXZDLEdBQTRDLGlCQUE1QyxHQUE0RCxDQUFDLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUFyQixDQUE1RCxHQUFzRixZQUF0RixHQUFrRyxHQUFsRyxHQUFzRyxHQUFsSCxFQUFBOztNQUZnQztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBakM7RUFqQ1M7Ozs7R0E5RW9CLE1BQU0sQ0FBQzs7OztBREx0QyxJQUFBOztBQUFDLFdBQVksT0FBQSxDQUFRLFVBQVI7O0FBRWIsc0JBQUEsR0FBNkIsSUFBQSxRQUFBLENBQzVCO0VBQUEsSUFBQSxFQUFNLHdCQUFOO0VBQ0EsSUFBQSxFQUFNLGtEQUROO0NBRDRCOztBQUk3QixnQkFBQSxHQUF1QixJQUFBLFFBQUEsQ0FDdEI7RUFBQSxJQUFBLEVBQU0sa0JBQU47RUFDQSxJQUFBLEVBQU0sMkNBRE47Q0FEc0I7O0FBSXZCLHFCQUFBLEdBQTRCLElBQUEsUUFBQSxDQUMzQjtFQUFBLElBQUEsRUFBTSx1QkFBTjtFQUNBLElBQUEsRUFBTSxpREFETjtDQUQyQjs7QUFJNUIsZUFBQSxHQUFzQixJQUFBLFFBQUEsQ0FDckI7RUFBQSxJQUFBLEVBQU0saUJBQU47RUFDQSxJQUFBLEVBQU0sMENBRE47Q0FEcUI7O0FBSXRCLDBCQUFBLEdBQWlDLElBQUEsUUFBQSxDQUNoQztFQUFBLElBQUEsRUFBTSw0QkFBTjtFQUNBLElBQUEsRUFBTSxzREFETjtDQURnQzs7QUFJakMsb0JBQUEsR0FBMkIsSUFBQSxRQUFBLENBQzFCO0VBQUEsSUFBQSxFQUFNLHNCQUFOO0VBQ0EsSUFBQSxFQUFNLCtDQUROO0NBRDBCOztBQUkzQixzQkFBQSxHQUE2QixJQUFBLFFBQUEsQ0FDNUI7RUFBQSxJQUFBLEVBQU0sd0JBQU47RUFDQSxJQUFBLEVBQU0sa0RBRE47Q0FENEI7O0FBSTdCLGdCQUFBLEdBQXVCLElBQUEsUUFBQSxDQUN0QjtFQUFBLElBQUEsRUFBTSxrQkFBTjtFQUNBLElBQUEsRUFBTSwyQ0FETjtDQURzQjs7QUFJdkIsdUJBQUEsR0FBOEIsSUFBQSxRQUFBLENBQzdCO0VBQUEsSUFBQSxFQUFNLHlCQUFOO0VBQ0EsSUFBQSxFQUFNLG1EQUROO0NBRDZCOztBQUk5QixpQkFBQSxHQUF3QixJQUFBLFFBQUEsQ0FDdkI7RUFBQSxJQUFBLEVBQU0sbUJBQU47RUFDQSxJQUFBLEVBQU0sNENBRE47Q0FEdUI7O0FBSXhCLHdCQUFBLEdBQStCLElBQUEsUUFBQSxDQUM5QjtFQUFBLElBQUEsRUFBTSwwQkFBTjtFQUNBLElBQUEsRUFBTSxvREFETjtDQUQ4Qjs7QUFJL0Isa0JBQUEsR0FBeUIsSUFBQSxRQUFBLENBQ3hCO0VBQUEsSUFBQSxFQUFNLG9CQUFOO0VBQ0EsSUFBQSxFQUFNLDZDQUROO0NBRHdCOztBQUl6Qix5QkFBQSxHQUFnQyxJQUFBLFFBQUEsQ0FDL0I7RUFBQSxJQUFBLEVBQU0sMkJBQU47RUFDQSxJQUFBLEVBQU0scURBRE47Q0FEK0I7O0FBSWhDLGtCQUFBLEdBQXlCLElBQUEsUUFBQSxDQUN4QjtFQUFBLElBQUEsRUFBTSxxQkFBTjtFQUNBLElBQUEsRUFBTSw4Q0FETjtDQUR3Qjs7QUFJekIscUJBQUEsR0FBNEIsSUFBQSxRQUFBLENBQzNCO0VBQUEsSUFBQSxFQUFNLHVCQUFOO0VBQ0EsSUFBQSxFQUFNLGlEQUROO0NBRDJCOztBQUk1QixlQUFBLEdBQXNCLElBQUEsUUFBQSxDQUNyQjtFQUFBLElBQUEsRUFBTSxpQkFBTjtFQUNBLElBQUEsRUFBTSwwQ0FETjtDQURxQjs7OztBRDdEdEIsT0FBTyxDQUFDLGFBQVIsR0FBd0IsSUFBSTs7QUFDNUIsT0FBTyxDQUFDLGFBQVIsR0FBd0I7O0FBQ3hCLE9BQU8sQ0FBQyxtQkFBUixHQUE4QixJQUFJOztBQUNsQyxPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLGNBQVIsR0FBeUI7O0FBQ3pCLE9BQU8sQ0FBQyxXQUFSLEdBQXNCOztBQUN0QixPQUFPLENBQUMsY0FBUixHQUF5Qjs7QUFHekIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxhQUFSLEdBQXdCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDOztBQUM3QyxPQUFPLENBQUMsWUFBUixHQUF1QixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQzs7QUFNNUMsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBRXJCLE9BQU8sQ0FBQyxjQUFSLEdBQXlCOztBQUV6QixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFDcEIsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBQ3JCLE9BQU8sQ0FBQyxjQUFSLEdBQXlCOztBQUN6QixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFpQnBCLE9BQU8sQ0FBQyxRQUFSLEdBQW1COzs7O0FEMUNuQixPQUFPLENBQUMsU0FBUixHQUFvQjtFQUNuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFFBSFA7R0FEbUIsRUFNbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxNQUhQO0dBTm1CLEVBV25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQVhtQixFQWdCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxTQUhQO0dBaEJtQixFQXFCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxXQUhQO0dBckJtQixFQTBCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxTQUhQO0dBMUJtQixFQStCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxVQUhQO0dBL0JtQixFQW9DbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxnQkFIUDtHQXBDbUIsRUF5Q25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sU0FIUDtHQXpDbUIsRUE4Q25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sZUFIUDtHQTlDbUIsRUFtRG5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQW5EbUIsRUF3RG5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sWUFIUDtHQXhEbUIsRUE2RG5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQTdEbUIsRUFrRW5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sZ0JBSFA7R0FsRW1CLEVBdUVuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFNBSFA7R0F2RW1COzs7QUE4RXBCLE9BQU8sQ0FBQyx5QkFBUixHQUFvQztFQUNuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFNBRlA7SUFHQyx3QkFBQSxFQUEwQiw4QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBRG1DLEVBUW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sUUFGUDtJQUdDLHdCQUFBLEVBQTBCLDhCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FSbUMsRUFlbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQWZtQyxFQXNCbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXRCbUMsRUE2Qm5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0E3Qm1DLEVBb0NuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBcENtQyxFQTJDbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQTNDbUMsRUFrRG5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sUUFGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FsRG1DLEVBeURuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLEtBRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBekRtQyxFQWdFbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQWhFbUMsRUF1RW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0F2RW1DLEVBOEVuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFNBRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBOUVtQyxFQXFGbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxVQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXJGbUMsRUE0Rm5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sUUFGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0E1Rm1DLEVBbUduQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiwrQkFIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBbkdtQyxFQTBHbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsK0JBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQTFHbUMsRUFpSG5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sU0FGUDtJQUdDLHdCQUFBLEVBQTBCLDhCQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0FqSG1DLEVBd0huQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLEtBRlA7SUFHQyx3QkFBQSxFQUEwQiwrQkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBeEhtQyxFQStIbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQS9IbUMsRUFzSW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sU0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0F0SW1DOzs7QUErSXBDLE9BQU8sQ0FBQyx5QkFBUixHQUFvQztFQUNuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiw4QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBRG1DLEVBUW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sUUFGUDtJQUdDLHdCQUFBLEVBQTBCLDhCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FSbUMsRUFlbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxTQUZQO0lBR0Msd0JBQUEsRUFBMEIsOEJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQWZtQyxFQXNCbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxNQUZQO0lBR0Msd0JBQUEsRUFBMEIsOEJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXRCbUMsRUE2Qm5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sU0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0E3Qm1DLEVBb0NuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLEtBRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBcENtQyxFQTJDbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxRQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQTNDbUM7OztBQW9EcEMsT0FBTyxDQUFDLG9CQUFSLEdBQStCO0VBQzlCO0lBQ0MsSUFBQSxFQUFNLFdBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQixnQ0FKakI7SUFLQyxPQUFBLEVBQVMsQ0FMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBRDhCLEVBUzlCO0lBQ0MsSUFBQSxFQUFNLGFBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQiw2REFKakI7SUFLQyxPQUFBLEVBQVMsQ0FMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBVDhCLEVBaUI5QjtJQUNDLElBQUEsRUFBTSxTQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0IsOENBSmpCO0lBS0MsT0FBQSxFQUFTLENBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQWpCOEIsRUF5QjlCO0lBQ0MsSUFBQSxFQUFNLGFBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxLQUhYO0lBSUMsY0FBQSxFQUFnQix3Q0FKakI7SUFLQyxPQUFBLEVBQVMsQ0FMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBekI4QixFQWlDOUI7SUFDQyxJQUFBLEVBQU0sV0FEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLDBEQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FqQzhCLEVBeUM5QjtJQUNDLElBQUEsRUFBTSxZQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsS0FIWDtJQUlDLGNBQUEsRUFBZ0Isd0NBSmpCO0lBS0MsT0FBQSxFQUFTLENBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQXpDOEIsRUFpRDlCO0lBQ0MsSUFBQSxFQUFNLFNBRFA7SUFFQyxNQUFBLEVBQVEsV0FGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQixnREFKakI7SUFLQyxPQUFBLEVBQVMsQ0FMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBakQ4QixFQXlEOUI7SUFDQyxJQUFBLEVBQU0sWUFEUDtJQUVDLE1BQUEsRUFBUSxXQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLHlDQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0F6RDhCLEVBaUU5QjtJQUNDLElBQUEsRUFBTSw0QkFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLGdCQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FqRThCOzs7QUEyRS9CLE9BQU8sQ0FBQyxvQkFBUixHQUErQjtFQUM5QjtJQUNDLElBQUEsRUFBTSxLQURQO0lBRUMsTUFBQSxFQUFRLFNBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0IsNkJBSmpCO0lBS0MsT0FBQSxFQUFTLEVBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQUQ4QixFQVM5QjtJQUNDLElBQUEsRUFBTSxZQURQO0lBRUMsTUFBQSxFQUFRLFNBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0IsMENBSmpCO0lBS0MsT0FBQSxFQUFTLEVBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQVQ4QixFQWlCOUI7SUFDQyxJQUFBLEVBQU0sS0FEUDtJQUVDLE1BQUEsRUFBUSxTQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLGtFQUpqQjtJQUtDLE9BQUEsRUFBUyxFQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FqQjhCLEVBeUI5QjtJQUNDLElBQUEsRUFBTSxhQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0Isc0RBSmpCO0lBS0MsT0FBQSxFQUFTLEVBTFY7SUFNQyxFQUFBLEVBQUksQ0FOTDtHQXpCOEIsRUFpQzlCO0lBQ0MsSUFBQSxFQUFNLFlBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQixzREFKakI7SUFLQyxPQUFBLEVBQVMsRUFMVjtJQU1DLEVBQUEsRUFBSSxDQU5MO0dBakM4QixFQXlDOUI7SUFDQyxJQUFBLEVBQU0sU0FEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLDhDQUpqQjtJQUtDLE9BQUEsRUFBUyxFQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0F6QzhCLEVBaUQ5QjtJQUNDLElBQUEsRUFBTSw0QkFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLGdCQUpqQjtJQUtDLE9BQUEsRUFBUyxDQUxWO0lBTUMsRUFBQSxFQUFJLENBTkw7R0FqRDhCOzs7QUEyRC9CLE9BQU8sQ0FBQyxvQkFBUixHQUErQjtFQUM5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sZUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsMENBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQUQ4QixFQVU5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sbUJBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQVY4Qjs7O0FBd1EvQixPQUFPLENBQUMsb0JBQVIsR0FBK0I7RUFDOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGlCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQiwwQ0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBRDhCLEVBVTlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxnQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBVjhCLEVBbUI5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sY0FIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBbkI4QixFQTRCOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxhQUZaO0lBR0MsSUFBQSxFQUFNLFlBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQTVCOEIsRUFxQzlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsYUFGWjtJQUdDLElBQUEsRUFBTSxZQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQix5Q0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBckM4QixFQThDOUI7SUFDQyxPQUFBLEVBQVMsRUFEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGtCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0E5QzhCLEVBdUQ5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sZUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsMENBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQXZEOEIsRUFnRTlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxXQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQix1Q0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBaEU4QixFQXlFOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxhQUZaO0lBR0MsSUFBQSxFQUFNLGVBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQXpFOEIsRUFrRjlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxpQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBbEY4QixFQTJGOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxhQUZaO0lBR0MsSUFBQSxFQUFNLGdCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQix5Q0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBM0Y4Qjs7O0FBd1EvQixPQUFPLENBQUMsWUFBUixHQUF1QjtFQUN0QjtJQUNDLFNBQUEsRUFBVyxXQURaO0lBRUMsVUFBQSxFQUFZLFdBRmI7R0FEc0IsRUFLdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBTHNCLEVBU3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQVRzQixFQWF0QjtJQUNDLFNBQUEsRUFBVyxXQURaO0lBRUMsVUFBQSxFQUFZLFdBRmI7R0Fic0IsRUFpQnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQWpCc0IsRUFxQnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXJCc0IsRUF5QnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXpCc0IsRUE2QnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQTdCc0IsRUFpQ3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQWpDc0IsRUFxQ3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXJDc0IsRUF5Q3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXpDc0IsRUE2Q3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQTdDc0I7Ozs7O0FEdDZCdkIsSUFBQTs7QUFBQSxNQUFBLEdBQVMsT0FBQSxDQUFRLFFBQVI7O0FBRVQsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxtQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxPQUZWOzs7QUFJSCxPQUFPLENBQUMsT0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksaUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxpQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsV0FBUixHQUNDO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlELE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksaUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLFFBQVIsR0FDQztFQUFBLFVBQUEsRUFBWSxtQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJRCxPQUFPLENBQUMsWUFBUixHQUNDO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlELE9BQU8sQ0FBQyxTQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVkscUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxxQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsY0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxTQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksb0JBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLFNBQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxxQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsTUFBUixHQUNHO0VBQUEsVUFBQSxFQUFZLGlCQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksbUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7Ozs7QUQxRUgsT0FBTyxDQUFDLGFBQVIsR0FBd0IsU0FBQTtBQUN0QixNQUFBO0VBQUEsR0FBQSxHQUFNLElBQUk7RUFXVixJQUFBLEdBQU8sR0FBRyxDQUFDLFdBQUosQ0FBQSxDQUFBLEdBQW9CO0VBQzNCLElBQUcsSUFBQSxJQUFRLEVBQVg7SUFDQyxJQUFBLEdBQU8sSUFBQSxHQUFPLEdBRGY7O0VBR0EsTUFBQSxHQUFTLEdBQUcsQ0FBQyxhQUFKLENBQUE7RUFDVCxNQUFBLEdBQVMsR0FBRyxDQUFDLGFBQUosQ0FBQTtFQUNULE9BQUEsR0FBYSxJQUFBLElBQVEsRUFBWCxHQUFtQixJQUFuQixHQUE2QjtFQUN2QyxJQUFBLEdBQVUsSUFBQSxJQUFRLEVBQVgsR0FBbUIsSUFBQSxHQUFPLEVBQTFCLEdBQWtDO0VBQ3pDLElBQUcsSUFBQSxLQUFRLENBQVg7SUFDQyxJQUFBLEdBQU8sR0FEUjs7RUFFQSxNQUFBLEdBQVksTUFBQSxHQUFTLEVBQVosR0FBb0IsR0FBQSxHQUFNLE1BQTFCLEdBQXNDO0VBQy9DLFVBQUEsR0FBYSxJQUFBLEdBQU8sR0FBUCxHQUFhLE1BQWIsR0FBc0I7QUFDbkMsU0FBTztBQXhCZTs7QUEwQnhCLE9BQU8sQ0FBQyx1QkFBUixHQUFrQyxTQUFDLFVBQUQ7QUFDaEMsTUFBQTtFQUFBLFVBQUEsR0FBYSxVQUFBLEdBQVc7RUFDeEIsTUFBQSxHQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsVUFBQSxHQUFhLEVBQXhCO0VBQ1QsVUFBQSxHQUFhLFVBQUEsR0FBVztFQUN4QixNQUFBLEdBQVMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxVQUFBLEdBQWEsRUFBeEI7RUFDVCxVQUFBLEdBQWEsVUFBQSxHQUFXO0VBQ3hCLElBQUEsR0FBTyxJQUFJLENBQUMsS0FBTCxDQUFXLFVBQUEsR0FBYSxFQUF4QjtFQUVQLElBQUEsR0FBVSxJQUFBLEdBQU8sRUFBVixHQUFrQixHQUFBLEdBQU0sSUFBeEIsR0FBa0M7RUFDekMsTUFBQSxHQUFZLE1BQUEsR0FBUyxFQUFaLEdBQW9CLEdBQUEsR0FBTSxNQUExQixHQUFzQztFQUMvQyxNQUFBLEdBQVksTUFBQSxHQUFTLEVBQVosR0FBb0IsR0FBQSxHQUFNLE1BQTFCLEdBQXNDO0VBRS9DLFVBQUEsR0FBYSxJQUFBLEdBQU8sR0FBUCxHQUFhLE1BQWIsR0FBc0IsR0FBdEIsR0FBNEI7QUFFekMsU0FBTztBQWR5QiJ9
