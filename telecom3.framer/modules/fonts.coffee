{FontFace} = require "FontFace"

ProximaNovaBlackItalic = new FontFace
	name: "ProximaNovaBlackItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Black Italic.otf"

ProximaNovaBlack = new FontFace
	name: "ProximaNovaBlack"
	file: "Fonts/Proxima Nova/Proxima Nova Black.otf"

ProximaNovaBoldItalic = new FontFace
	name: "ProximaNovaBoldItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Bold Italic.otf"

ProximaNovaBold = new FontFace
	name: "ProximaNovaBold"
	file: "Fonts/Proxima Nova/Proxima Nova Bold.otf"

ProximaNovaExtraboldItalic = new FontFace
	name: "ProximaNovaExtraboldItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Extrabold Italic.otf"

ProximaNovaExtrabold = new FontFace
	name: "ProximaNovaExtrabold"
	file: "Fonts/Proxima Nova/Proxima Nova Extrabold.otf"

ProximaNovaLightItalic = new FontFace
	name: "ProximaNovaLightItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Light Italic.otf"

ProximaNovaLight = new FontFace
	name: "ProximaNovaLight"
	file: "Fonts/Proxima Nova/Proxima Nova Light.otf"

ProximaNovaMediumItalic = new FontFace
	name: "ProximaNovaMediumItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Medium Italic.otf"

ProximaNovaMedium = new FontFace
	name: "ProximaNovaMedium"
	file: "Fonts/Proxima Nova/Proxima Nova Medium.otf"

ProximaNovaRegularItalic = new FontFace
	name: "ProximaNovaRegularItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Regular Italic.otf"

ProximaNovaRegular = new FontFace
	name: "ProximaNovaRegular"
	file: "Fonts/Proxima Nova/Proxima Nova Regular.otf"

ProximaNovaSemiboldItalic = new FontFace
	name: "ProximaNovaSemiboldItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Semibold Italic.otf"

ProximaNovaRegular = new FontFace
	name: "ProximaNovaSemibold"
	file: "Fonts/Proxima Nova/Proxima Nova Semibold.otf"

ProximaNovaThinItalic = new FontFace
	name: "ProximaNovaThinItalic"
	file: "Fonts/Proxima Nova/Proxima Nova Thin Italic.otf"

ProximaNovaThin = new FontFace
	name: "ProximaNovaThin"
	file: "Fonts/Proxima Nova/Proxima Nova Thin.otf"
