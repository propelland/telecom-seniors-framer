exports.getTimeString = () ->
  now = new Date
  # 	day = now.getUTCDay()
  # 	daylist = [
  # 		'Sunday'
  # 		'Monday'
  # 		'Tuesday'
  # 		'Wednesday'
  # 		'Thursday'
  # 		'Friday'
  # 		'Saturday'
  # 	]
  hour = now.getUTCHours() + 2
  if hour >= 24
  	hour = hour - 24
  # 		day += 1
  minute = now.getUTCMinutes()
  second = now.getUTCSeconds()
  prepand = if hour >= 12 then 'pm' else 'am'
  hour = if hour >= 12 then hour - 12 else hour
  if hour == 0
  	hour = 12
  minute = if minute < 10 then "0" + minute else minute
  timeString = hour + ":" + minute + prepand
  return timeString

exports.getTimeDifferenceString = (difference) ->
  difference = difference/1000;
  second = Math.floor(difference % 60);
  difference = difference/60;
  minute = Math.floor(difference % 60);
  difference = difference/60;
  hour = Math.floor(difference % 24);

  hour = if hour < 10 then "0" + hour else hour
  minute = if minute < 10 then "0" + minute else minute
  second = if second < 10 then "0" + second else second

  timeString = hour + ":" + minute + ":" + second

  return timeString
