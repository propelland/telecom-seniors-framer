exports.socialTalkContactsSpanish = [
	{
		photoName: "avatar12.jpg"
		name: "Miranda"
		lastConnectionDateString: "October 15 2018 | 47 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar08.jpg"
		name: "Olivia"
		lastConnectionDateString: "October 13 2018 | 98 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar02.jpg"
		name: "Simón"
		lastConnectionDateString: "October 9 2018 | 27 minutes"
		status: "Available"
		friend: false
	},
	{
		photoName: "avatar01.jpg"
		name: "Elías"
		lastConnectionDateString: "October 8 2018 | 42 minutes"
		status: "Unavailable"
		friend: true
	},
	{
		photoName: "avatar15.jpg"
		name: "Dante"
		lastConnectionDateString: "October 7 2018 | 37 minutes"
		status: "Unavailable"
		friend: false
	},
	{
		photoName: "avatar05.jpg"
		name: "Irene"
		lastConnectionDateString: "October 2 2018 | 47 minutes"
		status: "Available"
		friend: false
	},
	{
		photoName: "avatar04.jpg"
		name: "Luana"
		lastConnectionDateString: "September 30 2018 | 57 minutes"
		status: "Unavailable"
		friend: true
	},
	{
		photoName: "avatar07.jpg"
		name: "Isabel"
		lastConnectionDateString: "September 27 2018 | 72 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar09.jpg"
		name: "Elena"
		lastConnectionDateString: "September 24 2018 | 53 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar03.jpg"
		name: "Dylan"
		lastConnectionDateString: "September 23 2018 | 36 minutes"
		status: "Available"
		friend: false
	},
	{
		photoName: "avatar10.jpg"
		name: "Ángel"
		lastConnectionDateString: "September 19 2018 | 27 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar14.jpg"
		name: "Martina"
		lastConnectionDateString: "September 16 2018 | 14 minutes"
		status: "Unavailable"
		friend: false
	},
	{
		photoName: "avatar11.jpg"
		name: "Victoria"
		lastConnectionDateString: "September 12 2018 | 22 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar01.jpg"
		name: "Camila"
		lastConnectionDateString: "September 10 2018 | 39 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar03.jpg"
		name: "Sofia"
		lastConnectionDateString: "September 7 2018 | 10 minutes"
		status: "Unavailable"
		friend: false
	},
	{
		photoName: "avatar06.jpg"
		name: "Tomas"
		lastConnectionDateString: "September 6 2018 | 27 minutes"
		status: "Unavailable"
		friend: true
	},
	{
		photoName: "avatar08.jpg"
		name: "Joaquín"
		lastConnectionDateString: "September 3 2018 | 8 minutes"
		status: "Unavailable"
		friend: false
	},
	{
		photoName: "avatar07.jpg"
		name: "Mía"
		lastConnectionDateString: "September 1 2018 | 48 minutes"
		status: "Available"
		friend: true
	},
	{
		photoName: "avatar11.jpg"
		name: "Lucas"
		lastConnectionDateString: "August 29 2018 | 18 minutes"
		status: "Unavailable"
		friend: false
	},
	{
		photoName: "avatar13.jpg"
		name: "Natalia"
		lastConnectionDateString: "August 26 2018 | 53 minutes"
		status: "Available"
		friend: true
	}
]

exports.interests = [
	{
		photoNameActive: "icon01Active.png"
		photoNameInactive: "icon01Inactive.png"
		text: "Family"
	},
	{
		photoNameActive: "icon02Active.png"
		photoNameInactive: "icon02Inactive.png"
		text: "Pets"
	},
	{
		photoNameActive: "icon03Active.png"
		photoNameInactive: "icon03Inactive.png"
		text: "Travel"
	},
	{
		photoNameActive: "icon04Active.png"
		photoNameInactive: "icon04Inactive.png"
		text: "Reading"
	},
	{
		photoNameActive: "icon05Active.png"
		photoNameInactive: "icon05Inactive.png"
		text: "Gardening"
	},
	{
		photoNameActive: "icon07Active.png"
		photoNameInactive: "icon07Inactive.png"
		text: "Cooking"
	},
	{
		photoNameActive: "icon06Active.png"
		photoNameInactive: "icon06Inactive.png"
		text: "Outdoors"
	},
	{
		photoNameActive: "icon08Active.png"
		photoNameInactive: "icon08Inactive.png"
		text: "Current Events"
	},
	{
		photoNameActive: "icon09Active.png"
		photoNameInactive: "icon09Inactive.png"
		text: "Economy"
	},
	{
		photoNameActive: "icon10Active.png"
		photoNameInactive: "icon10Inactive.png"
		text: "Art & Culture"
	},
	{
		photoNameActive: "icon11Active.png"
		photoNameInactive: "icon11Inactive.png"
		text: "Movies"
	},
	{
		photoNameActive: "icon12Active.png"
		photoNameInactive: "icon12Inactive.png"
		text: "Television"
	},
	{
		photoNameActive: "icon13Active.png"
		photoNameInactive: "icon13Inactive.png"
		text: "Sports"
	},
	{
		photoNameActive: "icon14Active.png"
		photoNameInactive: "icon14Inactive.png"
		text: "Science & Tech"
	},
	{
		photoNameActive: "icon15Active.png"
		photoNameInactive: "icon15Inactive.png"
		text: "Fashion"
	},
]

exports.radioStationsSpanish = [
	{
		name: "Europa FM"
		number: "91.00 FM"
		favorite: true
		artistSongName: "Estopa en Levántate y Cárdenas"
		id: 0
	},
	{
		name: "Cadena Dial"
		number: "91.70 FM"
		favorite: true
		artistSongName: "Ana Guerra y Manel Fuentes despiertan a todos los Atrevidos"
		id: 1
	},
	{
		name: "Radiole"
		number: "92.40 FM"
		favorite: true
		artistSongName: "El Duende Callejero - La Jungla de Alquitrán"
		id: 2
	},
	{
		name: "Cadena COPE"
		number: "94.80 FM"
		favorite: false
		artistSongName: "#COPEhaceHistoria en la radio española"
		id: 3
	},
	{
		name: "Onda Cero"
		number: "98.00 FM"
		favorite: true
		artistSongName: "La última pregunta a David Calle, el profesor de Youtube"
		id: 4
	},
	{
		name: "Cadena 100"
		number: "99.50 FM"
		favorite: false
		artistSongName: "Los niños y Jimeno - ¿Playa o montaña?"
		id: 5
	},
	{
		name: "Kiss FM"
		number: "102.70 FM"
		favorite: true
		artistSongName: "KISS FM, lo mejor de los 80 y los 90 hasta hoy"
		id: 6
	},
	{
		name: "Cadena Ser"
		number: "105.40 FM"
		favorite: false
		artistSongName: "Por qué soy feminista y voy a la huelga"
		id: 7
	},
	{
		name: "Ghost radio, figure it out"
		number: "110.5 FM"
		favorite: false
		artistSongName: "artistSongName"
		id: 8
	},
]

exports.phoneContactsSpanish = [
	{
		initial: "A"
		photoName: ""
		name: "Sofía Alcaldo"
		nameInitials: "SA"
		lastCalledDateString: "Outgoing Call | October 15 2018, 12:34pm"
		lastCallType: 1
		favorite: false
	},
	{
		initial: "B"
		photoName: ""
		name: "Valentina Barbero"
		nameInitials: "VB"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: "image03.png"
		name: "Sebastián Bravo"
		nameInitials: "SB"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: true
	},
	{
		initial: "C"
		photoName: "image04.png"
		name: "Mateo Cabrero"
		nameInitials: "MC"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: true
	},
	{
		initial: ""
		photoName: "image05.png"
		name: "María José Cantor"
		nameInitials: "MC"
		lastCalledDateString: "Incoming Call | October 14 2018, 5:16pm"
		lastCallType: 2
		favorite: true
	},
	{
		initial: ""
		photoName: ""
		name: "Alejandro Castillejo"
		nameInitials: "AC"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Daniel Cola"
		nameInitials: "DC"
		lastCalledDateString: "Outgoing Call | October 14 2018, 10:07am"
		lastCallType: 1
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Lucía Cortés"
		nameInitials: "LC"
		lastCalledDateString: "Missed Call | October 13 2018, 2:30pm"
		lastCallType: 3
		favorite: false
	},
	{
		initial: "D"
		photoName: "image09.png"
		name: "Mateo Dominguez"
		nameInitials: "MD"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: true
	},
	{
		initial: ""
		photoName: "image10.png"
		name: "Elena Delgado"
		nameInitials: "ED"
		lastCalledDateString: "Outgoing Call | October 9 2018, 11:11am"
		lastCallType: 1
		favorite: true
	},
	{
		initial: "G"
		photoName: ""
		name: "Emmanuel Garza"
		nameInitials: "EG"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: "image12.png"
		name: "Paula Grand"
		nameInitials: "PG"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: true
	},
	{
		initial: "H"
		photoName: ""
		name: "Mariana Hernandez"
		nameInitials: "MH"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Lucia Herrera"
		nameInitials: "LH"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: "image15.png"
		name: "Zoe Hidalgo"
		nameInitials: "ZH"
		lastCalledDateString: "Incoming Call | October 2 2018, 9:15am"
		lastCallType: 2
		favorite: true
	},
	{
		initial: "L"
		photoName: "image16.png"
		name: "Thiago Lopez"
		nameInitials: "TL"
		lastCalledDateString: "Missed Call | September 29 2018, 10:30pm"
		lastCallType: 3
		favorite: true
	},
	{
		initial: ""
		photoName: "image17.png"
		name: "Renata Lopez"
		nameInitials: "RL"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: true
	},
	{
		initial: "M"
		photoName: ""
		name: "Catalina Marin"
		nameInitials: "CM"
		lastCalledDateString: "Outgoing Call | September 26 2018, 1:47pm"
		lastCallType: 1
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "María Martínez"
		nameInitials: "MM"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Fernando Marques"
		nameInitials: "FM"
		lastCalledDateString: "Missed Call | September 25 2018, 7:58pm"
		lastCallType: 3
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Juan José Molinero"
		nameInitials: "JM"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Natalia Moreno"
		nameInitials: "NM"
		lastCalledDateString: "Outgoing Call | September 20 2018, 3:33pm"
		lastCallType: 1
		favorite: false
	},
	{
		initial: "R"
		photoName: ""
		name: "Alonso Ramirez"
		nameInitials: "AR"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: ""
		photoName: ""
		name: "Olivia Rodríguez"
		nameInitials: "OR"
		lastCalledDateString: "Outgoing Call | September 20 2018, 3:33pm"
		lastCallType: 1
		favorite: false
	},
	{
		initial: ""
		photoName: "image25.png"
		name: "Lola Rubio"
		nameInitials: "LR"
		lastCalledDateString: "Incoming Call | September 14 2018, 7:30pm"
		lastCallType: 2
		favorite: true
	},
	{
		initial: ""
		photoName: ""
		name: "Juan Ruiz"
		nameInitials: "JR"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: "S"
		photoName: ""
		name: "Constanza Suarez"
		nameInitials: "CS"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
	{
		initial: "T"
		photoName: "image28.png"
		name: "Alessandra Torrero"
		nameInitials: "AT"
		lastCalledDateString: "Missed Call | September 11 2018, 11:11am"
		lastCallType: 3
		favorite: true
	},
	{
		initial: "V"
		photoName: ""
		name: "Miranda Velazquez"
		nameInitials: "MV"
		lastCalledDateString: ""
		lastCallType: 0
		favorite: false
	},
]

exports.photoGallery = [
	{
		imageName: "img01.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img02.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img03.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img04.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img05.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img06.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img07.jpg"
		dateString: "July 2018"
	},
	{
		imageName: "img08.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img09.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img10.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img11.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img12.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img13.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img14.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img15.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img16.jpg"
		dateString: "June 2018"
	},
	{
		imageName: "img17.jpg"
		dateString: "May 2018"
	},
	{
		imageName: "img18.jpg"
		dateString: "May 2018"
	},
	{
		imageName: "img19.jpg"
		dateString: "May 2018"
	},
	{
		imageName: "img20.jpg"
		dateString: "May 2018"
	},
	{
		imageName: "img21.jpg"
		dateString: "May 2018"
	},
	{
		imageName: "img22.jpg"
		dateString: "April 2018"
	},
	{
		imageName: "img23.jpg"
		dateString: "April 2018"
	},
]
