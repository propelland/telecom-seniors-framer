# {Fonts} = require "fonts"
colors = require 'colors'

exports.header0 =
   fontFamily: "ProximaNovaMedium"
   color: colors.blackColor
   fontSize: "144px"

exports.header1 =
   fontFamily: "ProximaNovaRegular"
   color: colors.blackColor
   fontSize: "92px"

exports.header2 =
   fontFamily: "ProximaNovaBold"
   color: colors.blackColor
   fontSize: "74px"

exports.header3 =
   fontFamily: "ProximaNovaBold"
   color: colors.blackColor
   fontSize: "56px"

exports.numberStyle =
	fontFamily: "ProximaNovaRegular"
	color: colors.blackColor
	fontSize: "56px"

exports.header4 =
   fontFamily: "ProximaNovaBold"
   color: colors.blackColor
   fontSize: "46px"

exports.callTime =
	fontFamily: "ProximaNovaMedium"
	color: colors.blackColor
	fontSize: "46px"

exports.subhead =
   fontFamily: "ProximaNovaSemibold"
   color: colors.blackColor
   fontSize: "38px"

exports.paragraph =
   fontFamily: "ProximaNovaRegular"
   color: colors.blackColor
   fontSize: "30px"

exports.tabActive =
   fontFamily: "ProximaNovaSemibold"
   color: colors.blackColor
   fontSize: "30px"

exports.button =
   fontFamily: "ProximaNovaBold"
   color: colors.blackColor
   fontSize: "30px"

exports.caption =
   fontFamily: "ProximaNovaMedium"
   color: colors.blackColor
   fontSize: "25px"
