require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"FontFace":[function(require,module,exports){
exports.FontFace = (function() {
  var TEST, addFontFace, loadTestingFileError, missingArgumentError, removeTestLayer, testNewFace;

  TEST = {
    face: "monospace",
    text: "foo",
    time: .01,
    maxLoadAttempts: 50,
    hideErrorMessages: true
  };

  TEST.style = {
    width: "auto",
    fontSize: "150px",
    fontFamily: TEST.face
  };

  TEST.layer = new Layer({
    name: "FontFace Tester",
    width: 0,
    height: 1,
    maxX: -Screen.width,
    visible: false,
    html: TEST.text,
    style: TEST.style
  });

  function FontFace(options) {
    this.name = this.file = this.testLayer = this.isLoaded = this.loadFailed = this.loadAttempts = this.originalSize = this.hideErrors = null;
    if (options != null) {
      this.name = options.name || null;
      this.file = options.file || null;
    }
    if (!((this.name != null) && (this.file != null))) {
      return missingArgumentError();
    }
    this.testLayer = TEST.layer.copy();
    this.testLayer.style = TEST.style;
    this.testLayer.maxX = -Screen.width;
    this.testLayer.visible = true;
    this.isLoaded = false;
    this.loadFailed = false;
    this.loadAttempts = 0;
    this.hideErrors = options.hideErrors;
    return addFontFace(this.name, this.file, this);
  }

  addFontFace = function(name, file, object) {
    var faceCSS, styleTag;
    styleTag = document.createElement('style');
    faceCSS = document.createTextNode("@font-face { font-family: '" + name + "'; src: url('" + file + "') format('truetype'); }");
    styleTag.appendChild(faceCSS);
    document.head.appendChild(styleTag);
    return testNewFace(name, object);
  };

  removeTestLayer = function(object) {
    object.testLayer.destroy();
    return object.testLayer = null;
  };

  testNewFace = function(name, object) {
    var initialWidth, widthUpdate;
    initialWidth = object.testLayer._element.getBoundingClientRect().width;
    if (initialWidth === 0) {
      if (object.hideErrors === false || TEST.hideErrorMessages === false) {
        print("Load testing failed. Attempting again.");
      }
      return Utils.delay(TEST.time, function() {
        return testNewFace(name, object);
      });
    }
    object.loadAttempts++;
    if (object.originalSize === null) {
      object.originalSize = initialWidth;
      object.testLayer.style = {
        fontFamily: name + ", " + TEST.face
      };
    }
    widthUpdate = object.testLayer._element.getBoundingClientRect().width;
    if (object.originalSize === widthUpdate) {
      if (object.loadAttempts < TEST.maxLoadAttempts) {
        return Utils.delay(TEST.time, function() {
          return testNewFace(name, object);
        });
      }
      if (!object.hideErrors) {
        print("⚠️ Failed loading FontFace: " + name);
      }
      object.isLoaded = false;
      object.loadFailed = true;
      if (!object.hideErrors) {
        loadTestingFileError(object);
      }
      return;
    } else {
      if (!(object.hideErrors === false || TEST.hideErrorMessages)) {
        print("LOADED: " + name);
      }
      object.isLoaded = true;
      object.loadFailed = false;
    }
    removeTestLayer(object);
    return name;
  };

  missingArgumentError = function() {
    error(null);
    return console.error("Error: You must pass name & file properites when creating a new FontFace. \n\nExample: myFace = new FontFace name:\"Gotham\", file:\"gotham.ttf\" \n");
  };

  loadTestingFileError = function(object) {
    error(null);
    return console.error("Error: Couldn't detect the font: \"" + object.name + "\" and file: \"" + object.file + "\" was loaded.  \n\nEither the file couldn't be found or your browser doesn't support the file type that was provided. \n\nSuppress this message by adding \"hideErrors: true\" when creating a new FontFace. \n");
  };

  return FontFace;

})();


},{}],"circleModule":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

exports.Circle = (function(superClass) {
  extend(Circle, superClass);

  Circle.prototype.currentValue = null;

  function Circle(options) {
    var base, base1, base2, base3, base4, base5, base6, base7, base8, counter, numberDuration, numberEnd, numberInterval, numberNow, numberStart, self, style;
    this.options = options != null ? options : {};
    if ((base = this.options).circleSize == null) {
      base.circleSize = 300;
    }
    if ((base1 = this.options).strokeWidth == null) {
      base1.strokeWidth = 24;
    }
    if ((base2 = this.options).strokeColor == null) {
      base2.strokeColor = "#fc245c";
    }
    if ((base3 = this.options).topColor == null) {
      base3.topColor = null;
    }
    if ((base4 = this.options).bottomColor == null) {
      base4.bottomColor = null;
    }
    if ((base5 = this.options).hasCounter == null) {
      base5.hasCounter = null;
    }
    if ((base6 = this.options).counterColor == null) {
      base6.counterColor = "#fff";
    }
    if ((base7 = this.options).counterFontSize == null) {
      base7.counterFontSize = 60;
    }
    if ((base8 = this.options).hasLinearEasing == null) {
      base8.hasLinearEasing = null;
    }
    this.options.value = 2;
    this.options.viewBox = this.options.circleSize + this.options.strokeWidth;
    Circle.__super__.constructor.call(this, this.options);
    this.backgroundColor = "";
    this.height = this.options.viewBox;
    this.width = this.options.viewBox;
    this.rotation = -90;
    this.pathLength = Math.PI * this.options.circleSize;
    this.circleID = "circle" + Math.floor(Math.random() * 1000);
    this.gradientID = "circle" + Math.floor(Math.random() * 1000);
    if (this.options.hasCounter !== null) {
      counter = new Layer({
        parent: this,
        html: "",
        width: this.width,
        height: this.height,
        backgroundColor: "",
        rotation: 90,
        color: this.options.counterColor
      });
      style = {
        textAlign: "center",
        fontSize: this.options.counterFontSize + "px",
        lineHeight: this.height + "px",
        fontWeight: "600",
        fontFamily: "-apple-system, Helvetica, Arial, sans-serif",
        boxSizing: "border-box",
        height: this.height
      };
      counter.style = style;
      numberStart = 0;
      numberEnd = 100;
      numberDuration = 2;
      numberNow = numberStart;
      numberInterval = numberEnd - numberStart;
    }
    this.html = "<svg viewBox='-" + (this.options.strokeWidth / 2) + " -" + (this.options.strokeWidth / 2) + " " + this.options.viewBox + " " + this.options.viewBox + "' >\n	<defs>\n	    <linearGradient id='" + this.gradientID + "' >\n	        <stop offset=\"0%\" stop-color='" + (this.options.topColor !== null ? this.options.bottomColor : this.options.strokeColor) + "'/>\n	        <stop offset=\"100%\" stop-color='" + (this.options.topColor !== null ? this.options.topColor : this.options.strokeColor) + "' stop-opacity=\"1\" />\n	    </linearGradient>\n	</defs>\n	<circle id='" + this.circleID + "'\n			fill='none'\n			stroke-linecap='round'\n			stroke-width      = '" + this.options.strokeWidth + "'\n			stroke-dasharray  = '" + this.pathLength + "'\n			stroke-dashoffset = '0'\n			stroke=\"url(#" + this.gradientID + ")\"\n			stroke-width=\"10\"\n			cx = '" + (this.options.circleSize / 2) + "'\n			cy = '" + (this.options.circleSize / 2) + "'\n			r  = '" + (this.options.circleSize / 2) + "'>\n</svg>";
    self = this;
    Utils.domComplete(function() {
      return self.path = document.querySelector("#" + self.circleID);
    });
    this.proxy = new Layer({
      opacity: 0,
      visible: false
    });
    this.proxy.on(Events.AnimationEnd, function(animation, layer) {
      return self.onFinished();
    });
    this.proxy.on('change:x', function() {
      var offset;
      offset = Utils.modulate(this.x, [0, 500], [self.pathLength, 0]);
      self.path.setAttribute('stroke-dashoffset', offset);
      if (self.options.hasCounter !== null) {
        numberNow = Utils.round(self.proxy.x / 5);
        return counter.html = numberNow;
      }
    });
    Utils.domComplete(function() {
      return self.proxy.x = 0.1;
    });
  }

  Circle.prototype.changeTo = function(value, time) {
    var customCurve;
    if (time === void 0) {
      time = 2;
    }
    if (this.options.hasCounter === true && this.options.hasLinearEasing === null) {
      customCurve = "linear";
    } else {
      customCurve = "ease-in-out";
    }
    this.proxy.animate({
      properties: {
        x: 500 * (value / 100)
      },
      time: time,
      curve: customCurve
    });
    return this.currentValue = value;
  };

  Circle.prototype.startAt = function(value) {
    this.proxy.animate({
      properties: {
        x: 500 * (value / 100)
      },
      time: 0.001
    });
    return this.currentValue = value;
  };

  Circle.prototype.hide = function() {
    return this.opacity = 0;
  };

  Circle.prototype.show = function() {
    return this.opacity = 1;
  };

  Circle.prototype.onFinished = function() {};

  return Circle;

})(Layer);


},{}],"colors":[function(require,module,exports){
exports.blackColor = "#1F1F1F";

exports.whiteColor = "#FFFFFF";

exports.gray800 = "#4B5161";

exports.gray600 = "#5A6175";

exports.gray400 = "#828DAB";

exports.gray200 = "#BEC6DA";

exports.stone800 = "#E1E5EA";

exports.stone600 = "#E7E9EF";

exports.stone400 = "#EFF2F6";

exports.stone200 = "#F1F4FA";

exports.blue800 = "#0095C2";

exports.blue600 = "#00B0E4";

exports.blue400 = "#68C8EE";

exports.blue200 = "#ADE8FF";

exports.green800 = "#56A479";

exports.green600 = "#5BCD8E";

exports.green400 = "#BAEED1";

exports.green200 = "#D6F6E4";

exports.red800 = "#B43737";

exports.red600 = "#E54D4D";

exports.red400 = "#FFCCCC";

exports.red200 = "#FFF0F0";

exports.yellow800 = "#EDB51D";

exports.yellow600 = "#FFC852";

exports.yellow400 = "#FED58C";

exports.yellow200 = "#FEEDCC";

exports.circleColor1 = "#FF7744";

exports.circleColor2 = "#918BE5";

exports.phoneContactBackgroundColors = ["#4DD0E1", "#9575CD", "#F06292", "#AED581", "#FFB74D", "#FF8A65", "#FFD54F", "#7986CB", "#4FC3F7"];


},{}],"firebase":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  slice = [].slice;

exports.Firebase = (function(superClass) {
  var parseArgs, request;

  extend(Firebase, superClass);

  Firebase.define("status", {
    get: function() {
      return this._status;
    }
  });

  function Firebase(options1) {
    var base, base1, base2;
    this.options = options1 != null ? options1 : {};
    this.projectID = (base = this.options).projectID != null ? base.projectID : base.projectID = null;
    this.secret = (base1 = this.options).secret != null ? base1.secret : base1.secret = null;
    this.debug = (base2 = this.options).debug != null ? base2.debug : base2.debug = false;
    if (this._status == null) {
      this._status = "disconnected";
    }
    this.secretEndPoint = this.secret ? "?auth=" + this.secret : "?";
    Firebase.__super__.constructor.apply(this, arguments);
    if (this.debug) {
      console.log("Firebase: Connecting to Firebase Project '" + this.projectID + "' ... \n URL: 'https://" + this.projectID + ".firebaseio.com'");
    }
    this.onChange("connection");
  }

  request = function(project, secret, path, callback, method, data, parameters, debug) {
    var options, r, url;
    url = "https://" + project + ".firebaseio.com" + path + ".json" + secret;
    if (parameters != null) {
      if (parameters.shallow) {
        url += "&shallow=true";
      }
      if (parameters.format === "export") {
        url += "&format=export";
      }
      switch (parameters.print) {
        case "pretty":
          url += "&print=pretty";
          break;
        case "silent":
          url += "&print=silent";
      }
      if (typeof parameters.download === "string") {
        url += "&download=" + parameters.download;
        window.open(url, "_self");
      }
      if (typeof parameters.orderBy === "string") {
        url += "&orderBy=" + '"' + parameters.orderBy + '"';
      }
      if (typeof parameters.limitToFirst === "number") {
        url += "&limitToFirst=" + parameters.limitToFirst;
      }
      if (typeof parameters.limitToLast === "number") {
        url += "&limitToLast=" + parameters.limitToLast;
      }
      if (typeof parameters.startAt === "number") {
        url += "&startAt=" + parameters.startAt;
      }
      if (typeof parameters.endAt === "number") {
        url += "&endAt=" + parameters.endAt;
      }
      if (typeof parameters.equalTo === "number") {
        url += "&equalTo=" + parameters.equalTo;
      }
    }
    if (debug) {
      console.log("Firebase: New '" + method + "'-request with data: '" + (JSON.stringify(data)) + "' \n URL: '" + url + "'");
    }
    options = {
      method: method,
      headers: {
        'content-type': 'application/json; charset=utf-8'
      }
    };
    if (data != null) {
      options.body = JSON.stringify(data);
    }
    r = fetch(url, options).then(function(res) {
      var json;
      if (!res.ok) {
        throw Error(res.statusText);
      }
      json = res.json();
      json.then(callback);
      return json;
    })["catch"]((function(_this) {
      return function(error) {
        return console.warn(error);
      };
    })(this));
    return r;
  };

  parseArgs = function() {
    var args, cb, i, l;
    l = arguments[0], args = 3 <= arguments.length ? slice.call(arguments, 1, i = arguments.length - 1) : (i = 1, []), cb = arguments[i++];
    if (typeof args[l - 1] === "object") {
      args[l] = args[l - 1];
      args[l - 1] = null;
    }
    return cb.apply(null, args);
  };

  Firebase.prototype.get = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [2].concat(slice.call(args), [(function(_this) {
      return function(path, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "GET", null, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.put = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "PUT", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.post = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "POST", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.patch = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [3].concat(slice.call(args), [(function(_this) {
      return function(path, data, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "PATCH", data, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype["delete"] = function() {
    var args;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return parseArgs.apply(null, [2].concat(slice.call(args), [(function(_this) {
      return function(path, callback, parameters) {
        return request(_this.projectID, _this.secretEndPoint, path, callback, "DELETE", null, parameters, _this.debug);
      };
    })(this)]));
  };

  Firebase.prototype.onChange = function(path, callback) {
    var currentStatus, source, url;
    if (path === "connection") {
      url = "https://" + this.projectID + ".firebaseio.com/.json" + this.secretEndPoint;
      currentStatus = "disconnected";
      source = new EventSource(url);
      source.addEventListener("open", (function(_this) {
        return function() {
          if (currentStatus === "disconnected") {
            _this._status = "connected";
            if (callback != null) {
              callback("connected");
            }
            if (_this.debug) {
              console.log("Firebase: Connection to Firebase Project '" + _this.projectID + "' established");
            }
          }
          return currentStatus = "connected";
        };
      })(this));
      source.addEventListener("error", (function(_this) {
        return function() {
          if (currentStatus === "connected") {
            _this._status = "disconnected";
            if (callback != null) {
              callback("disconnected");
            }
            if (_this.debug) {
              console.warn("Firebase: Connection to Firebase Project '" + _this.projectID + "' closed");
            }
          }
          return currentStatus = "disconnected";
        };
      })(this));
      return;
    }
    url = "https://" + this.projectID + ".firebaseio.com" + path + ".json" + this.secretEndPoint;
    source = new EventSource(url);
    if (this.debug) {
      console.log("Firebase: Listening to changes made to '" + path + "' \n URL: '" + url + "'");
    }
    source.addEventListener("put", (function(_this) {
      return function(ev) {
        if (callback != null) {
          callback(JSON.parse(ev.data).data, "put", JSON.parse(ev.data).path, _.tail(JSON.parse(ev.data).path.split("/"), 1));
        }
        if (_this.debug) {
          return console.log("Firebase: Received changes made to '" + path + "' via 'PUT': " + (JSON.parse(ev.data).data) + " \n URL: '" + url + "'");
        }
      };
    })(this));
    return source.addEventListener("patch", (function(_this) {
      return function(ev) {
        if (callback != null) {
          callback(JSON.parse(ev.data).data, "patch", JSON.parse(ev.data).path, _.tail(JSON.parse(ev.data).path.split("/"), 1));
        }
        if (_this.debug) {
          return console.log("Firebase: Received changes made to '" + path + "' via 'PATCH': " + (JSON.parse(ev.data).data) + " \n URL: '" + url + "'");
        }
      };
    })(this));
  };

  return Firebase;

})(Framer.BaseClass);


},{}],"fonts":[function(require,module,exports){
var FontFace, ProximaNovaBlack, ProximaNovaBlackItalic, ProximaNovaBold, ProximaNovaBoldItalic, ProximaNovaExtrabold, ProximaNovaExtraboldItalic, ProximaNovaLight, ProximaNovaLightItalic, ProximaNovaMedium, ProximaNovaMediumItalic, ProximaNovaRegular, ProximaNovaRegularItalic, ProximaNovaSemiboldItalic, ProximaNovaThin, ProximaNovaThinItalic;

FontFace = require("FontFace").FontFace;

ProximaNovaBlackItalic = new FontFace({
  name: "ProximaNovaBlackItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Black Italic.otf"
});

ProximaNovaBlack = new FontFace({
  name: "ProximaNovaBlack",
  file: "Fonts/Proxima Nova/Proxima Nova Black.otf"
});

ProximaNovaBoldItalic = new FontFace({
  name: "ProximaNovaBoldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Bold Italic.otf"
});

ProximaNovaBold = new FontFace({
  name: "ProximaNovaBold",
  file: "Fonts/Proxima Nova/Proxima Nova Bold.otf"
});

ProximaNovaExtraboldItalic = new FontFace({
  name: "ProximaNovaExtraboldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Extrabold Italic.otf"
});

ProximaNovaExtrabold = new FontFace({
  name: "ProximaNovaExtrabold",
  file: "Fonts/Proxima Nova/Proxima Nova Extrabold.otf"
});

ProximaNovaLightItalic = new FontFace({
  name: "ProximaNovaLightItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Light Italic.otf"
});

ProximaNovaLight = new FontFace({
  name: "ProximaNovaLight",
  file: "Fonts/Proxima Nova/Proxima Nova Light.otf"
});

ProximaNovaMediumItalic = new FontFace({
  name: "ProximaNovaMediumItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Medium Italic.otf"
});

ProximaNovaMedium = new FontFace({
  name: "ProximaNovaMedium",
  file: "Fonts/Proxima Nova/Proxima Nova Medium.otf"
});

ProximaNovaRegularItalic = new FontFace({
  name: "ProximaNovaRegularItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Regular Italic.otf"
});

ProximaNovaRegular = new FontFace({
  name: "ProximaNovaRegular",
  file: "Fonts/Proxima Nova/Proxima Nova Regular.otf"
});

ProximaNovaSemiboldItalic = new FontFace({
  name: "ProximaNovaSemiboldItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Semibold Italic.otf"
});

ProximaNovaRegular = new FontFace({
  name: "ProximaNovaSemibold",
  file: "Fonts/Proxima Nova/Proxima Nova Semibold.otf"
});

ProximaNovaThinItalic = new FontFace({
  name: "ProximaNovaThinItalic",
  file: "Fonts/Proxima Nova/Proxima Nova Thin Italic.otf"
});

ProximaNovaThin = new FontFace({
  name: "ProximaNovaThin",
  file: "Fonts/Proxima Nova/Proxima Nova Thin.otf"
});


},{"FontFace":"FontFace"}],"general":[function(require,module,exports){
exports.callStartDate = new Date;

exports.firstTimeKnob = true;

exports.lastIdleState = 1;

exports.lastInteractionDate = new Date;

exports.language = "spanish";

exports.onACall = false;

exports.proximityState = false;

exports.screen_height = Framer.Device.screen.height;

exports.screen_width = Framer.Device.screen.width;

exports.showingIdle = true;

exports.showLogs = false;

exports.swipeAnimation = false;


},{}],"protoFakeInfo":[function(require,module,exports){
exports.socialTalkContactsSpanish = [
  {
    photoName: "avatar12.jpg",
    name: "Miranda",
    lastConnectionDateString: "October 15 2018 | 47 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar08.jpg",
    name: "Olivia",
    lastConnectionDateString: "October 13 2018 | 98 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar02.jpg",
    name: "Simón",
    lastConnectionDateString: "October 9 2018 | 27 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar01.jpg",
    name: "Elías",
    lastConnectionDateString: "October 8 2018 | 42 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar15.jpg",
    name: "Dante",
    lastConnectionDateString: "October 7 2018 | 37 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar05.jpg",
    name: "Irene",
    lastConnectionDateString: "October 2 2018 | 47 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar04.jpg",
    name: "Luana",
    lastConnectionDateString: "September 30 2018 | 57 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar07.jpg",
    name: "Isabel",
    lastConnectionDateString: "September 27 2018 | 72 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar09.jpg",
    name: "Elena",
    lastConnectionDateString: "September 24 2018 | 53 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Dylan",
    lastConnectionDateString: "September 23 2018 | 36 minutes",
    status: "Available",
    friend: false
  }, {
    photoName: "avatar10.jpg",
    name: "Ángel",
    lastConnectionDateString: "September 19 2018 | 27 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar14.jpg",
    name: "Martina",
    lastConnectionDateString: "September 16 2018 | 14 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar11.jpg",
    name: "Victoria",
    lastConnectionDateString: "September 12 2018 | 22 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar01.jpg",
    name: "Camila",
    lastConnectionDateString: "September 10 2018 | 39 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar03.jpg",
    name: "Sofia",
    lastConnectionDateString: "September 7 2018 | 10 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar06.jpg",
    name: "Tomas",
    lastConnectionDateString: "September 6 2018 | 27 minutes",
    status: "Unavailable",
    friend: true
  }, {
    photoName: "avatar08.jpg",
    name: "Joaquín",
    lastConnectionDateString: "September 3 2018 | 8 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar07.jpg",
    name: "Mía",
    lastConnectionDateString: "September 1 2018 | 48 minutes",
    status: "Available",
    friend: true
  }, {
    photoName: "avatar11.jpg",
    name: "Lucas",
    lastConnectionDateString: "August 29 2018 | 18 minutes",
    status: "Unavailable",
    friend: false
  }, {
    photoName: "avatar13.jpg",
    name: "Natalia",
    lastConnectionDateString: "August 26 2018 | 53 minutes",
    status: "Available",
    friend: true
  }
];

exports.interests = [
  {
    photoNameActive: "icon01Active.png",
    photoNameInactive: "icon01Inactive.png",
    text: "Family"
  }, {
    photoNameActive: "icon02Active.png",
    photoNameInactive: "icon02Inactive.png",
    text: "Pets"
  }, {
    photoNameActive: "icon03Active.png",
    photoNameInactive: "icon03Inactive.png",
    text: "Travel"
  }, {
    photoNameActive: "icon04Active.png",
    photoNameInactive: "icon04Inactive.png",
    text: "Reading"
  }, {
    photoNameActive: "icon05Active.png",
    photoNameInactive: "icon05Inactive.png",
    text: "Gardening"
  }, {
    photoNameActive: "icon07Active.png",
    photoNameInactive: "icon07Inactive.png",
    text: "Cooking"
  }, {
    photoNameActive: "icon06Active.png",
    photoNameInactive: "icon06Inactive.png",
    text: "Outdoors"
  }, {
    photoNameActive: "icon08Active.png",
    photoNameInactive: "icon08Inactive.png",
    text: "Current Events"
  }, {
    photoNameActive: "icon09Active.png",
    photoNameInactive: "icon09Inactive.png",
    text: "Economy"
  }, {
    photoNameActive: "icon10Active.png",
    photoNameInactive: "icon10Inactive.png",
    text: "Art & Culture"
  }, {
    photoNameActive: "icon11Active.png",
    photoNameInactive: "icon11Inactive.png",
    text: "Movies"
  }, {
    photoNameActive: "icon12Active.png",
    photoNameInactive: "icon12Inactive.png",
    text: "Television"
  }, {
    photoNameActive: "icon13Active.png",
    photoNameInactive: "icon13Inactive.png",
    text: "Sports"
  }, {
    photoNameActive: "icon14Active.png",
    photoNameInactive: "icon14Inactive.png",
    text: "Science & Tech"
  }, {
    photoNameActive: "icon15Active.png",
    photoNameInactive: "icon15Inactive.png",
    text: "Fashion"
  }
];

exports.radioStationsSpanish = [
  {
    name: "Europa FM",
    number: "91.00 FM",
    favorite: true,
    artistSongName: "Estopa en Levántate y Cárdenas",
    id: 0
  }, {
    name: "Cadena Dial",
    number: "91.70 FM",
    favorite: true,
    artistSongName: "Ana Guerra y Manel Fuentes despiertan a todos los Atrevidos",
    id: 1
  }, {
    name: "Radiole",
    number: "92.40 FM",
    favorite: true,
    artistSongName: "El Duende Callejero - La Jungla de Alquitrán",
    id: 2
  }, {
    name: "Cadena COPE",
    number: "94.80 FM",
    favorite: false,
    artistSongName: "#COPEhaceHistoria en la radio española",
    id: 3
  }, {
    name: "Onda Cero",
    number: "98.00 FM",
    favorite: true,
    artistSongName: "La última pregunta a David Calle, el profesor de Youtube",
    id: 4
  }, {
    name: "Cadena 100",
    number: "99.50 FM",
    favorite: false,
    artistSongName: "Los niños y Jimeno - ¿Playa o montaña?",
    id: 5
  }, {
    name: "Kiss FM",
    number: "102.70 FM",
    favorite: true,
    artistSongName: "KISS FM, lo mejor de los 80 y los 90 hasta hoy",
    id: 6
  }, {
    name: "Cadena Ser",
    number: "105.40 FM",
    favorite: false,
    artistSongName: "Por qué soy feminista y voy a la huelga",
    id: 7
  }, {
    name: "Ghost radio, figure it out",
    number: "110.5 FM",
    favorite: false,
    artistSongName: "artistSongName",
    id: 8
  }
];

exports.phoneContactsSpanish = [
  {
    initial: "A",
    photoName: "",
    name: "Sofía Alcaldo",
    nameInitials: "SA",
    lastCalledDateString: "Outgoing Call | October 15 2018, 12:34pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "B",
    photoName: "",
    name: "Valentina Barbero",
    nameInitials: "VB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "image03.png",
    name: "Sebastián Bravo",
    nameInitials: "SB",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "C",
    photoName: "image04.png",
    name: "Mateo Cabrero",
    nameInitials: "MC",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "",
    photoName: "image05.png",
    name: "María José Cantor",
    nameInitials: "MC",
    lastCalledDateString: "Incoming Call | October 14 2018, 5:16pm",
    lastCallType: 2,
    favorite: true
  }, {
    initial: "",
    photoName: "",
    name: "Alejandro Castillejo",
    nameInitials: "AC",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Daniel Cola",
    nameInitials: "DC",
    lastCalledDateString: "Outgoing Call | October 14 2018, 10:07am",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Lucía Cortés",
    nameInitials: "LC",
    lastCalledDateString: "Missed Call | October 13 2018, 2:30pm",
    lastCallType: 3,
    favorite: false
  }, {
    initial: "D",
    photoName: "image09.png",
    name: "Mateo Dominguez",
    nameInitials: "MD",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "",
    photoName: "image10.png",
    name: "Elena Delgado",
    nameInitials: "ED",
    lastCalledDateString: "Outgoing Call | October 9 2018, 11:11am",
    lastCallType: 1,
    favorite: true
  }, {
    initial: "G",
    photoName: "",
    name: "Emmanuel Garza",
    nameInitials: "EG",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "image12.png",
    name: "Paula Grand",
    nameInitials: "PG",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "H",
    photoName: "",
    name: "Mariana Hernandez",
    nameInitials: "MH",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Lucia Herrera",
    nameInitials: "LH",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "image15.png",
    name: "Zoe Hidalgo",
    nameInitials: "ZH",
    lastCalledDateString: "Incoming Call | October 2 2018, 9:15am",
    lastCallType: 2,
    favorite: true
  }, {
    initial: "L",
    photoName: "image16.png",
    name: "Thiago Lopez",
    nameInitials: "TL",
    lastCalledDateString: "Missed Call | September 29 2018, 10:30pm",
    lastCallType: 3,
    favorite: true
  }, {
    initial: "",
    photoName: "image17.png",
    name: "Renata Lopez",
    nameInitials: "RL",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: true
  }, {
    initial: "M",
    photoName: "",
    name: "Catalina Marin",
    nameInitials: "CM",
    lastCalledDateString: "Outgoing Call | September 26 2018, 1:47pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "María Martínez",
    nameInitials: "MM",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Fernando Marques",
    nameInitials: "FM",
    lastCalledDateString: "Missed Call | September 25 2018, 7:58pm",
    lastCallType: 3,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Juan José Molinero",
    nameInitials: "JM",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Natalia Moreno",
    nameInitials: "NM",
    lastCalledDateString: "Outgoing Call | September 20 2018, 3:33pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "R",
    photoName: "",
    name: "Alonso Ramirez",
    nameInitials: "AR",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "",
    photoName: "",
    name: "Olivia Rodríguez",
    nameInitials: "OR",
    lastCalledDateString: "Outgoing Call | September 20 2018, 3:33pm",
    lastCallType: 1,
    favorite: false
  }, {
    initial: "",
    photoName: "image25.png",
    name: "Lola Rubio",
    nameInitials: "LR",
    lastCalledDateString: "Incoming Call | September 14 2018, 7:30pm",
    lastCallType: 2,
    favorite: true
  }, {
    initial: "",
    photoName: "",
    name: "Juan Ruiz",
    nameInitials: "JR",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "S",
    photoName: "",
    name: "Constanza Suarez",
    nameInitials: "CS",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }, {
    initial: "T",
    photoName: "image28.png",
    name: "Alessandra Torrero",
    nameInitials: "AT",
    lastCalledDateString: "Missed Call | September 11 2018, 11:11am",
    lastCallType: 3,
    favorite: true
  }, {
    initial: "V",
    photoName: "",
    name: "Miranda Velazquez",
    nameInitials: "MV",
    lastCalledDateString: "",
    lastCallType: 0,
    favorite: false
  }
];

exports.photoGallery = [
  {
    imageName: "img01.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img02.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img03.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img04.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img05.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img06.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img07.jpg",
    dateString: "July 2018"
  }, {
    imageName: "img08.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img09.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img10.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img11.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img12.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img13.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img14.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img15.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img16.jpg",
    dateString: "June 2018"
  }, {
    imageName: "img17.jpg",
    dateString: "May 2018"
  }, {
    imageName: "img18.jpg",
    dateString: "May 2018"
  }, {
    imageName: "img19.jpg",
    dateString: "May 2018"
  }, {
    imageName: "img20.jpg",
    dateString: "May 2018"
  }, {
    imageName: "img21.jpg",
    dateString: "May 2018"
  }, {
    imageName: "img22.jpg",
    dateString: "April 2018"
  }, {
    imageName: "img23.jpg",
    dateString: "April 2018"
  }
];


},{}],"styles":[function(require,module,exports){
var colors;

colors = require('colors');

exports.header0 = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "144px"
};

exports.header1 = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "92px"
};

exports.header2 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "74px"
};

exports.header3 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "56px"
};

exports.numberStyle = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "56px"
};

exports.header4 = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.callTime = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "46px"
};

exports.subhead = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "38px"
};

exports.paragraph = {
  fontFamily: "ProximaNovaRegular",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.tabActive = {
  fontFamily: "ProximaNovaSemibold",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.button = {
  fontFamily: "ProximaNovaBold",
  color: colors.blackColor,
  fontSize: "30px"
};

exports.caption = {
  fontFamily: "ProximaNovaMedium",
  color: colors.blackColor,
  fontSize: "25px"
};


},{"colors":"colors"}],"time":[function(require,module,exports){
exports.getTimeString = function() {
  var hour, minute, now, prepand, second, timeString;
  now = new Date;
  hour = now.getUTCHours() + 2;
  if (hour >= 24) {
    hour = hour - 24;
  }
  minute = now.getUTCMinutes();
  second = now.getUTCSeconds();
  prepand = hour >= 12 ? 'pm' : 'am';
  hour = hour >= 12 ? hour - 12 : hour;
  if (hour === 0) {
    hour = 12;
  }
  minute = minute < 10 ? "0" + minute : minute;
  timeString = hour + ":" + minute + prepand;
  return timeString;
};

exports.getTimeDifferenceString = function(difference) {
  var hour, minute, second, timeString;
  difference = difference / 1000;
  second = Math.floor(difference % 60);
  difference = difference / 60;
  minute = Math.floor(difference % 60);
  difference = difference / 60;
  hour = Math.floor(difference % 24);
  hour = hour < 10 ? "0" + hour : hour;
  minute = minute < 10 ? "0" + minute : minute;
  second = second < 10 ? "0" + second : second;
  timeString = hour + ":" + minute + ":" + second;
  return timeString;
};


},{}]},{},[])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJhbWVyLm1vZHVsZXMuanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbS5mcmFtZXIvbW9kdWxlcy90aW1lLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbS5mcmFtZXIvbW9kdWxlcy9zdHlsZXMuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tLmZyYW1lci9tb2R1bGVzL3Byb3RvRmFrZUluZm8uY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tLmZyYW1lci9tb2R1bGVzL2dlbmVyYWwuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tLmZyYW1lci9tb2R1bGVzL2ZvbnRzLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbS5mcmFtZXIvbW9kdWxlcy9maXJlYmFzZS5jb2ZmZWUiLCIuLi8uLi8uLi8uLi8uLi9MaWJyYXJ5L01vYmlsZSBEb2N1bWVudHMvY29tfmFwcGxlfkNsb3VkRG9jcy9Qcm9wZWxsYW5kL1Byb3RvdHlwZXMvVGVsZWNvbSBEZXNpZ24vQ29kZS90ZWxlY29tLXNlbmlvcnMtZnJhbWVyL3RlbGVjb20uZnJhbWVyL21vZHVsZXMvY29sb3JzLmNvZmZlZSIsIi4uLy4uLy4uLy4uLy4uL0xpYnJhcnkvTW9iaWxlIERvY3VtZW50cy9jb21+YXBwbGV+Q2xvdWREb2NzL1Byb3BlbGxhbmQvUHJvdG90eXBlcy9UZWxlY29tIERlc2lnbi9Db2RlL3RlbGVjb20tc2VuaW9ycy1mcmFtZXIvdGVsZWNvbS5mcmFtZXIvbW9kdWxlcy9jaXJjbGVNb2R1bGUuY29mZmVlIiwiLi4vLi4vLi4vLi4vLi4vTGlicmFyeS9Nb2JpbGUgRG9jdW1lbnRzL2NvbX5hcHBsZX5DbG91ZERvY3MvUHJvcGVsbGFuZC9Qcm90b3R5cGVzL1RlbGVjb20gRGVzaWduL0NvZGUvdGVsZWNvbS1zZW5pb3JzLWZyYW1lci90ZWxlY29tLmZyYW1lci9tb2R1bGVzL0ZvbnRGYWNlLmNvZmZlZSIsIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0cy5nZXRUaW1lU3RyaW5nID0gKCkgLT5cbiAgbm93ID0gbmV3IERhdGVcbiAgIyBcdGRheSA9IG5vdy5nZXRVVENEYXkoKVxuICAjIFx0ZGF5bGlzdCA9IFtcbiAgIyBcdFx0J1N1bmRheSdcbiAgIyBcdFx0J01vbmRheSdcbiAgIyBcdFx0J1R1ZXNkYXknXG4gICMgXHRcdCdXZWRuZXNkYXknXG4gICMgXHRcdCdUaHVyc2RheSdcbiAgIyBcdFx0J0ZyaWRheSdcbiAgIyBcdFx0J1NhdHVyZGF5J1xuICAjIFx0XVxuICBob3VyID0gbm93LmdldFVUQ0hvdXJzKCkgKyAyXG4gIGlmIGhvdXIgPj0gMjRcbiAgXHRob3VyID0gaG91ciAtIDI0XG4gICMgXHRcdGRheSArPSAxXG4gIG1pbnV0ZSA9IG5vdy5nZXRVVENNaW51dGVzKClcbiAgc2Vjb25kID0gbm93LmdldFVUQ1NlY29uZHMoKVxuICBwcmVwYW5kID0gaWYgaG91ciA+PSAxMiB0aGVuICdwbScgZWxzZSAnYW0nXG4gIGhvdXIgPSBpZiBob3VyID49IDEyIHRoZW4gaG91ciAtIDEyIGVsc2UgaG91clxuICBpZiBob3VyID09IDBcbiAgXHRob3VyID0gMTJcbiAgbWludXRlID0gaWYgbWludXRlIDwgMTAgdGhlbiBcIjBcIiArIG1pbnV0ZSBlbHNlIG1pbnV0ZVxuICB0aW1lU3RyaW5nID0gaG91ciArIFwiOlwiICsgbWludXRlICsgcHJlcGFuZFxuICByZXR1cm4gdGltZVN0cmluZ1xuXG5leHBvcnRzLmdldFRpbWVEaWZmZXJlbmNlU3RyaW5nID0gKGRpZmZlcmVuY2UpIC0+XG4gIGRpZmZlcmVuY2UgPSBkaWZmZXJlbmNlLzEwMDA7XG4gIHNlY29uZCA9IE1hdGguZmxvb3IoZGlmZmVyZW5jZSAlIDYwKTtcbiAgZGlmZmVyZW5jZSA9IGRpZmZlcmVuY2UvNjA7XG4gIG1pbnV0ZSA9IE1hdGguZmxvb3IoZGlmZmVyZW5jZSAlIDYwKTtcbiAgZGlmZmVyZW5jZSA9IGRpZmZlcmVuY2UvNjA7XG4gIGhvdXIgPSBNYXRoLmZsb29yKGRpZmZlcmVuY2UgJSAyNCk7XG5cbiAgaG91ciA9IGlmIGhvdXIgPCAxMCB0aGVuIFwiMFwiICsgaG91ciBlbHNlIGhvdXJcbiAgbWludXRlID0gaWYgbWludXRlIDwgMTAgdGhlbiBcIjBcIiArIG1pbnV0ZSBlbHNlIG1pbnV0ZVxuICBzZWNvbmQgPSBpZiBzZWNvbmQgPCAxMCB0aGVuIFwiMFwiICsgc2Vjb25kIGVsc2Ugc2Vjb25kXG5cbiAgdGltZVN0cmluZyA9IGhvdXIgKyBcIjpcIiArIG1pbnV0ZSArIFwiOlwiICsgc2Vjb25kXG5cbiAgcmV0dXJuIHRpbWVTdHJpbmdcbiIsIiMge0ZvbnRzfSA9IHJlcXVpcmUgXCJmb250c1wiXG5jb2xvcnMgPSByZXF1aXJlICdjb2xvcnMnXG5cbmV4cG9ydHMuaGVhZGVyMCA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhTWVkaXVtXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiMTQ0cHhcIlxuXG5leHBvcnRzLmhlYWRlcjEgPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCI5MnB4XCJcblxuZXhwb3J0cy5oZWFkZXIyID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFCb2xkXCJcbiAgIGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuICAgZm9udFNpemU6IFwiNzRweFwiXG5cbmV4cG9ydHMuaGVhZGVyMyA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhQm9sZFwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjU2cHhcIlxuXG5leHBvcnRzLm51bWJlclN0eWxlID1cblx0Zm9udEZhbWlseTogXCJQcm94aW1hTm92YVJlZ3VsYXJcIlxuXHRjb2xvcjogY29sb3JzLmJsYWNrQ29sb3Jcblx0Zm9udFNpemU6IFwiNTZweFwiXG5cbmV4cG9ydHMuaGVhZGVyNCA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhQm9sZFwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjQ2cHhcIlxuXG5leHBvcnRzLmNhbGxUaW1lID1cblx0Zm9udEZhbWlseTogXCJQcm94aW1hTm92YU1lZGl1bVwiXG5cdGNvbG9yOiBjb2xvcnMuYmxhY2tDb2xvclxuXHRmb250U2l6ZTogXCI0NnB4XCJcblxuZXhwb3J0cy5zdWJoZWFkID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFTZW1pYm9sZFwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjM4cHhcIlxuXG5leHBvcnRzLnBhcmFncmFwaCA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhUmVndWxhclwiXG4gICBjb2xvcjogY29sb3JzLmJsYWNrQ29sb3JcbiAgIGZvbnRTaXplOiBcIjMwcHhcIlxuXG5leHBvcnRzLnRhYkFjdGl2ZSA9XG4gICBmb250RmFtaWx5OiBcIlByb3hpbWFOb3ZhU2VtaWJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIzMHB4XCJcblxuZXhwb3J0cy5idXR0b24gPVxuICAgZm9udEZhbWlseTogXCJQcm94aW1hTm92YUJvbGRcIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIzMHB4XCJcblxuZXhwb3J0cy5jYXB0aW9uID1cbiAgIGZvbnRGYW1pbHk6IFwiUHJveGltYU5vdmFNZWRpdW1cIlxuICAgY29sb3I6IGNvbG9ycy5ibGFja0NvbG9yXG4gICBmb250U2l6ZTogXCIyNXB4XCJcbiIsImV4cG9ydHMuc29jaWFsVGFsa0NvbnRhY3RzU3BhbmlzaCA9IFtcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMi5qcGdcIlxuXHRcdG5hbWU6IFwiTWlyYW5kYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMTUgMjAxOCB8IDQ3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA4LmpwZ1wiXG5cdFx0bmFtZTogXCJPbGl2aWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDEzIDIwMTggfCA5OCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwMi5qcGdcIlxuXHRcdG5hbWU6IFwiU2ltw7NuXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiT2N0b2JlciA5IDIwMTggfCAyNyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWU6IFwiYXZhdGFyMDEuanBnXCJcblx0XHRuYW1lOiBcIkVsw61hc1wiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgOCAyMDE4IHwgNDIgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxNS5qcGdcIlxuXHRcdG5hbWU6IFwiRGFudGVcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJPY3RvYmVyIDcgMjAxOCB8IDM3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJVbmF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA1LmpwZ1wiXG5cdFx0bmFtZTogXCJJcmVuZVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIk9jdG9iZXIgMiAyMDE4IHwgNDcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjA0LmpwZ1wiXG5cdFx0bmFtZTogXCJMdWFuYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAzMCAyMDE4IHwgNTcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwNy5qcGdcIlxuXHRcdG5hbWU6IFwiSXNhYmVsXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDI3IDIwMTggfCA3MiBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwOS5qcGdcIlxuXHRcdG5hbWU6IFwiRWxlbmFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMjQgMjAxOCB8IDUzIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAzLmpwZ1wiXG5cdFx0bmFtZTogXCJEeWxhblwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAyMyAyMDE4IHwgMzYgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIkF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjEwLmpwZ1wiXG5cdFx0bmFtZTogXCLDgW5nZWxcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMTkgMjAxOCB8IDI3IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjE0LmpwZ1wiXG5cdFx0bmFtZTogXCJNYXJ0aW5hXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiU2VwdGVtYmVyIDE2IDIwMTggfCAxNCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiVW5hdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMS5qcGdcIlxuXHRcdG5hbWU6IFwiVmljdG9yaWFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMTIgMjAxOCB8IDIyIG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAxLmpwZ1wiXG5cdFx0bmFtZTogXCJDYW1pbGFcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMTAgMjAxOCB8IDM5IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJBdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjAzLmpwZ1wiXG5cdFx0bmFtZTogXCJTb2ZpYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciA3IDIwMTggfCAxMCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiVW5hdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwNi5qcGdcIlxuXHRcdG5hbWU6IFwiVG9tYXNcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgNiAyMDE4IHwgMjcgbWludXRlc1wiXG5cdFx0c3RhdHVzOiBcIlVuYXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwOC5qcGdcIlxuXHRcdG5hbWU6IFwiSm9hcXXDrW5cIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJTZXB0ZW1iZXIgMyAyMDE4IHwgOCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiVW5hdmFpbGFibGVcIlxuXHRcdGZyaWVuZDogZmFsc2Vcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIwNy5qcGdcIlxuXHRcdG5hbWU6IFwiTcOtYVwiXG5cdFx0bGFzdENvbm5lY3Rpb25EYXRlU3RyaW5nOiBcIlNlcHRlbWJlciAxIDIwMTggfCA0OCBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZTogXCJhdmF0YXIxMS5qcGdcIlxuXHRcdG5hbWU6IFwiTHVjYXNcIlxuXHRcdGxhc3RDb25uZWN0aW9uRGF0ZVN0cmluZzogXCJBdWd1c3QgMjkgMjAxOCB8IDE4IG1pbnV0ZXNcIlxuXHRcdHN0YXR1czogXCJVbmF2YWlsYWJsZVwiXG5cdFx0ZnJpZW5kOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lOiBcImF2YXRhcjEzLmpwZ1wiXG5cdFx0bmFtZTogXCJOYXRhbGlhXCJcblx0XHRsYXN0Q29ubmVjdGlvbkRhdGVTdHJpbmc6IFwiQXVndXN0IDI2IDIwMTggfCA1MyBtaW51dGVzXCJcblx0XHRzdGF0dXM6IFwiQXZhaWxhYmxlXCJcblx0XHRmcmllbmQ6IHRydWVcblx0fVxuXVxuXG5leHBvcnRzLmludGVyZXN0cyA9IFtcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDFBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDFJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiRmFtaWx5XCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDJBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDJJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiUGV0c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjAzQWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjAzSW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlRyYXZlbFwiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjA0QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjA0SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlJlYWRpbmdcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wNUFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wNUluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJHYXJkZW5pbmdcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wN0FjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wN0luYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJDb29raW5nXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMDZBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMDZJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiT3V0ZG9vcnNcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24wOEFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24wOEluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJDdXJyZW50IEV2ZW50c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjA5QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjA5SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIkVjb25vbXlcIlxuXHR9LFxuXHR7XG5cdFx0cGhvdG9OYW1lQWN0aXZlOiBcImljb24xMEFjdGl2ZS5wbmdcIlxuXHRcdHBob3RvTmFtZUluYWN0aXZlOiBcImljb24xMEluYWN0aXZlLnBuZ1wiXG5cdFx0dGV4dDogXCJBcnQgJiBDdWx0dXJlXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTFBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTFJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiTW92aWVzXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTJBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTJJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiVGVsZXZpc2lvblwiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjEzQWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjEzSW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlNwb3J0c1wiXG5cdH0sXG5cdHtcblx0XHRwaG90b05hbWVBY3RpdmU6IFwiaWNvbjE0QWN0aXZlLnBuZ1wiXG5cdFx0cGhvdG9OYW1lSW5hY3RpdmU6IFwiaWNvbjE0SW5hY3RpdmUucG5nXCJcblx0XHR0ZXh0OiBcIlNjaWVuY2UgJiBUZWNoXCJcblx0fSxcblx0e1xuXHRcdHBob3RvTmFtZUFjdGl2ZTogXCJpY29uMTVBY3RpdmUucG5nXCJcblx0XHRwaG90b05hbWVJbmFjdGl2ZTogXCJpY29uMTVJbmFjdGl2ZS5wbmdcIlxuXHRcdHRleHQ6IFwiRmFzaGlvblwiXG5cdH0sXG5dXG5cbmV4cG9ydHMucmFkaW9TdGF0aW9uc1NwYW5pc2ggPSBbXG5cdHtcblx0XHRuYW1lOiBcIkV1cm9wYSBGTVwiXG5cdFx0bnVtYmVyOiBcIjkxLjAwIEZNXCJcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHRcdGFydGlzdFNvbmdOYW1lOiBcIkVzdG9wYSBlbiBMZXbDoW50YXRlIHkgQ8OhcmRlbmFzXCJcblx0XHRpZDogMFxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJDYWRlbmEgRGlhbFwiXG5cdFx0bnVtYmVyOiBcIjkxLjcwIEZNXCJcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHRcdGFydGlzdFNvbmdOYW1lOiBcIkFuYSBHdWVycmEgeSBNYW5lbCBGdWVudGVzIGRlc3BpZXJ0YW4gYSB0b2RvcyBsb3MgQXRyZXZpZG9zXCJcblx0XHRpZDogMVxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJSYWRpb2xlXCJcblx0XHRudW1iZXI6IFwiOTIuNDAgRk1cIlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdFx0YXJ0aXN0U29uZ05hbWU6IFwiRWwgRHVlbmRlIENhbGxlamVybyAtIExhIEp1bmdsYSBkZSBBbHF1aXRyw6FuXCJcblx0XHRpZDogMlxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJDYWRlbmEgQ09QRVwiXG5cdFx0bnVtYmVyOiBcIjk0LjgwIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCIjQ09QRWhhY2VIaXN0b3JpYSBlbiBsYSByYWRpbyBlc3Bhw7FvbGFcIlxuXHRcdGlkOiAzXG5cdH0sXG5cdHtcblx0XHRuYW1lOiBcIk9uZGEgQ2Vyb1wiXG5cdFx0bnVtYmVyOiBcIjk4LjAwIEZNXCJcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHRcdGFydGlzdFNvbmdOYW1lOiBcIkxhIMO6bHRpbWEgcHJlZ3VudGEgYSBEYXZpZCBDYWxsZSwgZWwgcHJvZmVzb3IgZGUgWW91dHViZVwiXG5cdFx0aWQ6IDRcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiQ2FkZW5hIDEwMFwiXG5cdFx0bnVtYmVyOiBcIjk5LjUwIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJMb3MgbmnDsW9zIHkgSmltZW5vIC0gwr9QbGF5YSBvIG1vbnRhw7FhP1wiXG5cdFx0aWQ6IDVcblx0fSxcblx0e1xuXHRcdG5hbWU6IFwiS2lzcyBGTVwiXG5cdFx0bnVtYmVyOiBcIjEwMi43MCBGTVwiXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0XHRhcnRpc3RTb25nTmFtZTogXCJLSVNTIEZNLCBsbyBtZWpvciBkZSBsb3MgODAgeSBsb3MgOTAgaGFzdGEgaG95XCJcblx0XHRpZDogNlxuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJDYWRlbmEgU2VyXCJcblx0XHRudW1iZXI6IFwiMTA1LjQwIEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJQb3IgcXXDqSBzb3kgZmVtaW5pc3RhIHkgdm95IGEgbGEgaHVlbGdhXCJcblx0XHRpZDogN1xuXHR9LFxuXHR7XG5cdFx0bmFtZTogXCJHaG9zdCByYWRpbywgZmlndXJlIGl0IG91dFwiXG5cdFx0bnVtYmVyOiBcIjExMC41IEZNXCJcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0XHRhcnRpc3RTb25nTmFtZTogXCJhcnRpc3RTb25nTmFtZVwiXG5cdFx0aWQ6IDhcblx0fSxcbl1cblxuZXhwb3J0cy5waG9uZUNvbnRhY3RzU3BhbmlzaCA9IFtcblx0e1xuXHRcdGluaXRpYWw6IFwiQVwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJTb2bDrWEgQWxjYWxkb1wiXG5cdFx0bmFtZUluaXRpYWxzOiBcIlNBXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJPdXRnb2luZyBDYWxsIHwgT2N0b2JlciAxNSAyMDE4LCAxMjozNHBtXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDFcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiQlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJWYWxlbnRpbmEgQmFyYmVyb1wiXG5cdFx0bmFtZUluaXRpYWxzOiBcIlZCXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHRcdGxhc3RDYWxsVHlwZTogMFxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTAzLnBuZ1wiXG5cdFx0bmFtZTogXCJTZWJhc3Rpw6FuIEJyYXZvXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiU0JcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiQ1wiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMDQucG5nXCJcblx0XHRuYW1lOiBcIk1hdGVvIENhYnJlcm9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJNQ1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTA1LnBuZ1wiXG5cdFx0bmFtZTogXCJNYXLDrWEgSm9zw6kgQ2FudG9yXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTUNcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBPY3RvYmVyIDE0IDIwMTgsIDU6MTZwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAyXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkFsZWphbmRybyBDYXN0aWxsZWpvXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiQUNcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJEYW5pZWwgQ29sYVwiXG5cdFx0bmFtZUluaXRpYWxzOiBcIkRDXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJPdXRnb2luZyBDYWxsIHwgT2N0b2JlciAxNCAyMDE4LCAxMDowN2FtXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDFcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkx1Y8OtYSBDb3J0w6lzXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTENcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk1pc3NlZCBDYWxsIHwgT2N0b2JlciAxMyAyMDE4LCAyOjMwcG1cIlxuXHRcdGxhc3RDYWxsVHlwZTogM1xuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJEXCJcblx0XHRwaG90b05hbWU6IFwiaW1hZ2UwOS5wbmdcIlxuXHRcdG5hbWU6IFwiTWF0ZW8gRG9taW5ndWV6XCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTURcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiaW1hZ2UxMC5wbmdcIlxuXHRcdG5hbWU6IFwiRWxlbmEgRGVsZ2Fkb1wiXG5cdFx0bmFtZUluaXRpYWxzOiBcIkVEXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJPdXRnb2luZyBDYWxsIHwgT2N0b2JlciA5IDIwMTgsIDExOjExYW1cIlxuXHRcdGxhc3RDYWxsVHlwZTogMVxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIkdcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiRW1tYW51ZWwgR2FyemFcIlxuXHRcdG5hbWVJbml0aWFsczogXCJFR1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiXCJcblx0XHRwaG90b05hbWU6IFwiaW1hZ2UxMi5wbmdcIlxuXHRcdG5hbWU6IFwiUGF1bGEgR3JhbmRcIlxuXHRcdG5hbWVJbml0aWFsczogXCJQR1wiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJIXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIk1hcmlhbmEgSGVybmFuZGV6XCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTUhcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJMdWNpYSBIZXJyZXJhXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiTEhcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMTUucG5nXCJcblx0XHRuYW1lOiBcIlpvZSBIaWRhbGdvXCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiWkhcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIkluY29taW5nIENhbGwgfCBPY3RvYmVyIDIgMjAxOCwgOToxNWFtXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDJcblx0XHRmYXZvcml0ZTogdHJ1ZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJMXCJcblx0XHRwaG90b05hbWU6IFwiaW1hZ2UxNi5wbmdcIlxuXHRcdG5hbWU6IFwiVGhpYWdvIExvcGV6XCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiVExcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIk1pc3NlZCBDYWxsIHwgU2VwdGVtYmVyIDI5IDIwMTgsIDEwOjMwcG1cIlxuXHRcdGxhc3RDYWxsVHlwZTogM1xuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMTcucG5nXCJcblx0XHRuYW1lOiBcIlJlbmF0YSBMb3BlelwiXG5cdFx0bmFtZUluaXRpYWxzOiBcIlJMXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHRcdGxhc3RDYWxsVHlwZTogMFxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIk1cIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiQ2F0YWxpbmEgTWFyaW5cIlxuXHRcdG5hbWVJbml0aWFsczogXCJDTVwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyNiAyMDE4LCAxOjQ3cG1cIlxuXHRcdGxhc3RDYWxsVHlwZTogMVxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiTWFyw61hIE1hcnTDrW5lelwiXG5cdFx0bmFtZUluaXRpYWxzOiBcIk1NXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHRcdGxhc3RDYWxsVHlwZTogMFxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiRmVybmFuZG8gTWFycXVlc1wiXG5cdFx0bmFtZUluaXRpYWxzOiBcIkZNXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJNaXNzZWQgQ2FsbCB8IFNlcHRlbWJlciAyNSAyMDE4LCA3OjU4cG1cIlxuXHRcdGxhc3RDYWxsVHlwZTogM1xuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiSnVhbiBKb3PDqSBNb2xpbmVyb1wiXG5cdFx0bmFtZUluaXRpYWxzOiBcIkpNXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHRcdGxhc3RDYWxsVHlwZTogMFxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJcIlxuXHRcdHBob3RvTmFtZTogXCJcIlxuXHRcdG5hbWU6IFwiTmF0YWxpYSBNb3Jlbm9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJOTVwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiT3V0Z29pbmcgQ2FsbCB8IFNlcHRlbWJlciAyMCAyMDE4LCAzOjMzcG1cIlxuXHRcdGxhc3RDYWxsVHlwZTogMVxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXHR7XG5cdFx0aW5pdGlhbDogXCJSXCJcblx0XHRwaG90b05hbWU6IFwiXCJcblx0XHRuYW1lOiBcIkFsb25zbyBSYW1pcmV6XCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiQVJcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJPbGl2aWEgUm9kcsOtZ3VlelwiXG5cdFx0bmFtZUluaXRpYWxzOiBcIk9SXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJPdXRnb2luZyBDYWxsIHwgU2VwdGVtYmVyIDIwIDIwMTgsIDM6MzNwbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAxXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcImltYWdlMjUucG5nXCJcblx0XHRuYW1lOiBcIkxvbGEgUnViaW9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJMUlwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiSW5jb21pbmcgQ2FsbCB8IFNlcHRlbWJlciAxNCAyMDE4LCA3OjMwcG1cIlxuXHRcdGxhc3RDYWxsVHlwZTogMlxuXHRcdGZhdm9yaXRlOiB0cnVlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJKdWFuIFJ1aXpcIlxuXHRcdG5hbWVJbml0aWFsczogXCJKUlwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiXCJcblx0XHRsYXN0Q2FsbFR5cGU6IDBcblx0XHRmYXZvcml0ZTogZmFsc2Vcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiU1wiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJDb25zdGFuemEgU3VhcmV6XCJcblx0XHRuYW1lSW5pdGlhbHM6IFwiQ1NcIlxuXHRcdGxhc3RDYWxsZWREYXRlU3RyaW5nOiBcIlwiXG5cdFx0bGFzdENhbGxUeXBlOiAwXG5cdFx0ZmF2b3JpdGU6IGZhbHNlXG5cdH0sXG5cdHtcblx0XHRpbml0aWFsOiBcIlRcIlxuXHRcdHBob3RvTmFtZTogXCJpbWFnZTI4LnBuZ1wiXG5cdFx0bmFtZTogXCJBbGVzc2FuZHJhIFRvcnJlcm9cIlxuXHRcdG5hbWVJbml0aWFsczogXCJBVFwiXG5cdFx0bGFzdENhbGxlZERhdGVTdHJpbmc6IFwiTWlzc2VkIENhbGwgfCBTZXB0ZW1iZXIgMTEgMjAxOCwgMTE6MTFhbVwiXG5cdFx0bGFzdENhbGxUeXBlOiAzXG5cdFx0ZmF2b3JpdGU6IHRydWVcblx0fSxcblx0e1xuXHRcdGluaXRpYWw6IFwiVlwiXG5cdFx0cGhvdG9OYW1lOiBcIlwiXG5cdFx0bmFtZTogXCJNaXJhbmRhIFZlbGF6cXVlelwiXG5cdFx0bmFtZUluaXRpYWxzOiBcIk1WXCJcblx0XHRsYXN0Q2FsbGVkRGF0ZVN0cmluZzogXCJcIlxuXHRcdGxhc3RDYWxsVHlwZTogMFxuXHRcdGZhdm9yaXRlOiBmYWxzZVxuXHR9LFxuXVxuXG5leHBvcnRzLnBob3RvR2FsbGVyeSA9IFtcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwMy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwNy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVseSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwOC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcwOS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxMy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxNC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxNS5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxNi5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiSnVuZSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcxNy5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiTWF5IDIwMThcIlxuXHR9LFxuXHR7XG5cdFx0aW1hZ2VOYW1lOiBcImltZzE4LmpwZ1wiXG5cdFx0ZGF0ZVN0cmluZzogXCJNYXkgMjAxOFwiXG5cdH0sXG5cdHtcblx0XHRpbWFnZU5hbWU6IFwiaW1nMTkuanBnXCJcblx0XHRkYXRlU3RyaW5nOiBcIk1heSAyMDE4XCJcblx0fSxcblx0e1xuXHRcdGltYWdlTmFtZTogXCJpbWcyMC5qcGdcIlxuXHRcdGRhdGVTdHJpbmc6IFwiTWF5IDIwMThcIlxuXHR9LFxuXHR7XG5cdFx0aW1hZ2VOYW1lOiBcImltZzIxLmpwZ1wiXG5cdFx0ZGF0ZVN0cmluZzogXCJNYXkgMjAxOFwiXG5cdH0sXG5cdHtcblx0XHRpbWFnZU5hbWU6IFwiaW1nMjIuanBnXCJcblx0XHRkYXRlU3RyaW5nOiBcIkFwcmlsIDIwMThcIlxuXHR9LFxuXHR7XG5cdFx0aW1hZ2VOYW1lOiBcImltZzIzLmpwZ1wiXG5cdFx0ZGF0ZVN0cmluZzogXCJBcHJpbCAyMDE4XCJcblx0fSxcbl1cbiIsImV4cG9ydHMuY2FsbFN0YXJ0RGF0ZSA9IG5ldyBEYXRlXG5leHBvcnRzLmZpcnN0VGltZUtub2IgPSB0cnVlXG5leHBvcnRzLmxhc3RJZGxlU3RhdGUgPSAxXG5leHBvcnRzLmxhc3RJbnRlcmFjdGlvbkRhdGUgPSBuZXcgRGF0ZVxuZXhwb3J0cy5sYW5ndWFnZSA9IFwic3BhbmlzaFwiXG5leHBvcnRzLm9uQUNhbGwgPSBmYWxzZVxuZXhwb3J0cy5wcm94aW1pdHlTdGF0ZSA9IGZhbHNlXG5leHBvcnRzLnNjcmVlbl9oZWlnaHQgPSBGcmFtZXIuRGV2aWNlLnNjcmVlbi5oZWlnaHRcbmV4cG9ydHMuc2NyZWVuX3dpZHRoID0gRnJhbWVyLkRldmljZS5zY3JlZW4ud2lkdGhcbmV4cG9ydHMuc2hvd2luZ0lkbGUgPSB0cnVlXG5leHBvcnRzLnNob3dMb2dzID0gZmFsc2VcbmV4cG9ydHMuc3dpcGVBbmltYXRpb24gPSBmYWxzZVxuIiwie0ZvbnRGYWNlfSA9IHJlcXVpcmUgXCJGb250RmFjZVwiXG5cblByb3hpbWFOb3ZhQmxhY2tJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUJsYWNrSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIEJsYWNrIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YUJsYWNrID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFCbGFja1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBCbGFjay5vdGZcIlxuXG5Qcm94aW1hTm92YUJvbGRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUJvbGRJdGFsaWNcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgQm9sZCBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFCb2xkID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFCb2xkXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIEJvbGQub3RmXCJcblxuUHJveGltYU5vdmFFeHRyYWJvbGRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUV4dHJhYm9sZEl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBFeHRyYWJvbGQgSXRhbGljLm90ZlwiXG5cblByb3hpbWFOb3ZhRXh0cmFib2xkID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFFeHRyYWJvbGRcIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgRXh0cmFib2xkLm90ZlwiXG5cblByb3hpbWFOb3ZhTGlnaHRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YUxpZ2h0SXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIExpZ2h0IEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YUxpZ2h0ID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFMaWdodFwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBMaWdodC5vdGZcIlxuXG5Qcm94aW1hTm92YU1lZGl1bUl0YWxpYyA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhTWVkaXVtSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIE1lZGl1bSBJdGFsaWMub3RmXCJcblxuUHJveGltYU5vdmFNZWRpdW0gPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YU1lZGl1bVwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBNZWRpdW0ub3RmXCJcblxuUHJveGltYU5vdmFSZWd1bGFySXRhbGljID0gbmV3IEZvbnRGYWNlXG5cdG5hbWU6IFwiUHJveGltYU5vdmFSZWd1bGFySXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFJlZ3VsYXIgSXRhbGljLm90ZlwiXG5cblByb3hpbWFOb3ZhUmVndWxhciA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhUmVndWxhclwiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBSZWd1bGFyLm90ZlwiXG5cblByb3hpbWFOb3ZhU2VtaWJvbGRJdGFsaWMgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVNlbWlib2xkSXRhbGljXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFNlbWlib2xkIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YVJlZ3VsYXIgPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVNlbWlib2xkXCJcblx0ZmlsZTogXCJGb250cy9Qcm94aW1hIE5vdmEvUHJveGltYSBOb3ZhIFNlbWlib2xkLm90ZlwiXG5cblByb3hpbWFOb3ZhVGhpbkl0YWxpYyA9IG5ldyBGb250RmFjZVxuXHRuYW1lOiBcIlByb3hpbWFOb3ZhVGhpbkl0YWxpY1wiXG5cdGZpbGU6IFwiRm9udHMvUHJveGltYSBOb3ZhL1Byb3hpbWEgTm92YSBUaGluIEl0YWxpYy5vdGZcIlxuXG5Qcm94aW1hTm92YVRoaW4gPSBuZXcgRm9udEZhY2Vcblx0bmFtZTogXCJQcm94aW1hTm92YVRoaW5cIlxuXHRmaWxlOiBcIkZvbnRzL1Byb3hpbWEgTm92YS9Qcm94aW1hIE5vdmEgVGhpbi5vdGZcIlxuIiwiIyBEb2N1bWVudGF0aW9uIG9mIHRoaXMgTW9kdWxlOiBodHRwczovL2dpdGh1Yi5jb20vbWFyY2tyZW5uL2ZyYW1lci1GaXJlYmFzZVxuIyAtLS0tLS0gOiAtLS0tLS0tIEZpcmViYXNlIFJFU1QgQVBJOiBodHRwczovL2ZpcmViYXNlLmdvb2dsZS5jb20vZG9jcy9yZWZlcmVuY2UvcmVzdC9kYXRhYmFzZS9cblxuIyBGaXJlYmFzZSBSRVNUIEFQSSBDbGFzcyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbmNsYXNzIGV4cG9ydHMuRmlyZWJhc2UgZXh0ZW5kcyBGcmFtZXIuQmFzZUNsYXNzXG5cblxuXHRALmRlZmluZSBcInN0YXR1c1wiLFxuXHRcdGdldDogLT4gQF9zdGF0dXMgIyByZWFkT25seVxuXG5cdGNvbnN0cnVjdG9yOiAoQG9wdGlvbnM9e30pIC0+XG5cdFx0QHByb2plY3RJRCA9IEBvcHRpb25zLnByb2plY3RJRCA/PSBudWxsXG5cdFx0QHNlY3JldCAgICA9IEBvcHRpb25zLnNlY3JldCAgICA/PSBudWxsXG5cdFx0QGRlYnVnICAgICA9IEBvcHRpb25zLmRlYnVnICAgICA/PSBmYWxzZVxuXHRcdEBfc3RhdHVzICAgICAgICAgICAgICAgICAgICAgICAgPz0gXCJkaXNjb25uZWN0ZWRcIlxuXG5cdFx0QHNlY3JldEVuZFBvaW50ID0gaWYgQHNlY3JldCB0aGVuIFwiP2F1dGg9I3tAc2VjcmV0fVwiIGVsc2UgXCI/XCIgIyBob3RmaXhcblx0XHRzdXBlclxuXG5cdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogQ29ubmVjdGluZyB0byBGaXJlYmFzZSBQcm9qZWN0ICcje0Bwcm9qZWN0SUR9JyAuLi4gXFxuIFVSTDogJ2h0dHBzOi8vI3tAcHJvamVjdElEfS5maXJlYmFzZWlvLmNvbSdcIiBpZiBAZGVidWdcblx0XHRALm9uQ2hhbmdlIFwiY29ubmVjdGlvblwiXG5cblx0cmVxdWVzdCA9IChwcm9qZWN0LCBzZWNyZXQsIHBhdGgsIGNhbGxiYWNrLCBtZXRob2QsIGRhdGEsIHBhcmFtZXRlcnMsIGRlYnVnKSAtPlxuXG5cdFx0dXJsID0gXCJodHRwczovLyN7cHJvamVjdH0uZmlyZWJhc2Vpby5jb20je3BhdGh9Lmpzb24je3NlY3JldH1cIlxuXG5cdFx0aWYgcGFyYW1ldGVycz9cblx0XHRcdGlmIHBhcmFtZXRlcnMuc2hhbGxvdyAgICAgICAgICAgIHRoZW4gdXJsICs9IFwiJnNoYWxsb3c9dHJ1ZVwiXG5cdFx0XHRpZiBwYXJhbWV0ZXJzLmZvcm1hdCBpcyBcImV4cG9ydFwiIHRoZW4gdXJsICs9IFwiJmZvcm1hdD1leHBvcnRcIlxuXG5cdFx0XHRzd2l0Y2ggcGFyYW1ldGVycy5wcmludFxuXHRcdFx0XHR3aGVuIFwicHJldHR5XCIgdGhlbiB1cmwgKz0gXCImcHJpbnQ9cHJldHR5XCJcblx0XHRcdFx0d2hlbiBcInNpbGVudFwiIHRoZW4gdXJsICs9IFwiJnByaW50PXNpbGVudFwiXG5cblx0XHRcdGlmIHR5cGVvZiBwYXJhbWV0ZXJzLmRvd25sb2FkIGlzIFwic3RyaW5nXCJcblx0XHRcdFx0dXJsICs9IFwiJmRvd25sb2FkPSN7cGFyYW1ldGVycy5kb3dubG9hZH1cIlxuXHRcdFx0XHR3aW5kb3cub3Blbih1cmwsXCJfc2VsZlwiKVxuXG5cdFx0XHR1cmwgKz0gXCImb3JkZXJCeT1cIiArICdcIicgKyBwYXJhbWV0ZXJzLm9yZGVyQnkgKyAnXCInIGlmIHR5cGVvZiBwYXJhbWV0ZXJzLm9yZGVyQnkgICAgICBpcyBcInN0cmluZ1wiXG5cdFx0XHR1cmwgKz0gXCImbGltaXRUb0ZpcnN0PSN7cGFyYW1ldGVycy5saW1pdFRvRmlyc3R9XCIgICBpZiB0eXBlb2YgcGFyYW1ldGVycy5saW1pdFRvRmlyc3QgaXMgXCJudW1iZXJcIlxuXHRcdFx0dXJsICs9IFwiJmxpbWl0VG9MYXN0PSN7cGFyYW1ldGVycy5saW1pdFRvTGFzdH1cIiAgICAgaWYgdHlwZW9mIHBhcmFtZXRlcnMubGltaXRUb0xhc3QgIGlzIFwibnVtYmVyXCJcblx0XHRcdHVybCArPSBcIiZzdGFydEF0PSN7cGFyYW1ldGVycy5zdGFydEF0fVwiICAgICAgICAgICAgIGlmIHR5cGVvZiBwYXJhbWV0ZXJzLnN0YXJ0QXQgICAgICBpcyBcIm51bWJlclwiXG5cdFx0XHR1cmwgKz0gXCImZW5kQXQ9I3twYXJhbWV0ZXJzLmVuZEF0fVwiICAgICAgICAgICAgICAgICBpZiB0eXBlb2YgcGFyYW1ldGVycy5lbmRBdCAgICAgICAgaXMgXCJudW1iZXJcIlxuXHRcdFx0dXJsICs9IFwiJmVxdWFsVG89I3twYXJhbWV0ZXJzLmVxdWFsVG99XCIgICAgICAgICAgICAgaWYgdHlwZW9mIHBhcmFtZXRlcnMuZXF1YWxUbyAgICAgIGlzIFwibnVtYmVyXCJcblx0XHRcblx0XHRjb25zb2xlLmxvZyBcIkZpcmViYXNlOiBOZXcgJyN7bWV0aG9kfSctcmVxdWVzdCB3aXRoIGRhdGE6ICcje0pTT04uc3RyaW5naWZ5KGRhdGEpfScgXFxuIFVSTDogJyN7dXJsfSdcIiBpZiBkZWJ1Z1xuXHRcdFxuXHRcdG9wdGlvbnMgPVxuXHRcdFx0bWV0aG9kOiBtZXRob2Rcblx0XHRcdGhlYWRlcnM6XG5cdFx0XHRcdCdjb250ZW50LXR5cGUnOiAnYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOCdcblx0XHRcblx0XHRpZiBkYXRhP1xuXHRcdFx0b3B0aW9ucy5ib2R5ID0gSlNPTi5zdHJpbmdpZnkoZGF0YSlcblxuXHRcdHIgPSBmZXRjaCh1cmwsIG9wdGlvbnMpXG5cdFx0LnRoZW4gKHJlcykgLT5cblx0XHRcdGlmICFyZXMub2sgdGhlbiB0aHJvdyBFcnJvcihyZXMuc3RhdHVzVGV4dClcblx0XHRcdGpzb24gPSByZXMuanNvbigpXG5cdFx0XHRqc29uLnRoZW4gY2FsbGJhY2tcblx0XHRcdHJldHVybiBqc29uXG5cdFx0LmNhdGNoIChlcnJvcikgPT4gY29uc29sZS53YXJuKGVycm9yKVxuXHRcdFxuXHRcdHJldHVybiByXG5cblx0IyBUaGlyZCBhcmd1bWVudCBjYW4gYWxzbyBhY2NlcHQgb3B0aW9ucywgcmF0aGVyIHRoYW4gY2FsbGJhY2tcblx0cGFyc2VBcmdzID0gKGwsIGFyZ3MuLi4sIGNiKSAtPlxuXHRcdGlmIHR5cGVvZiBhcmdzW2wtMV0gaXMgXCJvYmplY3RcIlxuXHRcdFx0YXJnc1tsXSA9IGFyZ3NbbC0xXVxuXHRcdFx0YXJnc1tsLTFdID0gbnVsbFxuXG5cdFx0cmV0dXJuIGNiLmFwcGx5KG51bGwsIGFyZ3MpXG5cblx0IyBBdmFpbGFibGUgbWV0aG9kc1xuXG5cdGdldDogICAgKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAyLCBhcmdzLi4uLCAocGF0aCwgXHRcdCBjYWxsYmFjaywgcGFyYW1ldGVycykgPT4gcmVxdWVzdChAcHJvamVjdElELCBAc2VjcmV0RW5kUG9pbnQsIHBhdGgsIGNhbGxiYWNrLCBcIkdFVFwiLCAgICBudWxsLCBwYXJhbWV0ZXJzLCBAZGVidWcpXG5cdHB1dDogICAgKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAzLCBhcmdzLi4uLCAocGF0aCwgZGF0YSwgY2FsbGJhY2ssIHBhcmFtZXRlcnMpID0+IHJlcXVlc3QoQHByb2plY3RJRCwgQHNlY3JldEVuZFBvaW50LCBwYXRoLCBjYWxsYmFjaywgXCJQVVRcIiwgICAgZGF0YSwgcGFyYW1ldGVycywgQGRlYnVnKVxuXHRwb3N0OiAgIChhcmdzLi4uKSAtPiBwYXJzZUFyZ3MgMywgYXJncy4uLiwgKHBhdGgsIGRhdGEsIGNhbGxiYWNrLCBwYXJhbWV0ZXJzKSA9PiByZXF1ZXN0KEBwcm9qZWN0SUQsIEBzZWNyZXRFbmRQb2ludCwgcGF0aCwgY2FsbGJhY2ssIFwiUE9TVFwiLCAgIGRhdGEsIHBhcmFtZXRlcnMsIEBkZWJ1Zylcblx0cGF0Y2g6ICAoYXJncy4uLikgLT4gcGFyc2VBcmdzIDMsIGFyZ3MuLi4sIChwYXRoLCBkYXRhLCBjYWxsYmFjaywgcGFyYW1ldGVycykgPT4gcmVxdWVzdChAcHJvamVjdElELCBAc2VjcmV0RW5kUG9pbnQsIHBhdGgsIGNhbGxiYWNrLCBcIlBBVENIXCIsICBkYXRhLCBwYXJhbWV0ZXJzLCBAZGVidWcpXG5cdGRlbGV0ZTogKGFyZ3MuLi4pIC0+IHBhcnNlQXJncyAyLCBhcmdzLi4uLCAocGF0aCwgXHQgIFx0IGNhbGxiYWNrLCBwYXJhbWV0ZXJzKSA9PiByZXF1ZXN0KEBwcm9qZWN0SUQsIEBzZWNyZXRFbmRQb2ludCwgcGF0aCwgY2FsbGJhY2ssIFwiREVMRVRFXCIsIG51bGwsIHBhcmFtZXRlcnMsIEBkZWJ1ZylcblxuXG5cdG9uQ2hhbmdlOiAocGF0aCwgY2FsbGJhY2spIC0+XG5cblxuXHRcdGlmIHBhdGggaXMgXCJjb25uZWN0aW9uXCJcblxuXHRcdFx0dXJsID0gXCJodHRwczovLyN7QHByb2plY3RJRH0uZmlyZWJhc2Vpby5jb20vLmpzb24je0BzZWNyZXRFbmRQb2ludH1cIlxuXHRcdFx0Y3VycmVudFN0YXR1cyA9IFwiZGlzY29ubmVjdGVkXCJcblx0XHRcdHNvdXJjZSA9IG5ldyBFdmVudFNvdXJjZSh1cmwpXG5cblx0XHRcdHNvdXJjZS5hZGRFdmVudExpc3RlbmVyIFwib3BlblwiLCA9PlxuXHRcdFx0XHRpZiBjdXJyZW50U3RhdHVzIGlzIFwiZGlzY29ubmVjdGVkXCJcblx0XHRcdFx0XHRALl9zdGF0dXMgPSBcImNvbm5lY3RlZFwiXG5cdFx0XHRcdFx0Y2FsbGJhY2soXCJjb25uZWN0ZWRcIikgaWYgY2FsbGJhY2s/XG5cdFx0XHRcdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogQ29ubmVjdGlvbiB0byBGaXJlYmFzZSBQcm9qZWN0ICcje0Bwcm9qZWN0SUR9JyBlc3RhYmxpc2hlZFwiIGlmIEBkZWJ1Z1xuXHRcdFx0XHRjdXJyZW50U3RhdHVzID0gXCJjb25uZWN0ZWRcIlxuXG5cdFx0XHRzb3VyY2UuYWRkRXZlbnRMaXN0ZW5lciBcImVycm9yXCIsID0+XG5cdFx0XHRcdGlmIGN1cnJlbnRTdGF0dXMgaXMgXCJjb25uZWN0ZWRcIlxuXHRcdFx0XHRcdEAuX3N0YXR1cyA9IFwiZGlzY29ubmVjdGVkXCJcblx0XHRcdFx0XHRjYWxsYmFjayhcImRpc2Nvbm5lY3RlZFwiKSBpZiBjYWxsYmFjaz9cblx0XHRcdFx0XHRjb25zb2xlLndhcm4gXCJGaXJlYmFzZTogQ29ubmVjdGlvbiB0byBGaXJlYmFzZSBQcm9qZWN0ICcje0Bwcm9qZWN0SUR9JyBjbG9zZWRcIiBpZiBAZGVidWdcblx0XHRcdFx0Y3VycmVudFN0YXR1cyA9IFwiZGlzY29ubmVjdGVkXCJcblxuXHRcdFx0cmV0dXJuXG5cblx0XHR1cmwgPSBcImh0dHBzOi8vI3tAcHJvamVjdElEfS5maXJlYmFzZWlvLmNvbSN7cGF0aH0uanNvbiN7QHNlY3JldEVuZFBvaW50fVwiXG5cdFx0c291cmNlID0gbmV3IEV2ZW50U291cmNlKHVybClcblx0XHRjb25zb2xlLmxvZyBcIkZpcmViYXNlOiBMaXN0ZW5pbmcgdG8gY2hhbmdlcyBtYWRlIHRvICcje3BhdGh9JyBcXG4gVVJMOiAnI3t1cmx9J1wiIGlmIEBkZWJ1Z1xuXG5cdFx0c291cmNlLmFkZEV2ZW50TGlzdGVuZXIgXCJwdXRcIiwgKGV2KSA9PlxuXHRcdFx0Y2FsbGJhY2soSlNPTi5wYXJzZShldi5kYXRhKS5kYXRhLCBcInB1dFwiLCBKU09OLnBhcnNlKGV2LmRhdGEpLnBhdGgsIF8udGFpbChKU09OLnBhcnNlKGV2LmRhdGEpLnBhdGguc3BsaXQoXCIvXCIpLDEpKSBpZiBjYWxsYmFjaz9cblx0XHRcdGNvbnNvbGUubG9nIFwiRmlyZWJhc2U6IFJlY2VpdmVkIGNoYW5nZXMgbWFkZSB0byAnI3twYXRofScgdmlhICdQVVQnOiAje0pTT04ucGFyc2UoZXYuZGF0YSkuZGF0YX0gXFxuIFVSTDogJyN7dXJsfSdcIiBpZiBAZGVidWdcblxuXHRcdHNvdXJjZS5hZGRFdmVudExpc3RlbmVyIFwicGF0Y2hcIiwgKGV2KSA9PlxuXHRcdFx0Y2FsbGJhY2soSlNPTi5wYXJzZShldi5kYXRhKS5kYXRhLCBcInBhdGNoXCIsIEpTT04ucGFyc2UoZXYuZGF0YSkucGF0aCwgXy50YWlsKEpTT04ucGFyc2UoZXYuZGF0YSkucGF0aC5zcGxpdChcIi9cIiksMSkpIGlmIGNhbGxiYWNrP1xuXHRcdFx0Y29uc29sZS5sb2cgXCJGaXJlYmFzZTogUmVjZWl2ZWQgY2hhbmdlcyBtYWRlIHRvICcje3BhdGh9JyB2aWEgJ1BBVENIJzogI3tKU09OLnBhcnNlKGV2LmRhdGEpLmRhdGF9IFxcbiBVUkw6ICcje3VybH0nXCIgaWYgQGRlYnVnXG4iLCIjIEFkZCB0aGUgZm9sbG93aW5nIGxpbmUgdG8geW91ciBwcm9qZWN0IGluIEZyYW1lciBTdHVkaW8uXG4jIG15TW9kdWxlID0gcmVxdWlyZSBcIm15TW9kdWxlXCJcbiMgUmVmZXJlbmNlIHRoZSBjb250ZW50cyBieSBuYW1lLCBsaWtlIG15TW9kdWxlLm15RnVuY3Rpb24oKSBvciBteU1vZHVsZS5teVZhclxuXG4jIGV4cG9ydHMubXlWYXIgPSBcIm15VmFyaWFibGVcIlxuI1xuIyBleHBvcnRzLm15RnVuY3Rpb24gPSAtPlxuIyBcdHByaW50IFwibXlGdW5jdGlvbiBpcyBydW5uaW5nXCJcbiNcbiMgZXhwb3J0cy5teUFycmF5ID0gWzEsIDIsIDNdXG5cbmV4cG9ydHMuYmxhY2tDb2xvciA9IFwiIzFGMUYxRlwiXG5leHBvcnRzLndoaXRlQ29sb3IgPSBcIiNGRkZGRkZcIlxuXG5leHBvcnRzLmdyYXk4MDAgPSBcIiM0QjUxNjFcIlxuZXhwb3J0cy5ncmF5NjAwID0gXCIjNUE2MTc1XCJcbmV4cG9ydHMuZ3JheTQwMCA9IFwiIzgyOERBQlwiXG5leHBvcnRzLmdyYXkyMDAgPSBcIiNCRUM2REFcIlxuXG5leHBvcnRzLnN0b25lODAwID0gXCIjRTFFNUVBXCJcbmV4cG9ydHMuc3RvbmU2MDAgPSBcIiNFN0U5RUZcIlxuZXhwb3J0cy5zdG9uZTQwMCA9IFwiI0VGRjJGNlwiXG5leHBvcnRzLnN0b25lMjAwID0gXCIjRjFGNEZBXCJcblxuZXhwb3J0cy5ibHVlODAwID0gXCIjMDA5NUMyXCJcbmV4cG9ydHMuYmx1ZTYwMCA9IFwiIzAwQjBFNFwiXG5leHBvcnRzLmJsdWU0MDAgPSBcIiM2OEM4RUVcIlxuZXhwb3J0cy5ibHVlMjAwID0gXCIjQURFOEZGXCJcblxuZXhwb3J0cy5ncmVlbjgwMCA9IFwiIzU2QTQ3OVwiXG5leHBvcnRzLmdyZWVuNjAwID0gXCIjNUJDRDhFXCJcbmV4cG9ydHMuZ3JlZW40MDAgPSBcIiNCQUVFRDFcIlxuZXhwb3J0cy5ncmVlbjIwMCA9IFwiI0Q2RjZFNFwiXG5cbmV4cG9ydHMucmVkODAwID0gXCIjQjQzNzM3XCJcbmV4cG9ydHMucmVkNjAwID0gXCIjRTU0RDREXCJcbmV4cG9ydHMucmVkNDAwID0gXCIjRkZDQ0NDXCJcbmV4cG9ydHMucmVkMjAwID0gXCIjRkZGMEYwXCJcblxuZXhwb3J0cy55ZWxsb3c4MDAgPSBcIiNFREI1MURcIlxuZXhwb3J0cy55ZWxsb3c2MDAgPSBcIiNGRkM4NTJcIlxuZXhwb3J0cy55ZWxsb3c0MDAgPSBcIiNGRUQ1OENcIlxuZXhwb3J0cy55ZWxsb3cyMDAgPSBcIiNGRUVEQ0NcIlxuXG5leHBvcnRzLmNpcmNsZUNvbG9yMSA9IFwiI0ZGNzc0NFwiXG5leHBvcnRzLmNpcmNsZUNvbG9yMiA9IFwiIzkxOEJFNVwiXG5cbmV4cG9ydHMucGhvbmVDb250YWN0QmFja2dyb3VuZENvbG9ycyA9IFtcblx0XCIjNEREMEUxXCIsXG5cdFwiIzk1NzVDRFwiLFxuXHRcIiNGMDYyOTJcIixcblx0XCIjQUVENTgxXCIsXG5cdFwiI0ZGQjc0RFwiLFxuXHRcIiNGRjhBNjVcIixcblx0XCIjRkZENTRGXCIsXG5cdFwiIzc5ODZDQlwiLFxuXHRcIiM0RkMzRjdcIlxuXVxuIiwiY2xhc3MgZXhwb3J0cy5DaXJjbGUgZXh0ZW5kcyBMYXllclxuXHRjdXJyZW50VmFsdWU6IG51bGxcblxuXHRjb25zdHJ1Y3RvcjogKEBvcHRpb25zPXt9KSAtPlxuXG5cdFx0QG9wdGlvbnMuY2lyY2xlU2l6ZSA/PSAzMDBcblx0XHRAb3B0aW9ucy5zdHJva2VXaWR0aCA/PSAyNFxuXG5cdFx0QG9wdGlvbnMuc3Ryb2tlQ29sb3IgPz0gXCIjZmMyNDVjXCJcblx0XHRAb3B0aW9ucy50b3BDb2xvciA/PSBudWxsXG5cdFx0QG9wdGlvbnMuYm90dG9tQ29sb3IgPz0gbnVsbFxuXG5cdFx0QG9wdGlvbnMuaGFzQ291bnRlciA/PSBudWxsXG5cdFx0QG9wdGlvbnMuY291bnRlckNvbG9yID89IFwiI2ZmZlwiXG5cdFx0QG9wdGlvbnMuY291bnRlckZvbnRTaXplID89IDYwXG5cdFx0QG9wdGlvbnMuaGFzTGluZWFyRWFzaW5nID89IG51bGxcblxuXHRcdEBvcHRpb25zLnZhbHVlID0gMlxuXG5cdFx0QG9wdGlvbnMudmlld0JveCA9IChAb3B0aW9ucy5jaXJjbGVTaXplKSArIEBvcHRpb25zLnN0cm9rZVdpZHRoXG5cblx0XHRzdXBlciBAb3B0aW9uc1xuXG5cdFx0QC5iYWNrZ3JvdW5kQ29sb3IgPSBcIlwiXG5cdFx0QC5oZWlnaHQgPSBAb3B0aW9ucy52aWV3Qm94XG5cdFx0QC53aWR0aCA9IEBvcHRpb25zLnZpZXdCb3hcblx0XHRALnJvdGF0aW9uID0gLTkwXG5cblxuXHRcdEAucGF0aExlbmd0aCA9IE1hdGguUEkgKiBAb3B0aW9ucy5jaXJjbGVTaXplXG5cblx0XHRALmNpcmNsZUlEID0gXCJjaXJjbGVcIiArIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSoxMDAwKVxuXHRcdEAuZ3JhZGllbnRJRCA9IFwiY2lyY2xlXCIgKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkqMTAwMClcblxuXHRcdCMgUHV0IHRoaXMgaW5zaWRlIGxpbmVhcmdyYWRpZW50XG5cdFx0IyBncmFkaWVudFVuaXRzPVwidXNlclNwYWNlT25Vc2VcIlxuXHRcdCMgICAgeDE9XCIwJVwiIHkxPVwiMCVcIiB4Mj1cIjUwJVwiIHkyPVwiMCVcIiBncmFkaWVudFRyYW5zZm9ybT1cInJvdGF0ZSgxMjApXCJcblxuXG5cdFx0aWYgQG9wdGlvbnMuaGFzQ291bnRlciBpc250IG51bGxcblx0XHRcdGNvdW50ZXIgPSBuZXcgTGF5ZXJcblx0XHRcdFx0cGFyZW50OiBAXG5cdFx0XHRcdGh0bWw6IFwiXCJcblx0XHRcdFx0d2lkdGg6IEAud2lkdGhcblx0XHRcdFx0aGVpZ2h0OiBALmhlaWdodFxuXHRcdFx0XHRiYWNrZ3JvdW5kQ29sb3I6IFwiXCJcblx0XHRcdFx0cm90YXRpb246IDkwXG5cdFx0XHRcdGNvbG9yOiBAb3B0aW9ucy5jb3VudGVyQ29sb3JcblxuXHRcdFx0c3R5bGUgPSB7XG5cdFx0XHRcdHRleHRBbGlnbjogXCJjZW50ZXJcIlxuXHRcdFx0XHRmb250U2l6ZTogXCIje0BvcHRpb25zLmNvdW50ZXJGb250U2l6ZX1weFwiXG5cdFx0XHRcdGxpbmVIZWlnaHQ6IFwiI3tALmhlaWdodH1weFwiXG5cdFx0XHRcdGZvbnRXZWlnaHQ6IFwiNjAwXCJcblx0XHRcdFx0Zm9udEZhbWlseTogXCItYXBwbGUtc3lzdGVtLCBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmXCJcblx0XHRcdFx0Ym94U2l6aW5nOiBcImJvcmRlci1ib3hcIlxuXHRcdFx0XHRoZWlnaHQ6IEAuaGVpZ2h0XG5cdFx0XHR9XG5cblx0XHRcdGNvdW50ZXIuc3R5bGUgPSBzdHlsZVxuXG5cdFx0XHRudW1iZXJTdGFydCA9IDBcblx0XHRcdG51bWJlckVuZCA9IDEwMFxuXHRcdFx0bnVtYmVyRHVyYXRpb24gPSAyXG5cblx0XHRcdG51bWJlck5vdyA9IG51bWJlclN0YXJ0XG5cdFx0XHRudW1iZXJJbnRlcnZhbCA9IG51bWJlckVuZCAtIG51bWJlclN0YXJ0XG5cblxuXHRcdEAuaHRtbCA9IFwiXCJcIlxuXHRcdFx0PHN2ZyB2aWV3Qm94PSctI3tAb3B0aW9ucy5zdHJva2VXaWR0aC8yfSAtI3tAb3B0aW9ucy5zdHJva2VXaWR0aC8yfSAje0BvcHRpb25zLnZpZXdCb3h9ICN7QG9wdGlvbnMudmlld0JveH0nID5cblx0XHRcdFx0PGRlZnM+XG5cdFx0XHRcdCAgICA8bGluZWFyR3JhZGllbnQgaWQ9JyN7QGdyYWRpZW50SUR9JyA+XG5cdFx0XHRcdCAgICAgICAgPHN0b3Agb2Zmc2V0PVwiMCVcIiBzdG9wLWNvbG9yPScje2lmIEBvcHRpb25zLnRvcENvbG9yIGlzbnQgbnVsbCB0aGVuIEBvcHRpb25zLmJvdHRvbUNvbG9yIGVsc2UgQG9wdGlvbnMuc3Ryb2tlQ29sb3J9Jy8+XG5cdFx0XHRcdCAgICAgICAgPHN0b3Agb2Zmc2V0PVwiMTAwJVwiIHN0b3AtY29sb3I9JyN7aWYgQG9wdGlvbnMudG9wQ29sb3IgaXNudCBudWxsIHRoZW4gQG9wdGlvbnMudG9wQ29sb3IgZWxzZSBAb3B0aW9ucy5zdHJva2VDb2xvcn0nIHN0b3Atb3BhY2l0eT1cIjFcIiAvPlxuXHRcdFx0XHQgICAgPC9saW5lYXJHcmFkaWVudD5cblx0XHRcdFx0PC9kZWZzPlxuXHRcdFx0XHQ8Y2lyY2xlIGlkPScje0BjaXJjbGVJRH0nXG5cdFx0XHRcdFx0XHRmaWxsPSdub25lJ1xuXHRcdFx0XHRcdFx0c3Ryb2tlLWxpbmVjYXA9J3JvdW5kJ1xuXHRcdFx0XHRcdFx0c3Ryb2tlLXdpZHRoICAgICAgPSAnI3tAb3B0aW9ucy5zdHJva2VXaWR0aH0nXG5cdFx0XHRcdFx0XHRzdHJva2UtZGFzaGFycmF5ICA9ICcje0AucGF0aExlbmd0aH0nXG5cdFx0XHRcdFx0XHRzdHJva2UtZGFzaG9mZnNldCA9ICcwJ1xuXHRcdFx0XHRcdFx0c3Ryb2tlPVwidXJsKCMje0BncmFkaWVudElEfSlcIlxuXHRcdFx0XHRcdFx0c3Ryb2tlLXdpZHRoPVwiMTBcIlxuXHRcdFx0XHRcdFx0Y3ggPSAnI3tAb3B0aW9ucy5jaXJjbGVTaXplLzJ9J1xuXHRcdFx0XHRcdFx0Y3kgPSAnI3tAb3B0aW9ucy5jaXJjbGVTaXplLzJ9J1xuXHRcdFx0XHRcdFx0ciAgPSAnI3tAb3B0aW9ucy5jaXJjbGVTaXplLzJ9Jz5cblx0XHRcdDwvc3ZnPlwiXCJcIlxuXG5cdFx0c2VsZiA9IEBcblx0XHRVdGlscy5kb21Db21wbGV0ZSAtPlxuXHRcdFx0c2VsZi5wYXRoID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiMje3NlbGYuY2lyY2xlSUR9XCIpXG5cblx0XHRAcHJveHkgPSBuZXcgTGF5ZXJcblx0XHRcdG9wYWNpdHk6IDBcblx0XHRcdHZpc2libGU6IGZhbHNlXG5cblx0XHRAcHJveHkub24gRXZlbnRzLkFuaW1hdGlvbkVuZCwgKGFuaW1hdGlvbiwgbGF5ZXIpIC0+XG5cdFx0XHRzZWxmLm9uRmluaXNoZWQoKVxuXG5cdFx0QHByb3h5Lm9uICdjaGFuZ2U6eCcsIC0+XG5cblx0XHRcdG9mZnNldCA9IFV0aWxzLm1vZHVsYXRlKEAueCwgWzAsIDUwMF0sIFtzZWxmLnBhdGhMZW5ndGgsIDBdKVxuXG5cdFx0XHRzZWxmLnBhdGguc2V0QXR0cmlidXRlICdzdHJva2UtZGFzaG9mZnNldCcsIG9mZnNldFxuXG5cdFx0XHRpZiBzZWxmLm9wdGlvbnMuaGFzQ291bnRlciBpc250IG51bGxcblx0XHRcdFx0bnVtYmVyTm93ID0gVXRpbHMucm91bmQoc2VsZi5wcm94eS54IC8gNSlcblx0XHRcdFx0Y291bnRlci5odG1sID0gbnVtYmVyTm93XG5cblx0XHRVdGlscy5kb21Db21wbGV0ZSAtPlxuXHRcdFx0c2VsZi5wcm94eS54ID0gMC4xXG5cblx0Y2hhbmdlVG86ICh2YWx1ZSwgdGltZSkgLT5cblx0XHRpZiB0aW1lIGlzIHVuZGVmaW5lZFxuXHRcdFx0dGltZSA9IDJcblxuXHRcdGlmIEBvcHRpb25zLmhhc0NvdW50ZXIgaXMgdHJ1ZSBhbmQgQG9wdGlvbnMuaGFzTGluZWFyRWFzaW5nIGlzIG51bGwgIyBvdmVycmlkZSBkZWZhdWx0IFwiZWFzZS1pbi1vdXRcIiB3aGVuIGNvdW50ZXIgaXMgdXNlZFxuXHRcdFx0Y3VzdG9tQ3VydmUgPSBcImxpbmVhclwiXG5cdFx0ZWxzZVxuXHRcdFx0Y3VzdG9tQ3VydmUgPSBcImVhc2UtaW4tb3V0XCJcblxuXHRcdEBwcm94eS5hbmltYXRlXG5cdFx0XHRwcm9wZXJ0aWVzOlxuXHRcdFx0XHR4OiA1MDAgKiAodmFsdWUgLyAxMDApXG5cdFx0XHR0aW1lOiB0aW1lXG5cdFx0XHRjdXJ2ZTogY3VzdG9tQ3VydmVcblxuXG5cblx0XHRAY3VycmVudFZhbHVlID0gdmFsdWVcblxuXHRzdGFydEF0OiAodmFsdWUpIC0+XG5cdFx0QHByb3h5LmFuaW1hdGVcblx0XHRcdHByb3BlcnRpZXM6XG5cdFx0XHRcdHg6IDUwMCAqICh2YWx1ZSAvIDEwMClcblx0XHRcdHRpbWU6IDAuMDAxXG5cblx0XHRAY3VycmVudFZhbHVlID0gdmFsdWVcblxuXG5cblx0aGlkZTogLT5cblx0XHRALm9wYWNpdHkgPSAwXG5cblx0c2hvdzogLT5cblx0XHRALm9wYWNpdHkgPSAxXG5cblx0b25GaW5pc2hlZDogLT5cbiIsIiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG4jIENyZWF0ZWQgYnkgSm9yZGFuIFJvYmVydCBEb2Jzb24gb24gMDUgT2N0b2JlciAyMDE1XG4jIFxuIyBVc2UgdG8gYWRkIGZvbnQgZmlsZXMgYW5kIHJlZmVyZW5jZSB0aGVtIGluIHlvdXIgQ1NTIHN0eWxlIHNldHRpbmdzLlxuI1xuIyBUbyBHZXQgU3RhcnRlZC4uLlxuI1xuIyAxLiBQbGFjZSB0aGUgRm9udEZhY2UuY29mZmVlIGZpbGUgaW4gRnJhbWVyIFN0dWRpbyBtb2R1bGVzIGRpcmVjdG9yeVxuI1xuIyAyLiBJbiB5b3VyIHByb2plY3QgaW5jbHVkZTpcbiMgICAgIHtGb250RmFjZX0gPSByZXF1aXJlIFwiRm9udEZhY2VcIlxuI1xuIyAzLiBUbyBhZGQgYSBmb250IGZhY2U6IFxuIyAgICAgZ290aGFtID0gbmV3IEZvbnRGYWNlIG5hbWU6IFwiR290aGFtXCIsIGZpbGU6IFwiR290aGFtLnR0ZlwiXG4jIFxuIyA0LiBJdCBjaGVja3MgdGhhdCB0aGUgZm9udCB3YXMgbG9hZGVkLiBFcnJvcnMgY2FuIGJlIHN1cHByZXNzZWQgbGlrZSBzby4uLlxuIyAgICBnb3RoYW0gPSBuZXcgRm9udEZhY2UgbmFtZTogXCJHb3RoYW1cIiwgZmlsZTogXCJHb3RoYW0udHRmXCIsIGhpZGVFcnJvcnM6IHRydWUgXG4jXG4jIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXG5jbGFzcyBleHBvcnRzLkZvbnRGYWNlXG5cblx0VEVTVCA9XG5cdFx0ZmFjZTogXCJtb25vc3BhY2VcIlxuXHRcdHRleHQ6IFwiZm9vXCJcblx0XHR0aW1lOiAuMDFcblx0XHRtYXhMb2FkQXR0ZW1wdHM6IDUwXG5cdFx0aGlkZUVycm9yTWVzc2FnZXM6IHRydWVcblx0XHRcblx0VEVTVC5zdHlsZSA9IFxuXHRcdHdpZHRoOiBcImF1dG9cIlxuXHRcdGZvbnRTaXplOiBcIjE1MHB4XCJcblx0XHRmb250RmFtaWx5OiBURVNULmZhY2Vcblx0XHRcblx0VEVTVC5sYXllciA9IG5ldyBMYXllclxuXHRcdG5hbWU6XCJGb250RmFjZSBUZXN0ZXJcIlxuXHRcdHdpZHRoOiAwXG5cdFx0aGVpZ2h0OiAxXG5cdFx0bWF4WDogLShTY3JlZW4ud2lkdGgpXG5cdFx0dmlzaWJsZTogZmFsc2Vcblx0XHRodG1sOiBURVNULnRleHRcblx0XHRzdHlsZTogVEVTVC5zdHlsZVxuXHRcdFxuXHRcblx0IyBTRVRVUCBGT1IgRVZFUlkgSU5TVEFOQ0Vcblx0Y29uc3RydWN0b3I6IChvcHRpb25zKSAtPlxuXHRcblx0XHRAbmFtZSA9IEBmaWxlID0gQHRlc3RMYXllciA9IEBpc0xvYWRlZCA9IEBsb2FkRmFpbGVkID0gQGxvYWRBdHRlbXB0cyA9IEBvcmlnaW5hbFNpemUgPSBAaGlkZUVycm9ycyA9ICBudWxsXG5cdFx0XG5cdFx0aWYgb3B0aW9ucz9cblx0XHRcdEBuYW1lID0gb3B0aW9ucy5uYW1lIHx8IG51bGxcblx0XHRcdEBmaWxlID0gb3B0aW9ucy5maWxlIHx8IG51bGxcblx0XHRcblx0XHRyZXR1cm4gbWlzc2luZ0FyZ3VtZW50RXJyb3IoKSB1bmxlc3MgQG5hbWU/IGFuZCBAZmlsZT9cblx0XHRcblx0XHRAdGVzdExheWVyICAgICAgICAgPSBURVNULmxheWVyLmNvcHkoKVxuXHRcdEB0ZXN0TGF5ZXIuc3R5bGUgICA9IFRFU1Quc3R5bGVcblx0XHRAdGVzdExheWVyLm1heFggICAgPSAtKFNjcmVlbi53aWR0aClcblx0XHRAdGVzdExheWVyLnZpc2libGUgPSB0cnVlXG5cdFx0XG5cdFx0QGlzTG9hZGVkICAgICA9IGZhbHNlXG5cdFx0QGxvYWRGYWlsZWQgICA9IGZhbHNlXG5cdFx0QGxvYWRBdHRlbXB0cyA9IDBcblx0XHRAaGlkZUVycm9ycyAgID0gb3B0aW9ucy5oaWRlRXJyb3JzXG5cblx0XHRyZXR1cm4gYWRkRm9udEZhY2UgQG5hbWUsIEBmaWxlLCBAXG5cdFx0XG5cdCMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXHQjIFByaXZhdGUgSGVscGVyIE1ldGhvZHMgIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblx0XHRcblx0YWRkRm9udEZhY2UgPSAobmFtZSwgZmlsZSwgb2JqZWN0KSAtPlxuXHRcdCMgQ3JlYXRlIG91ciBFbGVtZW50ICYgTm9kZVxuXHRcdHN0eWxlVGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCAnc3R5bGUnXG5cdFx0ZmFjZUNTUyAgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSBcIkBmb250LWZhY2UgeyBmb250LWZhbWlseTogJyN7bmFtZX0nOyBzcmM6IHVybCgnI3tmaWxlfScpIGZvcm1hdCgndHJ1ZXR5cGUnKTsgfVwiXG5cdFx0IyBBZGQgdGhlIEVsZW1lbnQgJiBOb2RlIHRvIHRoZSBkb2N1bWVudFxuXHRcdHN0eWxlVGFnLmFwcGVuZENoaWxkIGZhY2VDU1Ncblx0XHRkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkIHN0eWxlVGFnXG5cdFx0IyBUZXN0IG91dCB0aGUgRmFzdCB0byBzZWUgaWYgaXQgY2hhbmdlZFxuXHRcdHRlc3ROZXdGYWNlIG5hbWUsIG9iamVjdFxuXHRcdFxuXHQjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblx0XHRcblx0cmVtb3ZlVGVzdExheWVyID0gKG9iamVjdCkgLT5cblx0XHRvYmplY3QudGVzdExheWVyLmRlc3Ryb3koKVxuXHRcdG9iamVjdC50ZXN0TGF5ZXIgPSBudWxsXG5cdFx0XG5cdCMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXHRcdFxuXHR0ZXN0TmV3RmFjZSA9IChuYW1lLCBvYmplY3QpIC0+XG5cdFx0XG5cdFx0aW5pdGlhbFdpZHRoID0gb2JqZWN0LnRlc3RMYXllci5fZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aFxuXHRcdFxuXHRcdCMgQ2hlY2sgdG8gc2VlIGlmIGl0J3MgcmVhZHkgeWV0XG5cdFx0aWYgaW5pdGlhbFdpZHRoIGlzIDBcblx0XHRcdGlmIG9iamVjdC5oaWRlRXJyb3JzIGlzIGZhbHNlIG9yIFRFU1QuaGlkZUVycm9yTWVzc2FnZXMgaXMgZmFsc2Vcblx0XHRcdFx0cHJpbnQgXCJMb2FkIHRlc3RpbmcgZmFpbGVkLiBBdHRlbXB0aW5nIGFnYWluLlwiXG5cdFx0XHRyZXR1cm4gVXRpbHMuZGVsYXkgVEVTVC50aW1lLCAtPiB0ZXN0TmV3RmFjZSBuYW1lLCBvYmplY3Rcblx0XHRcblx0XHRvYmplY3QubG9hZEF0dGVtcHRzKytcblx0XHRcblx0XHRpZiBvYmplY3Qub3JpZ2luYWxTaXplIGlzIG51bGxcblx0XHRcdG9iamVjdC5vcmlnaW5hbFNpemUgPSBpbml0aWFsV2lkdGhcblx0XHRcdG9iamVjdC50ZXN0TGF5ZXIuc3R5bGUgPSBmb250RmFtaWx5OiBcIiN7bmFtZX0sICN7VEVTVC5mYWNlfVwiXG5cdFx0XG5cdFx0d2lkdGhVcGRhdGUgPSBvYmplY3QudGVzdExheWVyLl9lbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoXG5cblx0XHRpZiBvYmplY3Qub3JpZ2luYWxTaXplIGlzIHdpZHRoVXBkYXRlXG5cdFx0XHQjIElmIHdlIGNhbiBhdHRlbXB0IHRvIGNoZWNrIGFnYWluLi4uIERvIGl0XG5cdFx0XHRpZiBvYmplY3QubG9hZEF0dGVtcHRzIDwgVEVTVC5tYXhMb2FkQXR0ZW1wdHNcblx0XHRcdFx0cmV0dXJuIFV0aWxzLmRlbGF5IFRFU1QudGltZSwgLT4gdGVzdE5ld0ZhY2UgbmFtZSwgb2JqZWN0XG5cdFx0XHRcdFxuXHRcdFx0cHJpbnQgXCLimqDvuI8gRmFpbGVkIGxvYWRpbmcgRm9udEZhY2U6ICN7bmFtZX1cIiB1bmxlc3Mgb2JqZWN0LmhpZGVFcnJvcnNcblx0XHRcdG9iamVjdC5pc0xvYWRlZCAgID0gZmFsc2Vcblx0XHRcdG9iamVjdC5sb2FkRmFpbGVkID0gdHJ1ZVxuXHRcdFx0bG9hZFRlc3RpbmdGaWxlRXJyb3Igb2JqZWN0IHVubGVzcyBvYmplY3QuaGlkZUVycm9yc1xuXHRcdFx0cmV0dXJuXG5cdFx0XHRcblx0XHRlbHNlXG5cdFx0XHRwcmludCBcIkxPQURFRDogI3tuYW1lfVwiIHVubGVzcyBvYmplY3QuaGlkZUVycm9ycyBpcyBmYWxzZSBvciBURVNULmhpZGVFcnJvck1lc3NhZ2VzXG5cdFx0XHRvYmplY3QuaXNMb2FkZWQgICA9IHRydWVcblx0XHRcdG9iamVjdC5sb2FkRmFpbGVkID0gZmFsc2VcblxuXHRcdHJlbW92ZVRlc3RMYXllciBvYmplY3Rcblx0XHRyZXR1cm4gbmFtZVxuXG5cdCMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuXHQjIEVycm9yIEhhbmRsZXIgTWV0aG9kcyAjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblxuXHRtaXNzaW5nQXJndW1lbnRFcnJvciA9IC0+XG5cdFx0ZXJyb3IgbnVsbFxuXHRcdGNvbnNvbGUuZXJyb3IgXCJcIlwiXG5cdFx0XHRFcnJvcjogWW91IG11c3QgcGFzcyBuYW1lICYgZmlsZSBwcm9wZXJpdGVzIHdoZW4gY3JlYXRpbmcgYSBuZXcgRm9udEZhY2UuIFxcblxuXHRcdFx0RXhhbXBsZTogbXlGYWNlID0gbmV3IEZvbnRGYWNlIG5hbWU6XFxcIkdvdGhhbVxcXCIsIGZpbGU6XFxcImdvdGhhbS50dGZcXFwiIFxcblwiXCJcIlxuXHRcdFx0XG5cdGxvYWRUZXN0aW5nRmlsZUVycm9yID0gKG9iamVjdCkgLT5cblx0XHRlcnJvciBudWxsXG5cdFx0Y29uc29sZS5lcnJvciBcIlwiXCJcblx0XHRcdEVycm9yOiBDb3VsZG4ndCBkZXRlY3QgdGhlIGZvbnQ6IFxcXCIje29iamVjdC5uYW1lfVxcXCIgYW5kIGZpbGU6IFxcXCIje29iamVjdC5maWxlfVxcXCIgd2FzIGxvYWRlZC4gIFxcblxuXHRcdFx0RWl0aGVyIHRoZSBmaWxlIGNvdWxkbid0IGJlIGZvdW5kIG9yIHlvdXIgYnJvd3NlciBkb2Vzbid0IHN1cHBvcnQgdGhlIGZpbGUgdHlwZSB0aGF0IHdhcyBwcm92aWRlZC4gXFxuXG5cdFx0XHRTdXBwcmVzcyB0aGlzIG1lc3NhZ2UgYnkgYWRkaW5nIFxcXCJoaWRlRXJyb3JzOiB0cnVlXFxcIiB3aGVuIGNyZWF0aW5nIGEgbmV3IEZvbnRGYWNlLiBcXG5cIlwiXCJcbiIsIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBU0FBO0FEb0JNLE9BQU8sQ0FBQztBQUViLE1BQUE7O0VBQUEsSUFBQSxHQUNDO0lBQUEsSUFBQSxFQUFNLFdBQU47SUFDQSxJQUFBLEVBQU0sS0FETjtJQUVBLElBQUEsRUFBTSxHQUZOO0lBR0EsZUFBQSxFQUFpQixFQUhqQjtJQUlBLGlCQUFBLEVBQW1CLElBSm5COzs7RUFNRCxJQUFJLENBQUMsS0FBTCxHQUNDO0lBQUEsS0FBQSxFQUFPLE1BQVA7SUFDQSxRQUFBLEVBQVUsT0FEVjtJQUVBLFVBQUEsRUFBWSxJQUFJLENBQUMsSUFGakI7OztFQUlELElBQUksQ0FBQyxLQUFMLEdBQWlCLElBQUEsS0FBQSxDQUNoQjtJQUFBLElBQUEsRUFBSyxpQkFBTDtJQUNBLEtBQUEsRUFBTyxDQURQO0lBRUEsTUFBQSxFQUFRLENBRlI7SUFHQSxJQUFBLEVBQU0sQ0FBRSxNQUFNLENBQUMsS0FIZjtJQUlBLE9BQUEsRUFBUyxLQUpUO0lBS0EsSUFBQSxFQUFNLElBQUksQ0FBQyxJQUxYO0lBTUEsS0FBQSxFQUFPLElBQUksQ0FBQyxLQU5aO0dBRGdCOztFQVdKLGtCQUFDLE9BQUQ7SUFFWixJQUFDLENBQUEsSUFBRCxHQUFRLElBQUMsQ0FBQSxJQUFELEdBQVEsSUFBQyxDQUFBLFNBQUQsR0FBYSxJQUFDLENBQUEsUUFBRCxHQUFZLElBQUMsQ0FBQSxVQUFELEdBQWMsSUFBQyxDQUFBLFlBQUQsR0FBZ0IsSUFBQyxDQUFBLFlBQUQsR0FBZ0IsSUFBQyxDQUFBLFVBQUQsR0FBZTtJQUV0RyxJQUFHLGVBQUg7TUFDQyxJQUFDLENBQUEsSUFBRCxHQUFRLE9BQU8sQ0FBQyxJQUFSLElBQWdCO01BQ3hCLElBQUMsQ0FBQSxJQUFELEdBQVEsT0FBTyxDQUFDLElBQVIsSUFBZ0IsS0FGekI7O0lBSUEsSUFBQSxDQUFBLENBQXFDLG1CQUFBLElBQVcsbUJBQWhELENBQUE7QUFBQSxhQUFPLG9CQUFBLENBQUEsRUFBUDs7SUFFQSxJQUFDLENBQUEsU0FBRCxHQUFxQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQVgsQ0FBQTtJQUNyQixJQUFDLENBQUEsU0FBUyxDQUFDLEtBQVgsR0FBcUIsSUFBSSxDQUFDO0lBQzFCLElBQUMsQ0FBQSxTQUFTLENBQUMsSUFBWCxHQUFxQixDQUFFLE1BQU0sQ0FBQztJQUM5QixJQUFDLENBQUEsU0FBUyxDQUFDLE9BQVgsR0FBcUI7SUFFckIsSUFBQyxDQUFBLFFBQUQsR0FBZ0I7SUFDaEIsSUFBQyxDQUFBLFVBQUQsR0FBZ0I7SUFDaEIsSUFBQyxDQUFBLFlBQUQsR0FBZ0I7SUFDaEIsSUFBQyxDQUFBLFVBQUQsR0FBZ0IsT0FBTyxDQUFDO0FBRXhCLFdBQU8sV0FBQSxDQUFZLElBQUMsQ0FBQSxJQUFiLEVBQW1CLElBQUMsQ0FBQSxJQUFwQixFQUEwQixJQUExQjtFQXBCSzs7RUF5QmIsV0FBQSxHQUFjLFNBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxNQUFiO0FBRWIsUUFBQTtJQUFBLFFBQUEsR0FBVyxRQUFRLENBQUMsYUFBVCxDQUF1QixPQUF2QjtJQUNYLE9BQUEsR0FBVyxRQUFRLENBQUMsY0FBVCxDQUF3Qiw2QkFBQSxHQUE4QixJQUE5QixHQUFtQyxlQUFuQyxHQUFrRCxJQUFsRCxHQUF1RCwwQkFBL0U7SUFFWCxRQUFRLENBQUMsV0FBVCxDQUFxQixPQUFyQjtJQUNBLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBZCxDQUEwQixRQUExQjtXQUVBLFdBQUEsQ0FBWSxJQUFaLEVBQWtCLE1BQWxCO0VBUmE7O0VBWWQsZUFBQSxHQUFrQixTQUFDLE1BQUQ7SUFDakIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFqQixDQUFBO1dBQ0EsTUFBTSxDQUFDLFNBQVAsR0FBbUI7RUFGRjs7RUFNbEIsV0FBQSxHQUFjLFNBQUMsSUFBRCxFQUFPLE1BQVA7QUFFYixRQUFBO0lBQUEsWUFBQSxHQUFlLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUExQixDQUFBLENBQWlELENBQUM7SUFHakUsSUFBRyxZQUFBLEtBQWdCLENBQW5CO01BQ0MsSUFBRyxNQUFNLENBQUMsVUFBUCxLQUFxQixLQUFyQixJQUE4QixJQUFJLENBQUMsaUJBQUwsS0FBMEIsS0FBM0Q7UUFDQyxLQUFBLENBQU0sd0NBQU4sRUFERDs7QUFFQSxhQUFPLEtBQUssQ0FBQyxLQUFOLENBQVksSUFBSSxDQUFDLElBQWpCLEVBQXVCLFNBQUE7ZUFBRyxXQUFBLENBQVksSUFBWixFQUFrQixNQUFsQjtNQUFILENBQXZCLEVBSFI7O0lBS0EsTUFBTSxDQUFDLFlBQVA7SUFFQSxJQUFHLE1BQU0sQ0FBQyxZQUFQLEtBQXVCLElBQTFCO01BQ0MsTUFBTSxDQUFDLFlBQVAsR0FBc0I7TUFDdEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFqQixHQUF5QjtRQUFBLFVBQUEsRUFBZSxJQUFELEdBQU0sSUFBTixHQUFVLElBQUksQ0FBQyxJQUE3QjtRQUYxQjs7SUFJQSxXQUFBLEdBQWMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQTFCLENBQUEsQ0FBaUQsQ0FBQztJQUVoRSxJQUFHLE1BQU0sQ0FBQyxZQUFQLEtBQXVCLFdBQTFCO01BRUMsSUFBRyxNQUFNLENBQUMsWUFBUCxHQUFzQixJQUFJLENBQUMsZUFBOUI7QUFDQyxlQUFPLEtBQUssQ0FBQyxLQUFOLENBQVksSUFBSSxDQUFDLElBQWpCLEVBQXVCLFNBQUE7aUJBQUcsV0FBQSxDQUFZLElBQVosRUFBa0IsTUFBbEI7UUFBSCxDQUF2QixFQURSOztNQUdBLElBQUEsQ0FBbUQsTUFBTSxDQUFDLFVBQTFEO1FBQUEsS0FBQSxDQUFNLDhCQUFBLEdBQStCLElBQXJDLEVBQUE7O01BQ0EsTUFBTSxDQUFDLFFBQVAsR0FBb0I7TUFDcEIsTUFBTSxDQUFDLFVBQVAsR0FBb0I7TUFDcEIsSUFBQSxDQUFtQyxNQUFNLENBQUMsVUFBMUM7UUFBQSxvQkFBQSxDQUFxQixNQUFyQixFQUFBOztBQUNBLGFBVEQ7S0FBQSxNQUFBO01BWUMsSUFBQSxDQUFBLENBQStCLE1BQU0sQ0FBQyxVQUFQLEtBQXFCLEtBQXJCLElBQThCLElBQUksQ0FBQyxpQkFBbEUsQ0FBQTtRQUFBLEtBQUEsQ0FBTSxVQUFBLEdBQVcsSUFBakIsRUFBQTs7TUFDQSxNQUFNLENBQUMsUUFBUCxHQUFvQjtNQUNwQixNQUFNLENBQUMsVUFBUCxHQUFvQixNQWRyQjs7SUFnQkEsZUFBQSxDQUFnQixNQUFoQjtBQUNBLFdBQU87RUFuQ007O0VBd0NkLG9CQUFBLEdBQXVCLFNBQUE7SUFDdEIsS0FBQSxDQUFNLElBQU47V0FDQSxPQUFPLENBQUMsS0FBUixDQUFjLHNKQUFkO0VBRnNCOztFQU12QixvQkFBQSxHQUF1QixTQUFDLE1BQUQ7SUFDdEIsS0FBQSxDQUFNLElBQU47V0FDQSxPQUFPLENBQUMsS0FBUixDQUFjLHFDQUFBLEdBQ3dCLE1BQU0sQ0FBQyxJQUQvQixHQUNvQyxpQkFEcEMsR0FDcUQsTUFBTSxDQUFDLElBRDVELEdBQ2lFLGtOQUQvRTtFQUZzQjs7Ozs7Ozs7QUR0SXhCLElBQUE7OztBQUFNLE9BQU8sQ0FBQzs7O21CQUNiLFlBQUEsR0FBYzs7RUFFRCxnQkFBQyxPQUFEO0FBRVosUUFBQTtJQUZhLElBQUMsQ0FBQSw0QkFBRCxVQUFTOztVQUVkLENBQUMsYUFBYzs7O1dBQ2YsQ0FBQyxjQUFlOzs7V0FFaEIsQ0FBQyxjQUFlOzs7V0FDaEIsQ0FBQyxXQUFZOzs7V0FDYixDQUFDLGNBQWU7OztXQUVoQixDQUFDLGFBQWM7OztXQUNmLENBQUMsZUFBZ0I7OztXQUNqQixDQUFDLGtCQUFtQjs7O1dBQ3BCLENBQUMsa0JBQW1COztJQUU1QixJQUFDLENBQUEsT0FBTyxDQUFDLEtBQVQsR0FBaUI7SUFFakIsSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUFULEdBQW9CLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVixHQUF3QixJQUFDLENBQUEsT0FBTyxDQUFDO0lBRXBELHdDQUFNLElBQUMsQ0FBQSxPQUFQO0lBRUEsSUFBQyxDQUFDLGVBQUYsR0FBb0I7SUFDcEIsSUFBQyxDQUFDLE1BQUYsR0FBVyxJQUFDLENBQUEsT0FBTyxDQUFDO0lBQ3BCLElBQUMsQ0FBQyxLQUFGLEdBQVUsSUFBQyxDQUFBLE9BQU8sQ0FBQztJQUNuQixJQUFDLENBQUMsUUFBRixHQUFhLENBQUM7SUFHZCxJQUFDLENBQUMsVUFBRixHQUFlLElBQUksQ0FBQyxFQUFMLEdBQVUsSUFBQyxDQUFBLE9BQU8sQ0FBQztJQUVsQyxJQUFDLENBQUMsUUFBRixHQUFhLFFBQUEsR0FBVyxJQUFJLENBQUMsS0FBTCxDQUFXLElBQUksQ0FBQyxNQUFMLENBQUEsQ0FBQSxHQUFjLElBQXpCO0lBQ3hCLElBQUMsQ0FBQyxVQUFGLEdBQWUsUUFBQSxHQUFXLElBQUksQ0FBQyxLQUFMLENBQVcsSUFBSSxDQUFDLE1BQUwsQ0FBQSxDQUFBLEdBQWMsSUFBekI7SUFPMUIsSUFBRyxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsS0FBeUIsSUFBNUI7TUFDQyxPQUFBLEdBQWMsSUFBQSxLQUFBLENBQ2I7UUFBQSxNQUFBLEVBQVEsSUFBUjtRQUNBLElBQUEsRUFBTSxFQUROO1FBRUEsS0FBQSxFQUFPLElBQUMsQ0FBQyxLQUZUO1FBR0EsTUFBQSxFQUFRLElBQUMsQ0FBQyxNQUhWO1FBSUEsZUFBQSxFQUFpQixFQUpqQjtRQUtBLFFBQUEsRUFBVSxFQUxWO1FBTUEsS0FBQSxFQUFPLElBQUMsQ0FBQSxPQUFPLENBQUMsWUFOaEI7T0FEYTtNQVNkLEtBQUEsR0FBUTtRQUNQLFNBQUEsRUFBVyxRQURKO1FBRVAsUUFBQSxFQUFhLElBQUMsQ0FBQSxPQUFPLENBQUMsZUFBVixHQUEwQixJQUYvQjtRQUdQLFVBQUEsRUFBZSxJQUFDLENBQUMsTUFBSCxHQUFVLElBSGpCO1FBSVAsVUFBQSxFQUFZLEtBSkw7UUFLUCxVQUFBLEVBQVksNkNBTEw7UUFNUCxTQUFBLEVBQVcsWUFOSjtRQU9QLE1BQUEsRUFBUSxJQUFDLENBQUMsTUFQSDs7TUFVUixPQUFPLENBQUMsS0FBUixHQUFnQjtNQUVoQixXQUFBLEdBQWM7TUFDZCxTQUFBLEdBQVk7TUFDWixjQUFBLEdBQWlCO01BRWpCLFNBQUEsR0FBWTtNQUNaLGNBQUEsR0FBaUIsU0FBQSxHQUFZLFlBM0I5Qjs7SUE4QkEsSUFBQyxDQUFDLElBQUYsR0FBUyxpQkFBQSxHQUNRLENBQUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULEdBQXFCLENBQXRCLENBRFIsR0FDZ0MsSUFEaEMsR0FDbUMsQ0FBQyxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsR0FBcUIsQ0FBdEIsQ0FEbkMsR0FDMkQsR0FEM0QsR0FDOEQsSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUR2RSxHQUMrRSxHQUQvRSxHQUNrRixJQUFDLENBQUEsT0FBTyxDQUFDLE9BRDNGLEdBQ21HLHlDQURuRyxHQUdtQixJQUFDLENBQUEsVUFIcEIsR0FHK0IsZ0RBSC9CLEdBSWdDLENBQUksSUFBQyxDQUFBLE9BQU8sQ0FBQyxRQUFULEtBQXVCLElBQTFCLEdBQW9DLElBQUMsQ0FBQSxPQUFPLENBQUMsV0FBN0MsR0FBOEQsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUF4RSxDQUpoQyxHQUlvSCxrREFKcEgsR0FLa0MsQ0FBSSxJQUFDLENBQUEsT0FBTyxDQUFDLFFBQVQsS0FBdUIsSUFBMUIsR0FBb0MsSUFBQyxDQUFBLE9BQU8sQ0FBQyxRQUE3QyxHQUEyRCxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQXJFLENBTGxDLEdBS21ILDBFQUxuSCxHQVFPLElBQUMsQ0FBQSxRQVJSLEdBUWlCLHdFQVJqQixHQVdrQixJQUFDLENBQUEsT0FBTyxDQUFDLFdBWDNCLEdBV3VDLDZCQVh2QyxHQVlrQixJQUFDLENBQUMsVUFacEIsR0FZK0Isa0RBWi9CLEdBY1UsSUFBQyxDQUFBLFVBZFgsR0Fjc0Isd0NBZHRCLEdBZ0JFLENBQUMsSUFBQyxDQUFBLE9BQU8sQ0FBQyxVQUFULEdBQW9CLENBQXJCLENBaEJGLEdBZ0J5QixjQWhCekIsR0FpQkUsQ0FBQyxJQUFDLENBQUEsT0FBTyxDQUFDLFVBQVQsR0FBb0IsQ0FBckIsQ0FqQkYsR0FpQnlCLGNBakJ6QixHQWtCRSxDQUFDLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVCxHQUFvQixDQUFyQixDQWxCRixHQWtCeUI7SUFHbEMsSUFBQSxHQUFPO0lBQ1AsS0FBSyxDQUFDLFdBQU4sQ0FBa0IsU0FBQTthQUNqQixJQUFJLENBQUMsSUFBTCxHQUFZLFFBQVEsQ0FBQyxhQUFULENBQXVCLEdBQUEsR0FBSSxJQUFJLENBQUMsUUFBaEM7SUFESyxDQUFsQjtJQUdBLElBQUMsQ0FBQSxLQUFELEdBQWEsSUFBQSxLQUFBLENBQ1o7TUFBQSxPQUFBLEVBQVMsQ0FBVDtNQUNBLE9BQUEsRUFBUyxLQURUO0tBRFk7SUFJYixJQUFDLENBQUEsS0FBSyxDQUFDLEVBQVAsQ0FBVSxNQUFNLENBQUMsWUFBakIsRUFBK0IsU0FBQyxTQUFELEVBQVksS0FBWjthQUM5QixJQUFJLENBQUMsVUFBTCxDQUFBO0lBRDhCLENBQS9CO0lBR0EsSUFBQyxDQUFBLEtBQUssQ0FBQyxFQUFQLENBQVUsVUFBVixFQUFzQixTQUFBO0FBRXJCLFVBQUE7TUFBQSxNQUFBLEdBQVMsS0FBSyxDQUFDLFFBQU4sQ0FBZSxJQUFDLENBQUMsQ0FBakIsRUFBb0IsQ0FBQyxDQUFELEVBQUksR0FBSixDQUFwQixFQUE4QixDQUFDLElBQUksQ0FBQyxVQUFOLEVBQWtCLENBQWxCLENBQTlCO01BRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFWLENBQXVCLG1CQUF2QixFQUE0QyxNQUE1QztNQUVBLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFiLEtBQTZCLElBQWhDO1FBQ0MsU0FBQSxHQUFZLEtBQUssQ0FBQyxLQUFOLENBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFYLEdBQWUsQ0FBM0I7ZUFDWixPQUFPLENBQUMsSUFBUixHQUFlLFVBRmhCOztJQU5xQixDQUF0QjtJQVVBLEtBQUssQ0FBQyxXQUFOLENBQWtCLFNBQUE7YUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFYLEdBQWU7SUFERSxDQUFsQjtFQTVHWTs7bUJBK0diLFFBQUEsR0FBVSxTQUFDLEtBQUQsRUFBUSxJQUFSO0FBQ1QsUUFBQTtJQUFBLElBQUcsSUFBQSxLQUFRLE1BQVg7TUFDQyxJQUFBLEdBQU8sRUFEUjs7SUFHQSxJQUFHLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBVCxLQUF1QixJQUF2QixJQUFnQyxJQUFDLENBQUEsT0FBTyxDQUFDLGVBQVQsS0FBNEIsSUFBL0Q7TUFDQyxXQUFBLEdBQWMsU0FEZjtLQUFBLE1BQUE7TUFHQyxXQUFBLEdBQWMsY0FIZjs7SUFLQSxJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FDQztNQUFBLFVBQUEsRUFDQztRQUFBLENBQUEsRUFBRyxHQUFBLEdBQU0sQ0FBQyxLQUFBLEdBQVEsR0FBVCxDQUFUO09BREQ7TUFFQSxJQUFBLEVBQU0sSUFGTjtNQUdBLEtBQUEsRUFBTyxXQUhQO0tBREQ7V0FRQSxJQUFDLENBQUEsWUFBRCxHQUFnQjtFQWpCUDs7bUJBbUJWLE9BQUEsR0FBUyxTQUFDLEtBQUQ7SUFDUixJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FDQztNQUFBLFVBQUEsRUFDQztRQUFBLENBQUEsRUFBRyxHQUFBLEdBQU0sQ0FBQyxLQUFBLEdBQVEsR0FBVCxDQUFUO09BREQ7TUFFQSxJQUFBLEVBQU0sS0FGTjtLQUREO1dBS0EsSUFBQyxDQUFBLFlBQUQsR0FBZ0I7RUFOUjs7bUJBVVQsSUFBQSxHQUFNLFNBQUE7V0FDTCxJQUFDLENBQUMsT0FBRixHQUFZO0VBRFA7O21CQUdOLElBQUEsR0FBTSxTQUFBO1dBQ0wsSUFBQyxDQUFDLE9BQUYsR0FBWTtFQURQOzttQkFHTixVQUFBLEdBQVksU0FBQSxHQUFBOzs7O0dBckpnQjs7OztBRFc3QixPQUFPLENBQUMsVUFBUixHQUFxQjs7QUFDckIsT0FBTyxDQUFDLFVBQVIsR0FBcUI7O0FBRXJCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUNsQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBQ2xCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUVsQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFDbkIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFFbkIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBQ2xCLE9BQU8sQ0FBQyxPQUFSLEdBQWtCOztBQUNsQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLE9BQVIsR0FBa0I7O0FBRWxCLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsUUFBUixHQUFtQjs7QUFDbkIsT0FBTyxDQUFDLFFBQVIsR0FBbUI7O0FBQ25CLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUVuQixPQUFPLENBQUMsTUFBUixHQUFpQjs7QUFDakIsT0FBTyxDQUFDLE1BQVIsR0FBaUI7O0FBQ2pCLE9BQU8sQ0FBQyxNQUFSLEdBQWlCOztBQUNqQixPQUFPLENBQUMsTUFBUixHQUFpQjs7QUFFakIsT0FBTyxDQUFDLFNBQVIsR0FBb0I7O0FBQ3BCLE9BQU8sQ0FBQyxTQUFSLEdBQW9COztBQUNwQixPQUFPLENBQUMsU0FBUixHQUFvQjs7QUFDcEIsT0FBTyxDQUFDLFNBQVIsR0FBb0I7O0FBRXBCLE9BQU8sQ0FBQyxZQUFSLEdBQXVCOztBQUN2QixPQUFPLENBQUMsWUFBUixHQUF1Qjs7QUFFdkIsT0FBTyxDQUFDLDRCQUFSLEdBQXVDLENBQ3RDLFNBRHNDLEVBRXRDLFNBRnNDLEVBR3RDLFNBSHNDLEVBSXRDLFNBSnNDLEVBS3RDLFNBTHNDLEVBTXRDLFNBTnNDLEVBT3RDLFNBUHNDLEVBUXRDLFNBUnNDLEVBU3RDLFNBVHNDOzs7O0FEMUN2QyxJQUFBOzs7O0FBQU0sT0FBTyxDQUFDO0FBR2IsTUFBQTs7OztFQUFBLFFBQUMsQ0FBQyxNQUFGLENBQVMsUUFBVCxFQUNDO0lBQUEsR0FBQSxFQUFLLFNBQUE7YUFBRyxJQUFDLENBQUE7SUFBSixDQUFMO0dBREQ7O0VBR2Esa0JBQUMsUUFBRDtBQUNaLFFBQUE7SUFEYSxJQUFDLENBQUEsNkJBQUQsV0FBUztJQUN0QixJQUFDLENBQUEsU0FBRCxpREFBcUIsQ0FBQyxnQkFBRCxDQUFDLFlBQWE7SUFDbkMsSUFBQyxDQUFBLE1BQUQsZ0RBQXFCLENBQUMsY0FBRCxDQUFDLFNBQWE7SUFDbkMsSUFBQyxDQUFBLEtBQUQsK0NBQXFCLENBQUMsYUFBRCxDQUFDLFFBQWE7O01BQ25DLElBQUMsQ0FBQSxVQUFrQzs7SUFFbkMsSUFBQyxDQUFBLGNBQUQsR0FBcUIsSUFBQyxDQUFBLE1BQUosR0FBZ0IsUUFBQSxHQUFTLElBQUMsQ0FBQSxNQUExQixHQUF3QztJQUMxRCwyQ0FBQSxTQUFBO0lBRUEsSUFBNkgsSUFBQyxDQUFBLEtBQTlIO01BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSw0Q0FBQSxHQUE2QyxJQUFDLENBQUEsU0FBOUMsR0FBd0QseUJBQXhELEdBQWlGLElBQUMsQ0FBQSxTQUFsRixHQUE0RixrQkFBeEcsRUFBQTs7SUFDQSxJQUFDLENBQUMsUUFBRixDQUFXLFlBQVg7RUFWWTs7RUFZYixPQUFBLEdBQVUsU0FBQyxPQUFELEVBQVUsTUFBVixFQUFrQixJQUFsQixFQUF3QixRQUF4QixFQUFrQyxNQUFsQyxFQUEwQyxJQUExQyxFQUFnRCxVQUFoRCxFQUE0RCxLQUE1RDtBQUVULFFBQUE7SUFBQSxHQUFBLEdBQU0sVUFBQSxHQUFXLE9BQVgsR0FBbUIsaUJBQW5CLEdBQW9DLElBQXBDLEdBQXlDLE9BQXpDLEdBQWdEO0lBRXRELElBQUcsa0JBQUg7TUFDQyxJQUFHLFVBQVUsQ0FBQyxPQUFkO1FBQXNDLEdBQUEsSUFBTyxnQkFBN0M7O01BQ0EsSUFBRyxVQUFVLENBQUMsTUFBWCxLQUFxQixRQUF4QjtRQUFzQyxHQUFBLElBQU8saUJBQTdDOztBQUVBLGNBQU8sVUFBVSxDQUFDLEtBQWxCO0FBQUEsYUFDTSxRQUROO1VBQ29CLEdBQUEsSUFBTztBQUFyQjtBQUROLGFBRU0sUUFGTjtVQUVvQixHQUFBLElBQU87QUFGM0I7TUFJQSxJQUFHLE9BQU8sVUFBVSxDQUFDLFFBQWxCLEtBQThCLFFBQWpDO1FBQ0MsR0FBQSxJQUFPLFlBQUEsR0FBYSxVQUFVLENBQUM7UUFDL0IsTUFBTSxDQUFDLElBQVAsQ0FBWSxHQUFaLEVBQWdCLE9BQWhCLEVBRkQ7O01BSUEsSUFBdUQsT0FBTyxVQUFVLENBQUMsT0FBbEIsS0FBa0MsUUFBekY7UUFBQSxHQUFBLElBQU8sV0FBQSxHQUFjLEdBQWQsR0FBb0IsVUFBVSxDQUFDLE9BQS9CLEdBQXlDLElBQWhEOztNQUNBLElBQXVELE9BQU8sVUFBVSxDQUFDLFlBQWxCLEtBQWtDLFFBQXpGO1FBQUEsR0FBQSxJQUFPLGdCQUFBLEdBQWlCLFVBQVUsQ0FBQyxhQUFuQzs7TUFDQSxJQUF1RCxPQUFPLFVBQVUsQ0FBQyxXQUFsQixLQUFrQyxRQUF6RjtRQUFBLEdBQUEsSUFBTyxlQUFBLEdBQWdCLFVBQVUsQ0FBQyxZQUFsQzs7TUFDQSxJQUF1RCxPQUFPLFVBQVUsQ0FBQyxPQUFsQixLQUFrQyxRQUF6RjtRQUFBLEdBQUEsSUFBTyxXQUFBLEdBQVksVUFBVSxDQUFDLFFBQTlCOztNQUNBLElBQXVELE9BQU8sVUFBVSxDQUFDLEtBQWxCLEtBQWtDLFFBQXpGO1FBQUEsR0FBQSxJQUFPLFNBQUEsR0FBVSxVQUFVLENBQUMsTUFBNUI7O01BQ0EsSUFBdUQsT0FBTyxVQUFVLENBQUMsT0FBbEIsS0FBa0MsUUFBekY7UUFBQSxHQUFBLElBQU8sV0FBQSxHQUFZLFVBQVUsQ0FBQyxRQUE5QjtPQWpCRDs7SUFtQkEsSUFBeUcsS0FBekc7TUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLGlCQUFBLEdBQWtCLE1BQWxCLEdBQXlCLHdCQUF6QixHQUFnRCxDQUFDLElBQUksQ0FBQyxTQUFMLENBQWUsSUFBZixDQUFELENBQWhELEdBQXNFLGFBQXRFLEdBQW1GLEdBQW5GLEdBQXVGLEdBQW5HLEVBQUE7O0lBRUEsT0FBQSxHQUNDO01BQUEsTUFBQSxFQUFRLE1BQVI7TUFDQSxPQUFBLEVBQ0M7UUFBQSxjQUFBLEVBQWdCLGlDQUFoQjtPQUZEOztJQUlELElBQUcsWUFBSDtNQUNDLE9BQU8sQ0FBQyxJQUFSLEdBQWUsSUFBSSxDQUFDLFNBQUwsQ0FBZSxJQUFmLEVBRGhCOztJQUdBLENBQUEsR0FBSSxLQUFBLENBQU0sR0FBTixFQUFXLE9BQVgsQ0FDSixDQUFDLElBREcsQ0FDRSxTQUFDLEdBQUQ7QUFDTCxVQUFBO01BQUEsSUFBRyxDQUFDLEdBQUcsQ0FBQyxFQUFSO0FBQWdCLGNBQU0sS0FBQSxDQUFNLEdBQUcsQ0FBQyxVQUFWLEVBQXRCOztNQUNBLElBQUEsR0FBTyxHQUFHLENBQUMsSUFBSixDQUFBO01BQ1AsSUFBSSxDQUFDLElBQUwsQ0FBVSxRQUFWO0FBQ0EsYUFBTztJQUpGLENBREYsQ0FNSixFQUFDLEtBQUQsRUFOSSxDQU1HLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxLQUFEO2VBQVcsT0FBTyxDQUFDLElBQVIsQ0FBYSxLQUFiO01BQVg7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBTkg7QUFRSixXQUFPO0VBekNFOztFQTRDVixTQUFBLEdBQVksU0FBQTtBQUNYLFFBQUE7SUFEWSxrQkFBRyxpR0FBUztJQUN4QixJQUFHLE9BQU8sSUFBSyxDQUFBLENBQUEsR0FBRSxDQUFGLENBQVosS0FBb0IsUUFBdkI7TUFDQyxJQUFLLENBQUEsQ0FBQSxDQUFMLEdBQVUsSUFBSyxDQUFBLENBQUEsR0FBRSxDQUFGO01BQ2YsSUFBSyxDQUFBLENBQUEsR0FBRSxDQUFGLENBQUwsR0FBWSxLQUZiOztBQUlBLFdBQU8sRUFBRSxDQUFDLEtBQUgsQ0FBUyxJQUFULEVBQWUsSUFBZjtFQUxJOztxQkFTWixHQUFBLEdBQVEsU0FBQTtBQUFhLFFBQUE7SUFBWjtXQUFZLFNBQUEsYUFBVSxDQUFBLENBQUcsU0FBQSxXQUFBLElBQUEsQ0FBQSxFQUFTLENBQUEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLElBQUQsRUFBVSxRQUFWLEVBQW9CLFVBQXBCO2VBQW1DLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFvQixLQUFDLENBQUEsY0FBckIsRUFBcUMsSUFBckMsRUFBMkMsUUFBM0MsRUFBcUQsS0FBckQsRUFBK0QsSUFBL0QsRUFBcUUsVUFBckUsRUFBaUYsS0FBQyxDQUFBLEtBQWxGO01BQW5DO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsQ0FBdEI7RUFBYjs7cUJBQ1IsR0FBQSxHQUFRLFNBQUE7QUFBYSxRQUFBO0lBQVo7V0FBWSxTQUFBLGFBQVUsQ0FBQSxDQUFHLFNBQUEsV0FBQSxJQUFBLENBQUEsRUFBUyxDQUFBLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLFFBQWIsRUFBdUIsVUFBdkI7ZUFBc0MsT0FBQSxDQUFRLEtBQUMsQ0FBQSxTQUFULEVBQW9CLEtBQUMsQ0FBQSxjQUFyQixFQUFxQyxJQUFyQyxFQUEyQyxRQUEzQyxFQUFxRCxLQUFyRCxFQUErRCxJQUEvRCxFQUFxRSxVQUFyRSxFQUFpRixLQUFDLENBQUEsS0FBbEY7TUFBdEM7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQSxDQUF0QjtFQUFiOztxQkFDUixJQUFBLEdBQVEsU0FBQTtBQUFhLFFBQUE7SUFBWjtXQUFZLFNBQUEsYUFBVSxDQUFBLENBQUcsU0FBQSxXQUFBLElBQUEsQ0FBQSxFQUFTLENBQUEsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsUUFBYixFQUF1QixVQUF2QjtlQUFzQyxPQUFBLENBQVEsS0FBQyxDQUFBLFNBQVQsRUFBb0IsS0FBQyxDQUFBLGNBQXJCLEVBQXFDLElBQXJDLEVBQTJDLFFBQTNDLEVBQXFELE1BQXJELEVBQStELElBQS9ELEVBQXFFLFVBQXJFLEVBQWlGLEtBQUMsQ0FBQSxLQUFsRjtNQUF0QztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLENBQXRCO0VBQWI7O3FCQUNSLEtBQUEsR0FBUSxTQUFBO0FBQWEsUUFBQTtJQUFaO1dBQVksU0FBQSxhQUFVLENBQUEsQ0FBRyxTQUFBLFdBQUEsSUFBQSxDQUFBLEVBQVMsQ0FBQSxDQUFBLFNBQUEsS0FBQTthQUFBLFNBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxRQUFiLEVBQXVCLFVBQXZCO2VBQXNDLE9BQUEsQ0FBUSxLQUFDLENBQUEsU0FBVCxFQUFvQixLQUFDLENBQUEsY0FBckIsRUFBcUMsSUFBckMsRUFBMkMsUUFBM0MsRUFBcUQsT0FBckQsRUFBK0QsSUFBL0QsRUFBcUUsVUFBckUsRUFBaUYsS0FBQyxDQUFBLEtBQWxGO01BQXRDO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsQ0FBdEI7RUFBYjs7c0JBQ1IsUUFBQSxHQUFRLFNBQUE7QUFBYSxRQUFBO0lBQVo7V0FBWSxTQUFBLGFBQVUsQ0FBQSxDQUFHLFNBQUEsV0FBQSxJQUFBLENBQUEsRUFBUyxDQUFBLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQyxJQUFELEVBQVksUUFBWixFQUFzQixVQUF0QjtlQUFxQyxPQUFBLENBQVEsS0FBQyxDQUFBLFNBQVQsRUFBb0IsS0FBQyxDQUFBLGNBQXJCLEVBQXFDLElBQXJDLEVBQTJDLFFBQTNDLEVBQXFELFFBQXJELEVBQStELElBQS9ELEVBQXFFLFVBQXJFLEVBQWlGLEtBQUMsQ0FBQSxLQUFsRjtNQUFyQztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLENBQXRCO0VBQWI7O3FCQUdSLFFBQUEsR0FBVSxTQUFDLElBQUQsRUFBTyxRQUFQO0FBR1QsUUFBQTtJQUFBLElBQUcsSUFBQSxLQUFRLFlBQVg7TUFFQyxHQUFBLEdBQU0sVUFBQSxHQUFXLElBQUMsQ0FBQSxTQUFaLEdBQXNCLHVCQUF0QixHQUE2QyxJQUFDLENBQUE7TUFDcEQsYUFBQSxHQUFnQjtNQUNoQixNQUFBLEdBQWEsSUFBQSxXQUFBLENBQVksR0FBWjtNQUViLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixNQUF4QixFQUFnQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7VUFDL0IsSUFBRyxhQUFBLEtBQWlCLGNBQXBCO1lBQ0MsS0FBQyxDQUFDLE9BQUYsR0FBWTtZQUNaLElBQXlCLGdCQUF6QjtjQUFBLFFBQUEsQ0FBUyxXQUFULEVBQUE7O1lBQ0EsSUFBc0YsS0FBQyxDQUFBLEtBQXZGO2NBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSw0Q0FBQSxHQUE2QyxLQUFDLENBQUEsU0FBOUMsR0FBd0QsZUFBcEUsRUFBQTthQUhEOztpQkFJQSxhQUFBLEdBQWdCO1FBTGU7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhDO01BT0EsTUFBTSxDQUFDLGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtVQUNoQyxJQUFHLGFBQUEsS0FBaUIsV0FBcEI7WUFDQyxLQUFDLENBQUMsT0FBRixHQUFZO1lBQ1osSUFBNEIsZ0JBQTVCO2NBQUEsUUFBQSxDQUFTLGNBQVQsRUFBQTs7WUFDQSxJQUFrRixLQUFDLENBQUEsS0FBbkY7Y0FBQSxPQUFPLENBQUMsSUFBUixDQUFhLDRDQUFBLEdBQTZDLEtBQUMsQ0FBQSxTQUE5QyxHQUF3RCxVQUFyRSxFQUFBO2FBSEQ7O2lCQUlBLGFBQUEsR0FBZ0I7UUFMZ0I7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpDO0FBT0EsYUFwQkQ7O0lBc0JBLEdBQUEsR0FBTSxVQUFBLEdBQVcsSUFBQyxDQUFBLFNBQVosR0FBc0IsaUJBQXRCLEdBQXVDLElBQXZDLEdBQTRDLE9BQTVDLEdBQW1ELElBQUMsQ0FBQTtJQUMxRCxNQUFBLEdBQWEsSUFBQSxXQUFBLENBQVksR0FBWjtJQUNiLElBQW1GLElBQUMsQ0FBQSxLQUFwRjtNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksMENBQUEsR0FBMkMsSUFBM0MsR0FBZ0QsYUFBaEQsR0FBNkQsR0FBN0QsR0FBaUUsR0FBN0UsRUFBQTs7SUFFQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsS0FBeEIsRUFBK0IsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLEVBQUQ7UUFDOUIsSUFBc0gsZ0JBQXRIO1VBQUEsUUFBQSxDQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUE3QixFQUFtQyxLQUFuQyxFQUEwQyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBOUQsRUFBb0UsQ0FBQyxDQUFDLElBQUYsQ0FBTyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBSSxDQUFDLEtBQXpCLENBQStCLEdBQS9CLENBQVAsRUFBMkMsQ0FBM0MsQ0FBcEUsRUFBQTs7UUFDQSxJQUFzSCxLQUFDLENBQUEsS0FBdkg7aUJBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxzQ0FBQSxHQUF1QyxJQUF2QyxHQUE0QyxlQUE1QyxHQUEwRCxDQUFDLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUFyQixDQUExRCxHQUFvRixZQUFwRixHQUFnRyxHQUFoRyxHQUFvRyxHQUFoSCxFQUFBOztNQUY4QjtJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBL0I7V0FJQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsQ0FBQSxTQUFBLEtBQUE7YUFBQSxTQUFDLEVBQUQ7UUFDaEMsSUFBd0gsZ0JBQXhIO1VBQUEsUUFBQSxDQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsRUFBRSxDQUFDLElBQWQsQ0FBbUIsQ0FBQyxJQUE3QixFQUFtQyxPQUFuQyxFQUE0QyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBaEUsRUFBc0UsQ0FBQyxDQUFDLElBQUYsQ0FBTyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBSSxDQUFDLEtBQXpCLENBQStCLEdBQS9CLENBQVAsRUFBMkMsQ0FBM0MsQ0FBdEUsRUFBQTs7UUFDQSxJQUF3SCxLQUFDLENBQUEsS0FBekg7aUJBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxzQ0FBQSxHQUF1QyxJQUF2QyxHQUE0QyxpQkFBNUMsR0FBNEQsQ0FBQyxJQUFJLENBQUMsS0FBTCxDQUFXLEVBQUUsQ0FBQyxJQUFkLENBQW1CLENBQUMsSUFBckIsQ0FBNUQsR0FBc0YsWUFBdEYsR0FBa0csR0FBbEcsR0FBc0csR0FBbEgsRUFBQTs7TUFGZ0M7SUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpDO0VBakNTOzs7O0dBOUVvQixNQUFNLENBQUM7Ozs7QURMdEMsSUFBQTs7QUFBQyxXQUFZLE9BQUEsQ0FBUSxVQUFSOztBQUViLHNCQUFBLEdBQTZCLElBQUEsUUFBQSxDQUM1QjtFQUFBLElBQUEsRUFBTSx3QkFBTjtFQUNBLElBQUEsRUFBTSxrREFETjtDQUQ0Qjs7QUFJN0IsZ0JBQUEsR0FBdUIsSUFBQSxRQUFBLENBQ3RCO0VBQUEsSUFBQSxFQUFNLGtCQUFOO0VBQ0EsSUFBQSxFQUFNLDJDQUROO0NBRHNCOztBQUl2QixxQkFBQSxHQUE0QixJQUFBLFFBQUEsQ0FDM0I7RUFBQSxJQUFBLEVBQU0sdUJBQU47RUFDQSxJQUFBLEVBQU0saURBRE47Q0FEMkI7O0FBSTVCLGVBQUEsR0FBc0IsSUFBQSxRQUFBLENBQ3JCO0VBQUEsSUFBQSxFQUFNLGlCQUFOO0VBQ0EsSUFBQSxFQUFNLDBDQUROO0NBRHFCOztBQUl0QiwwQkFBQSxHQUFpQyxJQUFBLFFBQUEsQ0FDaEM7RUFBQSxJQUFBLEVBQU0sNEJBQU47RUFDQSxJQUFBLEVBQU0sc0RBRE47Q0FEZ0M7O0FBSWpDLG9CQUFBLEdBQTJCLElBQUEsUUFBQSxDQUMxQjtFQUFBLElBQUEsRUFBTSxzQkFBTjtFQUNBLElBQUEsRUFBTSwrQ0FETjtDQUQwQjs7QUFJM0Isc0JBQUEsR0FBNkIsSUFBQSxRQUFBLENBQzVCO0VBQUEsSUFBQSxFQUFNLHdCQUFOO0VBQ0EsSUFBQSxFQUFNLGtEQUROO0NBRDRCOztBQUk3QixnQkFBQSxHQUF1QixJQUFBLFFBQUEsQ0FDdEI7RUFBQSxJQUFBLEVBQU0sa0JBQU47RUFDQSxJQUFBLEVBQU0sMkNBRE47Q0FEc0I7O0FBSXZCLHVCQUFBLEdBQThCLElBQUEsUUFBQSxDQUM3QjtFQUFBLElBQUEsRUFBTSx5QkFBTjtFQUNBLElBQUEsRUFBTSxtREFETjtDQUQ2Qjs7QUFJOUIsaUJBQUEsR0FBd0IsSUFBQSxRQUFBLENBQ3ZCO0VBQUEsSUFBQSxFQUFNLG1CQUFOO0VBQ0EsSUFBQSxFQUFNLDRDQUROO0NBRHVCOztBQUl4Qix3QkFBQSxHQUErQixJQUFBLFFBQUEsQ0FDOUI7RUFBQSxJQUFBLEVBQU0sMEJBQU47RUFDQSxJQUFBLEVBQU0sb0RBRE47Q0FEOEI7O0FBSS9CLGtCQUFBLEdBQXlCLElBQUEsUUFBQSxDQUN4QjtFQUFBLElBQUEsRUFBTSxvQkFBTjtFQUNBLElBQUEsRUFBTSw2Q0FETjtDQUR3Qjs7QUFJekIseUJBQUEsR0FBZ0MsSUFBQSxRQUFBLENBQy9CO0VBQUEsSUFBQSxFQUFNLDJCQUFOO0VBQ0EsSUFBQSxFQUFNLHFEQUROO0NBRCtCOztBQUloQyxrQkFBQSxHQUF5QixJQUFBLFFBQUEsQ0FDeEI7RUFBQSxJQUFBLEVBQU0scUJBQU47RUFDQSxJQUFBLEVBQU0sOENBRE47Q0FEd0I7O0FBSXpCLHFCQUFBLEdBQTRCLElBQUEsUUFBQSxDQUMzQjtFQUFBLElBQUEsRUFBTSx1QkFBTjtFQUNBLElBQUEsRUFBTSxpREFETjtDQUQyQjs7QUFJNUIsZUFBQSxHQUFzQixJQUFBLFFBQUEsQ0FDckI7RUFBQSxJQUFBLEVBQU0saUJBQU47RUFDQSxJQUFBLEVBQU0sMENBRE47Q0FEcUI7Ozs7QUQ5RHRCLE9BQU8sQ0FBQyxhQUFSLEdBQXdCLElBQUk7O0FBQzVCLE9BQU8sQ0FBQyxhQUFSLEdBQXdCOztBQUN4QixPQUFPLENBQUMsYUFBUixHQUF3Qjs7QUFDeEIsT0FBTyxDQUFDLG1CQUFSLEdBQThCLElBQUk7O0FBQ2xDLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsT0FBUixHQUFrQjs7QUFDbEIsT0FBTyxDQUFDLGNBQVIsR0FBeUI7O0FBQ3pCLE9BQU8sQ0FBQyxhQUFSLEdBQXdCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDOztBQUM3QyxPQUFPLENBQUMsWUFBUixHQUF1QixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQzs7QUFDNUMsT0FBTyxDQUFDLFdBQVIsR0FBc0I7O0FBQ3RCLE9BQU8sQ0FBQyxRQUFSLEdBQW1COztBQUNuQixPQUFPLENBQUMsY0FBUixHQUF5Qjs7OztBRFh6QixPQUFPLENBQUMseUJBQVIsR0FBb0M7RUFDbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxTQUZQO0lBR0Msd0JBQUEsRUFBMEIsOEJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQURtQyxFQVFuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFFBRlA7SUFHQyx3QkFBQSxFQUEwQiw4QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBUm1DLEVBZW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0FmbUMsRUFzQm5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0F0Qm1DLEVBNkJuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBN0JtQyxFQW9DbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsNkJBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQXBDbUMsRUEyQ25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0EzQ21DLEVBa0RuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFFBRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBbERtQyxFQXlEbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXpEbUMsRUFnRW5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0FoRW1DLEVBdUVuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLE9BRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBdkVtQyxFQThFbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxTQUZQO0lBR0Msd0JBQUEsRUFBMEIsZ0NBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQTlFbUMsRUFxRm5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sVUFGUDtJQUdDLHdCQUFBLEVBQTBCLGdDQUgzQjtJQUlDLE1BQUEsRUFBUSxXQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0FyRm1DLEVBNEZuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFFBRlA7SUFHQyx3QkFBQSxFQUEwQixnQ0FIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBNUZtQyxFQW1HbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxPQUZQO0lBR0Msd0JBQUEsRUFBMEIsK0JBSDNCO0lBSUMsTUFBQSxFQUFRLGFBSlQ7SUFLQyxNQUFBLEVBQVEsS0FMVDtHQW5HbUMsRUEwR25DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLCtCQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLElBTFQ7R0ExR21DLEVBaUhuQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFNBRlA7SUFHQyx3QkFBQSxFQUEwQiw4QkFIM0I7SUFJQyxNQUFBLEVBQVEsYUFKVDtJQUtDLE1BQUEsRUFBUSxLQUxUO0dBakhtQyxFQXdIbkM7SUFDQyxTQUFBLEVBQVcsY0FEWjtJQUVDLElBQUEsRUFBTSxLQUZQO0lBR0Msd0JBQUEsRUFBMEIsK0JBSDNCO0lBSUMsTUFBQSxFQUFRLFdBSlQ7SUFLQyxNQUFBLEVBQVEsSUFMVDtHQXhIbUMsRUErSG5DO0lBQ0MsU0FBQSxFQUFXLGNBRFo7SUFFQyxJQUFBLEVBQU0sT0FGUDtJQUdDLHdCQUFBLEVBQTBCLDZCQUgzQjtJQUlDLE1BQUEsRUFBUSxhQUpUO0lBS0MsTUFBQSxFQUFRLEtBTFQ7R0EvSG1DLEVBc0luQztJQUNDLFNBQUEsRUFBVyxjQURaO0lBRUMsSUFBQSxFQUFNLFNBRlA7SUFHQyx3QkFBQSxFQUEwQiw2QkFIM0I7SUFJQyxNQUFBLEVBQVEsV0FKVDtJQUtDLE1BQUEsRUFBUSxJQUxUO0dBdEltQzs7O0FBK0lwQyxPQUFPLENBQUMsU0FBUixHQUFvQjtFQUNuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFFBSFA7R0FEbUIsRUFNbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxNQUhQO0dBTm1CLEVBV25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQVhtQixFQWdCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxTQUhQO0dBaEJtQixFQXFCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxXQUhQO0dBckJtQixFQTBCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxTQUhQO0dBMUJtQixFQStCbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxVQUhQO0dBL0JtQixFQW9DbkI7SUFDQyxlQUFBLEVBQWlCLGtCQURsQjtJQUVDLGlCQUFBLEVBQW1CLG9CQUZwQjtJQUdDLElBQUEsRUFBTSxnQkFIUDtHQXBDbUIsRUF5Q25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sU0FIUDtHQXpDbUIsRUE4Q25CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sZUFIUDtHQTlDbUIsRUFtRG5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQW5EbUIsRUF3RG5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sWUFIUDtHQXhEbUIsRUE2RG5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sUUFIUDtHQTdEbUIsRUFrRW5CO0lBQ0MsZUFBQSxFQUFpQixrQkFEbEI7SUFFQyxpQkFBQSxFQUFtQixvQkFGcEI7SUFHQyxJQUFBLEVBQU0sZ0JBSFA7R0FsRW1CLEVBdUVuQjtJQUNDLGVBQUEsRUFBaUIsa0JBRGxCO0lBRUMsaUJBQUEsRUFBbUIsb0JBRnBCO0lBR0MsSUFBQSxFQUFNLFNBSFA7R0F2RW1COzs7QUE4RXBCLE9BQU8sQ0FBQyxvQkFBUixHQUErQjtFQUM5QjtJQUNDLElBQUEsRUFBTSxXQURQO0lBRUMsTUFBQSxFQUFRLFVBRlQ7SUFHQyxRQUFBLEVBQVUsSUFIWDtJQUlDLGNBQUEsRUFBZ0IsZ0NBSmpCO0lBS0MsRUFBQSxFQUFJLENBTEw7R0FEOEIsRUFROUI7SUFDQyxJQUFBLEVBQU0sYUFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLDZEQUpqQjtJQUtDLEVBQUEsRUFBSSxDQUxMO0dBUjhCLEVBZTlCO0lBQ0MsSUFBQSxFQUFNLFNBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxJQUhYO0lBSUMsY0FBQSxFQUFnQiw4Q0FKakI7SUFLQyxFQUFBLEVBQUksQ0FMTDtHQWY4QixFQXNCOUI7SUFDQyxJQUFBLEVBQU0sYUFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLHdDQUpqQjtJQUtDLEVBQUEsRUFBSSxDQUxMO0dBdEI4QixFQTZCOUI7SUFDQyxJQUFBLEVBQU0sV0FEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLDBEQUpqQjtJQUtDLEVBQUEsRUFBSSxDQUxMO0dBN0I4QixFQW9DOUI7SUFDQyxJQUFBLEVBQU0sWUFEUDtJQUVDLE1BQUEsRUFBUSxVQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLHdDQUpqQjtJQUtDLEVBQUEsRUFBSSxDQUxMO0dBcEM4QixFQTJDOUI7SUFDQyxJQUFBLEVBQU0sU0FEUDtJQUVDLE1BQUEsRUFBUSxXQUZUO0lBR0MsUUFBQSxFQUFVLElBSFg7SUFJQyxjQUFBLEVBQWdCLGdEQUpqQjtJQUtDLEVBQUEsRUFBSSxDQUxMO0dBM0M4QixFQWtEOUI7SUFDQyxJQUFBLEVBQU0sWUFEUDtJQUVDLE1BQUEsRUFBUSxXQUZUO0lBR0MsUUFBQSxFQUFVLEtBSFg7SUFJQyxjQUFBLEVBQWdCLHlDQUpqQjtJQUtDLEVBQUEsRUFBSSxDQUxMO0dBbEQ4QixFQXlEOUI7SUFDQyxJQUFBLEVBQU0sNEJBRFA7SUFFQyxNQUFBLEVBQVEsVUFGVDtJQUdDLFFBQUEsRUFBVSxLQUhYO0lBSUMsY0FBQSxFQUFnQixnQkFKakI7SUFLQyxFQUFBLEVBQUksQ0FMTDtHQXpEOEI7OztBQWtFL0IsT0FBTyxDQUFDLG9CQUFSLEdBQStCO0VBQzlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxlQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQiwwQ0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBRDhCLEVBVTlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxtQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBVjhCLEVBbUI5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0saUJBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQW5COEIsRUE0QjlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsYUFGWjtJQUdDLElBQUEsRUFBTSxlQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0E1QjhCLEVBcUM5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sbUJBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLHlDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0FyQzhCLEVBOEM5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sc0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQTlDOEIsRUF1RDlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxhQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQiwwQ0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBdkQ4QixFQWdFOUI7SUFDQyxPQUFBLEVBQVMsRUFEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGNBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLHVDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FoRThCLEVBeUU5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0saUJBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQXpFOEIsRUFrRjlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsYUFGWjtJQUdDLElBQUEsRUFBTSxlQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQix5Q0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBbEY4QixFQTJGOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGdCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0EzRjhCLEVBb0c5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sYUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxJQVBYO0dBcEc4QixFQTZHOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLG1CQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0E3RzhCLEVBc0g5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sZUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBdEg4QixFQStIOUI7SUFDQyxPQUFBLEVBQVMsRUFEVjtJQUVDLFNBQUEsRUFBVyxhQUZaO0lBR0MsSUFBQSxFQUFNLGFBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLHdDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0EvSDhCLEVBd0k5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sY0FIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsMENBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQXhJOEIsRUFpSjlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsYUFGWjtJQUdDLElBQUEsRUFBTSxjQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLElBUFg7R0FqSjhCLEVBMEo5QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sZ0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLDJDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0ExSjhCLEVBbUs5QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sZ0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQW5LOEIsRUE0SzlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxrQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IseUNBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQTVLOEIsRUFxTDlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxvQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBckw4QixFQThMOUI7SUFDQyxPQUFBLEVBQVMsRUFEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGdCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQiwyQ0FMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBOUw4QixFQXVNOUI7SUFDQyxPQUFBLEVBQVMsR0FEVjtJQUVDLFNBQUEsRUFBVyxFQUZaO0lBR0MsSUFBQSxFQUFNLGdCQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0F2TThCLEVBZ045QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sa0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLDJDQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FoTjhCLEVBeU45QjtJQUNDLE9BQUEsRUFBUyxFQURWO0lBRUMsU0FBQSxFQUFXLGFBRlo7SUFHQyxJQUFBLEVBQU0sWUFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsMkNBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQXpOOEIsRUFrTzlCO0lBQ0MsT0FBQSxFQUFTLEVBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxXQUhQO0lBSUMsWUFBQSxFQUFjLElBSmY7SUFLQyxvQkFBQSxFQUFzQixFQUx2QjtJQU1DLFlBQUEsRUFBYyxDQU5mO0lBT0MsUUFBQSxFQUFVLEtBUFg7R0FsTzhCLEVBMk85QjtJQUNDLE9BQUEsRUFBUyxHQURWO0lBRUMsU0FBQSxFQUFXLEVBRlo7SUFHQyxJQUFBLEVBQU0sa0JBSFA7SUFJQyxZQUFBLEVBQWMsSUFKZjtJQUtDLG9CQUFBLEVBQXNCLEVBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsS0FQWDtHQTNPOEIsRUFvUDlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsYUFGWjtJQUdDLElBQUEsRUFBTSxvQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsMENBTHZCO0lBTUMsWUFBQSxFQUFjLENBTmY7SUFPQyxRQUFBLEVBQVUsSUFQWDtHQXBQOEIsRUE2UDlCO0lBQ0MsT0FBQSxFQUFTLEdBRFY7SUFFQyxTQUFBLEVBQVcsRUFGWjtJQUdDLElBQUEsRUFBTSxtQkFIUDtJQUlDLFlBQUEsRUFBYyxJQUpmO0lBS0Msb0JBQUEsRUFBc0IsRUFMdkI7SUFNQyxZQUFBLEVBQWMsQ0FOZjtJQU9DLFFBQUEsRUFBVSxLQVBYO0dBN1A4Qjs7O0FBd1EvQixPQUFPLENBQUMsWUFBUixHQUF1QjtFQUN0QjtJQUNDLFNBQUEsRUFBVyxXQURaO0lBRUMsVUFBQSxFQUFZLFdBRmI7R0FEc0IsRUFLdEI7SUFDQyxTQUFBLEVBQVcsV0FEWjtJQUVDLFVBQUEsRUFBWSxXQUZiO0dBTHNCLEVBU3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQVRzQixFQWF0QjtJQUNDLFNBQUEsRUFBVyxXQURaO0lBRUMsVUFBQSxFQUFZLFdBRmI7R0Fic0IsRUFpQnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQWpCc0IsRUFxQnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXJCc0IsRUF5QnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXpCc0IsRUE2QnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQTdCc0IsRUFpQ3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQWpDc0IsRUFxQ3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXJDc0IsRUF5Q3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXpDc0IsRUE2Q3RCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQTdDc0IsRUFpRHRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQWpEc0IsRUFxRHRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXJEc0IsRUF5RHRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQXpEc0IsRUE2RHRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksV0FGYjtHQTdEc0IsRUFpRXRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksVUFGYjtHQWpFc0IsRUFxRXRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksVUFGYjtHQXJFc0IsRUF5RXRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksVUFGYjtHQXpFc0IsRUE2RXRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksVUFGYjtHQTdFc0IsRUFpRnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksVUFGYjtHQWpGc0IsRUFxRnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksWUFGYjtHQXJGc0IsRUF5RnRCO0lBQ0MsU0FBQSxFQUFXLFdBRFo7SUFFQyxVQUFBLEVBQVksWUFGYjtHQXpGc0I7Ozs7O0FEdGlCdkIsSUFBQTs7QUFBQSxNQUFBLEdBQVMsT0FBQSxDQUFRLFFBQVI7O0FBRVQsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxtQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxPQUZWOzs7QUFJSCxPQUFPLENBQUMsT0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksaUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLE9BQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxpQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsV0FBUixHQUNDO0VBQUEsVUFBQSxFQUFZLG9CQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlELE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksaUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLFFBQVIsR0FDQztFQUFBLFVBQUEsRUFBWSxtQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJRCxPQUFPLENBQUMsT0FBUixHQUNHO0VBQUEsVUFBQSxFQUFZLHFCQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxTQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksb0JBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7O0FBSUgsT0FBTyxDQUFDLFNBQVIsR0FDRztFQUFBLFVBQUEsRUFBWSxxQkFBWjtFQUNBLEtBQUEsRUFBTyxNQUFNLENBQUMsVUFEZDtFQUVBLFFBQUEsRUFBVSxNQUZWOzs7QUFJSCxPQUFPLENBQUMsTUFBUixHQUNHO0VBQUEsVUFBQSxFQUFZLGlCQUFaO0VBQ0EsS0FBQSxFQUFPLE1BQU0sQ0FBQyxVQURkO0VBRUEsUUFBQSxFQUFVLE1BRlY7OztBQUlILE9BQU8sQ0FBQyxPQUFSLEdBQ0c7RUFBQSxVQUFBLEVBQVksbUJBQVo7RUFDQSxLQUFBLEVBQU8sTUFBTSxDQUFDLFVBRGQ7RUFFQSxRQUFBLEVBQVUsTUFGVjs7Ozs7QUQzREgsT0FBTyxDQUFDLGFBQVIsR0FBd0IsU0FBQTtBQUN0QixNQUFBO0VBQUEsR0FBQSxHQUFNLElBQUk7RUFXVixJQUFBLEdBQU8sR0FBRyxDQUFDLFdBQUosQ0FBQSxDQUFBLEdBQW9CO0VBQzNCLElBQUcsSUFBQSxJQUFRLEVBQVg7SUFDQyxJQUFBLEdBQU8sSUFBQSxHQUFPLEdBRGY7O0VBR0EsTUFBQSxHQUFTLEdBQUcsQ0FBQyxhQUFKLENBQUE7RUFDVCxNQUFBLEdBQVMsR0FBRyxDQUFDLGFBQUosQ0FBQTtFQUNULE9BQUEsR0FBYSxJQUFBLElBQVEsRUFBWCxHQUFtQixJQUFuQixHQUE2QjtFQUN2QyxJQUFBLEdBQVUsSUFBQSxJQUFRLEVBQVgsR0FBbUIsSUFBQSxHQUFPLEVBQTFCLEdBQWtDO0VBQ3pDLElBQUcsSUFBQSxLQUFRLENBQVg7SUFDQyxJQUFBLEdBQU8sR0FEUjs7RUFFQSxNQUFBLEdBQVksTUFBQSxHQUFTLEVBQVosR0FBb0IsR0FBQSxHQUFNLE1BQTFCLEdBQXNDO0VBQy9DLFVBQUEsR0FBYSxJQUFBLEdBQU8sR0FBUCxHQUFhLE1BQWIsR0FBc0I7QUFDbkMsU0FBTztBQXhCZTs7QUEwQnhCLE9BQU8sQ0FBQyx1QkFBUixHQUFrQyxTQUFDLFVBQUQ7QUFDaEMsTUFBQTtFQUFBLFVBQUEsR0FBYSxVQUFBLEdBQVc7RUFDeEIsTUFBQSxHQUFTLElBQUksQ0FBQyxLQUFMLENBQVcsVUFBQSxHQUFhLEVBQXhCO0VBQ1QsVUFBQSxHQUFhLFVBQUEsR0FBVztFQUN4QixNQUFBLEdBQVMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxVQUFBLEdBQWEsRUFBeEI7RUFDVCxVQUFBLEdBQWEsVUFBQSxHQUFXO0VBQ3hCLElBQUEsR0FBTyxJQUFJLENBQUMsS0FBTCxDQUFXLFVBQUEsR0FBYSxFQUF4QjtFQUVQLElBQUEsR0FBVSxJQUFBLEdBQU8sRUFBVixHQUFrQixHQUFBLEdBQU0sSUFBeEIsR0FBa0M7RUFDekMsTUFBQSxHQUFZLE1BQUEsR0FBUyxFQUFaLEdBQW9CLEdBQUEsR0FBTSxNQUExQixHQUFzQztFQUMvQyxNQUFBLEdBQVksTUFBQSxHQUFTLEVBQVosR0FBb0IsR0FBQSxHQUFNLE1BQTFCLEdBQXNDO0VBRS9DLFVBQUEsR0FBYSxJQUFBLEdBQU8sR0FBUCxHQUFhLE1BQWIsR0FBc0IsR0FBdEIsR0FBNEI7QUFFekMsU0FBTztBQWR5QiJ9
