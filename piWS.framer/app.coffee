showLogs = false

if showLogs
	print("FIRST LINE OF CODE")


updateButton = (message) ->
	ws.send('button-' + message)

updateKnobValue = (message) ->
	ws.send('knobValue-' + message)

updateProximityState = (message) ->
	ws.send('proximityState-' + message)


# Set default value
if showLogs
	print("Setting button to 0")
# firebase.put("/button", "0")
updateButton(0)

button1.onTap ->
	if showLogs
		print("Tapping button1")
	updateButton(1)

button2.onTap ->
	if showLogs
		print("Tapping button2")
	updateButton(2)

button3.onTap ->
	if showLogs
		print("Tapping button3")
	updateButton(3)

button4.onTap ->
	if showLogs
		print("Tapping button4")
	updateButton(4)

button5.onTap ->
	if showLogs
		print("Tapping button5")
	updateButton(5)

button6.onTap ->
	if showLogs
		print("Tapping button6")
	updateButton(6)

button7.onTap ->
	if showLogs
		print("Tapping button7")
	updateButton(7)


# Knob

mouseStart = null
rotationStart = null
panning = false
lastKnobValue = 0
mute = false

knob.borderRadius = knob.width

indicator = new Layer
	parent: knob
	y: 8
	width: 8
	height: 8
	backgroundColor: "black"
	borderRadius: 10
indicator.centerX()

# Rotates the potentiometer
knob.onPanStart (event) ->
	mouseStart = x:event.clientX, y:event.clientY
	rotationStart = knob.rotation
	panning = true

# Computes the rotation angle
rotationNormalizer = Utils.rotationNormalizer()
origin = Utils.convertPointToContext({x:knob.midX, y:knob.midY}, knob, true, false)
knob.onPan (event) ->
	return unless mouseStart?
	mouse = x:event.clientX, y:event.clientY
	angle1 = Math.atan2(mouseStart.y - origin.y, mouseStart.x - origin.x)
	angle2 = Math.atan2(mouse.y - origin.y, mouse.x - origin.x)
	angle = angle2 - angle1
	angle = angle * 180 / Math.PI
	rotation = rotationStart + angle
	knob.rotation = rotationNormalizer(rotation)
# 	print(knob.rotation)

knob.onPanEnd (event) ->
	mouseStart = x:event.clientX, y:event.clientY
	knobValue = parseInt(knob.rotation / 3.6) 
	lastKnobValue = knobValue
	knobValueString = knobValue.toString()
	if !mute
		updateKnobValue(knobValueString)
	panning = false

knob.onClick (event, layer) ->
	if showLogs
		print("Tapping knob")
	if !panning
		mute = !mute
		if mute
			updateKnobValue(0)
		else 
			updateKnobValue(lastKnobValue.toString())

# Set to default, in this case 0
updateKnobValue(0)

# Proximity sensor
proximity.state = false
if showLogs
	print(proximity.state)

# Default proximity
proximityState = proximity.state
proximityStateString = proximityState.toString()
updateProximityState(proximityStateString)

# Updates the firebase
proximity.onClick (event, layer) ->
	@state = !@state
	state = @state
	stateString = state.toString()
	if showLogs
		print("Tapping proximity " + stateString)
	updateProximityState(stateString)


ws.onmessage = (event) ->
	print("Data received: " + event.data)


if showLogs	
	print("LAST LINE OF CODE")
