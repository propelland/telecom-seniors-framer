showLogs = true

if showLogs
	print("FIRST LINE OF CODE")

# Setting up Firebase
# {Firebase} = require 'firebase'
# firebase = new Firebase
# 	projectID: "framer-test-1-ff664"
# 	secret: "FeoF0DXgGEFMxI7p63P5ao77WT7WTdIhpKlQaZvQ"

# Set default value
if showLogs
	print("Setting button to 0")
# firebase.put("/button", "0")
updateButton(0)

button1.onTap ->
	if showLogs
		print("Tapping button1")
	updateButton(1)
# 	firebase.put("/button", "1")
button2.onTap ->
	if showLogs
		print("Tapping button2")
	updateButton(2)
# 	firebase.put("/button", "2")
button3.onTap ->
	if showLogs
		print("Tapping button3")
	updateButton(3)
# 	firebase.put("/button", "3")
button4.onTap ->
	if showLogs
		print("Tapping button4")
	updateButton(4)
# 	firebase.put("/button", "4")
button5.onTap ->
	if showLogs
		print("Tapping button5")
	updateButton(5)
# 	firebase.put("/button", "5")
button6.onTap ->
	if showLogs
		print("Tapping button6")
	updateButton(6)
# 	firebase.put("/button", "6")
button7.onTap ->
	if showLogs
		print("Tapping button7")
	updateButton(7)
# 	firebase.put("/button", "7")

# Knob

mouseStart = null
rotationStart = null
panning = false
lastKnobValue = 0
mute = false

knob.borderRadius = knob.width

indicator = new Layer
	parent: knob
	y: 8
	width: 8
	height: 8
	backgroundColor: "black"
	borderRadius: 10
indicator.centerX()

# rotates the potentiometer
knob.onPanStart (event) ->
	mouseStart = x:event.clientX, y:event.clientY
	rotationStart = knob.rotation
	panning = true

# computes the rotation angle
rotationNormalizer = Utils.rotationNormalizer()
origin = Utils.convertPointToContext({x:knob.midX, y:knob.midY}, knob, true, false)
knob.onPan (event) ->
	return unless mouseStart?
	mouse = x:event.clientX, y:event.clientY
	angle1 = Math.atan2(mouseStart.y - origin.y, mouseStart.x - origin.x)
	angle2 = Math.atan2(mouse.y - origin.y, mouse.x - origin.x)
	angle = angle2 - angle1
	angle = angle * 180 / Math.PI
	rotation = rotationStart + angle
	knob.rotation = rotationNormalizer(rotation)
# 	print(knob.rotation)
# 	firebase.put("/knobValue", knob.rotation.toString())

knob.onPanEnd (event) ->
	mouseStart = x:event.clientX, y:event.clientY
	knobValue = parseInt(knob.rotation / 3.6) 
	lastKnobValue = knobValue
	knobValueString = knobValue.toString()
	if !mute
# 		firebase.put("/knobValue", knobValueString)
		updateKnobValue(knobValueString)
	panning = false

knob.onClick (event, layer) ->
	if showLogs
		print("Tapping knob")
	if !panning
		mute = !mute
		if mute
# 			firebase.put("/knobValue", 0)
			updateKnobValue(0)
		else 
# 			firebase.put("/knobValue", lastKnobValue.toString())
			updateKnobValue(lastKnobValue.toString())

# Set to default, in this case 0
# firebase.put("/knobValue", 0)
updateKnobValue(0)

# Proximity sensor
proximity.state = false
if showLogs
	print(proximity.state)

# Default proximity
proximityState = proximity.state
proximityStateString = proximityState.toString()
# firebase.put("/proximityState", proximityStateString)
updateProximityState(proximityStateString)

# Updates the firebase
proximity.onClick (event, layer) ->
	@state = !@state
	state = @state
	stateString = state.toString()
	if showLogs
		print("Tapping proximity " + stateString)
# 	firebase.put("/proximityState", stateString)
	updateProximityState(stateString)

# Firebase listeners

# Listens for changes on Firebase
# firebase.onChange "/button", (buttonNumberString) ->
# 	if showLogs
# 		print("buttonNumber changed to " + buttonNumberString)
# 	
# firebase.onChange "/knobValue", (knobValue) ->
# 	if showLogs
# 		print("knobValue changed to " + knobValue)
# 	
# firebase.onChange "/proximityState", (proximityState) ->
# 	if showLogs
# 		print("proximityState changed to " + proximityState)
# 	if proximityState == "true"
# 		proximity.backgroundColor = "#CCDD33"
# 	else if proximityState == "false"
# 		proximity.backgroundColor = "#EB7070"

# Listeners
@idleListener = (message) ->
	print("IDLE RECEIVED: " + message)

@radioStationListener = (message) ->
	print("RADIOSTATION RECEIVED: " + message)

if showLogs	
	print("LAST LINE OF CODE")
